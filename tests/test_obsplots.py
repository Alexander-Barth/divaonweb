#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
from divaonweb.webmap import *
from divaonweb.obsplots import *
from divaonweb.vocab import *
from cgi import parse_qs
import sys

obs_index = '../../divaonweb-test-data/obs_index.txt'

        
def test_obs_index():
    oi = ObsIndex(obs_index)
    for s in oi.load():
        print(s)


def test_obs_index_select():
    oi = ObsIndexSelect(obs_index)
    for s in oi.load([13,13.6],[-90,90]):
        print(s)


#op = ObsPlots('/home/abarth/workspace/ObsWMS')
#layers = op.list()
#print(op.list())
#print(op.capabilities(layers[0]))


#test_obs_index()
#test_obs_index_select()


class TestObsPlot(unittest.TestCase):
    '''unittest for Web Map Server'''

    def setUp(self):
        '''set up test case'''

        vocabdir = 'tmp/P011'

        wms_url = ''
        dbfile = '../utils/emodnet-chemistry.sqlite'
        groupsname = '../utils/groups-id.json'
        service = {'title': 'test'}

        # Setup WMS
        self.WMS = obs_webmapserver(
            obs_index,
            vocabdir,
            wms_url,
            service,
            dbfile,
            groupsname
        )


    def test_group(self):
        test = 'toto' not in self.WMS.groups
        self.assertTrue(test)


    def test_getcap(self):
        self.WMS.save_request('request=GetCapabilities&version=1.3.0&SERVICE=WMS','tmp/test_getcap_obs.xml')

    def test_getcap_basedir(self):
        self.WMS.save_request('request=GetCapabilities&version=1.3.0&SERVICE=WMS&basedir=Heavy_Metal','tmp/test_getcap_obs_metal.xml')
        
    def test_get_feature_info(self):
        self.WMS.save_request('LAYERS=__allplots__&STYLES=&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&EXCEPTIONS=application%2Fvnd.ogc.se_xml&SRS=EPSG%3A4326&X=49&Y=44&INFO_FORMAT=text%2Fhtml&QUERY_layers=__allplots__&BBOX=13.5,44,14,46&WIDTH=256&HEIGHT=256',"tmp/test_fi.xml")


    def test_get_feature_info_download(self):
        self.WMS.save_request('http://localhost/web-vis-test/Python/web/plots?LAYERS=__allplots__Fertilisers_Nitrogen&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&STYLES=&SRS=EPSG%3A4326&EXCEPTIONS=application%2Fvnd.ogc.se_xml&BBOX=-73.71582%2C4.663086%2C93.71582%2C80.336914&X=928&Y=293&INFO_FORMAT=application%2Fvnd.ogc.gml&QUERY_layers=__allplots__Fertilisers_Nitrogen&WIDTH=1905&HEIGHT=861&',"tmp/test_fi_download.xml")

    def test_get_feature_info_nothing(self):
        self.WMS.save_request('LAYERS=__allplots__&STYLES=&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&EXCEPTIONS=application%2Fvnd.ogc.se_xml&SRS=EPSG%3A4326&X=249&Y=44&INFO_FORMAT=text%2Fhtml&QUERY_layers=__allplots__&BBOX=13.5,44,14,46&WIDTH=256&HEIGHT=256',"tmp/test_fi_nothing.xml")

    def test_get_feature_info_NH4(self):
        self.WMS.save_request('LAYERS=SDN:P011::AMONAADC&STYLES=&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&EXCEPTIONS=application%2Fvnd.ogc.se_xml&SRS=EPSG%3A4326&X=49&Y=44&INFO_FORMAT=text%2Fhtml&QUERY_layers=SDN:P011::AMONAADC&BBOX=13.5,44,14,46&WIDTH=256&HEIGHT=256',"tmp/test_fi_nh4.xml")
        
    def test_get_map(self):
        self.WMS.save_request('LAYERS=__allplots__&STYLES=markersize:10%2Bmarkerfacecolor:red&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&TIME=366&SRS=EPSG%3A4326&BBOX=13.5,44,14,46&WIDTH=256&HEIGHT=256','tmp/test_obs.png')

    def test_get_map2(self):
        self.WMS.save_request('LAYERS=__allplots__&STYLES=&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&TIME=366&SRS=EPSG%3A4326&BBOX=12,44,14,46&WIDTH=256&HEIGHT=256','tmp/test_obs2.png')

    def test_get_map_metal(self):
        self.WMS.save_request('LAYERS=__allplots__Heavy_metals&STYLES=markersize:10%2Bmarkerfacecolor:red&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&TIME=366&SRS=EPSG%3A4326&BBOX=0,20,50,60&WIDTH=256&HEIGHT=256','tmp/test_obs_metal.png')

    def test_get_legend_graphic(self):
        #self.WMS.save_request('LAYERS=__allplots__&STYLES=&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&TIME=366&SRS=EPSG%3A4326&BBOX=12,44,14,46&WIDTH=256&HEIGHT=256','tmp/test_obs.png')

        self.WMS.save_request('request=GetLegendGraphic&width=80&height=150&transparent=true&decorated=true&style=&color=%23000000&format=image%2Fpng&layer=','tmp/na_legend.png')



    def test_obsindex(self):
        oi = ObsIndexSelect(obs_index);
        print(oi.variables())

        for s in oi.load([13,13.6],[-90,90],['SDN:P011::AMONAADC']):
            print(s)


        print('closest ',oi.closest(13.596167,45.663833))
        print('range ',oi.range('longitude',minr = 1))


    def test_distance(self):
        d = distance(37,-76, 37,-9)
        print(d)
        assert abs(d - 52.30942093) < 1e-7

    def test_vocab(self):
        v = Vocab()
        print(v.label('AMONAADC'))

    def test_shortvocab(self):
        v = ShortVocab()
        print(v.label('AMONAADC'))


        #/emodnet/Python/web/bluemarble?_proxy_url=http%3A%2F%2Flocalhost%2Fweb-vis-test%2FPython%2Fweb%2Fplots%3FLAYERS%3D__allplots__%26STYLES%3Dscatter%26TRANSPARENT%3Dtrue%26FORMAT%3Dimage%252Fpng%26SERVICE%3DWMS%26VERSION%3D1.1.1%26REQUEST%3DGetFeatureInfo%26EXCEPTIONS%3Dapplication%252Fvnd.ogc.se_xml%26SRS%3DEPSG%253A4326%26BBOX%3D27.711182%252C40.963989%252C38.565674%252C46.863647%26X%3D531%26Y%3D207%26INFO_FORMAT%3Dapplication%252Fvnd.ogc.gml%26QUERY_layers%3D__allplots__%26WIDTH%3D988%26HEIGHT%3D537

        
if __name__ == "__main__":
    unittest.main()
        
