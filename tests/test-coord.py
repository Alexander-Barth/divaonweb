#import sys
#sys.path.append("/home/abarth/workspace/divaonweb-coord") 
#sys.path.append("/home/abarth/Python") 
from divaonweb.webmap import *

service = {
    'title': 'test',
    'abstract': 'test',
    'contact_person': 'test',
    'contact_organization': 'test',
    'email': 'test',
    'keywords': []}

directory = '../../divaonweb-test-data/WMS/'

# Setup WMS
WMS = webmapserver(root=directory, 
                   directory='', 
                   filemask='*.nc', 
                   use_pic_cache = False,
                   service = service)

#hr = WMS.request(LAYERS='tDINEOFsst_20100210.nc#SST',STYLES='method:pcolor_flat+ncontours:40+inverted:false+cmap:jet+vmin:10+vmax:20',TRANSPARENT='true',FORMAT='image/png',SERVICE='WMS',VERSION='1.1.1',REQUEST='GetMap',EXCEPTIONS='application%2Fvnd.ogc.se_inimage',TIME='734188',SRS='EPSG:4326',BBOX='-11.25,33.75,0,45',WIDTH='256',HEIGHT='256')

#hr = WMS.request(LAYERS='tDINEOFsst_20100210.nc#SST',STYLES='method:pcolor_flat+ncontours:40+inverted:false+cmap:jet+vmin:10+vmax:20',TRANSPARENT='true',FORMAT='image/png',SERVICE='WMS',VERSION='1.1.1',REQUEST='GetMap',EXCEPTIONS='application%2Fvnd.ogc.se_inimage',TIME='734188',SRS='EPSG:4326',BBOX='-80,33.75,-60,45',WIDTH='256',HEIGHT='256')

hr = WMS.request(LAYERS='tDINEOFsst_20100210.nc#SST',STYLES='method:pcolor_flat+ncontours:40+inverted:false+cmap:jet+vmin:10+vmax:20',TRANSPARENT='true',FORMAT='image/png',SERVICE='WMS',VERSION='1.1.1',REQUEST='GetMap',EXCEPTIONS='application%2Fvnd.ogc.se_inimage',TIME='734188',SRS='EPSG:4326',
                 BBOX='-5,34.82,12.6,44.1',WIDTH='256',HEIGHT='256')



print hr.content_type


filename = "tmp/image-test_coord.png"
f = open(filename,"w")
f.write(hr.content)
f.close()
