from divaonweb.webmap import *
from cgi import parse_qs

service = {
    'title': 'test',
    }

directory = '../../divaonweb-test-data/WMS/Adriatic'
directory = '../../divaonweb-test-data/WMS/emodnet-combined/'

# Setup WMS
WMS = webmapserver(root=directory,
                   directory='',
                   filemask='*nc',
                   use_pic_cache = False,
                   service = service)

# obsid
# http://localhost:8002/Python/web/wms?&layer=point%3ASalinity.19002009.4Danl.nc*obsid&request=GetStats&elevation=-10.0&time=04
#WMS.save_request('request=GetCapabilities&version=1.3.0&SERVICE=WMS','tmp/test_getcap.xml')

WMS.save_request('elevation=0.0&time=winter%202000&layer=Water_body_ammonium.combined.nc*Water_body_ammonium_L2&request=GetStats','tmp/test_getstat_range.xml')

#WMS.save_request('LAYERS=LigurianSea%2FRun-20100622-20100628%2Focean_avg.nc*temp&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A18.516204834%2Bvmax%3A22.2165679932%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&ELEVATION=0&TIME=2010-06-22T12%3A00&SRS=EPSG%3A4326&EXCEPTIONS=application%2Fvnd.ogc.se_xml&BBOX=4.367776%2C41.101843%2C14.832254%2C45.831457&X=797&Y=425&INFO_FORMAT=application%2Fvnd.ogc.gml&QUERY_layers=LigurianSea%2FRun-20100622-20100628%2Focean_avg.nc*temp&WIDTH=1905&HEIGHT=861&','tmp/feature_info.xml')


#WMS.save_request('layer=point%3ASalinity.19002009.4Danl.nc*obsid&request=GetStats&elevation=-10.0&time=04','tmp/test_getstat2.xml')


#WMS.save_request('LAYERS=Salinity.19002009.4Danl.nc*Salinity&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A17.6622%2Bvmax%3A18.4271%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&ELEVATION=-20.0&TIME=01&SRS=EPSG%3A4326&BBOX=28.125,39.375,33.75,45&WIDTH=512&HEIGHT=512','tmp/out.png')



#WMS.save_request('LAYERS=GOS-ChlHR-MED_monthlyCLIMv2_1.nc*chl_climatology&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A0.0%2Bvmax%3A7.46e-15%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&TIME=1&SRS=EPSG%3A4326&BBOX=5.625,33.75,11.25,39.375&WIDTH=512&HEIGHT=512','tmp/med.png')


# works on gher17 but not on gher-diva
# probably netcdf problem
#Traceback (most recent call last):
#  File "/home/abarth/workspace/divaonweb/divaonweb/geodataset.py", line 512, in _load
#    v = self.nv[n,k,j,i]
#  File "netCDF4.pyx", line 1836, in netCDF4.Variable.__getitem__ (netCDF4.c:10285)
#  File "build/bdist.linux-x86_64/egg/netCDF4_utils.py", line 178, in _StartCountStride
#    step = e[1]-e[0]

#WMS.save_request('LAYERS=ocean_his.nc%23temp&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A7.27886199951%2Bvmax%3A9.23727703094%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&EXCEPTIONS=application%2Fvnd.ogc.se_xml&ELEVATION=0&TIME=2010-05-12T12%3A00&SRS=EPSG%3A4326&BBOX=6.95231%2C42.002722%2C12.24772%2C44.930578&X=300&Y=275&INFO_FORMAT=text%2Fhtml&QUERY_layers=ocean_his.nc%23temp&WIDTH=964&HEIGHT=533',"tmp/test_fi.xml")


#WMS.save_request('layer=ORCA2-L053_1d_19600101_19601231_grid_T.nc%23sosaline&request=GetStats&time=1960-01-01T12%3A00','tmp/out.xml')

#WMS.save_request('layer=ORCA2-L053_1d_19600101_19601231_grid_T.nc%23sosaline&request=GetStats&elevation=1&time=1960-01-01T12%3A00','tmp/out.xml')

#WMS.save_request('request=GetCapabilities&version=1.3.0&SERVICE=WMS','tmp/test_getcap.xml')

#WMS.save_request('LAYERS=Atlantic%2FTemperature.19752005.4Danl.nc%23Temperature&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A-2.10202%2Bvmax%3A29.5163%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&EXCEPTIONS=application%2Fvnd.ogc.se_xml&ELEVATION=-0&TIME=180.5&SRS=EPSG%3A4326&BBOX=-137.929687%2C-24.849609%2C77.929688%2C81.849609&X=804&Y=140&INFO_FORMAT=text%2Fxml&QUERY_layers=Atlantic%2FTemperature.19752005.4Danl.nc%23Temperature&WIDTH=1228&HEIGHT=607&',"tmp/test_fi.xml")

#WMS.save_request('LAYERS=Atlantic%2FTemperature.19752005.4Danl.nc%23Temperature&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A-2.10202%2Bvmax%3A29.5163%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&EXCEPTIONS=application%2Fvnd.ogc.se_xml&ELEVATION=-0&TIME=180.5&SRS=EPSG%3A4326&BBOX=-137.929687%2C-24.849609%2C77.929688%2C81.849609&X=804&Y=140&INFO_FORMAT=text%2Fhtml&QUERY_layers=Atlantic%2FTemperature.19752005.4Danl.nc%23Temperature&WIDTH=1228&HEIGHT=607&',"tmp/test_fi.html")


#WMS.save_request('layers=PO4_Lg.nc%23databins&request=GetMap&width=800&height=500&bbox=21.163789%2C37.914424%2C48.146211%2C50.087276&transparent=true&decorated=true&styles=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A0.0%2Bvmax%3A2.36736%2Bncontours%3A40&format=image%2Fpng',"tmp/test_log.png");
