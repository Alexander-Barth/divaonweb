#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
from divaonweb.util import *
import unittest

class TestUtils(unittest.TestCase):
    '''unittest for Web Map Server'''

    def setUp(self):
        pass

    def test_time(self):
        stamp = 1415897965.0

        date = formatHTTPdate(stamp)
        
        stamp2 = parseHTTPdate(date)

        print(stamp,date,stamp2)
        self.assertEqual(stamp,stamp2)
        


if __name__ == "__main__":
    unittest.main()
