#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
import unittest
from divaonweb.webmap import WMSList, WMSError

class TestWMSList(unittest.TestCase):
    '''unittest for WMSList'''

    def setUp(self):
        pass

    def test_list(self):
        r = WMSList([1,2,3])
        self.assertEqual(r.min(),1)
        self.assertEqual(r.max(),3)
        self.assertEqual(str(r),'1,2,3')
        self.assertEqual(r.closestTo(1.2),1)
        self.assertEqual(r.closestTo(2.99),3)

    def test_reglist(self):
        r = WMSList([(1,4,1)])
        self.assertEqual(r.min(),1)
        self.assertEqual(r.max(),4)
        self.assertEqual(str(r),'1/4/1')
        self.assertEqual(r.expand(),[1,2,3,4])
        self.assertEqual(r.first(),1)

    def test_reglist2(self):
        r = WMSList([-10,(1,4,1)])
        self.assertEqual(r.min(),-10)
        self.assertEqual(r.max(),4)
        self.assertEqual(str(r),'-10,1/4/1')
        self.assertEqual(r.expand(),[-10,1,2,3,4])
        self.assertEqual(r.closestTo(0),1)
        self.assertEqual(r.closestTo(7),4)
        self.assertEqual(r.first(),-10)


    def test_contlist(self):
        r = WMSList([(1,4)])
        self.assertEqual(str(r),'1/4')
        self.assertEqual(r.closestTo(7),4)
        self.assertEqual(r.closestTo(0),1)
        self.assertEqual(r.first(),1)

        with self.assertRaises(WMSError):
            r.expand()

    def test_time(self):
        r = WMSList([('2000-12-31T00:00:00.000Z','2100-12-31T00:00:00.000Z')])
        self.assertEqual(str(r),'2000-12-31T00:00:00.000Z/2100-12-31T00:00:00.000Z')


#        self.assertEqual(r.min(),-10)
#        self.assertEqual(r.max(),4)

if __name__ == "__main__":
    unittest.main()
        
