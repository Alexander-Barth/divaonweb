import urllib
from divaonweb.webmap import *
from divaonweb.webmap_proxy import proxy_webmapserver

# Setup WMS


directory = '/var/www/data/SeaDataNet-domains/North Sea/'
directory = '../../divaonweb-test-data/'
directory = '../../divaonweb-test-data/WMS/'
#directory = '/var/www/data/GHER/MediterraneanSea/'
directory = '/var/www/data/emodnet-domains/'
#directory = '/var/www/data/SeaDataNet-domains/'


# Setup WMS
WMS = webmapserver(root=directory, 
                   directory='', 
                   filemask='*', 
                   use_pic_cache = False,
                   service = {'title': 'test' })

#WMS.save_request('request=GetCapabilities&version=1.3.0&SERVICE=WMS','tmp/test_getcap.xml')

#WMS.save_request('layer=point%3ABaltic%20Sea%2FAutumn%20(September-November)%20-%2010-years%20running%20averages%2FWater_body_nitrate.4Danl.nc*obsid&request=GetStats&elevation=-0.0&time=1966','tmp/test_getstat.xml')


#WMS.save_request('LAYERS=North+Sea%2FNTRI.19702009.4Danl.nc*NTRI_L2&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&STYLES=&TRANSPARENT=true&FORMAT=image%2Fpng&SRS=EPSG:4326&BBOX=1.98,50.63,2.98,51.63&X=0&Y=0&INFO_FORMAT=application%2Fvnd.ogc.gml&QUERY_LAYERS=North+Sea%2FNTRI.19702009.4Danl.nc*NTRI_L2&FEATURE_COUNT=1&WIDTH=180&HEIGHT=90','tmp/fi.xml')

#WMS.save_request('layer=North%20Sea%2FNTRA.19702009.4Danl.nc*NTRA_L1&request=GetStats&elevation=-0.0&time=1','tmp/getstat.xml')

#WMS.save_request('LAYERS=North%20Sea%2FNTRI.19702009.4Danl.nc*NTRI_L1&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A-0.0706926509738%2Bvmax%3A3.46811962128%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&EXCEPTIONS=application%2Fvnd.ogc.se_xml&ELEVATION=-0.0&TIME=1&SRS=EPSG%3A4326&BBOX=-18.947888%2C46.702735%2C27.919788%2C68.213965&X=1047&Y=224&INFO_FORMAT=application%2Fvnd.ogc.gml&QUERY_layers=North%20Sea%2FNTRI.19702009.4Danl.nc*NTRI_L1&WIDTH=2133&HEIGHT=979&','tmp/fi2.xml');

#WMS.save_request('request=GetLegendGraphic&width=80&height=150&transparent=true&decorated=true&style=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A2.17201128997%2Bvmax%3A27.8493836285%2Bncontours%3A40&color=%23000000&format=image%2Fpng&layer=Atlantic%2FTemperature.19752005.4Danl.nc*Temperature&ll','na_legend.png')



#WMS.save_request('layers=temperature_annual_1deg.nc*t_an&styles=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A-2.0759%2Bvmax%3A29.557%2Bncontours%3A40&format=image%2Fpng&service=WMS&version=1.3.0&request=GetMap&exceptions=application%2Fvnd.ogc.se_inimage&srs=EPSG%3A4326&bbox=-180%2C-90%2C180%2C90&width=1024&height=512&elevation=-10.0&time=182.0','tmp/levitus_global.png');



#WMS.save_request('layers=Salinity.19002009.4Danl.nc*obsid&styles=scattercmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A-2.0759%2Bvmax%3A29.557%2Bncontours%3A40&format=image%2Fpng&service=WMS&version=1.3.0&request=GetMap&exceptions=application%2Fvnd.ogc.se_inimage&srs=EPSG%3A4326&bbox=-180%2C-90%2C180%2C90&width=1024&height=512&elevation=-10.0&time=182.0','tmp/levitus_global.png');

#WMS.save_request('LAYERS=blacksea_ts_data&TRANSPARENT=true&FORMAT=image%2Fpng&STYLES=scatter&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&SRS=EPSG%3A4326&BBOX=27,40,42,47&WIDTH=256&HEIGHT=256','tmp/test_bs_data.png')

#OK
#WMS.save_request('request=GetCapabilities&version=1.3.0&SERVICE=WMS','tmp/test_getcap.xml')


#WMS.save_request('request=GetLegendGraphic&width=150&height=50&transparent=true&decorated=true&style=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A-1%2Bvmax%3A1%2Bncontours%3A40&color=%23000000&format=image%2Fpng&layer=point%3ASalinity.19002009.4Danl.nc*obsid',
#                 'tmp/legend_obs_id.png');

#OK






#OK
#WMS.save_request('LAYERS=point%3ASalinity.19002009.4Danl.nc*obsid&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A28209.1666667%2Bvmax%3A35452.5416667%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&ELEVATION=-10.0&TIME=648.5&SRS=EPSG%3A4326&BBOX=28.125,39.375,33.75,45&WIDTH=512&HEIGHT=512','tmp/cdi.png');


#WMS.save_request('LAYERS=point%3ASalinity.19002009.4Danl.nc*obsid&STYLES=markerfacecolor%3Ak%2Bmarkeredgecolor%3Ak%2Bmarkersize%3A20%2Bmarker%3AD&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&ELEVATION=-10.0&TIME=1986-01-25T13%3A45&SRS=EPSG%3A4326&BBOX=39.375,39.375,45,45&WIDTH=512&HEIGHT=512','tmp/cdi_custom.png');


#WMS.save_request('LAYERS=point%3ASalinity.19002009.4Danl.nc*obsid&STYLES=markerfacecolor%3Ac%2Bmarkeredgecolor%3Ak%2Bmarkersize%3A7%2Bmarker%3Ao&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&ELEVATION=-10.0&TIME=1986-01-25T13%3A45&SRS=EPSG%3A4326&BBOX=41.048,39.375,45,45&WIDTH=512&HEIGHT=512','tmp/cdi_edge.png');
#WMS.save_request('LAYERS=point%3ASalinity.19002009.4Danl.nc*obsid&STYLES=markerfacecolor%3Ac%2Bmarkeredgecolor%3Ak%2Bmarkersize%3A7%2Bmarker%3Ao&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&ELEVATION=-10.0&TIME=1986-01-25T13%3A45&SRS=EPSG%3A4326&BBOX=41.049,39.375,45,45&WIDTH=512&HEIGHT=512&decorated=false','tmp/cdi_edge.png');

#WMS.save_request('LAYERS=ocean_his.nc#salt&STYLES=&TRANSPARENT=true&FORMAT=image/png&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application/vnd.ogc.se_inimage&ELEVATION=-1&TIME=2010-05-12T12:00&SRS=EPSG:4326&BBOX=8.4375,42.1875,9.84375,43.59375&WIDTH=256&HEIGHT=256','tmp/test_getmap_roms2.png')

#WMS.save_request('LAYERS=ocean_his.nc*salt&STYLES=&TRANSPARENT=true&FORMAT=image/png&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application/vnd.ogc.se_inimage&ELEVATION=-1&TIME=2010-05-12T12:00&SRS=EPSG:4326&BBOX=8.4375,42.1875,9.84375,43.59375&WIDTH=256&HEIGHT=256','tmp/test_getmap_roms.png')


time = '1967,1968,1969,1970,1971,1972,1973,1974,1975,1976,1977,1978,1979,1980,1981,1982,1983,1984,1985,1986,1987,1988,1989,1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010'
#time = '1967,1968,1969,1970,1971,1972,2000,2010'
#time = '1972,2000,2010'

#varname = 'Water_body_phosphate'
#title = 'Water_body_phosphate; depth: 0.0 meters'

# varname = 'Water_body_phosphate'
# ncvarname = varname + '_L2'
# title = 'Water_body_phosphate masked using relative error threshold 0.5\ndepth: 0.0 meters'
# style = 'cmap:jet+method:pcolor_flat+vmin:0+vmax:2'

varname = 'Water_body_nitrate'
ncvarname = varname + '_L2'
title = 'Water body nitrate masked using relative error threshold 0.5\ndepth: 0.0 meters'
style = 'cmap:jet+method:pcolor_flat+vmin:0+vmax:5'

layers = [
#        'Atlantic Sea/Spring (April-June) - 10-years running averages/' + varname + '.4Danl.nc*' + ncvarname,
        'Baltic Sea/Spring (March-May) - 10-years running averages/' + varname + '.4Danl.nc*' + ncvarname,
        'Black Sea/Spring (March-May) - 10-years running averages/' + varname + '.4Danl.nc*' + ncvarname,
        'Mediterranean Sea/Spring (April-June) - 10-years running averages/' + varname + '.4Danl.nc*' + ncvarname,
        'North Sea/Spring (March-May) - 10-years running averages/' + varname + '.4Danl.nc*' + ncvarname,
    ]


WMS.save_request(urllib.urlencode({
    'bbox': '-19.399414,21.494141,42.563477,74.052734',
    'crs': 'CRS:84',
    'decorated': 'true',
    'elevation': '-0.0',
    'format': 'video/mp4',
    'height': '500',
    'layers': ','.join(layers),                       
    'request': 'GetMap',
    'styles': ','.join([style]* len(layers)),
    'time': time,
    'title': title,
    'transparent': 'true',
    'version': '1.3.0',
    'width': '800',
    'basemap': 'shadedrelief',
    'rate': 2,
}),ncvarname + '.mp4')

WMS = vertsection_webmapserver(root=directory,
                               use_pic_cache = False)


#WMS.save_request('layers=ocean_his.nc*temp&section=9.2484525%2C43.719335546875%7C9.413247421875%2C43.345800390625%7C10.12735875%2C43.345800390625%7C10.182290390625%2C43.104101171875&request=GetMap&width=800&height=500&bbox=0%2C-1393.43%2C1950.802%2C0&transparent=true&decorated=true&styles=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A12.9585420464%2Bvmax%3A16.5861611769%2Bncontours%3A40&format=video%2Fwebm&ratio=0.0005928279753660357&time=2010-05-01T12%3A00%2C2010-05-02T12%3A00%2C2010-05-03T12%3A00%2C2010-05-04T12%3A00%2C2010-05-05T12%3A00%2C2010-05-06T12%3A00%2C2010-05-07T12%3A00%2C2010-05-08T12%3A00%2C2010-05-09T12%3A00%2C2010-05-10T12%3A00%2C2010-05-11T12%3A00%2C2010-05-12T12%3A00%2C2010-05-13T12%3A00%2C2010-05-14T12%3A00%2C2010-05-15T12%3A00%2C2010-05-16T12%3A00%2C2010-05-17T12%3A00%2C2010-05-18T12%3A00%2C2010-05-19T12%3A00%2C2010-05-20T12%3A00%2C2010-05-21T12%3A00%2C2010-05-22T12%3A00%2C2010-05-23T12%3A00%2C2010-05-24T12%3A00%2C2010-05-25T12%3A00%2C2010-05-26T12%3A00%2C2010-05-27T12%3A00%2C2010-05-28T12%3A00%2C2010-05-29T12%3A00%2C2010-05-30T12%3A00%2C2010-05-31T12%3A00','tmp/test_error.webm');

#WMS.save_request('section=31.07827618%2C45.9%7C31.38%2C45.82218847%7C31.7%2C45.76465193&layer=Annual_Cs134_BlackSea.nc*Cs134&time=1986-01-01T12%3A00&request=GetStats','tmp/test_getstat.xml');


#WMS.save_request('section=31.07827618%2C45.9%7C31.38%2C45.82218847%7C31.7%2C45.76465193%7C31.98%2C45.670874%7C31.84347208%2C45.42%7C31.94%2C45.124453%7C32.18%2C44.94923706%7C32.5%2C44.87319958%7C32.81777918%2C44.78%7C32.78%2C44.45030206%7C32.98%2C44.22719401%7C33.22%2C44.08718672%7C33.5%2C43.96713952%7C33.85790348%2C43.94%7C34.14%2C43.95357876%7C34.46%2C44.04214109%7C34.71574211%2C44.18%7C34.94%2C44.35175981%7C35.26%2C44.35738475%7C35.54%2C44.47726308%7C35.86%2C44.54584063%7C36.26%2C44.57684692%7C36.58%2C44.6294051%7C36.86%2C44.50071034%7C37.08355991%2C44.34%7C37.38%2C44.24113256%7C37.7%2C44.15308481%7C37.94%2C44.008185%7C38.26%2C43.92499668%7C38.54%2C43.80839771%7C38.78%2C43.65772432%7C39.02%2C43.47832916%7C39.23561628%2C43.3%7C39.46%2C43.12026794%7C39.72219019%2C42.98%7C39.94%2C42.80382002%7C40.24359048%2C42.7%7C40.57393857%2C42.62%7C40.78%2C42.43782127%7C40.98%2C42.25292945%7C41.1%2C41.97018506%7C40.98%2C41.72940097%7C40.7%2C41.61616217%7C40.4162818%2C41.5%7C40.1%2C41.41561349%7C39.78%2C41.48865933%7C39.46%2C41.56028246%7C39.1%2C41.52756261%7C38.74%2C41.47299625%7C38.42%2C41.39406098%7C38.11820593%2C41.5%7C37.81292736%2C41.58%7C37.46%2C41.57900251%7C37.20472758%2C41.7%7C36.9%2C41.79599503%7C36.58838848%2C41.9%7C36.38%2C42.07030621%7C36.07050543%2C42.18%7C35.78%2C42.18211739%7C35.58%2C42.38769209%7C35.28750103%2C42.5%7C34.94%2C42.55133443%7C34.62%2C42.47992179%7C34.3%2C42.40611151%7C33.94%2C42.4315133%7C33.54%2C42.45691062%7C33.18008741%2C42.46%7C32.9%2C42.37169205%7C32.58%2C42.28115477%7C32.26%2C42.21147141%7C31.98%2C42.09455616%7C31.7%2C41.94960731%7C31.42%2C41.8300912%7C31.14%2C41.72467808%7C30.9%2C41.57241714%7C30.55226065%2C41.62%7C30.22%2C41.66164071%7C29.9%2C41.59782198%7C29.54%2C41.64103205%7C29.18%2C41.69465231%7C28.86%2C41.77721268%7C28.65448918%2C41.94%7C28.54%2C42.2441831%7C28.38%2C42.42357978%7C28.51066667%2C42.7%7C28.68630812%2C42.94%7C28.94%2C43.07050351%7C29.14%2C43.29011701%7C29.22686726%2C43.58%7C29.2720752%2C43.86%7C29.34%2C44.21692491%7C29.58%2C44.34067618%7C29.91794004%2C44.42%7C30.14%2C44.58999578%7C30.26%2C44.88725834%7C30.4034995%2C45.14%7C30.5%2C45.44199994%7C30.74%2C45.59404853%7C30.96581337%2C45.78&layer=Annual_Cs134_BlackSea.nc*Cs134&time=1986-01-01T12%3A00&request=GetStats','tmp/test_getstat.xml');


WMS = proxy_webmapserver('')

#WMS.save_request('layer=North%20Sea%2FNTRA.19702009.4Danl.nc*NTRA_deepest_L2&request=GetStats&time=winter&_proxy_url=http%3A%2F%2Foceanbrowser.net%2Femodnet%2FPython%2Fweb%2Fwms%3F','tmp/proxy.out')
