#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function

from divaonweb.webmap import *
import sys
import unittest

service = {
    'title': 'test',
    }

directory = '../../divaonweb-test-data/WMS/'
#directory = '../../divaonweb-test-data/WMS/Baltic/'

class TestHorizontalWMS(unittest.TestCase):
    '''unittest for Web Map Server (horizontal sections)'''

    def setUp(self):
        '''set up test case'''

        # Setup WMS
        self.WMS = webmapserver(root=directory,
                           directory='',
                           filemask='*nc',
                           use_pic_cache = False,
                           service = service)

    def test_getcap(self):
        self.WMS.save_request('request=GetCapabilities&version=1.3.0&SERVICE=WMS','tmp/test_getcap.xml')

    def test_obsid_stat(self):
        # obsid
        #self.WMS.save_request('layer=point%3AWater_body_phosphate.4Danl.nc*obsid&request=GetStats&elevation=-10.0&time=2000','tmp/test_getstat2.xml')
        self.WMS.save_request('layer=point%3AWater_body_ammonium.4Danl.nc*obsid&request=GetStats&elevation=-0.0&time=1985','tmp/test_getstat2.xml')

    def test_obsid_map(self):
        # obsid
        self.WMS.save_request('LAYERS=point%3AWater_body_ammonium.4Danl.nc*obsid&STYLES=markerfacecolor%3Ar%2Bmarkeredgecolor%3Ak%2Bmarkersize%3A7%2Bmarker%3AD&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&ELEVATION=-0.0&TIME=1985&SRS=EPSG%3A4326&BBOX=28.125,42.1875,30.9375,45&WIDTH=512&HEIGHT=512','tmp/test_obsid_map.png')

    def test_obsid_feature_info(self):
        # obsid
        self.WMS.save_request('LAYERS=point%3AWater_body_ammonium.4Danl.nc*obsid&STYLES=markerfacecolor%3Ac%2Bmarkeredgecolor%3Ak%2Bmarkersize%3A7%2Bmarker%3AD&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&ELEVATION=-0.0&TIME=1985&SRS=EPSG%3A4326&EXCEPTIONS=application%2Fvnd.ogc.se_xml&BBOX=14.798445%2C32.053292%2C56.656355%2C51.015695&X=587&Y=392&INFO_FORMAT=application%2Fvnd.ogc.gml&QUERY_layers=point%3AWater_body_ammonium.4Danl.nc*obsid&WIDTH=1905&HEIGHT=863&','tmp/test_fi_cdi2.xml')


    def test_legend_range(self):
        self.WMS.save_request('request=GetLegendGraphic&width=150&height=50&transparent=true&decorated=true&style=&color=%23000000&format=image%2Fpng&layer=emodnet-combined%2FWater_body_ammonium.combined.nc*Water_body_ammonium_L2','tmp/legend_actual_range.png')

    def test_getstat_large_file(self):
        self.WMS.save_request('elevation=0.0&time=winter%202000&layer=emodnet-combined%2FWater_body_ammonium.combined.nc*Water_body_ammonium_L2&request=GetStats','tmp/getstat_large_file.xml')


    def test_small(self):
        self.WMS.save_request('LAYERS=JRA8_TEMP.19752005.4Danl.nc*TEMP&STYLES=method:pcolor_flat%2Bncontours:40%2Binverted:false%2Bcmap:jet%2Bvmin:0%2Bvmax:17.1447&TRANSPARENT=true&FORMAT=image/png&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application/vnd.ogc.se_inimage&ELEVATION=-0&TIME=1&SRS=EPSG:4326&BBOX=0,56.25,11.25,67.5&WIDTH=512&HEIGHT=512','tmp/test_getmap_northsea.png')

        self.WMS.save_request('request=GetLegendGraphic&width=80&height=150&transparent=true&decorated=true&style=&color=%23000000&format=image/png&layer=vector:LigurianSea/Run-20100622-20100628/ocean_avg.nc*u:LigurianSea/Run-20100622-20100628/ocean_avg.nc*v','tmp/test_roms_vel_graphics.png')

        self.WMS.save_request('layer=vector%3ALigurianSea%2FRun-20100622-20100628%2Focean_avg.nc*u%3ALigurianSea%2FRun-20100622-20100628%2Focean_avg.nc*v&request=GetStats&elevation=0&time=2010-06-23T12%3A00','tmp/test_roms_vel.png')

        self.WMS.save_request('LAYERS=blacksea_ts_data&TRANSPARENT=true&FORMAT=image%2Fpng&STYLES=scatter&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&SRS=EPSG%3A4326&BBOX=27,40,42,47&WIDTH=256&HEIGHT=256','tmp/test_bs_data.png')


        self.WMS.save_request('LAYERS=Salinity.19002009.4Danl.nc*Salinity&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A17.1768%2Bvmax%3A18.4063%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&EXCEPTIONS=application%2Fvnd.ogc.se_xml&ELEVATION=-10.0&TIME=01&SRS=EPSG%3A4326&BBOX=24.008057%2C39.025879%2C44.991943%2C48.474121&X=818&Y=406&INFO_FORMAT=text%2Fhtml&QUERY_layers=Salinity.19002009.4Danl.nc*Salinity&WIDTH=1910&HEIGHT=860&','tmp/test_fi_cdi.html');


        self.WMS.save_request('LAYERS=Salinity.19002009.4Danl.nc*Salinity&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A17.1768%2Bvmax%3A18.4063%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&EXCEPTIONS=application%2Fvnd.ogc.se_xml&ELEVATION=-10.0&TIME=01&SRS=EPSG%3A4326&BBOX=24.008057%2C39.025879%2C44.991943%2C48.474121&X=818&Y=406&INFO_FORMAT=application%2Fvnd.ogc.gml&QUERY_layers=Salinity.19002009.4Danl.nc*Salinity&WIDTH=1910&HEIGHT=860&','tmp/test_fi_cdi.gml');

    def test_large(self):
        self.WMS.save_request('request=GetLegendGraphic&width=150&height=50&transparent=true&decorated=true&style=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A3%2Bvmax%3A26.%2Bncontours%3A41&color=%23000000&format=image%2Fpng&layer=Atlantic%2FTemperature.19752005.4Danl.nc*Temperature','tmp/colorbar_hor.png')

        self.WMS.save_request('request=GetLegendGraphic&width=150&height=50&transparent=true&decorated=true&style=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A3.3228696123%2Bvmax%3A26.9898830615%2Bncontours%3A40&color=%23000000&format=image%2Fpng&layer=Atlantic%2FTemperature.19752005.4Danl.nc*Temperature','tmp/colorbar_hor2.png')

        self.WMS.save_request('request=GetLegendGraphic&width=350&height=50&transparent=true&decorated=true&style=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A3.3228696123%2Bvmax%3A26.9898830615%2Bncontours%3A40&color=%23000000&format=image%2Fpng&layer=Atlantic%2FTemperature.19752005.4Danl.nc*Temperature','tmp/colorbar_hor_350.png')

        #self.WMS.save_request('request=GetLegendGraphic&width=80&height=557&transparent=true&decorated=true&style=method%3Apcolor_flat%2Bncontours%3A40%2Binverted%3Afalse%2Bcmap%3Ajet%2Bvmin%3A6.2e-8%2Bvmax%3A0.000007463&color=%23000000&format=image%2Fpng','tmp/test_GetLegendGraphic.png')

        #self.WMS.save_request('LAYERS=example.nc*temp&STYLES=method%3Apcolor_flat%2Bncontours%3A40%2Binverted%3Afalse%2Bcmap%3Ajet%2Bvmin%3A-1%2Bvmax%3A1&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&TIME=366&SRS=EPSG%3A4326&BBOX=-180,-90,0,90&WIDTH=256&HEIGHT=256','tmp/test_getmap_example.png')



        #self.WMS.save_request('layers=NO2_BS.nc*NO2&request=GetMap&width=800&height=500&bbox=23.592002,39.153027,44.487998,50.446973&transparent=true&decorated=true&styles=cmap:jet%2Binverted:false%2Bmethod:pcolor_flat%2Bvmin:-0.0071197738871%2Bvmax:0.375520378351%2Bncontours:40&format=image/png&elevation=0&time=289.5','tmp/map.png')


        #self.WMS.save_request('layers=NO2_BS.nc*NO2&request=GetMap&width=800&height=500&bbox=23.592002,39.153027,44.487998,50.446973&transparent=true&decorated=true&styles=cmap:jet%2Binverted:false%2Bmethod:pcolor_flat%2Bvmin:-0.0071197738871%2Bvmax:0.375520378351%2Bncontours:40&format=image/png&elevation=0&time=1984-02-15T15:45','tmp/map2.png')


        self.WMS.save_request('LAYERS=Atlantic%2FTemperature.19752008.4Danl.nc*Temperature&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A-2.10202%2Bvmax%3A29.5163%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&ELEVATION=-0&TIME=180.5&SRS=EPSG%3A4326&BBOX=-180,-90,0,90&WIDTH=512&HEIGHT=512','tmp/na_map.png')


        self.WMS.save_request('LAYERS=Atlantic%2FTemperature.19752008.4Danl.nc*Temperature&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A-2.10202%2Bvmax%3A29.5163%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&ELEVATION=-0&TIME=180.5&SRS=EPSG%3A54004&BBOX=-5e6,1e6,5e6,10e6&WIDTH=1024&HEIGHT=512','tmp/na_map_merc_pcolor.png')


        self.WMS.save_request('LAYERS=Atlantic%2FTemperature.19752008.4Danl.nc*Temperature&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Acontourf%2Bvmin%3A-2.10202%2Bvmax%3A29.5163%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&ELEVATION=-0&TIME=180.5&SRS=EPSG%3A54004&BBOX=-5e6,1e6,5e6,10e6&WIDTH=1024&HEIGHT=512','tmp/na_map_merc_contourf.png')

        #self.WMS.save_request('LAYERS=Calvi%2FCalvi000.nc*temp&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A13.4135770798%2Bvmax%3A13.4801416397%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&ELEVATION=-10.0&TIME=1998-01-01T00%3A00&SRS=EPSG%3A4326&BBOX=8.437499228159993,42.187499458559984,9.140624225279993,42.89062445567998&WIDTH=512&HEIGHT=512','tmp/calvi.png')


        # ROMS + vertical interplation


        self.WMS.save_request('LAYERS=ocean_his.nc*salt&STYLES=&TRANSPARENT=true&FORMAT=image/png&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application/vnd.ogc.se_inimage&ELEVATION=-1&TIME=2010-05-12T12:00&SRS=EPSG:4326&BBOX=8.4375,42.1875,9.84375,43.59375&WIDTH=256&HEIGHT=256','tmp/test_getmap_roms.png')

        self.WMS.save_request('LAYERS=ocean_his.nc#salt&STYLES=&TRANSPARENT=true&FORMAT=image/png&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application/vnd.ogc.se_inimage&ELEVATION=-1&TIME=2010-05-12T12:00&SRS=EPSG:4326&BBOX=8.4375,42.1875,9.84375,43.59375&WIDTH=256&HEIGHT=256','tmp/test_getmap_roms_old_sep.png')

        self.WMS.save_request('LAYERS=ocean_his.nc*salt&STYLES=&TRANSPARENT=true&FORMAT=image/png&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application/vnd.ogc.se_inimage&ELEVATION=-1&TIME=2010-05-12T12:00&SRS=EPSG:4326&BBOX=8.4375,42.1875,9.84375,43.59375&WIDTH=256&HEIGHT=256','tmp/test_getmap_roms_f.png')

    def test_vel(self):
        self.WMS.save_request('LAYERS=vector:ocean_his.nc*u:ocean_his.nc*v&STYLES=method%3Apcolor_flat%2Bncontours%3A40%2Binverted%3Afalse%2Bcmap%3Ajet%2Bvmin%3A-0.151044%2Bvmax%3A0.116536&TRANSPARENT=true&FORMAT=image/png&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application/vnd.ogc.se_inimage&ELEVATION=-1&TIME=2010-05-12T12:00&SRS=EPSG:4326&BBOX=8.4375,42.1875,9.84375,43.59375&WIDTH=256&HEIGHT=256',"tmp/test_getmap_roms_vel.png")


        self.WMS.save_request('LAYER=vector:ocean_his.nc*u:ocean_his.nc*v&STYLE=color%3Ar&TRANSPARENT=true&FORMAT=image/png&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&EXCEPTIONS=application/vnd.ogc.se_inimage&ELEVATION=-1&TIME=2010-05-12T12:00&WIDTH=80&HEIGHT=150',"tmp/test_getmap_roms_vel_legend.png")

        self.WMS.save_request('layer=vector%3Aocean_his.nc*u%3Aocean_his.nc*v&request=GetStats&elevation=0&time=2010-05-12T12%3A00',"tmp/test_getstat_vel.xml")



        self.WMS.save_request('request=GetLegendGraphic&width=80&height=150&transparent=true&decorated=true&style=&color=%23000000&format=image%2Fpng&layer=vector%3ABlackSea%2FBOTTOM_96_00.nc*u%3ABlackSea%2FBOTTOM_96_00.nc*v','tmp/bs.png')


        self.WMS.save_request('LAYERS=vector%3ABlackSea%2FBOTTOM_96_00.nc*u%3ABlackSea%2FBOTTOM_96_00.nc*v&STYLES=scale%3A45.781057782%2Bspace%3A10%2Bcolor%3Am&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&ELEVATION=0.0&TIME=1992-01-06T00%3A00&SRS=EPSG%3A4326&BBOX=22.5,33.75,33.75,45&WIDTH=512&HEIGHT=512','tmp/test_bs_vel.png')

        #self.WMS.save_request('layer=North%20Sea%2FNTRA.19702009.4Danl.nc*NTRA_L1&request=GetStats&elevation=-0.0&time=1','tmp/test_getstat.xml')





        #self.WMS.save_request('LAYERS=Mediterranean%20Sea%2FAdriatic%2FNitrate.4Danl.nc*Nitrate_err&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A0.0%2Bvmax%3A2.56100201607%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&ELEVATION=-0.0&TIME=1977.0&SRS=EPSG%3A4326&BBOX=11.25,39.375,16.875,45&WIDTH=512&HEIGHT=512&ll','tmp/test_med_adriatic.png')

        #self.WMS.save_request('LAYERS=Black%20Sea/diva_results_O2.nc*O2&STYLES=cmap:jet%2Binverted:false%2Bmethod:pcolor_flat%2Bvmin:0%2Bvmax:8.76971912384%2Bncontours:40&TRANSPARENT=true&FORMAT=image/png&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application/vnd.ogc.se_inimage&SRS=EPSG:4326&BBOX=33.75,33.75,45,45&WIDTH=512&HEIGHT=512','tmp/test_bs_O2.png')


        self.WMS.save_request('LAYERS=Calvi%2FCalvi000.nc*temp&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&ELEVATION=-10.0&TIME=1998-01-01T00%3A00&SRS=EPSG%3A4326&BBOX=8.437499228159993,42.187499458559984,9.140624225279993,42.89062445567998&WIDTH=512&HEIGHT=512&colorscalerange=15,35','tmp/calvi_cs.png')

        self.WMS.save_request('LAYERS=Atlantic%2FTemperature.19752008.4Danl.nc*Temperature&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A-2.10202%2Bvmax%3A29.5163%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&ELEVATION=-0&TIME=180.5&SRS=EPSG%3A54004&BBOX=-5e6,1e6,5e6,10e6&WIDTH=1024&HEIGHT=512','na_map_merc_pcolor.png')


        # animation

        # ok
        self.WMS.save_request('layers=ocean_his.nc*temp&request=GetMap&width=500&height=300&bbox=7.913614%2C42.447668%2C11.286416%2C44.485632&transparent=true&decorated=true&styles=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A15.8894529343%2Bvmax%3A17.4072608948%2Bncontours%3A40&format=video%2Fwebm&elevation=0&time=2010-05-01T12%3A00%2C2010-05-02T12%3A00%2C2010-05-03T12%3A00%2C2010-05-04T12%3A00','tmp/anim.webm');

        # ok
        self.WMS.save_request('layers=ocean_his.nc*temp&request=GetMap&width=500&height=300&bbox=7.913614%2C42.447668%2C11.286416%2C44.485632&transparent=true&decorated=true&styles=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A15.8894529343%2Bvmax%3A17.4072608948%2Bncontours%3A40&format=video%2Fmp4&elevation=0&time=2010-05-01T12%3A00%2C2010-05-02T12%3A00%2C2010-05-03T12%3A00%2C2010-05-04T12%3A00','tmp/anim.mp4');


    def test_getmap_long_title(self):
        self.WMS.save_request('layers=Salinity.19002009.4Danl.nc*Salinity_L2&request=GetMap&width=800&height=500&bbox=16.849976%2C34.309692%2C58.707886%2C53.228149&transparent=true&decorated=true&crs=CRS%3A84&version=1.3.0&styles=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A17.1768%2Bvmax%3A18.4063%2Bncontours%3A40&format=image%2Fpng&elevation=-10.0&time=01&title=Salinity%20masked%20using%20relative%20error%20threshold%200.5%2C%20Units%3A%20psu%2C%20depth%3A%20-10.0%20meters%2C%20time%3A%2001%20month','tmp/long_title.png');

    def test_getmap_basemap_shadedrelief(self):
        self.WMS.save_request('layers=Salinity.19002009.4Danl.nc*Salinity_L2&request=GetMap&width=800&height=500&bbox=16.849976%2C34.309692%2C58.707886%2C53.228149&transparent=true&decorated=true&crs=CRS%3A84&version=1.3.0&styles=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A17.1768%2Bvmax%3A18.4063%2Bncontours%3A40&format=image%2Fpng&elevation=-10.0&time=01&title=Salinity%20masked%20using%20relative%20error%20threshold%200.5%2C%20Units%3A%20psu%2C%20depth%3A%20-10.0%20meters%2C%20time%3A%2001%20month&basemap=shadedrelief','tmp/basemap_shadedrelief.png');

    def test_getmap_basemap_bluemarble(self):
        self.WMS.save_request('layers=Salinity.19002009.4Danl.nc*Salinity_L2&request=GetMap&width=800&height=500&bbox=16.849976%2C34.309692%2C58.707886%2C53.228149&transparent=true&decorated=true&crs=CRS%3A84&version=1.3.0&styles=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A17.1768%2Bvmax%3A18.4063%2Bncontours%3A40&format=image%2Fpng&elevation=-10.0&time=01&title=Salinity%20masked%20using%20relative%20error%20threshold%200.5%2C%20Units%3A%20psu%2C%20depth%3A%20-10.0%20meters%2C%20time%3A%2001%20month&basemap=bluemarble','tmp/basemap_bluemarble.png');



class TestVerticalWMS(unittest.TestCase):
    '''unittest for Web Map Server (vertical sections)'''

    def setUp(self):
        # Setup WMS
        self.WMS = vertsection_webmapserver(root=directory,
                                            use_pic_cache = False)

    def test_getstat1(self):
        self.WMS.save_request('LAYER=Salinity.19002009.4Danl.nc*Salinity&TIME=01&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetStats&SECTION=29.75390625%2C42.87109375%7C32.302734375%2C43.486328125%7C35.642578125%2C42.958984375%7C39.509765625%2C42.51953125','tmp/bs_vsec.xml')


    def test_getmap1(self):
        self.WMS.save_request('LAYERS=Salinity.19002009.4Danl.nc*Salinity&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A18.1159638648%2Bvmax%3A18.5011550988%2Bncontours%3A40&FORMAT=image%2Fpng&TRANSPARENT=true&RATIO=0.4550797399380805&SECTION=29.75390625%2C42.87109375%7C32.302734375%2C43.486328125%7C35.642578125%2C42.958984375%7C39.509765625%2C42.51953125&TIME=01&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&SRS=EPSG%3A4326&BBOX=0,-32.262060063753,6.5276607334226,-25.734399330331&WIDTH=256&HEIGHT=256','tmp/bs_vsec.png')

    def test_feature_info(self):
        self.WMS.save_request('LAYERS=Salinity.19002009.4Danl.nc*Salinity&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A18.1159638648%2Bvmax%3A18.5011550988%2Bncontours%3A40&FORMAT=image%2Fpng&TRANSPARENT=true&RATIO=0.4550797399380805&SECTION=29.75390625%2C42.87109375%7C32.302734375%2C43.486328125%7C35.642578125%2C42.958984375%7C39.509765625%2C42.51953125&TIME=01&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&SRS=EPSG%3A4326&EXCEPTIONS=application%2Fvnd.ogc.se_xml&BBOX=-11.304755%2C-29.54128%2C25.617326%2C-10.468272&X=662&Y=546&INFO_FORMAT=test%2html&QUERY_layers=Salinity.19002009.4Danl.nc*Salinity&WIDTH=1448&HEIGHT=748','tmp/test_fi_vsec.html')

    def test_getmap(self):

        self.WMS.save_request('layers=Atlantic%2FTemperature.19752008.4Danl.nc*Temperature&ratio=0.01&section=-39.375%2C60.46875|-4.21875%2C-29.53125&format=image%2Fpng&service=WMS&version=1.3.0&request=GetMap&exceptions=application%2Fvnd.ogc.se_inimage&srs=EPSG%3A4326&bbox=0%2C-5000%2C9616.673491552212%2C0&width=512&height=256&elevation=-0&time=180.5&styles=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A3.07768841088%2Bvmax%3A28.4103258093%2Bncontours%3A40','tmp/na_vsec.png')


        self.WMS.save_request('request=GetLegendGraphic&width=80&height=150&transparent=true&decorated=true&style=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A3.07768841088%2Bvmax%3A28.4103258093%2Bncontours%3A40&color=%23000000&format=image%2Fpng&layer=Atlantic%2FTemperature.19752008.4Danl.nc*Temperature','tmp/na_vsec_legend.png')

        #self.WMS.save_request('LAYERS=JRA8_TEMP.19752005.4Danl.nc*TEMP&STYLES=&FORMAT=image/png&TRANSPARENT=true&RATIO=0.007455181556195965&SECTION=-1.6984374999999998,58.802734375|3.75078125,55.0234375&TIME=1&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application/vnd.ogc.se_inimage&SRS=EPSG:4326&BBOX=223.3856893542757,-171.02966841186856,446.7713787085514,52.356020942407156&WIDTH=256&HEIGHT=256',"tmp/test_vsec.png")

        # animation

        # ok
        self.WMS.save_request('layers=ocean_his.nc*temp&section=9.07267125%2C44.081884375%7C9.435220078125%2C43.5984859375%7C10.050454453125%2C43.35678671875%7C10.358071640625%2C43.016210546875&request=GetMap&width=800&height=500&bbox=0%2C-1067.83%2C1494.962%2C0&transparent=true&decorated=true&styles=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A12.9978080701%2Bvmax%3A16.6550196511%2Bncontours%3A40&format=image%2Fpng&ratio=0.0009799981537992402&time=2010-05-01T12%3A00','tmp/vert_test.png')

        # ok
        self.WMS.save_request('layers=ocean_his.nc*temp&section=9.07267125%2C44.081884375%7C9.435220078125%2C43.5984859375%7C10.050454453125%2C43.35678671875%7C10.358071640625%2C43.016210546875&request=GetMap&width=800&height=500&bbox=0%2C-1067.83%2C1494.962%2C0&transparent=true&decorated=true&styles=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A12.9978080701%2Bvmax%3A16.6550196511%2Bncontours%3A40&format=video%2Fwebm&ratio=0.0009799981537992402&time=2010-05-01T12%3A00%2C2010-05-02T12%3A00%2C2010-05-03T12%3A00%2C2010-05-04T12%3A00','tmp/vert_test.webm')

        # ok
        self.WMS.save_request('layers=ocean_his.nc*temp&section=9.07267125%2C44.081884375%7C9.435220078125%2C43.5984859375%7C10.050454453125%2C43.35678671875%7C10.358071640625%2C43.016210546875&request=GetMap&width=800&height=500&bbox=0%2C-1067.83%2C1494.962%2C0&transparent=true&decorated=true&styles=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A12.9978080701%2Bvmax%3A16.6550196511%2Bncontours%3A40&format=video%2Fmp4&ratio=0.0009799981537992402&time=2010-05-01T12%3A00%2C2010-05-02T12%3A00%2C2010-05-03T12%3A00%2C2010-05-04T12%3A00','tmp/vert_test.mp4')



# ------------------------------------------------------------------------------
#
# horizontal sections (diva on web)

class TestDivaOnWebWMS(unittest.TestCase):
    '''unittest for Web Map Server'''

    def setUp(self):

        service = {
            'title': 'test',
            'abstract': 'test',
            'contact_person': 'test',
            'contact_organization': 'test',
            'email': 'test',
            'keywords': []}

        directory = '../../divaonweb-test-data/divaonweb/'


        # Setup WMS
        self.WMS = webmapserver(root=directory,
                                directory='',
                                filemask='*',
                                use_pic_cache = False,
                                service = service)

    def test_getmap(self):
        self.WMS.save_request("layers=analysis.nc*analyzed_field%2Canalysis.nc*error_field%2Cblacksea_ts_data&request=GetMap&width=748.9828571428571&height=512&bbox=23.15%2C35.85957%2C45.65%2C51.24043&transparent=true&decorated=true&styles=method%3Apcolor_flat%2Bncontours%3A40%2Binverted%3Afalse%2Bcmap%3Ajet%2Bvmin%3A-10.5679%2Bvmax%3A26.5859%2Cmax_contourf%2Cscatter&format=application%2Fvnd.google-earth.kmz&db=gebco&max_error=0.3",os.path.join("tmp","image-test_coord.kmz"))



if __name__ == "__main__":
    unittest.main()
