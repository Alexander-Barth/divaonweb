from divaonweb.webmap import *


service = {
    'title': 'test',
    }

bathdir = '../../divaonweb-test-data/DivaData'


ContWMS = continents_webmapserver(bathdir,
                                  use_pic_cache = False)

ContWMS.save_request('LAYERS=contour0&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&EXCEPTIONS=application%2Fvnd.ogc.se_xml&DB=corsica&SRS=EPSG%3A4326&BBOX=11.25,33.75,22.5,45&WIDTH=256&HEIGHT=256','tmp/cont0.png')
