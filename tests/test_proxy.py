
from __future__ import print_function
from StringIO import StringIO

import requests


from divaonweb.webmap_proxy import *
from divaonweb.util import proxy
import sys

def start_response(resp,headers):
    print('resp |%s|' % (resp,), headers,resp == '200 OK')
    assert(resp == '200 OK')
    #assert(dict(headers)['Content-type'] == 'application/xml')
    #print('resp |%s|' % (resp,), headers,resp == '200 OK')





data = '''<wfs:GetFeature xmlns:wfs="http://www.opengis.net/wfs" service="WFS" version="1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.1.0/wfs.xsd"><wfs:Query typeName="feature:observed_cindex" srsName="EPSG:4326" xmlns:feature="emodnet"><ogc:Filter xmlns:ogc="http://www.opengis.net/ogc"><ogc:And><ogc:And><ogc:Or><ogc:PropertyIsEqualTo matchCase="true"><ogc:PropertyName>p35_id</ogc:PropertyName><ogc:Literal>EPC00004</ogc:Literal></ogc:PropertyIsEqualTo></ogc:Or><ogc:PropertyIsBetween><ogc:PropertyName>z</ogc:PropertyName><ogc:LowerBoundary><ogc:Literal>0</ogc:Literal></ogc:LowerBoundary><ogc:UpperBoundary><ogc:Literal>10</ogc:Literal></ogc:UpperBoundary></ogc:PropertyIsBetween><ogc:PropertyIsBetween><ogc:PropertyName>datetime</ogc:PropertyName><ogc:LowerBoundary><ogc:Literal>2000-01-01T00:00:00.000Z</ogc:Literal></ogc:LowerBoundary><ogc:UpperBoundary><ogc:Literal>2000-12-31T00:00:00.000Z</ogc:Literal></ogc:UpperBoundary></ogc:PropertyIsBetween></ogc:And><ogc:BBOX><ogc:PropertyName>geom</ogc:PropertyName><gml:Envelope xmlns:gml="http://www.opengis.net/gml" srsName="EPSG:4326"><gml:lowerCorner>-129.306640625 -17.79296875</gml:lowerCorner><gml:upperCorner>149.306640625 102.79296875</gml:upperCorner></gml:Envelope></ogc:BBOX></ogc:And></ogc:Filter></wfs:Query></wfs:GetFeature>'''


url = "http://emodnet02.cineca.it/geoserver/emodnet/ows"
#r = requests.post(url, data=data)
#print('request',len(r.text))


# from urllib2 import urlopen, Request
# r = Request(url, data, {'CONTENT_TYPE': 'text/plain'})
# y = urlopen(r)
# print('urllib2',y.read())
# print('urllib2',len(y.read()))

#sys.exit(0)

environ = {
    'REQUEST_METHOD': 'POST',
    'wsgi.input': StringIO(data),
    'CONTENT_LENGTH': len(data),
    'CONTENT_TYPE': 'text/plain',
    'QUERY_STRING': 'url=http%3A%2F%2Femodnet02.cineca.it%2Fgeoserver%2Femodnet%2Fows'
}

#print('here')
resp = proxy(environ,start_response)

#print('here 2')
data = ''.join(list(resp))

print('data',len(data))
print('data',data)
#assert(len(data)  > 1000)



sys.exit(0)
#print('here 3')

environ = {
    'REQUEST_METHOD': 'GET',
    'QUERY_STRING': 'url=http%3A%2F%2Fpmr-geoserver.deltares.nl%2Fgeoserver%2Femodnet%2Fows%3Frequest%3DGetCapabilities%26service%3DWFS%26version%3D1.0.0',
}

resp = proxy(environ,start_response)

assert(len(''.join(list(resp)))  > 1000)

data = 'this is a test'
environ = {
    'REQUEST_METHOD': 'POST',
    'wsgi.input': StringIO(data),
    'CONTENT_LENGTH': len(data),
    'CONTENT_TYPE': 'text/plain',
    'QUERY_STRING': 'url=http%3A%2F%2Fhttpbin.org%2Fpost'
}




#resp = proxy(environ,start_response)
#print(resp)

#http://httpbin.org/post


#ProxyWMS = proxy_webmapserver("http://wms.jpl.nasa.gov/wms.cgi?")
#ProxyWMS.save_request('request=GetCapabilities&service=WMS&version=1.3.0&_proxy_url=http%3A%2F%2Fgher-diva.phys.ulg.ac.be%2Femodnet%2FPython%2Fweb%2Fplots','tmp/proxy.out')
