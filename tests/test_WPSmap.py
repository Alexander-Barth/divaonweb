#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
from divaonweb.webmap import *
from divaonweb.WPSwebmapserver import *
from divaonweb.vocab import *
from cgi import parse_qs
import sys
try:
    #python2
    from urllib import urlencode
except ImportError:
    #python3
    from urllib.parse import urlencode


class TestWPS_webmapserver(unittest.TestCase):
    '''unittest for Web Map Server'''

    def setUp(self):
        '''set up test case'''

        # Setup WMS
        baseurl = 'http://localhost/cgi-bin/pywps.cgi?DataInputs=[bbox=BOX(-324.51171875 -57.34375,344.51171875 142.34375);mindatetime=1900-01-01T00:00:00Z;maxdatetime=2100-01-01T00:00:00Z;minz=0;maxz=10;p35=EPC00004;width=1000]&service=wps&request=Execute&Identifier=observed_cindex&version=1.0.0'

        baseurl = 'http://localhost/test/pywps2.xml'
        baseurl = 'http://emodnet02.cineca.it/wps?DataInputs=[bbox=BOX(-324.51171875%20-57.34375,344.51171875%20142.34375);mindatetime=1900-01-01T00:00:00Z;maxdatetime=2100-01-01T00:00:00Z;minz=0;maxz=10;p35=EPC00004;width=1000]&service=wps&request=Execute&Identifier=observed_cindex&version=1.0.0'

#http://emodnet02.cineca.it/wps?DataInputs=[mindatetime=1900-01-01T00:00:00Z;maxz=0;minz=0;width=512;maxdatetime=1900-01-01T00:00:00Z;bbox=BOX(90 45, 135 90);p35=EPC00005]&service=wps&request=Execute&Identifier=observed_cindex&version=1.0.0

        baseurl = 'http://emodnet02.cineca.it/wps'
        usedparams_url = 'http://localhost/test/p35_used.json'
        usedparams_url = 'http://emodnet02.cineca.it/geoserver/emodnet2019/ows?service=WFS&version=1.0.0&outputFormat=json&request=GetFeature&typeName=emodnet2019:p35_used'
        wms_url = 'http://localhost/web-vis-test/Python/web/wpswms'
        vocabpath = '/home/abarth/workspace/divaonweb/tests'
        service = {'title': 'test'}

        self.WMS = WPS_webmapserver(baseurl,usedparams_url,wms_url,vocabpath,service)


    def test_getcap(self):
        self.WMS.save_request('request=GetCapabilities&version=1.3.0&SERVICE=WMS','tmp/test_getcap_WPSobs.xml')

    def test_getstats(self):
        self.WMS.save_request(urlencode({
            "request": "GetStats",
            "version": "1.3.0",
            "SERVICE": "WMS",
            "TIME": "2000-01-01T00:00:00Z/2001-01-01T00:00:00Z",
            "elevation": "0/10",
            "layer": "EPC00007"}),'tmp/test_getstats_WPSobs.xml')


    def test_getmap(self):
        self.WMS.save_request('LAYERS=EPC00004&STYLES=vmin%3A1%2Bvmax%3A1079&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&TIME=1900-01-01T00:00:00Z/2100-01-01T00:00:00Z&elevation=0/10&SRS=EPSG%3A4326&BBOX=13.5,44,14,46&WIDTH=512&HEIGHT=512','tmp/test_obs.png')

    def test_getmap2(self):
        self.WMS.save_request('LAYERS=EPC00005&STYLES=vmin%3A1%2Bvmax%3A1079&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&ELEVATION=0%2F0&TIME=1900-01-01T00%3A00%3A00.000Z%2F1900-01-01T00%3A00%3A00.000Z&SRS=EPSG%3A4326&BBOX=90,45,135,90&WIDTH=512&HEIGHT=512','tmp/test_obs2.png')

    def test_getmap_med(self):
        self.WMS.save_request('LAYERS=EPC00007&STYLES=vmax%3A100000.0%2Bnorm%3Alog%2Bmethod%3Apcolor%2Bvmin%3A1&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&ELEVATION=0%2F10&TIME=2000-01-01T00%3A00%3A00.000Z%2F2001-01-01T00%3A00%3A00.000Z&SRS=EPSG%3A4326&BBOX=0,0,45,45&WIDTH=512&HEIGHT=512','tmp/test_obs_med.png')

    def test_fi(self):
        self.WMS.save_request('LAYERS=EPC00007&STYLES=vmin%3A1%2Bvmax%3A1079&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&ELEVATION=0%2F10&TIME=2000-01-01T00%3A00%3A00.000Z%2F2001-01-01T00%3A00%3A00.000Z&SRS=EPSG%3A4326&EXCEPTIONS=application%2Fvnd.ogc.se_xml&BBOX=-59.65332%2C12.353516%2C79.65332%2C72.646484&X=745&Y=203&INFO_FORMAT=application%2Fvnd.ogc.gml&QUERY_layers=EPC00007&WIDTH=1585&HEIGHT=686&','tmp/test_fi.html')

    # def test_get_map2(self):
    #     self.WMS.save_request('LAYERS=__allplots__&STYLES=&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&TIME=366&SRS=EPSG%3A4326&BBOX=12,44,14,46&WIDTH=256&HEIGHT=256','tmp/test_obs2.png')

    # def test_get_map_metal(self):
    #     self.WMS.save_request('LAYERS=__allplots__Heavy_metals&STYLES=markersize:10%2Bmarkerfacecolor:red&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&TIME=366&SRS=EPSG%3A4326&BBOX=0,20,50,60&WIDTH=256&HEIGHT=256','tmp/test_obs_metal.png')

    # def test_get_legend_graphic(self):
    #     #self.WMS.save_request('LAYERS=__allplots__&STYLES=&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&TIME=366&SRS=EPSG%3A4326&BBOX=12,44,14,46&WIDTH=256&HEIGHT=256','tmp/test_obs.png')

    #     self.WMS.save_request('request=GetLegendGraphic&width=80&height=150&transparent=true&decorated=true&style=&color=%23000000&format=image%2Fpng&layer=','tmp/na_legend.png')




if __name__ == "__main__":
    unittest.main()
