#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
from divaonweb.webmap import *
from divaonweb.obsplots import *
from divaonweb.vocab import *
import xml.etree.ElementTree as ET
import sys
import os
import os.path

class TestVocab(unittest.TestCase):
    '''unittest for Web Map Server'''

    def setUp(self):
        '''set up test case'''

        path = os.getcwd()
        self.loader = CachedLoader({
            'http://vocab.nerc.ac.uk/collection/P35/current/': ET.parse(os.path.join(path,'P35.xml')),
            'http://vocab.nerc.ac.uk/collection/P36/current/': ET.parse(os.path.join(path,'P36.xml')),
            'http://vocab.nerc.ac.uk/collection/P06/current/': ET.parse(os.path.join(path,'P06.xml'))
        })
        
    def test_label(self):
        c = Concept('http://vocab.nerc.ac.uk/collection/P35/current/EPC00006')
        self.assertEqual(c.prefLabel,"Water body nitrite")
        units = c.find('related','P06')
        p36 = c.find('broader','P36')
        self.assertEqual(units.prefLabel,"Micromoles per litre")
        self.assertEqual(units.notation,"SDN:P06::UPOX")


    def test_collection(self):
        col = Collection('http://vocab.nerc.ac.uk/collection/P35/current/')
        c = col['SDN:P35::EPC00006']
        self.assertEqual(c.prefLabel,"Water body nitrite")

    def test_cache(self):
        tree = self.loader('http://vocab.nerc.ac.uk/collection/P35/current/')
        self.assertEqual(tree.getroot().tag,'{http://www.w3.org/1999/02/22-rdf-syntax-ns#}RDF')


    def test_collection_cache(self):
        col = Collection('http://vocab.nerc.ac.uk/collection/P35/current/',loader = self.loader)
        c = col['SDN:P35::EPC00006']
        self.assertEqual(c.prefLabel,"Water body nitrite")
        p36 = c.find('broader','P36')

if __name__ == "__main__":
    unittest.main()
        
