#!/usr/bin/python
# This is an example on using Diva-on-web as a web-service
from __future__ import print_function
import sys
import requests
import xml.etree.ElementTree as ET

host = 'ec.oceanbrowser.net'
urlpath = '/emodnet'

host = 'localhost:9090'
urlpath = ''

def get_request(call,**kwargs):
    '''make a get request of the function called call and with parameters kwargs'''

    url = 'http://' + host + urlpath + '/Python/web/' + call
    r = requests.get(url,params = kwargs)
    return r.content



def save_file(filename,data):
    '''saves data in a file called filename'''
    print('Saving file:',filename)
    with open(filename,"wb") as f:
        f.write(data)




# data to interpolate

fname = '../../divaonweb-test-data/Obs/temperature_argo.txt'

if len(sys.argv) == 2:
    fname = sys.argv[1]

print('Using ',fname)

# upload to the data to the server

url = 'http://' + host + urlpath + '/Python/web/upload?columnsep=whitespace&decimalsep=.'
r = requests.post(url, files={'data': open(fname, 'rb')})
body = r.text

#print('upload response: ',body)

root = ET.fromstring(body)
param = {stat.attrib['id']: stat.text for stat in root.findall('stat')}

sessionid = param['sessionid']
x0 = param['obs_x0']
x1 = param['obs_x1']
y0 = param['obs_y0']
y1 = param['obs_y1']
print('sessionid ',sessionid)

bbox = ','.join([x0,y0,x1,y1])

# get data distribution from Wep Map Service

img = get_request('diva_on_web',version="1.1.1", request="GetMap",
                             srs="EPSG:4326",
                             width='512', height='256',
                             layers=sessionid,
                             format="image/png",
                             bbox=bbox,
                             styles='scatter')
save_file("tmp/test_obs.png",img)

# make a DIVA fit
print('run DIVA fit')
s = get_request('fit',sessionid = sessionid)
#print(s)

root = ET.fromstring(s)
length = float(root.find("stats/stat[@id='len']").text)
stn = float(root.find("stats/stat[@id='stn']").text)

print('length: ',length)
print('stn: ',stn)

# we use these values because the analysis is faster (for testing)
length = 20
stn = 1

# make a DIVA analysis
print('make a DIVA analysis')
dx = 3
dy = 3
s = get_request('make_analysis',sessionid = sessionid,
                len = length, stn = stn, dx = dx, dy = dy,
                x0=x0,x1=x1,y0=y0,y1=y1)

#print('make_analysis response: ',s)

root = ET.fromstring(s)
analysis = root.find('result').attrib['src']


layers = analysis + '#analyzed_field'

# get an image from Wep Map Service
img = get_request('diva_on_web',version="1.1.1", request="GetMap",
                             srs="EPSG:4326",
                             width='512', height='256',
                             layers=layers,
                             format="image/png",
                             bbox=bbox,
                             styles='pcolor')
save_file("tmp/test_analysis.png",img)

# get the results as NetCDF file

ncfile = get_request('download',fieldname=analysis)
save_file("tmp/test_analysis.nc",ncfile)

# get bathymetry as NetCDF file

ncfile = get_request(
    'download',
    dx=dx, dy=dy,
    x0=x0,x1=x1,y0=y0,y1=y1,
    db="gebco",type="bat-nc")

save_file("tmp/test_bathymetry.nc",ncfile)
