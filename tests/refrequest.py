from divaonweb.webmap import *
from cgi import parse_qs

service = {
    'title': 'test',
    }

directory = '../../divaonweb-test-data/WMS'

# Setup WMS
WMS = webmapserver(root=directory, 
                   directory='', 
                   filemask='*nc', 
                   use_pic_cache = False,
                   service = service)


WMS.save_request('LAYERS=Salinity.19002009.4Danl.nc*Salinity&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A17.6622%2Bvmax%3A18.4271%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&ELEVATION=-20.0&TIME=01&SRS=EPSG%3A4326&BBOX=28.125,39.375,33.75,45&WIDTH=512&HEIGHT=512','tmp/out.png')
