from divaonweb.webmap import *
from divaonweb.webmap_proxy import proxy_webmapserver

class TestDivaOnWebWMS(unittest.TestCase):
    '''unittest for Web Map Server'''

    def setUp(self):

        service = {
            'title': 'test',
            'abstract': 'test',
            'contact_person': 'test',
            'contact_organization': 'test',
            'email': 'test',
            'keywords': []}

        directory = '/var/www/data/SeaDataNet-domains/'

        # Setup WMS
        self.WMS = webmapserver(root=directory, 
                                directory='', 
                                filemask='*', 
                                use_pic_cache = False,
                                service = service)

    def test_getmap(self):
        self.WMS.save_request("LAYERS=Mediterranean%20Sea%2FJRA5_Salinity.19002009.4Danl.nc*Salinity_L2&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Acontour%2Bvmin%3A31%2Bvmax%3A40%2Bncontours%3A10&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&ELEVATION=-0.0&TIME=01&SRS=EPSG%3A4326&BBOX=-22.5,45,0,67.5&WIDTH=512&HEIGHT=512",os.path.join("tmp","test_sdn_getmap.png"))



if __name__ == "__main__":
    unittest.main()
