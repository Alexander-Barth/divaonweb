

tests = [
         'test_obsplots.py',
         'test_exceptions.py',
         # works only on gher17, bug in numpy?
         #execfile('test_geodataset.py')
         'test_cont.py',
         'test_webmap.py',
         'test_WPSmap.py',
         'test_vocab.py',
         'test_request.py',
         ]


if __name__ == "__main__":
    for test in tests:
        print '=' * 80
        print test
        execfile(test)
        print "\n" * 2
