import urllib


baseurl = 'http://gher-diva.phys.ulg.ac.be:8081/';

def testurl(url,filename,type):
    '''saves data in a file called filename''' 
    #print 'url ',baseurl + url
    f = urllib.urlopen(baseurl + url)
    data = f.read()
    f.close()

    content_type = ''

    for h in f.headers.items():
        if h[0] == 'content-type':
            content_type = h[1]
    #print 'content_type ',content_type


    print 'File:',filename,
    if content_type == type:
        print 'OK'
    else:
        print 'wrong type. Expected', type, ' recieved ',content_type
        

    f = open(filename,"w")
    f.write(data)
    f.close()
    


#testurl('Python/web/continents?LAYERS=contour0&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&EXCEPTIONS=application%2Fvnd.ogc.se_xml&DB=corsica&SRS=EPSG%3A4326&BBOX=11.25,33.75,22.5,45&WIDTH=256&HEIGHT=256','tmp/cont0.png','image/png')

testurl('Python/web/wms?&layer=North%20Sea%2FNTRA.19702009.4Danl.nc%23NTRA_L1&request=GetStats&elevation=-0.0&time=1','tmp/nodata.xml','text/xml')
