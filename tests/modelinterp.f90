
subroutine modelinterp(imax1,jmax1,kmax1,nmax1,z1,t1,v1, &
     valex,kmax2,nmax2,z2,t2,v2)
 implicit none 
 !     
 !     INPUT PARAMETERS
 !     ****************
 !     
 !     grid 1: source grid
 !     ------ 
 !     dimension      
 integer, intent(in) :: imax1,jmax1,kmax1,nmax1
 !     coordinate
 real(8), intent(in) :: z1(imax1,jmax1,kmax1)
 real(8), intent(in) :: t1(nmax1)
 !     variable
 real(8), intent(in) :: v1(imax1,jmax1,kmax1,nmax1)
 real(8), intent(in) :: valex

 !     
 !     grid 2: destination grid
 !     ------
 !     dimension      
 
 integer, intent(in) :: kmax2,nmax2
 !     coordinate
 real(8), intent(in) :: z2(kmax2), t2(nmax2)

 !     
 !     OUTPUT PARAMETERS
 !     *****************
 !     
 !     variable
 real(8), intent(out) :: v2(imax1,jmax1,kmax2,nmax2)
 

 !     
 !     LOCAL VARIABLES
 !     ***************
 !     
 integer :: k1,n1,n1n,k1n
 integer :: k2,n2     
 integer :: i,j
 real(8) :: c,d
 real(8) :: tmpz

! write(6,*) 'valex ',valex,imax1,jmax1,kmax1,nmax1,kmax2,nmax2
! write(6,*) 'kmax1 ',kmax1
! return

 v2 = valex

 do n2=1,nmax2
   call locate(1,nmax1,t1,t2(n2),1,n1,n1n,d)

   if (n1 == -1) cycle
   
   do j=1,jmax1
     do i=1,imax1

       do k2=1,kmax2

         !              tmpz = min(z2(k2), max(z1(i,j,1),z1(i,j,kmax1)))
         tmpz = min(z2(k2), z1(i,j,kmax1))
         !              tmpz = -20


         !write(6,*) 'kmax1 ',imax1*jmax1,kmax1,tmpz,i+(j-1)*imax1

         call locate(imax1*jmax1,kmax1,z1,tmpz, &
              i+(j-1)*imax1,k1,k1n,c)

         !write(6,*) 'kmax1 ',imax1*jmax1,kmax1,tmpz,i+(j-1)*imax1

         !         write(6,*) 'tmpz, z2(k2),  z1(i,j,kmax1) ',
         !     & tmpz, z2(k2),z1(i,j,kmax1),k1 

         if (k1 == -1) cycle



         if (v1(i,j,k1  ,n1  ) /= valex.and. &
              v1(i,j,k1  ,n1n) /= valex.and. &
              v1(i,j,k1n,n1  ) /= valex.and. &
              v1(i,j,k1n,n1n) /= valex) then

           v2(i,j,k2,n2) = (1-c)*(1-d)*v1(i,j,k1  ,n1  ) + &
                (1-c)*(  d)*v1(i,j,k1  ,n1n) +  &
                (  c)*(1-d)*v1(i,j,k1n,n1  ) +  &
                (  c)*(  d)*v1(i,j,k1n,n1n) 
         end if
       end do
     end do
   end do
 end do

end subroutine modelinterp



subroutine locate(imax,jmax,x,xi,i,j,jn,coeff)
 implicit none
 integer :: imax,jmax
 real(8) :: x(imax,jmax)
 real(8) :: xi
 integer  :: i,j,jn
 real(8) :: coeff

 integer :: lp

 !#define OLD_SEARCH
 !#ifdef OLD_SEARCH

 j = -1
 

 if (jmax == 1) then
   if (x(i,1) == xi) then
     j = 1
     coeff = 1.
     jn = 1
     return
   end if
 else
   do lp = 1,jmax-1
     if ((x(i,lp) <= xi.and.xi <= x(i,lp+1)).or.  &
          (x(i,lp+1) <= xi.and.xi <= x(i,lp))) then
       j = lp
       exit
     end if
   end do
 end if


 if (j /= -1) then
   jn = j+1
   coeff = (xi-x(i,j))/(x(i,j+1)-x(i,j))
 end if
 
 ! + (xi-x(l))/(x(l+1)-x(l))


end subroutine locate


