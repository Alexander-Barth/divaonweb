#!/usr/bin/python
from __future__ import print_function
from urllib2 import urlopen
from urllib import urlencode
from datetime import datetime
import random
import string
import time
import argparse


parser = argparse.ArgumentParser(description='Benchmark OceanBrowser.')
parser.add_argument('--baseurl', help='base URL of OceanBrowser', dest='baseurl', default="http://gher-diva.phys.ulg.ac.be/emodnet/")

args = parser.parse_args()
baseurl = args.baseurl



def median(lst):
    lst = sorted(lst)
    import math
    if len(lst) < 1:
            return None
    if len(lst) %2 == 1:
            return lst[((len(lst)+1)/2)-1]
    if len(lst) %2 == 0:
            return float(sum(lst[(len(lst)/2)-1:(len(lst)/2)+1]))/2.0


def test_cap():
    url = baseurl + '?' + urlencode({
        'request': 'GetCapabilities', 
        'service': 'WMS', 
        'version': '1.3.0',
        'time': datetime.now().strftime('%s')})

    url = 'http://gher-diva.phys.ulg.ac.be/'
    response = urlopen(url)
    data = response.read()

class TestCase(object):
    def __init__(self,description,url,params = {}):
        self.description = description
        self.url = url
        self.params = params
        self.N = 10

    def run(self):
        params = self.params.copy()
        params['nocache'] = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(self.N))

        if '?' in self.url:
            url = self.url + '&'
        else:
            url = self.url + '?'

        url += urlencode(params)

        print('get',url)
        response = urlopen(self.url)
        data = response.read()


cases = [
    TestCase(
        'Start page',
        baseurl),

    TestCase(
        'Download a DIVA product (150 Mb NetCDF file)',
        baseurl + '../data/emodnet-domains/North%20Sea/NTRA.19702009.4Danl.nc'),

    TestCase(
        'Iventory of all layers (WMS GetCapabilities)',
        baseurl + '/Python/web/wms?request=GetCapabilities&service=WMS&version=1.3.0'),

    TestCase(
        'Plot a single map (WMS GetMap)',
        baseurl + '/Python/web/wms?LAYERS=North%20Sea%2FNTRA.19702009.4Danl.nc*NTRA&STYLES=&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&ELEVATION=-0.0&TIME=winter&SRS=EPSG%3A4326&BBOX=0,45,22.5,67.5&WIDTH=256&HEIGHT=256'),

    TestCase(
        'Extract vertical section',
        baseurl + '/Python/web/wms_vert?section=11.36105579%2C66.9%7C10.9%2C66.62435821%7C10.82%2C66.16664598%7C10.61317382%2C65.66%7C10.1%2C65.45524246%7C9.7%2C65.10798685%7C9.54%2C64.66487007%7C8.94%2C64.58213168%7C8.42%2C64.41131485%7C7.869058668%2C64.22%7C7.396963761%2C63.98%7C6.94%2C63.70326124%7C6.560904926%2C63.38%7C5.95316122%2C63.26%7C5.426422977%2C63.06%7C5.02%2C62.77627319%7C4.5%2C62.54518004%7C4.11012334%2C62.22%7C3.835422099%2C61.78%7C3.658392799%2C61.34%7C3.74%2C60.78884751%7C3.982248981%2C60.34%7C4.173932021%2C59.78%7C3.987421339%2C59.38%7C4.253403253%2C58.98%7C4.644312448%2C58.66%7C4.98%2C58.28061332%7C5.46%2C58.03309646%7C5.94%2C57.80734131%7C6.5%2C57.62255114%7C7.1%2C57.53230133%7C7.74%2C57.52332932%7C8.34%2C57.64081257%7C8.886328612%2C57.82%7C9.346779958%2C58.06%7C9.795184671%2C58.34%7C10.26%2C58.42298815%7C10.06%2C58.10738983%7C9.5%2C57.96918282%7C9.06%2C57.69234126%7C8.46%2C57.56399253%7C7.919079969%2C57.38%7C7.54%2C57.05891948%7C7.304692783%2C56.54%7C7.332685266%2C55.9%7C7.347497838%2C55.38%7C7.54%2C55.02808349%7C7.389033629%2C54.54%7C7.06%2C54.16766296%7C6.46%2C54.04248428%7C5.86%2C53.91163859%7C5.18%2C53.86273558%7C4.637154406%2C53.7%7C4.182695965%2C53.42%7C3.958888592%2C52.94%7C3.742620758%2C52.42%7C3.337398721%2C52.14%7C2.9%2C51.83819905%7C2.452774876%2C51.58%7C2.006231713%2C51.66%7C2.3%2C52.00291252%7C2.501009543%2C52.5%7C2.3%2C52.98932027%7C1.836745707%2C53.26%7C1.26%2C53.40239234%7C0.8746915017%2C53.74%7C0.66%2C54.23930254%7C0.26%2C54.55148831%7C-0.1551928172%2C54.86%7C-0.62%2C55.11529334%7C-0.8316555634%2C55.62%7C-1.18%2C55.98298396%7C-1.621789366%2C56.26%7C-1.58%2C56.59298983%7C-1.26%2C56.99642386%7C-0.9402330748%2C57.42%7C-1.14%2C57.87426347%7C-1.622291808%2C58.1%7C-2.3%2C58.14879756%7C-2.06%2C58.58183775%7C-1.7%2C58.95311581%7C-1.14157752%2C59.14%7C-0.7435656812%2C59.46%7C-0.3738628746%2C59.78%7C-0.02%2C60.14551136%7C0.1478527047%2C60.62%7C-0.02%2C61.09111404%7C-0.54%2C61.26987829%7C-1.247885%2C61.26%7C-1.746240216%2C61.06%7C-2.26%2C60.83126876%7C-2.66%2C60.50571788%7C-3.017266265%2C60.14%7C-3.099767847%2C59.82%7C-3.66%2C59.66581483&layer=North%20Sea%2FNTRA.19702009.4Danl.nc*NTRA&time=winter&request=GetStats'),

    TestCase(
        'Extract contourline at a fixed distance from coastline',
        baseurl + '../ContourExtractor/cgi-bin/contour_extractor.cgi?maxlen=100&type=dist2coast&lev=50&xr=-4%2C12.9719&yr=48%2C66.9167')
]
    
#cases = [cases[0]]


if __name__ == '__main__':
    import timeit
    
    
    tmedian = []
    for i in range(len(cases)):
        t = timeit.repeat("cases[%g].run()" % (i,), setup="from __main__ import cases", number=1, repeat = 10)
        tmedian.append(median(t))

    for i in range(len(cases)):
        print('%s: %g sec' % (cases[i].description,tmedian[i]))
