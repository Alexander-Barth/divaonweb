from __future__ import print_function
from divaonweb.geodataset import *


if False:
    bat = GeoDataset('/home/abarth/Desktop/test.nc','analysed_sst_climatology')
    x,y,z,t,v = bat.load(longitude=[10,20],latitude=[30,40])


    bat = GeoDataset('/home/abarth/Desktop/test.nc','DT_sst_skin')
    x,y,z,t,v = bat.load(longitude=[10,20],latitude=[30,40])


    bat = GeoDataset('/home/abarth/Desktop/01-GOS-ChlHR-MED_monthlyCLIMv2_1.nc','chl_climatology')
    print(bat.getDimNames())
    x,y,z,t,v = bat.load(longitude=[10,20],latitude=[30,40])
    print(v)



def test_roms():
    #bat = GeoDataset('/home/abarth/workspace/divaonweb/divaonweb/paster_templates/WMS-Data/coads.nc','VWND')

    bat = GeoDataset('../../divaonweb-test-data/WMS/ocean_his.nc','v')
    #bat = GeoDataset('/home/abarth/Test/ROMS/ocean_avg.nc','h')

    print(bat.getDimNames())
    z = bat.coord('depth')

    [x,y,z,t,v] = bat.load(longitude=[8,10],latitude=[43,44],depth=-100,time='default')
    [x,y,z,t,v] = bat.load(depth=-100,time='default')
    [x,y,z,t,v] = bat.load(time='default')

    #print 'test',v.shape
    print('test',bat.coord_range('depth'))

    #print 'shape',v.shape,x.shape,y.shape

    print('time ', bat.coord('time'))


    [x,y,z,t,v] = bat.load(longitude=9.1,latitude=43.5,depth=-100,time='default')

    print('v' , x,y,z,t,v)
    #import pylab
    #pylab.pcolor(x,y,v)
    #pylab.show()

def test_wrf():
    bat = GeoDataset('../../divaonweb-test-data/WMS/wrf.nc','T')

    print(bat.coord('longitude').shape,bat.coord('longitude').ndim)
    print(bat.coord('latitude').shape)
    #print bat.coord('time').shape

    z = bat.coord('depth')

    print(z.shape,z.min(),z.max())

    [x,y,z,t,v] = bat.load(depth=30,time='default')
    [x,y,z,t,v] = bat.load(longitude=[8,10],latitude=[43,44],depth=100,time='default')

    print('here',v.shape,v.min(),v.max())

import netCDF4
class NCCFDataSet(object):
    def __init__(self,filename,varname):
        self.filename = filename
        self.varname = varname        
        self.nc = netCDF4.Dataset(filename,'r')
        self.nv = self.nc.variables[varname]
        self.dimnames = self.nv.dimensions
        self.coord = []

        if hasattr(self.nv,'coordinates'):
            for c in self.nv.coordinates.split(' '):
                nvc = self.nc.variables[c]

                coordstruct = {
                    'name': c,
                    'nv': nvc,
                    'dims': nvc.dimensions}
 
                self.coord.append(coordstruct)
                        

        
def test_obs():
    fname = '../../divaonweb-test-data/WMS/example.nc'
    fname = '../../divaonweb-test-data/WMS/Salinity.19002009.4Danl.nc'

    vv = GeoDataset(fname,'Salinity')
    print('hr ',vv.hr_t)

    vv = GeoDataset(fname,'obsid')
    print(vv.has_coord('longitude'))
    x = vv.coord('longitude')
    y = vv.coord('latitude')
    z = vv.coord('depth')
    t = vv.coord('time')

# more work is needed for this
# since obsid has the dimensions ('observations', 'idlen')
#    vv.load(longitude=35.0063225546875, latitude=44.32944691790194)

    print(vv,x[0],y[0],z[0],t[0],vv.dims)

#    vv = NCCFDataSet('../../divaonweb-test-data/WMS/example.nc','obsid')

# does not work
#    (x, y, z, t, v) = vv.load(longitude=[15, 30], latitude=[15, 30], 
#                                      depth=-1)


#test_roms()
#test_wrf()
test_obs()
