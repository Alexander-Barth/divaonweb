#import sys
#sys.path.append("/home/abarth/workspace/divaonweb-coord") 
#sys.path.append("/home/abarth/Python") 
from divaonweb.webmap import *
from cgi import parse_qs
import logging
import StringIO


import logging
logger = logging.getLogger('myapp')
buff = StringIO.StringIO()
hdlr = logging.StreamHandler(buff)
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.WARNING)


service = {
    'title': 'test',
    'abstract': 'test',
    'contact_person': 'test',
    'contact_organization': 'test',
    'email': 'test',
    'keywords': []}

directory = '../../divaonweb-test-data/WMS/'

# Setup WMS
WMS = webmapserver(root=directory, 
                   directory='', 
                   filemask='*.nc', 
                   use_pic_cache = False,
                   service = service,
                   fontfile = '../../divaonweb-test-data/Fonts/DejaVuSans.ttf',log=logger)


hr = WMS.request(LAYERS='invalid', STYLES="method:contour", 
                 TRANSPARENT="true", FORMAT="image/png", 
                 SERVICE="WMS", VERSION="1.1.1", 
                 REQUEST="GetMap", 
                 EXCEPTIONS="application/vnd.ogc.se_inimage", 
                 ELEVATION="-0",
                 TIME="180.5", SRS="EPSG:4326", 
                 BBOX="-90, 0, 0, 90", 
                 WIDTH="256", HEIGHT="257")

assert hr.content_type == "image/png"

filename = "tmp/image_exceptions_inimage.png"
f = open(filename,"w")
f.write(hr.content)
f.close()


hr = WMS.request(LAYERS='invalid', STYLES="method:contour", 
                 TRANSPARENT="true", FORMAT="image/png", 
                 SERVICE="WMS", VERSION="1.1.1", 
                 REQUEST="GetMap", 
                 EXCEPTIONS="application/vnd.ogc.se_blank", 
                 ELEVATION="-0",
                 TIME="180.5", SRS="EPSG:4326", 
                 BBOX="-90, 0, 0, 90", 
                 WIDTH="256", HEIGHT="257")

assert hr.content_type == "image/png"

filename = "tmp/image_exceptions_blank.png"
f = open(filename,"w")
f.write(hr.content)
f.close()

#print buff.getvalue()
