from divaonweb.geodataset import *
from divaonweb.interpolation import vinterp,vinterp_fortran,vinterp_python
import numpy
import numpy.ma as ma
import time




bat = GeoDataset('/home/abarth/Test/ROMS/ocean_his.nc','v')
#bat = GeoDataset('/home/abarth/Test/ROMS/ocean_avg.nc','h')

print bat.getDimNames()
z = bat.coord('depth')

#[x,y,z,t,v] = bat.load(longitude=[8,10],latitude=[43,44],depth=-100)
#[x,y,z,t,v] = bat.load(depth=-100)
[x,y,z,t,v] = bat.load(time='default')

print 'test',v.shape
print 'test',bat.coord_range('depth')

#print 'shape',v.shape,x.shape,y.shape

print 'time ', bat.coord('time')

zi = -10

#vi = vinterp(v,z,zi)
#print 'vi ',vi[10,10]

#print modelinterp.modelinterp.__doc__


#t0 = time.time()
#vi2 = vinterp_fortran(v,z,zi)
vi2 = vinterp(v,z,zi)

#print "time in interpolation", time.time()-t0

print 'vi2 ',vi2[10,10]


#print 'vi ', vi

if False:
    import pylab
    pylab.pcolor(x,y,vi2)
    pylab.show()
