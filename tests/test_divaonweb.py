#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function

import os
import sys
import tempfile
from xml.dom.minidom import parseString
import xml.etree.ElementTree as ET
import cProfile
import pstats
from datetime import datetime
import io
import unittest

from divaonweb import settings
from divaonweb.divaonweb import *


class TestFile(object):
    def __init__(self, str, mode = 'r',encoding = 'utf-8'):

        if 'b' in mode or sys.version_info[0] == 2:
            self.file = open(str,mode)
        else:            
            self.file = open(str,mode,encoding=encoding)

        self.filename = 'sdn.txt'

def parseStat(s):
    root = ET.fromstring(s)
    return {_.attrib['id']: _.text for _ in root.findall('stat')}


class TestDivaOnWeb(unittest.TestCase):

    def setUp(self):
        self.dir = tempfile.mkdtemp()

        
        try:
            os.mkdir(self.dir)
        except:
            pass

        ddir = os.path.join(self.dir, settings.datadir)
        if not os.path.isdir(ddir):
            os.makedirs(ddir)

        #self.diva_template = '/var/www/web-vis_bak/Diva/divastripped_template'
        self.diva_template = '/var/www/web-vis/Diva/divastripped_template'
        self.datadir = 'Data'
        self.bath = settings.bath


        script_path = os.path.abspath(__file__)
        octave_path = os.path.join(os.path.split(script_path)[0],'..','divaonweb','paster_templates','Octave')

        settings.bathdir = '../../divaonweb-test-data/DivaData/'
        settings.topdir = '/var/www/web-vis_bak/'
        settings.diva_template = '/var/www/web-vis_bak/Diva/divastripped_template'
        settings.octave = {'bin': 'octave', 
                           'env': os.environ,
                           'path': octave_path}


#        self.log.addHandler(logging.StreamHandler(sys.stdout))
        self.log = logging.Logger('divaonweb')
        #self.log.addHandler(logging.FileHandler('tmp/test.log'))
        self.log.addHandler(logging.StreamHandler(sys.stdout))

        self.diva = diva_on_web(self.dir,self.dir,self.diva_template,
                                self.datadir, log = self.log, do_debug=False)

    

#     def test_upload_odv(self):
#         class TestFile(object):
#             def __init__(self, str):
#                 self.file = io.StringIO(str)
#                 self.filename = 'odv.txt'

        
#         data = TestFile("""// user commennts
# //
# //SDN_parameter_mapping
# //<subject>SDN:LOCAL:Julian Date (chronological)</subject><object>SDN:P011::CJDY1101</object><units>SDN:P061::UTAA</units>
# //<subject>SDN:LOCAL:Surface elevation (unspecified datum) of the water column</subject><object>SDN:P011::ASLVZZ01</object><units>SDN:P061::ULAA</units>
# //
# Cruise	Station	Type	yyyy-mm-ddThh:mm:ss.sss	Longitude [degrees_east]	Latitude [degrees_north]	LOCAL_CDI_ID	EDMO_code	Bot. Depth [m]	Julian Date (chronological) [Days]	QV:SEADATANET	Surface elevation (unspecified datum) of the water column [m]	QV:SEADATANET	
# WIERMGDN-1999-RIKZMON_WAT-DNM	WIERMGDN	*	1999-01-01T00:00:00.000	5.959	53.517	13728	1526	0	2451179.500000	1	-0.61	1
# 									2451179.506944	1	-0.7	1
# 									2451179.513889	1	-0.78	1
# 									2451179.520833	1	-0.85	1
# 									2451179.527778	1	-0.93	1
# 									2451179.534722	1	-1	1
# 									2451179.541667	1	-1.07	1
# 									2451179.548611	1	-1.14	1
# 									2451179.555556	1	-1.19	1
# 									2451179.562500	1	-1.24	1
# 									2451179.569444	1	-1.29	1
# 									2451179.576389	1	-1.33	1
# 									2451179.583333	1	-1.36	1
# """)


#         resp = diva_on_web(self.dir).upload('1234', data, type='odv4')
#         xml = resp.content
    
#         dom = parseString(xml)
#         print xml
#         stat = dom.getElementsByTagName('stat')
#         self.assertEqual(stat[0].firstChild.data, '13')
#         self.assertEqual(stat[1].firstChild.data, '5.959')
#         self.assertEqual(stat[2].firstChild.data, '5.959')
#         self.assertEqual(stat[3].firstChild.data, '53.517')
#         self.assertEqual(stat[4].firstChild.data, '53.517')



    def test_interp_odv(self):
        class TestFile(object):
            def __init__(self, str):
                self.file = io.StringIO(str)
                self.filename = 'odv.txt'

        
        data = TestFile(u"""Cruise	Station	Type	yyyy-mm-ddThh:mm:ss	Longitude [degrees_east]	Latitude [degrees_north]	Depth [m]	Julian Date (chronological) [Days]	QV:SEADATANET	Temperature  [degrees_C]	QV:SEADATANET
Test_cruise	A1	*	1999-01-01T00:00:00	5.96	53.52	0	2448257.5	1	10.11	1
			1999-01-01T00:00:00			2	2448257.5		11	
			1999-01-01T00:00:00			3	2448257.5		12	
			1999-01-01T00:00:00			4	2448257.5		13	
			1999-01-01T00:00:00			5	2448257.5		14	
			1999-01-01T00:00:00			6	2448257.5		15	
			1999-01-01T00:00:00			7	2448257.5		16	
			1999-01-01T00:00:00			8	2448257.5		17	
			1999-01-01T00:00:00			9	2448257.5		18	
			1999-01-01T00:00:00			10	2448257.5		19	
			1999-01-01T00:00:00			11	2448257.5		20	
			1999-01-01T00:00:00			12	2448257.5		21	
Test_cruise	A2	*	2001-01-01T00:00:00	15.96	43.52	1	2451910.5	1	10	1
			2001-01-01T00:00:00			2	2451910.5		11	
			2001-01-01T00:00:00			3	2451910.5		12	
			2001-01-01T00:00:00			4	2451910.5		13	
			2001-01-01T00:00:00			5	2451910.5		14	
			2001-01-01T00:00:00			6	2451910.5		15	
			2001-01-01T00:00:00			7	2451910.5		16	
			2001-01-01T00:00:00			8	2451910.5		17	
			2001-01-01T00:00:00			9	2451910.5		18	
			2001-01-01T00:00:00			10	2451910.5		19	
			2001-01-01T00:00:00			11	2451910.5		20	
			2001-01-01T00:00:00			12	2451910.5		21	""")



        resp = self.diva.upload('1234', data, type='odv4', variable='Temperature', depth=5)
        print(resp.content)

    def test_odv_single_profile(self):
        data = TestFile('../../divaonweb-test-data/Obs/229407_20120627_173506.txt')
        resp = self.diva.upload('1234', data, type='odv4', variable='Temperature',
                                                          t0='0990-01-01', t1='3000-01-01',
                                                          depth='5')
        param = parseStat(resp.content)
        param_ref = {'obs_x1': '-15.683', 
                     'obs_y1': '9.133', 
                     'obs_y0': '9.133', 
                     'obs_v1': '27.456', 
                     'obs_count': '1', 
                     'obs_v0': '27.456', 
                     'obs_x0': '-15.683'}

        for v in param_ref:
            self.assertEqual(param[v],param_ref[v])


    def test_odv_missingval(self):
        data = TestFile('../../divaonweb-test-data/Obs/data_from_Cyprus_all_4background.txt',encoding='latin-1')
        resp = self.diva.upload('1234', data, type='odv4', variable='SALNTY',
                                                          t0='0990-01-01', t1='3000-01-01',
                                                          depth='5')

        param = parseStat(resp.content)
        param_ref = {'obs_count': '984',
                     'obs_v0': '38.721',
                     'obs_v1': '39.648',
                     'obs_x0': '31',
                     'obs_x1': '34.6667',
                     'obs_y0': '33.083',
                     'obs_y1': '35.167'}

        for v in param_ref:
            self.assertEqual(param[v],param_ref[v])


    # should work, but is slow
    def test_odv_export(self):
        data = TestFile('../../divaonweb-test-data/Obs/data_from_NEW_WOD_COLLECTION2.txt')
        resp = self.diva.upload('1234', data, type='odv4', variable='Temperature',
                                                          t0='0990-01-01', t1='3000-01-01',
                                                          depth='0')
        print(resp.content)


    def test_uploadvel(self):
        data = TestFile('../../divaonweb-test-data/Obs/229407_20120627_173506.txt')
        resp = self.diva.upload('1234', data, type='odv4', variable='Temperature',
                                                          t0='0990-01-01', t1='3000-01-01',
                                                          depth='5')

        param = parseStat(resp.content)
        param_ref = {'obs_count': '1', 
                     'obs_x1': '-15.683', 
                     'obs_v0': '27.456', 
                     'obs_y0': '9.133', 
                     'obs_y1': '9.133', 
                     'obs_v1': '27.456', 
                     'obs_x0': '-15.683'}

        for v in param_ref:
            self.assertEqual(param[v],param_ref[v])

        # get session id
        sessionid = param['sessionid']
        datavel = TestFile('../../divaonweb-test-data/velocity/example123.nc','rb')
        respvel = self.diva.uploadvel(sessionid, datavel)

        paramvel = parseStat(respvel.content)
        self.assertEqual(paramvel['vel_md5'],'4d291a63dcb010fd2dfc4c9e64ea5809')



    def test_zip_odv(self):
        class TestFile(object):
            def __init__(self, str):
                self.file = open(str,'rb')
                self.filename = 'sdn.zip'


        data = TestFile('../../divaonweb-test-data/usermo30d5f-data_centre1578-2012-06-28_result.zip')

        resp = self.diva.upload('1234', data, type='odv4', variable='Temperature',
                                                          t0='0990-01-01', t1='3000-01-01',
                                                          depth='5')
        print(resp.content)

    # def test_zip_odv2(self):
    #     class TestFile(object):
    #         def __init__(self, str):
    #             self.file = open(str)
    #             self.filename = 'sdn.zip'


    #     data = TestFile('../../../Downloads/usertm31099-data_centre630-2013-03-03_result.zip')

    #     resp = self.diva.upload('1234', data, type='odv4', variable='Salinity',
    #                                                       t0='0990-01-01', t1='3000-01-01',
    #                                                       depth='5')
    #     print resp.content




#     def test_odv2(self):
#         class TestFile(object):
#             def __init__(self, str):
#                 self.file = open(str)
#                 self.filename = 'sdn.txt'


#         #data = TestFile('/home/abarth/Data/sdn.zip')
#         data = TestFile('/home/abarth/Test/data_from_4 - Ship for ODV.txt')

#         resp = self.diva.upload('1234', data, type='odv4', variable='Temperature',
#                                                           t0='0990-01-01', t1='3000-01-01',
#                                                           depth='5')
#         print resp.content


    def test_analysis(self):
        class TestFile(object):
            def __init__(self, str):
                self.file = open(str)
                self.filename = 'sdn.zip'


        data = TestFile('../../divaonweb-test-data/Obs/blacksea_ts_data.txt')

        resp = self.diva.upload('1234', data)
        param = parseStat(resp.content)
        sessionid = param['sessionid']
        x0 = param['obs_x0']
        x1 = param['obs_x1']
        y0 = param['obs_y0']
        y1 = param['obs_y1']
        print(param['sessionid'])
        
        resp = self.diva.make_analysis(sessionid, len = 2, std = 1, dx = 0.1, dy = 0.1,
                                               x0=x0,x1=x1,y0=y0,y1=y1)

        print("resp.content ",resp.content)

        analysis = ET.fromstring(resp.content).find('result').attrib['src']

        #dom = parseString(resp.content)
        #analysis = dom.getElementsByTagName('result')[0].getAttribute('src')

        layer = analysis + '*analyzed_field' 
        print('layer ',layer)

        WMS = webmap.webmapserver(root=self.dir, directory='', service = {}, use_pic_cache = False)

        resp = WMS.make_plot(version="1.1.1", request="GetMap", 
                                  srs="EPSG:4326",
                                  width=256, height=256, 
                                  layers=[layer,sessionid],
                                  transparent=True, format="image/png", 
                                  bbox=(27, 40, 42, 47), 
                                  styles=[{'method':'pcolor'},{'method': 'scatter'}])

        filename = os.path.join("tmp","test_analysis.png")
        f = open(filename,"wb")
        f.write(resp.content)
        f.close()
        self.assertEqual(resp.content_type,"image/png")


        resp = WMS.make_plot(version="1.1.1", request="GetMap", 
                                  srs="EPSG:4326",
                                  width=256, height=256, 
                                  layers=[layer,sessionid],
                                  transparent=True, format="image/png", decorated=True,
                                  bbox=(27, 40, 42, 47), 
                                  styles=[{'method':'pcolor'},{'method': 'scatter'}])

        filename = os.path.join("tmp","test_analysis_decorated.png")
        f = open(filename,"wb")
        f.write(resp.content)
        f.close()
        self.assertEqual(resp.content_type,"image/png")


        error_layer = analysis + '*error_field' 

        resp = WMS.make_plot(version="1.1.1", request="GetMap", 
                             srs="EPSG:4326",
                             width=256, height=256, 
                             layers=[layer,error_layer,sessionid],
                             styles=[{'method':'pcolor'},{'method': 'max_contourf'},{'method': 'scatter'}],
                             transparent=True, 
                             decorated=True, 
                             format="application/vnd.google-earth.kmz", 
                             bbox=(27, 40, 42, 47), 
                             db='gebco',
                             max_error=0.3)

        filename = os.path.join("tmp","test_analysis.kmz")
        f = open(filename,"wb")
        f.write(resp.content)
        f.close()
        self.assertEqual(resp.content_type,"application/vnd.google-earth.kmz")


        resp = self.diva.download(fieldname=analysis,type='nc',
                                  varname='temperature',
                                  # variable attribute
                                  va_longname='temperature',
                                  va_units='deg C',
                                  # global attribute
                                  ga_title='a test file',
                                  ga_institution='ULG',
                                  ga_source='source',
                                  ga_history='first test',
                                  ga_references='ref',
                                  ga_comment='blabla',
                                  ga_contact='me@domain.name'
                                  )

        filename = os.path.join("tmp","test_analysis.nc")
        f = open(filename,"wb")
        f.write(resp.content.read())
        f.close()
        self.assertEqual(resp.content_type,"application/x-netcdf")






    def test_extract_bath(self):
        resp = self.diva.download(type='bat-nc',
                                  dx = 0.1, dy = 0.1,
                                  x0 = 27, x1 = 41.75, y0 = 40.38, y1 = 46.733)

        filename = os.path.join("tmp","test_bath.nc")
        f = open(filename,"wb")
        f.write(resp.content.read())
        f.close()
        self.assertEqual(resp.content_type,"application/x-netcdf")


def run(tests):
    if len(tests) == 0:
        unittest.main()
    else:
        for test in tests:
            TestDivaOnWeb(test)()


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-p","--profile", help="enable profiling",action="store_true",default=False)
    parser.add_argument("-t","--test", help="run specific test e.g. 'test_analysis' or use 'all'",default='all')
    args = parser.parse_args()

    if args.profile:
        import cProfile, pstats
        pr = cProfile.Profile()
        pr.enable()

    if args.test == 'all':
        unittest.main()
    else:    
        TestDivaOnWeb(args.test)()

    if args.profile:
        pr.disable()

        fname = 'run-' + datetime.now().strftime('%Y%m%d-%H%M%S') + '-' + test
        pr.dump_stats(os.path.join('profile',fname + '.data'))

        pstats.Stats(pr).sort_stats('cumulative').print_stats(30)

        for key in ['cumulative','nfl','time']:
            with open(os.path.join('profile',fname + '-' + key + '.txt'),'w') as f:
                ps = pstats.Stats(pr, stream=f).sort_stats(key).print_stats()

