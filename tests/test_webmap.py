#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
from divaonweb.webmap import *
from distutils.version import StrictVersion

from divaonweb.webmap_proxy import proxy_webmapserver
from xml.dom.minidom import parseString
from numpy.random import uniform
from lxml import etree

class TestWebmap(unittest.TestCase):
    '''unittest for Web Map Server'''

    def setUp(self):
        '''set up test case'''
        directory = '/tmp/WMS'

        try:
            os.mkdir(directory)
        except:
            pass

        try:
            os.mkdir(os.path.join(directory,'Sub'))
        except:
            pass

        # create sample NetCDF file

        filename = os.path.join(directory, 'test.nc')
        #print "ceate ", filename


        lats =  numpy.arange(-90, 91, 2.5)
        lons =  numpy.arange(-180, 180, 2.5)
        ts =  numpy.arange(0, 12, 1)
        zs =  numpy.arange(-1000, 0, 100)
        data = uniform(size=(len(ts), len(zs), len(lats), len(lons)))
        self.create_nc_file(filename, lons, lats, zs, ts, data, 'Test1')


        filename = os.path.join(directory, 'Sub', 'test.nc')
        self.create_nc_file(filename, lons, lats, zs, ts, data, 'Test2')


        service = {
            'title': 'test',
            'abstract': 'test',
            'contact_person': 'test',
            'contact_organization': 'test',
            'email': 'test',
            'keywords': []}

        # Setup WMS
        self.WMS = webmapserver(root=directory,
                                directory='',
                                filemask='*.nc',
                                use_pic_cache = False,
                                service = service)
        #print "root ", self.WMS.root

        self.ContWMS = continents_webmapserver(directory)

        self.save = True
        self.test_layer = 'test.nc' + layersep + 'temp'
        self.default_time = '2001-01-01T00:00'

    def create_nc_file(self, filename, lons, lats, zs, ts, data, description):
        '''Creates a netCDF for the WMS server'''
        nc = netCDF4.Dataset(filename, 'w', format='NETCDF3_CLASSIC')

        nc.createDimension('depth', len(zs))
        nc.createDimension('time', len(ts))
        nc.createDimension('lat', len(lats))
        nc.createDimension('lon', len(lons))

        depth = nc.createVariable('depth', 'f8', ('depth',))
        times = nc.createVariable('time', 'f8',  ('time',))
        latitudes = nc.createVariable('lat', 'f4', ('lat',))
        longitudes = nc.createVariable('lon', 'f4', ('lon',))
        temp = nc.createVariable('temp', 'f4', ('time', 'depth', 'lat', 'lon'),fill_value=-99999)

        latitudes[:] = lats
        longitudes[:] = lons
        times[:] = ts
        depth[:] = zs
        temp[:,:,:,:] = data

        nc.description = description
        nc.product_id = "blablabla/some/path/lol"

        latitudes.units = 'degrees north'
        longitudes.units = 'degrees east'
        #temp.units = 'K'

        if StrictVersion(netCDF4.__version__) >= StrictVersion('0.9.9'):
            temp.units = u'°C'
        else:
            temp.units = u'°C'.encode('iso-8859-1')

        #temp._FillValue = -99999
        times.units = 'days since 2001-01-01 00:00:00.0'
        times.calendar = 'gregorian'

        nc.close()


    def test_getcap(self):
        '''Test a GetCapabilities request'''

        hr = self.WMS.request(version="1.1.1",
                                     request="GetCapabilities",
                                     SERVICE="WMS")

        dom = parseString(hr.content)

        #print "layer", dom.getElementsByTagName('Layer')
        rootlayer = dom.getElementsByTagName('Layer')[0]
        layer1 = rootlayer.getElementsByTagName('Layer')[0]

        if self.save:
            filename = os.path.join("tmp","GetCapabilities.xml")
            f = open(filename,"wb")
            f.write(dom.toprettyxml(encoding='utf-8'))
            f.close()

        self.assertEqual(
            layer1.getElementsByTagName('Title')[0].firstChild.data,
            'Test1')

        self.assertEqual(
            len(layer1.getElementsByTagName('MetadataURL')),2)

        layer2 = layer1.getElementsByTagName('Layer')[0]
        self.assertEqual(
            layer2.getElementsByTagName('Title')[0].firstChild.data,
            'temp')


    def test_cap_valid111(self):
        '''Test validity of GetCapabilities request'''

        with open('WMS_MS_Capabilities.dtd') as f:
            dtd = etree.DTD(f)

            hr = self.WMS.request(version="1.1.1",
                                     request="GetCapabilities",
                                     SERVICE="WMS")

            dom = etree.XML(hr.content)

            if not dtd.validate(dom):
                raise BaseException(dtd.error_log.filter_from_errors()[0])


    def test_cap_valid130(self):
        '''Test validity of GetCapabilities request'''

        hr = self.WMS.request(version="1.3.0",
                              request="GetCapabilities",
                              SERVICE="WMS")

        dom = etree.XML(hr.content)

        if self.save:
            filename = os.path.join("tmp","GetCapabilities130.xml")
            f = open(filename,"wb")
            f.write(etree.tostring(dom, pretty_print=True))
            f.close()
            #dom.write(filename)

        xmlschema = etree.XMLSchema(file='capabilities_1_3_0.xsd')
        #xmlschema = etree.XMLSchema(file='http://inspire.ec.europa.eu/schemas/inspire_vs/1.0/inspire_vs.xsd')
        xmlschema.assertValid(dom)


    def test_cap_sub(self):
        '''Test a GetCapabilities request (sub)'''

        hr = self.WMS.request(version="1.1.1",
                              request="GetCapabilities",
                              SERVICE="WMS",
                              basedir='Sub')

        dom = parseString(hr.content)
        rootlayer = dom.getElementsByTagName('Layer')[0]
        layer1 = rootlayer.getElementsByTagName('Layer')[0]

        #print "layer", dom.getElementsByTagName('Layer'),layer1.getElementsByTagName('Title')[0].firstChild.data
        self.assertEqual(
            layer1.getElementsByTagName('Title')[0].firstChild.data,
            'Test2')

        layer2 = layer1.getElementsByTagName('Layer')[0]
        self.assertEqual(
            layer2.getElementsByTagName('Title')[0].firstChild.data,
            'temp')

        if self.save:
            filename = os.path.join("tmp","test-sub.xml")
            f = open(filename,"wb")
            f.write(dom.toprettyxml(encoding='utf-8'))
            f.close()


    def test_cap_cache(self):
        '''Test a GetCapabilities request (sub)'''


        hr = self.WMS.request(
            version="1.1.1",
            request="GetCapabilities",
            SERVICE="WMS"
        )


        # request while having current version
        hr = self.WMS.request(
            version="1.1.1",
            request="GetCapabilities",
            SERVICE="WMS",
            _env = {
                'HTTP_IF_MODIFIED_SINCE': hr.headers['Last-Modified']
            }
        )

        self.assertEqual(hr.http_code,'304 Not Modified')


        # request while having old version
        hr = self.WMS.request(
            version="1.1.1",
            request="GetCapabilities",
            SERVICE="WMS",
            _env = {
                'HTTP_IF_MODIFIED_SINCE': 'Thu, 13 Nov 2014 16:59:25 GMT'
            }
        )

        self.assertEqual(hr.http_code,'200 OK')



    def test_getcap_future(self):
        '''Test a GetCapabilities request'''

        hr = self.WMS.request(version="31.3.0",
                              request="GetCapabilities",
                              SERVICE="WMS")

        root = etree.fromstring(hr.content)
        self.assertEqual(root.attrib["version"],"1.3.0")


    def testWMS(self):
        '''test a GetMap request'''
        hr = self.WMS.make_plot(version="1.3.0", request="GetMap",
                                service="WMS",
                                crs="EPSG:4326",
                                width=256, height=256,
                                layers=[self.test_layer],
                                transparent=True, format="image/png",
                                bbox=(-50, 0, 0, 60), styles=[{'method':''}])

        if self.save:
            filename = os.path.join("tmp","test_wms.png")
            f = open(filename,"wb")
            f.write(hr.content)
            f.close()

        self.assertEqual(hr.content_type,"image/png")


    def testGetStats(self):
        '''test a GetMap request'''
        hr = self.WMS.request(request="GetStats",service="WMS",version="1.3.0",
                              layer=self.test_layer)

        self.assertTrue('Last-Modified' in hr.headers)
        self.assertEqual(hr.content_type,"text/xml")


    def test_kml(self):
        '''test a GetKML request'''
        hr = self.WMS.request(request="GetKML",service="WMS",version="1.3.0")

        filename = os.path.join("tmp","test.kml")
        f = open(filename,"wb")
        f.write(hr.content)
        f.close()

        self.assertEqual(hr.content_type, 'application/vnd.google-earth.kml')

    def test_legend(self):
        '''test a GetLegend request'''
        hr = self.WMS.get_legend(width=100, height=300,
                                        layer=self.test_layer,
                                        format="image/png",
                                        style={'method': 'pcolor','cmap':'hsv','vmin': 10,'vmax': 20})

        filename = os.path.join("tmp","test_colorbar.png")
        f = open(filename,"wb")
        f.write(hr.content)
        f.close()

        self.assertEqual(hr.content_type,"image/png")


    def test_legend_svg(self):
        '''test a GetLegend request'''
        hr = self.WMS.get_legend(width=150, height=50,
                                 layer=self.test_layer,
                                 format="image/svg+xml",
                                 style={'method': 'pcolor','vmin': 10,'vmax': 20})

        filename = os.path.join("tmp","test_colorbar.svg")
        f = open(filename,"wb")
        f.write(hr.content)
        f.close()

        self.assertEqual(hr.content_type,"image/svg+xml")


    def test_legend_log(self):
        '''test a GetLegend request'''
        hr = self.WMS.get_legend(width=150, height=50,
                                 layer=self.test_layer,
                                 format="image/png",
                                 style={'method': 'pcolor','vmin': 1,'vmax': 1e5,'norm': 'log'})

        filename = os.path.join("tmp","test_colorbar_log.png")
        f = open(filename,"wb")
        f.write(hr.content)
        f.close()

        self.assertEqual(hr.content_type,"image/png")

    def test_legend_svg_log(self):
        '''test a GetLegend request'''
        hr = self.WMS.get_legend(width=150, height=50,
                                 layer=self.test_layer,
                                 format="image/svg+xml",
                                 style={'method': 'pcolor','vmin': 1,'vmax': 1e5,'norm': 'log'})

        filename = os.path.join("tmp","test_colorbar_log.svg")
        f = open(filename,"wb")
        f.write(hr.content)
        f.close()

        self.assertEqual(hr.content_type,"image/svg+xml")


    def test_map_contour_style(self):
        '''test a GetMap request with non-default style'''

        hr = self.WMS.request(version="1.1.1", request="GetMap",
                              service="WMS",
                                     srs="EPSG:4326",
                                     width=256, height=256,
                                     layers=self.test_layer,
                                     transparent="TRUE", format="image/png",
                                     bbox="-80.0,-20.0, 20.0, 77.0",
                                     styles="method:contour", time='default')

        filename = os.path.join("tmp","test_contour.png")
        f = open(filename,"wb")
        f.write(hr.content)
        f.close()

        self.assertEqual(hr.content_type,"image/png")

    def test_map_contourf_style(self):
        '''test a GetMap request with filled countours as style'''
        hr = self.WMS.request(version="1.1.1", request="GetMap",
                              service="WMS",
                                     srs="EPSG:4326",
                                     width=256, height=256,
                                     layers=self.test_layer,
                                     transparent="TRUE", format="image/png",
                                     bbox="-80.0,-20.0, 20.0, 77.0",
                                     styles="method:contourf")

        filename = os.path.join("tmp","test_contourf.png")
        f = open(filename,"wb")
        f.write(hr.content)
        f.close()

        self.assertEqual(hr.content_type,"image/png")


    def test_map_contourf_style2(self):
        '''test a GetMap request with filled countours as style'''
        hr = self.WMS.request(LAYERS=self.test_layer, STYLES="method:contour",
                                     TRANSPARENT="true", FORMAT="image/png",
                                     SERVICE="WMS", VERSION="1.1.1",
                                     REQUEST="GetMap",
                                     EXCEPTIONS="application/vnd.ogc.se_xml",
                                     ELEVATION="-0",
                                     SRS="EPSG:4326",
                                     BBOX="-90, 0, 0, 90",
                                     WIDTH="256", HEIGHT="257")

        filename = os.path.join("tmp","test_contour2.png")
        f = open(filename,"wb")
        f.write(hr.content)
        f.close()

        self.assertEqual(hr.content_type,"image/png")

    def test_map_contourf_dec(self):
        '''test a GetMap request with filled countours as style and with
        axes (decorated)'''
        hr = self.WMS.request(LAYERS=self.test_layer, STYLES="method:contourf",
                                     TRANSPARENT="true", FORMAT="image/png",
                                     SERVICE="WMS", VERSION="1.1.1",
                                     REQUEST="GetMap",
                                     EXCEPTIONS="application/vnd.ogc.se_xml",
                                     ELEVATION="-0",
                                     SRS="EPSG:4326",
                                     BBOX="-80,-20, 20, 77", WIDTH="256",
                                     HEIGHT="257", DECORATED="TRUE")

        filename = os.path.join("tmp","test_dec.png")
        f = open(filename,"wb")
        f.write(hr.content)
        f.close()

        self.assertEqual(hr.content_type,"image/png")

    def test_map_contourf_dec_range(self):
        '''test a GetMap request with filled countours as style and with
        axes (decorated)'''
        hr = self.WMS.request(LAYERS=self.test_layer, STYLES="method:contourf+vmin:-10+vmax:20+ncontours:40",
                                     TRANSPARENT="true", FORMAT="image/png",
                                     SERVICE="WMS", VERSION="1.1.1",
                                     REQUEST="GetMap",
                                     EXCEPTIONS="application/vnd.ogc.se_xml",
                                     ELEVATION="-0",
                                     SRS="EPSG:4326",
                                     BBOX="-80,-20, 20, 77", WIDTH="256",
                                     HEIGHT="257", DECORATED="TRUE")

        filename = os.path.join("tmp","test_dec_range.png")
        f = open(filename,"wb")
        f.write(hr.content)
        f.close()

        self.assertEqual(hr.content_type,"image/png")


    def test_map_mercator_decorated(self):
        '''test a GetMap request in Mercator projection as style and with
        axes (decorated)'''

        bbox = "-0.0007999967783689499, 5009377.0840000035, " + \
            "5009377.0840000035, 10018754.168800004"

        hr = self.WMS.request(LAYERS=self.test_layer, STYLES="method:contourf",
                                     TRANSPARENT="true", FORMAT="image/png",
                                     SERVICE="WMS", VERSION="1.1.1",
                                     REQUEST="GetMap",
                                     EXCEPTIONS="application/vnd.ogc.se_xml",
                                     ELEVATION="-0",
                                     SRS="EPSG:54004", BBOX=bbox,
                                     WIDTH="512", HEIGHT="256",
                                     DECORATED="TRUE")

        filename = os.path.join("tmp","test_mercator_dec.png")
        f = open(filename,"wb")
        f.write(hr.content)
        f.close()

        self.assertEqual(hr.content_type,"image/png")

    def test_map_mercator(self):
        '''test a GetMap request in Mercator projection'''

        bbox = "-0.0007999967783689499, 5009377.0840000035, " + \
            "5009377.0840000035, 10018754.168800004"

        hr = self.WMS.request(LAYERS=self.test_layer, STYLES="method:contourf",
                                     TRANSPARENT="true", FORMAT="image/png",
                                     SERVICE="WMS", VERSION="1.1.1",
                                     REQUEST="GetMap",
                                     EXCEPTIONS="application/vnd.ogc.se_xml",
                                     ELEVATION="-0",
                                     SRS="EPSG:54004", BBOX=bbox,
                                     WIDTH="512", HEIGHT="256")

        #print('header',hr.headers)
        self.assertTrue('Last-Modified' in hr.headers)

        filename = os.path.join("tmp","test_mercator.png")
        f = open(filename,"wb")
        f.write(hr.content)
        f.close()

        self.assertEqual(hr.content_type,"image/png")


    def test_continents(self):
        '''test a GetMap request of ContWMS'''

        bbox = "-0.0007999967783689499, 5009377.0840000035, " + \
            "5009377.0840000035, 10018754.168800004"

# http://labs.metacarta.com/wms/vmap0?LAYERS=basic&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&FORMAT=image%2Fjpeg&SRS=EPSG%3A900913&BBOX=-0.0007999967783689499, 5009377.0840000035, 5009377.0840000035, 10018754.168800004&WIDTH=256&HEIGHT=256

        hr = self.ContWMS.request(LAYERS="cont", STYLES="method:contourf",
                                    TRANSPARENT="true", FORMAT="image/png",
                                    SERVICE="WMS", VERSION="1.1.1",
                                    REQUEST="GetMap",
                                    EXCEPTIONS="application/vnd.ogc.se_xml",
                                    ELEVATION="-0",
                                    SRS="EPSG:54004", BBOX=bbox,
                                    WIDTH="256", HEIGHT="256")

        if hasattr(hr.content, 'read'):
            hr.content = hr.content.read()

        filename = os.path.join("tmp","test_cont_mercator.png")
        f = open(filename,"wb")
        f.write(hr.content)
        f.close()

        self.assertEqual(hr.content_type,"image/png")


    def test_continents_notransparent(self):
        '''test a GetMap request of ContWMS'''

        bbox = "-0.0007999967783689499, 5009377.0840000035, " + \
            "5009377.0840000035, 10018754.168800004"

        hr = self.ContWMS.request(LAYERS="cont", STYLES="method:contourf",
                                    TRANSPARENT="false", FORMAT="image/png",
                                    SERVICE="WMS", VERSION="1.1.1",
                                    REQUEST="GetMap",
                                    EXCEPTIONS="application/vnd.ogc.se_xml",
                                    ELEVATION="-0",
                                    SRS="EPSG:54004", BBOX=bbox,
                                    WIDTH="256", HEIGHT="256")

        if hasattr(hr.content, 'read'):
            hr.content = hr.content.read()

        filename = os.path.join("tmp","test_cont_mercator_notransparent.png")
        f = open(filename,"wb")
        f.write(hr.content)
        f.close()

        self.assertEqual(hr.content_type,"image/png")


    def test_map_colormap(self):
        '''test a GetMap request with specified colormap'''

        hr = self.WMS.request(version="1.1.1", request="GetMap",
                              service="WMS",
                                     srs="EPSG:4326",width=256, height=256,
                                     layers=self.test_layer,transparent="TRUE",
                                     format="image/png",
                                     bbox="-80.0,-20.0, 20.0, 77.0",
                                     styles="method:pcolor+cmap:gray")

        filename = os.path.join("tmp","test_colorbar_gray.png")
        f = open(filename,"wb")
        f.write(hr.content)
        f.close()

        self.assertEqual(hr.content_type,"image/png")


    def test_style(self):
        '''test the WMS style class'''

        #print "style['method']"
        style = WMSStyle('method:pcolor')
        style = WMSStyle(style)
        #style = WMSStyle('method:pcolor+Ncontour:10')
        #print "style['method']",style['method']
        self.assertEqual(style['method'],'pcolor')


    def test_fancy_legend(self):
        '''test a fancy GetLegend request'''
        hr = self.WMS.get_legend(width=100, height=300,
                                 layer=self.test_layer,
                                 format="image/png",
                                 decorated=True,
                                 style={'method': 'contour','cmap':'hsv'}
                                 )

        filename = os.path.join("tmp","test_fancy_legend.png")
        f = open(filename,"wb")
        f.write(hr.content)
        f.close()

        self.assertEqual(hr.content_type,"image/png")



class TestWebmapProxy(unittest.TestCase):
    '''unittest for Web Map Server'''

    def setUp(self):
        '''set up test case'''

        self.WMS = proxy_webmapserver('http://ec.oceanbrowser.net/web-vis/Python/web/wms?')

    def test_getcap(self):
        hr = self.WMS.request(version="1.3.0",
                              request="GetCapabilities",
                              SERVICE="WMS")

        self.assertTrue(hr.content_type.startswith("text/xml"))






if __name__ == "__main__":
#    test1 = TestWebmap('test_map_mercator_decorated')()
#    test1 = TestWebmap('test_map_mercator')()
#    test1 = TestWebmap('test_continents')()
#    test1 = TestWebmap('test_cap')()
#    test1 = TestWebmap('test_cap_sub')()
#    TestWebmap('test_fancy_legend')()
#    test1 = TestWebmap('test_map_contour_style')()
#    test1 = TestWebmap('test_cap_valid130')()

#   test1 = TestWebmap('test_cap_cache')()
#   test1 = TestWebmap('testGetStats')()

    unittest.main()
