import netCDF4
import re

def find_coord(nc,nv_name,nv,coord):
    common_dimnames = {'longitude': [re.compile('.*lon.*'),re.compile('^x$')],
                       'latitude': [re.compile('.*lat.*'),re.compile('^y$')],
                       'depth': [re.compile('.*depth.*'),re.compile('^z$')],
                       'time': [re.compile('.*time.*'),re.compile('^t$')] }
    common_units = {'longitude': [re.compile('.*east.*')],
                    'latitude': [re.compile('.*north.*')],
                    'depth': [re.compile('^m$')],
                    'time': [re.compile('.*year.*'), re.compile('.*month.*'), re.compile('.*day.*'), 
                             re.compile('.*minute.*'), re.compile('.*second.*')]}

    dim =  set(nv.dimensions)
    
    # all dimensions of ncv must also be a dimension of nv
    candidates = [(nvc_name,nvc) for (nvc_name,nvc) in nc.variables.items()
                  if nvc_name != nv_name and len(set(nvc.dimensions) - dim) == 0]

    # test based on dimension name
    for (nvc_name,nvc) in candidates:
        for r in common_dimnames[coord]:
            if r.match(nvc_name):
                return nvc_name

    # test based on units
    for (nvc_name,nvc) in candidates:
        if hasattr(nvc,'units'):
            unit = nvc.units
            for r in common_units[coord]:
                if r.match(unit):
                    return nvc_name

    # not found
    return None
        


nc =  netCDF4.Dataset('/home/abarth/Desktop/test.nc','r')
nv_name = 'DT_sst_skin'
nv_name = 'analysed_sst_climatology'

#nc =  netCDF4.Dataset('/home/abarth/tmp/LigurianSea/Output32-cosmo-short-mpi42-dt150-diff0-visc0/ocean_his.nc','r')
#nv_name = 'salt'

nv = nc.variables[nv_name]


for coord in ['longitude','latitude','depth','time']:
    nvc_name = find_coord(nc,nv_name,nv,coord)
    print  coord,':',nvc_name
    
#            print nvc_name,nvc.dimensions
