

git checkout in:

~/workspace/OceanBrowser/emodnet-test/log/divaonweb

Inside container

```bash
cd /var/log/divaonweb
pip install -e .
```

To reload new code

```bash
touch /var/oceanbrowser/uwsgi.ini 
```

print statements to go `uwsgi.log`

```bash
~/workspace/OceanBrowser/emodnet-test/log/uwsgi $ tail -f uwsgi.log
```

TLS certificates:

```bash
certbot -d sdn2.oceanbrowser.net --agree-tos --apache
```