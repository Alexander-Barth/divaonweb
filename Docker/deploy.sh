#!/bin/bash
# exit on first error
set -e

(cd ~/workspace/divaonweb; svn up)

./prep_images.sh

if [ "$HOSTNAME" == "ogs01" ]; then
    ./run.sh -s emodnet
else

  for server in emodnet gher seadatanet; do
    ./run.sh -s $server
  done

fi
