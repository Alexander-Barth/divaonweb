TAG=$(git rev-parse --short HEAD)
TAG=test
DOCKER=docker



for SERVER in gher seadatanet emodnet; do
    $DOCKER stop oceanbrowser-$SERVER-container
    $DOCKER rm oceanbrowser-$SERVER-container
    echo "hit control-C"
    make SERVER=$SERVER TAG=$TAG run &
    PID=$!
    sleep 5
    kill -INT $PID
    $DOCKER start oceanbrowser-$SERVER-container    
done

