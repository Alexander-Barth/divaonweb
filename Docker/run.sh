#!/bin/bash

SERVER=emodnet
TAG=latest
NAME=oceanbrowser
INTERACTIVE=off

while getopts ":s:t:ih" opt; do
  case $opt in
    s)
      SERVER=$OPTARG
      ;;
    t)
      TAG=$OPTARG
      ;;
    i)
      INTERACTIVE=on
      ;;
    h)
      echo "run.sh -s [seadatanet|emodnet|gher|emodnet-test|gher-test] -t [latest|4353]"
      ;;
    \?)
      echo "Invalid option: -$OPTARG"
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument."
      exit 1
      ;;
  esac
done

DOCKER=docker
DOCKER_OPTION=''

OCEANBROWSER_STATIC_WMS_ROOT='/var/www/data'
OCEANBROWSER_WMS_CONTACT_PERSON='Alexander Barth'
OCEANBROWSER_WMS_EMAIL='a dot barth at ulg dot ac dot be'
OCEANBROWSER_WMS_CONTACT_ORGANIZATION="GHER, ULg, Belgium"
OCEANBROWSER_EMAIL_FROMADDR='diva.on.web@gmail.com'
OCEANBROWSER_EMAIL_LOGIN='diva.on.web@gmail.com'
OCEANBROWSER_EMAIL_PASSWORD='ghergher'
OCEANBROWSER_EMAIL_TOADDRS='a.barth@ulg.ac.be'
OCEANBROWSER_EMAIL_SUBJECT='[diva-on-web] bug report'

LOGDIR=/home/abarth/workspace/OceanBrowser/$SERVER/log
TMPDIR=/home/abarth/workspace/OceanBrowser/$SERVER/tmp
OBSDIR=/home/abarth/workspace/OceanBrowser/$SERVER/obs
DIVADATADIR=/home/abarth/Data/DivaData




if [ "$SERVER" == "seadatanet" ]; then
    PROJECT=SeaDataNet

    PORT=8001
    OCEANBROWSER_TITLE="SeaDataNet products"
    OCEANBROWSER_URL="http://www.seadatanet.org/"
    OCEANBROWSER_CONTACT_URL="https://www.seadatanet.org/sendform/contact"
    OCEANBROWSER_SUBTITLE="Viewing and Downloading service"
    OCEANBROWSER_THEME="seadatanet"
    OCEANBROWSER_WMS_TITLE="SeaDataNet Map Server"
    OCEANBROWSER_WMS_ABSTRACT="Map Server maintained by the GHER showing ocean climatologies."
    OCEANBROWSER_WMS_KEYWORDS="ocean climatology diva SeaDataNet"

    OCEANBROWSER_WMS_URL="http://gher-diva.phys.ulg.ac.be/web-vis/Python/web/wms?"
    OCEANBROWSER_OPENDAP_BASEURL="http://gher-diva.phys.ulg.ac.be:8080/SeaDataNet-domains/"
    OCEANBROWSER_HTTP_BASEURL="http://gher-diva.phys.ulg.ac.be/data/SeaDataNet-domains/"
fi

if [ "$SERVER" == "emodnet" ] || [ "$SERVER" == "emodnet-test" ] || [ "$SERVER" == "emodnet-combined" ]  || [ "$SERVER" == "emodnet-projects" ]; then
    PROJECT=emodnet
    if [ "$SERVER" == "emodnet" ]; then
       PORT=8002
       OCEANBROWSER_WMS_TITLE="EMODNET Chemistry - Eutrophication by Sea Region"
       OCEANBROWSER_WMS_INSPIRE_METADATA_URL="https://sextant.ifremer.fr/geonetwork/srv/eng/csw?request=GetRecordById&elementSetName=full&service=CSW&version=2.0.2&id=92388888ad7bdf6f9f3f308dd0e79a74f133d25f&OutputSchema=http://www.isotc211.org/2005/gmd"
    elif [ "$SERVER" == "emodnet-combined" ]; then
       #PORT=8004
       PORT=8006
       OCEANBROWSER_WMS_TITLE="EMODNET Chemistry - Eutrophication"
    elif [ "$SERVER" == "emodnet-projects" ]; then
       PORT=8014
       OCEANBROWSER_WMS_TITLE="DIVA maps"
       OCEANBROWSER_WMS_INSPIRE_METADATA_URL="https://sextant.ifremer.fr/geonetwork/srv/eng/csw?request=GetRecordById&elementSetName=full&service=CSW&version=2.0.2&id=60889f3e2e761dd7f0e0a81223343a059c1cfe93&OutputSchema=http://www.isotc211.org/2005/gmd"
    else
       #PORT=8004
       #PORT=8005
       # TEMPORARY (mount point /test-1)
	PORT=8007
	OCEANBROWSER_WMS_TITLE="EMODNET Chemistry - Eutrophication"
       OCEANBROWSER_WMS_INSPIRE_METADATA_URL="https://sextant.ifremer.fr/geonetwork/srv/eng/csw?request=GetRecordById&elementSetName=full&service=CSW&version=2.0.2&id=92388888ad7bdf6f9f3f308dd0e79a74f133d25f&OutputSchema=http://www.isotc211.org/2005/gmd"
    fi

    OCEANBROWSER_TITLE="Chemistry"
    OCEANBROWSER_URL="https://www.emodnet-chemistry.eu/"
    OCEANBROWSER_CONTACT_URL="https://www.emodnet-chemistry.eu/help/contact"
    OCEANBROWSER_SUBTITLE="Viewing and Downloading service"
    OCEANBROWSER_THEME="emodnet-chemical"
    OCEANBROWSER_WMS_ABSTRACT="EMODnet (Chemical data) Map Server with ocean climatologies."
    OCEANBROWSER_WMS_KEYWORDS="ocean climatology diva SeaDataNet EMODnet Chemical"
#    OCEANBROWSER_OBS_INDEX="/var/www/obs/obs_index.txt"
#    OCEANBROWSER_OBS_DBFILE="/var/www/obs/emodnet-chemistry.sqlite"
#    OCEANBROWSER_OBS_GROUPSNAME="/var/www/obs/groups-id.json"
#    OCEANBROWSER_OBS_WMS_TITLE="EMODNET Chemistry - Static Plots"


    # if [ "$SERVER" == "emodnet" ]; then
    # 	OCEANBROWSER_WMS_URL="http://gher-diva.phys.ulg.ac.be/emodnet/Python/web/wms?"
    # else
    # 	OCEANBROWSER_WMS_URL="http://gher-diva.phys.ulg.ac.be/emodnet-combined/Python/web/wms?"
    # fi

    # OCEANBROWSER_OPENDAP_BASEURL="http://gher-diva.phys.ulg.ac.be:8080/emodnet-domains/"
    # OCEANBROWSER_HTTP_BASEURL="http://gher-diva.phys.ulg.ac.be/data/emodnet-domains/"

fi

# if [ "$SERVER" == "gher" ] || [ "$SERVER" == "gher-test" ]; then
#     PROJECT=GHER

#     if [ "$SERVER" == "gher" ]; then
#        PORT=8003
#        OCEANBROWSER_WMS_URL="http://gher-diva.phys.ulg.ac.be/gher-data/Python/web/wms?"
#     else
#        PORT=8103
#        OCEANBROWSER_WMS_URL="http://gher-diva.phys.ulg.ac.be/gher-data-test/Python/web/wms?"
#     fi

#     OCEANBROWSER_TITLE="OceanBrowser - GHER, ULg"
#     OCEANBROWSER_URL="http://modb.oce.ulg.ac.be/mediawiki/index.php/GeoHydrodynamics_and_Environment_Research"
#     OCEANBROWSER_SUBTITLE="Viewing and Downloading service"
#     OCEANBROWSER_THEME="default"
#     OCEANBROWSER_WMS_TITLE="GHER, ULg"
#     OCEANBROWSER_WMS_ABSTRACT="Map Server maintained by the GHER showing ocean climatologies."
#     OCEANBROWSER_WMS_KEYWORDS="ocean climatology diva"

#     OCEANBROWSER_OPENDAP_BASEURL="http://gher-diva.phys.ulg.ac.be:8080/GHER/"
#     OCEANBROWSER_HTTP_BASEURL="http://gher-diva.phys.ulg.ac.be/data/GHER/"
# fi

DATADIR=/var/www/data/$PROJECT-domains

    DATADIR=/production/apache/data/$SERVER-domains
    TMPDIR=/production/OceanBrowser/$SERVER/tmp
    OBSDIR=/production/OceanBrowser/$SERVER/obs
    LOGDIR=/production/OceanBrowser/$SERVER/log
    DIVADATADIR=/production/OceanBrowser/$SERVER/DivaData

    if [ "$SERVER" == "seadatanet" ]; then
	DATADIR=/production/apache/data/SeaDataNet-domains
	OCEANBROWSER_WMS_URL="http://sdn.oceanbrowser.net/web-vis/Python/web/wms?"
	OCEANBROWSER_HTTP_BASEURL="http://sdn.oceanbrowser.net/data/SeaDataNet-domains/"
	OCEANBROWSER_OPENDAP_BASEURL="http://opendap.oceanbrowser.net/thredds/dodsC/data/SeaDataNet-domains/"
        OCEANBROWSER_CATALOGUE_URL='https://sextant.ifremer.fr/en/web/seadatanet/catalogue#/metadata/$(ID)'
    elif [ "$SERVER" == "emodnet" ]; then
	DATADIR=/production/apache/data/emodnet-domains
	OCEANBROWSER_WMS_URL="http://ec.oceanbrowser.net/emodnet/Python/web/wms?"
	OCEANBROWSER_HTTP_BASEURL="http://ec.oceanbrowser.net/data/emodnet-domains/"
	OCEANBROWSER_OPENDAP_BASEURL="http://opendap.oceanbrowser.net/thredds/dodsC/data/emodnet-domains/"
    elif [ "$SERVER" == "emodnet-projects" ]; then
	DATADIR=/production/apache/data/emodnet-projects
	OCEANBROWSER_WMS_URL="http://ec.oceanbrowser.net/emodnet-projects/Python/web/wms?"
	OCEANBROWSER_HTTP_BASEURL="http://ec.oceanbrowser.net/data/emodnet-projects/"
	OCEANBROWSER_OPENDAP_BASEURL="http://opendap.oceanbrowser.net/thredds/dodsC/data/emodnet-projects/"
    elif [ "$SERVER" == "emodnet-test" ]; then
	DATADIR=/production/apache/data/emodnet-domains
	OCEANBROWSER_WMS_URL="http://ec.oceanbrowser.net/test-1/Python/web/wms?"
	###OCEANBROWSER_WMS_URL="http://ec.oceanbrowser.net/oceanbrowser/Python/web/wms?"
	OCEANBROWSER_HTTP_BASEURL="http://ec.oceanbrowser.net/data/emodnet-domains/"
	OCEANBROWSER_OPENDAP_BASEURL="http://opendap.oceanbrowser.net/thredds/dodsC/data/emodnet-domains/"
    else
#	if [ "$HOSTNAME" == "gher-diva" ]; then	
#	    OCEANBROWSER_WMS_URL="http://gher-diva.phys.ulg.ac.be/$PROJECT/Python/web/wms?"
#	    echo OCEANBROWSER_WMS_URL: $OCEANBROWSER_WMS_URL
#	else
#	    OCEANBROWSER_WMS_URL="http://ec.oceanbrowser.net/$PROJECT/Python/web/wms?"
#	fi
        OCEANBROWSER_WMS_URL="http://ec.oceanbrowser.net/$PROJECT/Python/web/wms?"
	OCEANBROWSER_HTTP_BASEURL="http://ec.oceanbrowser.net/data/$PROJECT-domains/"
	OCEANBROWSER_OPENDAP_BASEURL="http://opendap.oceanbrowser.net/thredds/dodsC/data/$PROJECT-domains/"
    fi

    DOCKER_OPTION='--restart=always'

    if [ "$HOSTNAME" == "gher-diva" ]; then
	echo "replace ec by ec2, sdn by sdn2"
	OCEANBROWSER_WMS_URL=$(echo $OCEANBROWSER_WMS_URL | sed 's/ec\./ec2./g' | sed 's/sdn\./sdn2./g')
	OCEANBROWSER_HTTP_BASEURL=$(echo $OCEANBROWSER_HTTP_BASEURL | sed 's/ec\./ec2./g' | sed 's/sdn\./sdn2./g')
	echo OCEANBROWSER_WMS_URL: $OCEANBROWSER_WMS_URL
	echo OCEANBROWSER_HTTP_BASEURL: $OCEANBROWSER_HTTP_BASEURL
    fi

echo OCEANBROWSER_WMS_URL: $OCEANBROWSER_WMS_URL
echo OCEANBROWSER_HTTP_BASEURL: $OCEANBROWSER_HTTP_BASEURL
echo OCEANBROWSER_OPENDAP_BASEURL: $OCEANBROWSER_OPENDAP_BASEURL

# make directories if necessary
mkdir -p $LOGDIR/apache2
mkdir -p $LOGDIR/supervisor
mkdir -p $LOGDIR/nginx
mkdir -p $LOGDIR/uwsgi

$DOCKER stop oceanbrowser-$SERVER-container
$DOCKER rm oceanbrowser-$SERVER-container


DOCKER_OPTION="$DOCKER_OPTION --memory=4G"

# generate environment file

# ( set -o posix ; set ) list all env. variables (even not exported ones)
# http://askubuntu.com/questions/275965/how-to-list-all-variables-names-and-their-current-values
# it is necessary to remove single quotes e.g.
# var='some text'
# should become
# var=some text

( set -o posix ; set ) | grep '^OCEANBROWSER' | sed -e "s/='\(.*\)'/=\1/" > env-$SERVER-$HOSTNAME.list

if [ "$INTERACTIVE" == "off" ]; then

$DOCKER run -p $PORT:80 --name oceanbrowser-$SERVER-container \
    --env-file env-$SERVER-$HOSTNAME.list \
    $DOCKER_OPTION \
    -v $DATADIR:/var/www/data:ro \
    -v $TMPDIR:/var/oceanbrowser/tmp \
    -v $OBSDIR:/var/www/obs:ro \
    -v $DIVADATADIR:/var/oceanbrowser/DivaData/:ro \
    -v $LOGDIR:/var/log \
    --detach=true abarth/$NAME:$TAG
else
$DOCKER run -p $PORT:80 --name oceanbrowser-$SERVER-container \
    --env-file env-$SERVER-$HOSTNAME.list \
    $DOCKER_OPTION \
    -v $DATADIR:/var/www/data:ro \
    -v $TMPDIR:/var/oceanbrowser/tmp \
    -v $OBSDIR:/var/www/obs:ro \
    -v $DIVADATADIR:/var/oceanbrowser/DivaData/:ro \
    -v $LOGDIR:/var/log \
    -it --entrypoint=/bin/bash  \
    abarth/$NAME:$TAG -i
fi
