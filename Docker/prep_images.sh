#!/bin/bash

TAG=$(git rev-parse --short HEAD)
#TAG=test
DOCKER=docker
NAME=oceanbrowser

$DOCKER rmi abarth/$NAME:latest

(cd ..;
$DOCKER build -t abarth/$NAME:$TAG . | tee build.log
id=$($DOCKER images | awk "/$NAME *$TAG/ { print \$3}" | tail -n 1)
$DOCKER tag $id abarth/$NAME:latest	
)

