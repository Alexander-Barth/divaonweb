cd $HOME/src/tmp
rm -Rf diva diva-2009-01-08.tar.gz
svn export -r '{2009-01-08}' svn+ssh://modb.oce.ulg.ac.be/home/svn/repos/diva/trunk diva
tar -czf diva-2009-01-08.tar.gz diva

scp $HOME/src/tmp/diva-2009-01-08.tar.gz  modb:/var/lib/mediawiki/upload/Alex/DIVA
