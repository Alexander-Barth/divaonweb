from setuptools import setup, find_packages
import sys, os

version = '1.9.0'

setup(name='divaonweb',
      version=version,
      description="Gridding and visualizing of geophysical datasets",
      long_description="""\
Gridding and visualizing of geophysical datasets
""",
      classifiers=['Development Status :: 4 - Beta',
                   'Environment :: Console',
                   'Environment :: Web Environment',
                   'Framework :: Paste',
                   'Intended Audience :: Developers',
                   'Intended Audience :: Science/Research',
                   'License :: OSI Approved :: GNU Affero General Public License v3',
                   'Topic :: Scientific/Engineering :: Visualization'],
      keywords='gridding',
      author='Alexander Barth',
      author_email='a.barth@ulg.ac.be',
      url='http://modb.oce.ulg.ac.be/mediawiki/index.php/Diva_on_web',
      license='AGPL',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
       'scipy>=0.6.0',
       'matplotlib',
#        'basemap',
        'netCDF4>=0.7.7',
        'numpy>=1.0.4',
        'httplib2>=0.4.0',
        'Paste',
        'PasteScript',
        'PasteDeploy',
        'Pillow',
        'lxml',
#        'urllib2',
      ],
      entry_points="""
      # -*- Entry points: -*-

      [paste.app_factory]
      main = divaonweb.wsgi:make_app
      wsgi = divaonweb.wsgi:make_app

      [paste.paster_create_template]
      divaonweb_server = divaonweb.templates:DivaServerTemplate
      """,
      )
