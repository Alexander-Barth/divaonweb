from __future__ import print_function
import numpy
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import xml.etree.ElementTree as ET


namespaces = {'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
              'skos': 'http://www.w3.org/2004/02/skos/core#',
              'dc': 'http://purl.org/dc/terms/',
              'rdfs': 'http://www.w3.org/2000/01/rdf-schema#',
              'grg': 'http://www.isotc211.org/schemas/grg/',
              'owl': 'http://www.w3.org/2002/07/owl#',
              'void': 'http://rdfs.org/ns/void#',
              'gmx': 'http://www.isotc211.org/2005/gmx',
              'gml': 'http://www.opengis.net/gml'
}


# get data with
#curl 'http://emodnet02.cineca.it/geoserver/emodnet/ows' -H 'Origin: http://oceanbrowser.net' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.8,de;q=0.6,fr;q=0.4,nl;q=0.2' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.99 Safari/537.36' -H 'Content-Type: application/xml' -H 'Accept: */*' -H 'Referer: http://oceanbrowser.net/emodnet/' -H 'X-Requested-With: XMLHttpRequest' -H 'Connection: keep-alive' --data-binary '<wfs:GetFeature xmlns:wfs="http://www.opengis.net/wfs" service="WFS" version="1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.1.0/wfs.xsd"><wfs:Query typeName="feature:observed_cindex" srsName="EPSG:4326" xmlns:feature="emodnet"><ogc:Filter xmlns:ogc="http://www.opengis.net/ogc"><ogc:And><ogc:And><ogc:Or><ogc:PropertyIsEqualTo matchCase="true"><ogc:PropertyName>p35_id</ogc:PropertyName><ogc:Literal>EPC00006</ogc:Literal></ogc:PropertyIsEqualTo></ogc:Or><ogc:PropertyIsBetween><ogc:PropertyName>z</ogc:PropertyName><ogc:LowerBoundary><ogc:Literal>0</ogc:Literal></ogc:LowerBoundary><ogc:UpperBoundary><ogc:Literal>10</ogc:Literal></ogc:UpperBoundary></ogc:PropertyIsBetween><ogc:PropertyIsBetween><ogc:PropertyName>datetime</ogc:PropertyName><ogc:LowerBoundary><ogc:Literal>2000-01-01T00:00:00.000Z</ogc:Literal></ogc:LowerBoundary><ogc:UpperBoundary><ogc:Literal>2000-12-31T00:00:00.000Z</ogc:Literal></ogc:UpperBoundary></ogc:PropertyIsBetween></ogc:And></ogc:And></ogc:Filter></wfs:Query></wfs:GetFeature>' --compressed > out

tree = ET.parse('out');
a = tree.findall('gml:pos',namespaces)
XY = [[float(coord) for coord in pos.text.split(' ')] for pos in tree.findall('.//gml:pos',namespaces)]
XY
lon = [_[0] for _ in XY];
lat = [_[1] for _ in XY];

print('lon range',min(lon),max(lon))
print('lat range',min(lat),max(lat))


m = Basemap(projection='merc',llcrnrlon=-20, llcrnrlat=25,urcrnrlon=45, urcrnrlat=70)

m.drawmapboundary(fill_color='white')
m.fillcontinents(color='grey',lake_color='white')
m.drawcoastlines()


x1,y1=m(lon,lat)

m.scatter(x1, y1, s=10, c='blue')

#m.fillcontinents(color='white',lake_color='black',zorder=0)
plt.savefig('wfs_distribution.png')
#plt.show()
