from __future__ import print_function
import sys
import xml.etree.ElementTree as ET
import json
import requests
from divaonweb import vocab
from divaonweb import WPSwebmapserver

usedparams_url = 'http://localhost/test/p35_used.json'

variables = WPSwebmapserver.list_used_params(usedparams_url)
    
print(json.dumps(variables, sort_keys=True,
                 indent=4, separators=(',', ': ')))

    

    
