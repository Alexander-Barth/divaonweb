#!/usr/bin/python

from xml.etree.ElementTree import ElementTree, fromstring

s = """
<div>
      <p class="instruction">Choose either a server from the server list or enter the URL of a WMS server directly.</p>
      <table>
	<tbody>
	  <tr>
	    <td>
	      Server:
	    </td>
	    <td>
	      <select id="IDID_wms_server">
	      </select>
	    </td>
	  </tr>
	  <tr>
	    <td>
	      WMS URL:
	    </td>
	    <td>
	      <input type="text" size="40" id="IDID_wms_url" /> 
	    </td>
	  </tr>
	  <tr>
	    <td>
	      WMS version:
	    </td>
	    <td>
	      <select id="IDID_wms_version">
		<option>1.3.0</option>
		<option>1.1.1</option>
	      </select>
	    </td>
	  </tr>
	</tbody>
      </table>
</div>
"""

#s = """
#      <label>
#<input type="radio" id="ID_horizontal_section" name="section_type" value="horizontal" checked="checked" />
#Horizontal
#</label>
#"""

# s = """
#     <div>
#       <h3>Data:</h3>
# </div>
# """

#tree = ElementTree()
#tree.parse("index.xhtml")

indentinc = 4

def todomstr(elem,ni=0):
    indent = (' ' * ni)
    stri = indent

    cs = []


    for c in elem.getchildren():
        cs.append(todomstr(c,ni+indentinc))

    if elem.text:
        text = elem.text.strip()
        if text:
            cs.append((' ' * (ni+indentinc)) + "'%s'" % text)

    if len(cs) > 0:
        csstr = ',\n'.join(cs)
        stri += """dom('%s',%s,[\n%s""" % (elem.tag,elem.attrib,csstr)
        stri += '\n' + indent + "])""" 
    else:
        if elem.attrib:
            stri += "dom('%s',%s)" % (elem.tag,elem.attrib)
        else:
            stri += "dom('%s')" % (elem.tag)

    return stri


def todomstr_jq(elem,ni=0):
    indent = (' ' * ni)
    stri = indent

    cs = []

    for c in elem:
        #print 'c',c
        cs.append(todomstr_jq(c,ni+indentinc))

    #print "elem.tag ",elem.tag,elem.text,elem.tail,len(elem)
    if len(elem) > 0:
        if elem[-1].tail:
            text = elem[-1].tail.strip()
            if len(text) > 0:
                cs.append((' ' * (ni+indentinc)) + "'%s'" % text)


    if elem.text:
        text = elem.text.strip()
        #print "text ",text
        if text:
            cs.append((' ' * (ni+indentinc)) + "'%s'" % text)

    #print 'cs ',cs
    stri += "$('<%s/>')" % (elem.tag,)

    if elem.attrib:
        stri += ".attr(%s)" % (elem.attrib,)

    if len(cs) > 0:
        csstr = ',\n'.join(cs)

        stri += """.append(\n%s""" % (csstr,)
        stri += '\n' + indent + ")""" 

    return stri
    

tree = fromstring(s)

print todomstr_jq(tree,15)
#print todomstr(tree,15)

#for elem in 

