import csv
import sys
import json

def uniq(seq):
    seen = set()
    seen_add = seen.add
    return [ x for x in seq if x not in seen and not seen_add(x)]

def convert(filename):

    spamReader = csv.reader(open(filename, 'r'))

    headers = spamReader.next()
    data = list(spamReader)

    p021 = uniq([l[0] for l in data])

    groups = []

    for c in p021:
        title = [l[1] for l in data if l[0] == c][0]
        P011 = ['SDN:P011::' + l[5] for l in data if l[0] == c]

        groups.append([c,title,P011])

    return groups

    

if __name__ == '__main__':

#p011_p021_list.csv
    groups = convert(sys.argv[1])

    print "number of groups ",len(groups)
    outfile = sys.argv[2]
    f = open(outfile,'w')
    f.write(json.dumps(groups,sort_keys=True, indent=4))
    f.close()

