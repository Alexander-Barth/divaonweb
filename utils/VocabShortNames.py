#!/usr/bin/python
import urllib2
import xml.dom.minidom
import sys
from divaonweb.vocab import *

# http://nodc.ogs.trieste.it/wikiemodnet/index.php/P011_short_titles

url = 'http://nodc.ogs.trieste.it/wikiemodnet/index.php/Special:Export/P011_short_titles'
username = 'emduser'
password = '91L07G207'


def parseTable(out):
    dom = xml.dom.minidom.parseString(out)
    text = dom.getElementsByTagName("text")[0].firstChild.data

    # get indexes of first table
    i0 = text.find("{|")
    i1 = text.find("|}",i0)
    shortnames = {}

    for row in text[i0+2:i1].split('|-'):
        if '|' in row:
            cells = row.split('|')
            code = cells[1].strip();
            name = cells[2].strip();

            print '%s:%s ' % (code,name)

            shortnames[code] = name

    return shortnames

def loadTable(url,username,password):
    # http://docs.python.org/howto/urllib2.html
    
    # create a password manager
    password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()

    # Add the username and password.
    # If we knew the realm, we could use it instead of None.
    #top_level_url = "http://nodc.ogs.trieste.it"
    top_level_url = url
    password_mgr.add_password(None, top_level_url, username, password)

    handler = urllib2.HTTPBasicAuthHandler(password_mgr)

    # create "opener" (OpenerDirector instance)
    opener = urllib2.build_opener(handler)

    # use the opener to fetch a URL
    opener.open(url)

    # Install the opener.
    # Now all calls to urllib2.urlopen use our opener.
    urllib2.install_opener(opener)

    f = urllib2.urlopen(url)
    out = f.read()
    f.close()
    
    return out

def main():
    out = loadTable(url,username,password)
    tab = parseTable(out)
    #print tab


if __name__ == "__main__":
    main()


