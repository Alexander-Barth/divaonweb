import urllib
import netCDF4
import shlex
import os
import re
import numpy

try:
    from matplotlib.toolkits.basemap import Basemap
except ImportError:
    from mpl_toolkits.basemap import Basemap


def get_slice(hyperslab):
    """
    Parse a hyperslab into a Python tuple of slices.

    """
    output = []
    dimslices = [ds for ds in hyperslab[1:-1].split('][') if ds]
    for dimslice in dimslices:
        tokens = dimslice.split(':')
        start = int(tokens[0])
        step = 1
        stop = start
        if len(tokens) == 2:
            stop = int(tokens[1])
        elif len(tokens) == 3:
            step = int(tokens[1])
            stop = int(tokens[2])
        output.append(slice(start, stop+1, step))
    return tuple(output)

def parse_qs(query):
    """
    Parse the constraint expression.

    """
    projection = []
    selection = [urllib.unquote(token) for token in query.split('&')]
    if selection and not re.search('<=|>=|!=|=~|>|<|=', selection[0]): 
        projection = [p for p in selection.pop(0).split(',') if p]

    fields = []
    for var in projection:
        tokens = var.split('.')
        tokens = [re.match('(.*?)(\[.*\])?$', token).groups() for token in tokens]
        tokens = [(token, get_slice(slice_ or '')) for (token, slice_) in tokens]
        fields.append(tokens)

    return fields, selection


def parse_logfile(logfile,root):
    f = open(logfile,'r')

    for line in f:
        token = shlex.split(line)

        if len(token) > 1 and token[0] == 'WARNING:pydap:access':
            type,date,time,ip,path,query,useragent = token

            path = path.replace('.dods','')
            path = path.replace('.ascii','')
            filename = root + path

            q = parse_qs(query)
            fields, queries = parse_qs(query)
            #print 'fields',query,fields

            nc = netCDF4.Dataset(filename)
            title = nc.title

            for var in fields:
                while var:
                    name, slice_ = var.pop(0)
                    #print 'before if ',name,slice_, nc.variables[name].dimensions
                    
                    if name not in nc.variables:
                        print "not found",name
                        continue

                    if (name in nc.dimensions or
                            not nc.variables[name].dimensions):
                        #print 'dim or scalar',name
                        pass
                    elif var:
                        #print "do nothing"
                        pass
                    else:
                        # name of dimensions
                        xname = nc.variables[name].dimensions[-1]
                        yname = nc.variables[name].dimensions[-2]
                        #print 'get',name,slice_,xname,yname

                        if ('east' in nc.variables[xname].units and
                            'north' in nc.variables[yname].units):

                            try:
                                if len(slice_) >= 2:
                                    x = nc.variables[xname][slice_[-1]]
                                    y = nc.variables[yname][slice_[-2]]
                                    slice2 = (slice_[-2],slice_[-1])
                                else:
                                    x = nc.variables[xname][:]
                                    y = nc.variables[yname][:]
                                    slice2 = (slice(0,len(y)+1),
                                              slice(0,len(x)+1))


                                bbox = (min(x),max(x),min(y),max(y))
                                #print "slice2 ",slice2
                                yield date,time,ip,query,useragent,filename,title,path.replace('.dods',''),bbox,slice2
                            except:
                                pass


            nc.close()







    f.close()


def main():
    logfile = '/var/pydap3.0.rc.5/log/server.log'
    root = '/var/www/data'
    outdir = './'

    import getopt, sys

    try:
        opts, args = getopt.getopt(sys.argv[1:], "l:o:h", ["help", "outdir=","logfile="])
    except getopt.GetoptError, err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)

    outputfile = None
    inputfile = None

    for o, a in opts:
        if o in ("-l","--logfile"):
            logfile = a
        elif o in ("-o", "--outdir"):
            outdir = a
        elif o in ("-i", "--input"):
            inputfile = a
        elif o in ("-h", "--help"):
            usage()
            sys.exit()
        else:
            assert False, "unhandled option"

    agg = {}

    for p in parse_logfile(logfile,root):
        date,time,ip,query,useragent,filename,title,path,bbox,slice = p

        if path not in agg:
            agg[path] = []

        agg[path].append({"bbox": bbox, "slice": slice})

    #print agg

    genrep(plot2(root,agg,outdir),outdir)




def plot(root,agg,outdir):
    import matplotlib
    import matplotlib.pyplot as plt
    from matplotlib.patches import Polygon

    def draw_screen_poly( lons, lats, m,ax):
        x, y = m( lons, lats )
        xy = zip(x,y)
        poly = Polygon( xy, facecolor='blue', alpha=0.2)
        ax.add_patch(poly)


    res = 'h'
        
    for filename in agg:

        rfname = filename
        if rfname[0] == '/':
            rfname = rfname[1:]

        fig = plt.figure()

        fig.hold(True)
        ax = fig.add_subplot(111)
        title = filename.replace('/','>')
        title = title.replace('.nc','')
        #print title,filename
        #ax.set_title(title)

        ffname = os.path.join(root,rfname)
        #print "filename ",ffname
        nc = netCDF4.Dataset(ffname)
        x = nc.variables['lon'][:]
        y = nc.variables['lat'][:]

        count = numpy.zeros((len(y),len(x)))

        
        m = Basemap(llcrnrlon=x[0],llcrnrlat=y[0],urcrnrlon=x[-1],urcrnrlat=y[-1],resolution=res,suppress_ticks=False)
        
        m.drawcoastlines(color='0.8')
        m.fillcontinents()

        #plt.show()
        for bbox in agg[filename]:
            #print 'bbox',bbox,bbox['slice'][1]
            x = bbox['bbox'][0:2]
            y = bbox['bbox'][2:4]


            downloaded = numpy.zeros((len(y),len(x)))
            downloaded[bbox['slice'][0],bbox['slice'][1]] = 1

            draw_screen_poly([x[0],x[-1],x[-1],x[0],x[0]],[y[0],y[0],y[-1],y[-1],y[0]],m,ax)
            h = m.plot([x[0],x[-1],x[-1],x[0],x[0]],[y[0],y[0],y[-1],y[-1],y[0]],'b',linewidth=2,alpha=.3)

        
        outname = filename
        if outname[0] == '/':
            outname = outname[1:]
        outname = outname.replace('/','_')
        outname = outname.replace('.nc','.png')
        #print outname
        plt.savefig(os.path.join(outdir,outname))




        yield {"title": title,"image": outname, "count": len(agg[filename])}




def plot2(root,agg,outdir):
    import matplotlib
    import matplotlib.pyplot as plt
    from matplotlib.patches import Polygon


    res = 'h'
        
    for filename in agg:

        rfname = filename
        if rfname[0] == '/':
            rfname = rfname[1:]

        fig = plt.figure()

        fig.hold(True)
        ax = fig.add_subplot(111)
        title = filename.replace('/','>')
        title = title.replace('.nc','')
        #print title,filename
        ax.set_title('Number of downloads')

        ffname = os.path.join(root,rfname)
        #print "filename ",ffname
        nc = netCDF4.Dataset(ffname)
        gx = nc.variables['lon'][:]
        gy = nc.variables['lat'][:]

        count = numpy.zeros((len(gy),len(gx)))

        #print "gx ",gx,gy
        m = Basemap(llcrnrlon=gx[0],llcrnrlat=max(gy[0],-90),
                    urcrnrlon=gx[-1],urcrnrlat=min(gy[-1],90),resolution=res,suppress_ticks=False)
        
        m.drawcoastlines(color='0.8')
        m.fillcontinents()

        #plt.show()
        for bbox in agg[filename]:
            #print 'bbox',bbox,bbox['slice'][1]
            x = bbox['bbox'][0:2]
            y = bbox['bbox'][2:4]


            downloaded = numpy.zeros((len(gy),len(gx)))
            #print "shape ",count.shape,downloaded.shape
            downloaded[bbox['slice'][0],bbox['slice'][1]] = 1
            #print "shape ",count.shape,downloaded.shape
            count = count + downloaded

    
        Xp, Yp = m(gx,gy)
        m.pcolor(Xp,Yp,count,shading='flat')
        plt.colorbar()

        outname = filename
        if outname[0] == '/':
            outname = outname[1:]
        outname = outname.replace('/','_')
        outname = outname.replace('.nc','.png')
        #print outname
        plt.savefig(os.path.join(outdir,outname))

        print "count ",outname,count.min(),count.max()



        yield {"title": title,"image": outname, "count": len(agg[filename])}





def genrep(plots,outdir):
    from Cheetah.Template import Template


    template = """<html>
    <head><title>$title</title></head>
    <body>
      <table>
        #for $file in $files
          <h2>$file.title</h2>
          <img src="$file.image">
          <p>Total number of subset $file.count</p>
        #end for
      </table>
    </body>
  </html>"""


    t = Template(template, searchList=[{"files": plots},{"title": 'title'}], filter='EncodeUnicode')
    f = open(os.path.join(outdir,'index.html'),'w')
    f.write(unicode(t).encode('utf-8'))
    f.close()

# from mpl_toolkits.basemap import Basemap
# import matplotlib.pyplot as plt
# # setup Lambert Conformal basemap.
# # set resolution=None to skip processing of boundary datasets.
# m = Basemap(width=12000000,height=9000000,projection='lcc',
#             resolution=None,lat_1=45.,lat_2=55,lat_0=50,lon_0=-107.)
# # draw a land-sea mask for a map background.
# # lakes=True means plot inland lakes with ocean color.
# m.drawlsmask(land_color='grey',ocean_color='white',lakes=True)
# m.drawparallels()
# plt.show()
# #plt.savefig(outfile)

main()
