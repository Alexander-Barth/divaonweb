#!/usr/bin/python

from __future__ import print_function
import urllib2
import os
import netCDF4
import sys

ncname = sys.argv[1]

addcomment = 'Every year of the time dimension corresponds to a 10-year centred average'

with netCDF4.Dataset(ncname,'r+') as nc:

    if 'comment' not in nc.ncattrs():        
        nc.comment = addcomment
    else:
        if nc.comment == 'No comment':
            nc.comment = addcomment
        else:
            nc.comment = nc.comment + '; ' + addcomment


