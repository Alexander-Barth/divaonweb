from divaonweb.obsplots import *
from divaonweb.vocab import *
import sys

obs_index = sys.argv[1]

oi = ObsIndex(obs_index);

errors = oi.validate()

for line,message in errors:
    print 'line: ',line,message


for code in oi.stat:
    print "code ",code,oi.stat[code]

