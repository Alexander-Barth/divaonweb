import matplotlib.pyplot as plt
import numpy
import netCDF4
import time

def contourc(x,y,dd,lev):
    cs = plt.contour(x,y,dd, levels=[lev])

    segments = []

    for collection in cs.collections:
        for path in collection.get_paths():
            segments.append([list(i[0]) for i in path.iter_segments()])

    return segments
    
    

t0 = time.time()    

nc = netCDF4.Dataset('/home/abarth/Downloads/dist2coast.nc','r');
x = nc.variables['lon'][:]
y = nc.variables['lat'][:]
dist = nc.variables['distance2coast'][:]

t1 = time.time()    

xr = [8.0083332,11.191667];
yr = [42.508335,44.424999];

i = numpy.logical_and(xr[0] <= x,x <= xr[1])
j = numpy.logical_and(yr[0] <= y,y <= yr[1])
lev = 10

dd = dist[j,:][:,i]

segments = contourc(x[i],y[j],dd,lev)

# get longest segment
slen = [len(s) for s in segments]
ind = slen.index(max(slen))

seg = segments[ind]
print len(seg)

t2 = time.time()    

print 'reading data',t1-t0
print 'extracting contour',t2-t1

