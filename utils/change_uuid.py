#!/usr/bin/python

from __future__ import print_function
import csv
import sys
import argparse
import netCDF4

# cd /var/www/data/emodnet-domains
# find . -name "*nc" -exec /home/abarth/workspace/divaonweb/utils/change_uuid.py --filename "{}" \; | tee /home/abarth/workspace/divaonweb/utils/change_uuid.log2
#
parser = argparse.ArgumentParser(description='Change product_id in netCDF file.')
parser.add_argument('--filename', help='The name of the netCDF file',required=True)
args = parser.parse_args()

fname = '/home/abarth/workspace/divaonweb/utils/old_new_uuid.csv'
ncname = args.filename

with open(fname, 'rb') as csvfile:
    table = list(csv.reader(csvfile))

mtable = {row[0]: row[1] for row in table}


nc = netCDF4.Dataset(ncname,'r+')


if 'product_id' in nc.ncattrs():
    id = nc.product_id 

    if id in mtable:
        print('%s: change %s to %s' % (ncname,id,mtable[id]))
        nc.product_id = mtable[id]
    else:
        print('%s: %s not found' % (ncname,id))
else:
    print('%s: no product_id' % (ncname,))

nc.close()

