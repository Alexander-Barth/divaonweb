#!/usr/bin/python
from __future__ import print_function
from __future__ import absolute_import

import netCDF4
import glob
import matplotlib
import hashlib
import fnmatch
import os
import sys
import datetime

from divaonweb.util import obsload

matplotlib.use('Agg')  # force the antigrain backend

try:
    from matplotlib.toolkits.basemap import Basemap, shiftgrid
except ImportError:
    from mpl_toolkits.basemap import Basemap, shiftgrid
import pylab


def rglob(rootdir,fn):
    for root, subFolders, files in os.walk(rootdir):
        for file in fnmatch.filter(files, fn):
            yield (root,file)


def analysis_stat(filename):
#    nc=netCDF4.Dataset(filename,'r')

#    x = nc.variables['x'][:]
#    y = nc.variables['y'][:]
#    nc.close()

    [sessionid,step,method,db,stn,clen,x0,x1,rx,y0,y1,ry] = filename[0:-3].split('_')[0:12]

    stn = float(stn)
    clen = float(clen)
    x0 = float(x0)
    x1 = float(x1)
    y0 = float(y0)
    y1 = float(y1)
#    x = [float(x0),float(x1)]
#    y = [float(y0),float(y1)]
    rx = float(rx)
    ry = float(ry)
    mtime = datetime.datetime.fromtimestamp(os.path.getmtime(filename)).strftime('%Y-%m-%dT%H:%M:%S')
    

    return (sessionid,step,method,db,stn,clen,x0,x1,rx,y0,y1,ry,mtime)


def stat(x0,x1,y0,y1,res,fname,alist):
    fig = pylab.figure()
    m = Basemap(llcrnrlon=x0,llcrnrlat=y0,urcrnrlon=x1,urcrnrlat=y1,resolution=res,suppress_ticks=False)

    m.drawcoastlines(color='0.8')
    m.fillcontinents()
    fig.hold(True)

    count = 0
    with open(alist,'r') as f:
        for line in f:
            [sessionid,step,method,db,stn,clen,x0,x1,rx,y0,y1,ry,mtime] = line.split()
            
            x = [float(x0),float(x1)]
            y = [float(y0),float(y1)]
            

            #print [x[0],x[-1]]
            h = m.plot([x[0],x[-1],x[-1],x[0],x[0]],[y[0],y[0],y[-1],y[-1],y[0]],'b',linewidth=2,alpha=.3)

            count = count+1
#        m.fill([x[0],x[-1],x[-1],x[0],x[0]],[y[0],y[0],y[-1],y[-1],y[0]],'b')

    fig.hold(False)
    fig.savefig(fname)
    print("number of analysis",count)


def listobs(file,output):
    try:
        obs = obsload(file)
        mtime = datetime.datetime.fromtimestamp(os.path.getmtime(file)).strftime('%Y-%m-%dT%H:%M:%S')


        with open(file) as f:
            print(file, hashlib.md5(f.read()).hexdigest(), len(obs),
                  min(obs[:,0]), max(obs[:,0]), 
                  min(obs[:,1]), max(obs[:,1]), 
                  min(obs[:,2]), max(obs[:,2]), mtime, file=output)

    except:
        pass

def usage():
    print('see code')

def main():
    import getopt, sys

    command = sys.argv[1]
    try:
        opts, args = getopt.getopt(sys.argv[2:], "d:r:o:i:h", ["help", "output=","rootdir=","input="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(str(err)) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)

    outputfile = None
    inputfile = None
    rootdir = '.'

    for o, a in opts:
        if o in ("-r","--rootdir"):
            rootdir = a
        elif o in ("-o", "--output"):
            outputfile = a
        elif o in ("-i", "--input"):
            inputfile = a
        elif o in ("-h", "--help"):
            usage()
            sys.exit()
        else:
            assert False, "unhandled option"


    if command == 'list-analysis':
        if outputfile == None:
            outputfile = 'list-analysis.txt'

        print("create/append ",outputfile)
        with open(outputfile, "a") as f:
            for (path,file) in rglob(rootdir,'*analysis*.nc'):
                sessionid,step,method,db,stn,clen,x0,x1,rx,y0,y1,ry,mtime = analysis_stat(
                    os.path.join(path,file))
                print(sessionid,step,method,db,stn,clen,x0,x1,rx,y0,y1,ry,mtime,file=f)

    elif command == "list-obs":
        if outputfile == None:
            outputfile = 'list-obs.txt'

        print("create/append ",outputfile)
        with open(outputfile, "a") as f:
            for (path,file) in rglob(rootdir,'*.dat'):
                listobs(os.path.join(path,file),f)

    elif command == "stat-obs":
        if inputfile == None:
            inputfile = 'list-obs.txt'

        with open(inputfile) as f:
            a = {}
            count = 0
            for line in f:                
                [filename,md5sum,datacount,x0,x1,y0,y1,v0,v1,mtime] = line.split()
                a[md5sum] = int(float(datacount))
                count = count+1

            print("number of observation files",count)
            print("number of observations (in unique obs. file)",sum(a.values()))
            print("number of different observation files",len(a))
        

    elif command == "plot":
        if inputfile == None:
            inputfile = 'list-analysis.txt'

        stat(-180,180,-90,90,'c','stat_world.png',inputfile)
        stat(-20,50,25,80,'h','stat_eu.png',inputfile)
        print("created stat*png")
    
if __name__ == "__main__":
    main()



            
    


