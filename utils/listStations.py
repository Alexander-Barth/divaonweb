from divaonweb.obsplots import ObsIndex
import hashlib
import sqlite3
import sys
import getopt

def load():
    oi = ObsIndex('obs_index.txt')

    stations = {}

    for s in oi.load():
        if s['CDI'] != 'not_available':
            md5hash = hashlib.md5(s['CDI']).hexdigest()


            stations[md5hash] = s['CDI']

    return stations
    
def showlist(stations):    
    for k in sorted(stations.keys()):
        print '%s,%s' % (k,stations[k])

def createtable(c):
    # Create table
    c.execute('''create table cdi_combination (id integer, cdis text, unique (cdis), unique (id) )''')
    

def importdata(c,filename):
    "import data in filename using connection to database c and returns all new entries"
    
    c.execute('''select max(id) from cdi_combination''')
    num = c.next()[0]
    stations = {}

    if num == None:
        num = 1000000

    oi = ObsIndex(filename)

    for s in oi.load():
        if s['CDI'] != 'not_available':

            count = c.execute("select count(*) from cdi_combination where cdis == ?",(s['CDI'],)).next()[0]

            if count == 0:
                # new entry
                num = num+1
                c.execute("""insert into cdi_combination values (?,?)""", (num,s['CDI'],))
    
                stations[num] = s['CDI']
    
                #print '%d,%s' % (num,s['CDI'])
            else:
                #print 'present %d,%s' % (num,s['CDI'])
                pass

            
    return stations


def usage():
    print 'see code'

def main(argv):
    filename = None
    dbfile = None

    try:                                
        opts, args = getopt.getopt(argv, "hi:d:", ["help", "input=","database="])
    except getopt.GetoptError:          
        usage()                         
        sys.exit(2)                     
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-i", "--input"):
            filename = arg               
        elif opt in ("-d", "--database"):
            dbfile = arg               


    
    
    
    
filename = 'obs_index.txt'
dbfile = 'emodnet-chemistry.sqlite'

    
    
conn = sqlite3.connect(dbfile)
c = conn.cursor()

createtable(c)

newcdi = importdata(c,filename)

showlist(newcdi)

# Save (commit) the changes
conn.commit()

#c.execute('select * from cdi_combination')
#for row in c:
#    print row

#c.execute('''select max(id) from cdi_combination''')
#print c.next()[0]


# We can also close the cursor if we are done with it
c.close()
