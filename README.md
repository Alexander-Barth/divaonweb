## Installation of OceanBrowser

OceanBrowser is a web-service that allows the on-line visualisation of gridded
fields. It is developed by the University of Liège for the SeaDataCloud and
EMODnet Chemistry projects. The source code and the Docker image are
released under the terms of the GNU Affero General Public License (version
3) from the Free Software Foundation (https://www.gnu.org/licenses/agpl-3.0.html).

### Procedure

- Install Docker (we use version: 18.09.1)
- Import the container following the instructions at:
https://docs.docker.com/engine/reference/commandline/import/
- Extract the DIVA or DIVAnd netCDF files
- Define a configuration file `env.list` by adapting the following example:
```bash
OCEANBROWSER_CONTACT_URL=http://www.emodnet-chemistry.eu/help/contact
OCEANBROWSER_EMAIL_FROMADDR=...
OCEANBROWSER_EMAIL_LOGIN=...
OCEANBROWSER_EMAIL_PASSWORD=...
OCEANBROWSER_EMAIL_SUBJECT=...
OCEANBROWSER_EMAIL_TOADDRS=...
OCEANBROWSER_HTTP_BASEURL=http://ec.oceanbrowser.net/data/emodnet-domains/
OCEANBROWSER_STATIC_WMS_ROOT=/var/www/data
OCEANBROWSER_SUBTITLE=Viewing and Downloading service
OCEANBROWSER_THEME=emodnet-chemical
OCEANBROWSER_TITLE=Chemistry
OCEANBROWSER_URL=http://www.emodnet-chemistry.eu/
OCEANBROWSER_WMS_ABSTRACT=EMODnet (Chemical data) Map Server with ocean climatologies.
OCEANBROWSER_WMS_CONTACT_ORGANIZATION=...
OCEANBROWSER_WMS_CONTACT_PERSON=...
OCEANBROWSER_WMS_EMAIL=...
OCEANBROWSER_WMS_KEYWORDS=ocean climatology diva SeaDataNet EMODnet Chemical
OCEANBROWSER_WMS_TITLE=EMODNET Chemistry - Concentration Maps by Sea Region
OCEANBROWSER_WMS_URL=http://ec.oceanbrowser.net/emodnet/Python/web/wms?
```

It is important that the URLs (`OCEANBROWSER_HTTP_BASEURL` and
`OCEANBROWSER_WMS_URL`) are publicly accessible from the client.
- Run OceanBrowser as follows (https://docs.docker.com/engine/reference/commandline/run/):

```bash
docker run -p $PORT:80 \
  --env-file env.list \
  -v $DATADIR:/var/www/data:ro \
    --detach=true abarth/$NAME:$TAG
```
where `$PORT` is the port number and `$DATADIR` is the directory containing the
netCDF files.
