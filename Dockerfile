
FROM ubuntu:18.04
MAINTAINER Alexander Barth


ENV TZ=Etc/UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

## nginx from PPA
#RUN echo 'deb http://ppa.launchpad.net/nginx/stable/ubuntu trusty main ' >> /etc/apt/sources.list
#RUN echo 'deb-src http://ppa.launchpad.net/nginx/stable/ubuntu trusty main ' >> /etc/apt/sources.list
#RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys C300EE8C

RUN apt-get update && apt-get -y upgrade
RUN apt-get install -y python3-scipy  python3-matplotlib
RUN apt-get install -y    python3-mpltoolkits.basemap python3-lxml python3-setuptools
RUN apt-get install -y    python3-pil python3-pip python3-dev
RUN apt-get install -y  nginx supervisor
RUN apt-get install -y    netcdf-bin libnetcdf-dev libhdf5-dev octave
RUN apt-get install -y    gfortran  build-essential wget curl
RUN cat /etc/apt/sources.list
RUN sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list
RUN apt-get update
#RUN apt-get -y build-dep libav-tools

RUN apt-get -y install ffmpeg

# https://github.com/Unidata/netcdf4-python/issues/341
# for ubuntu 15.04
# RUN cp -R /usr/lib/x86_64-linux-gnu/hdf5/serial/lib/* /usr/lib
# RUN cp /usr/include/hdf5/serial/* /usr/include/

# NetCDF
RUN python3 -m pip install --upgrade pip
#RUN python3 -m pip install netCDF4
RUN apt-get -y install python3-netcdf4
RUN python3 -m pip install uwsgi

# libav Tools with x264 (for video support in OceanBrowser)

WORKDIR /root

#RUN   wget --no-verbose http://ftp.via.ecp.fr/pub/videolan/x264/snapshots/x264-snapshot-20141201-2245-stable.tar.bz2; \
# RUN wget --no-verbose ftp://ftp.videolan.org/pub/videolan/x264/snapshots/x264-snapshot-20141201-2245-stable.tar.bz2; \
#   tar xf x264-snapshot-20141201-2245-stable.tar.bz2; \
#   cd /root/x264-snapshot-20141201-2245-stable; \
#   ./configure  --enable-static && make -j 3 && make install; \
#   rm -R /root/x264-snapshot-20141201-2245-stable


# RUN wget --no-verbose http://libav.org/releases/libav-11.1.tar.gz; \
#   tar zxf libav-11.1.tar.gz; \
#   cd /root/libav-11.1/; \
#   ./configure --enable-gpl --enable-libx264 --enable-libvpx  && make && make install; \
#   rm -R /root/libav-11.1/


# nginx

ADD Docker/nginx.conf /etc/nginx/sites-enabled/
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
RUN rm /etc/nginx/sites-enabled/default
RUN mkdir -p /var/log/nginx/

# wsgi

ADD Docker/uwsgi.ini /var/oceanbrowser/
RUN mkdir -p /var/log/uwsgi/

# supervisor

ADD Docker/supervisor-app.conf /etc/supervisor/conf.d/
RUN mkdir -p /var/log/supervisor/

RUN mkdir -p /var/www/
RUN touch /var/www/.octave_hist
RUN chown  www-data.www-data /var/www/.octave_hist
RUN apt-get install -y liboctave-dev
#RUN HOME=/home/abarth/workspace/my-server/tmp octave --no-gui --no-history --quiet --eval 'pkg install -forge netcdf'
RUN octave --no-gui --no-history --quiet --eval 'pkg install -forge netcdf'

# DIVA
WORKDIR /root
RUN apt-get install -y libnetcdff-dev
RUN wget --no-verbose http://modb.oce.ulg.ac.be/mediawiki/upload/Alex/DIVA/diva-2009-01-08.tar.gz
RUN tar -xvf diva-2009-01-08.tar.gz
WORKDIR /root/diva
RUN mkdir bin
WORKDIR /root/diva/src/Fortran
ADD Docker/divacompile /root/diva/src/Fortran/divacompile
RUN ./divacompile
WORKDIR /root/diva/
RUN mkdir -p /var/www/web-vis/Diva/
RUN cp -R /root/diva/bin /root/diva/divastripped /var/www/web-vis/Diva/
RUN mkdir /var/www/web-vis/Diva/divastripped_template
WORKDIR /var/www/web-vis/Diva/divastripped_template
RUN ln -s ../divastripped/* .
RUN rm divawork gnuwork input meshgenwork output
RUN mkdir -p divawork gnuwork input meshgenwork output/ghertonetcdf output/meshvisu
RUN chown -R www-data.www-data /var/www/web-vis/Diva

EXPOSE 80

RUN apt-get install -y emacs-nox
RUN apt-get -y install python3-requests

# OceanBrowser

WORKDIR /root
# for docker 1.0.1 we must add files individually
ADD COPYING             /root/divaonweb/
ADD MANIFEST.in         /root/divaonweb/
ADD setup.py            /root/divaonweb/
ADD divaonweb           /root/divaonweb/divaonweb/
ADD tests               /root/divaonweb/tests/
ADD utils               /root/divaonweb/utils/
ADD docs                /root/divaonweb/docs/
ADD Docker/setup_config /root/divaonweb/Docker/setup_config

#RUN easy_install divaonweb
RUN cd /root/divaonweb/; python3 setup.py install

WORKDIR /var
#RUN paster create -t divaonweb_server oceanbrowser
RUN mkdir -p /var/oceanbrowser
RUN cp -R /root/divaonweb/divaonweb/paster_templates/* /var/oceanbrowser
RUN mv /var/oceanbrowser/server.ini_tmpl /var/oceanbrowser/server.ini.template

WORKDIR /var/oceanbrowser
RUN mkdir tmp/P011 tmp/video
RUN touch divaonweb.log; chown -R www-data.www-data divaonweb.log data tmp
#ADD Docker/server.ini.template /var/oceanbrowser/server.ini.template
#RUN cp /var/oceanbrowser/server.ini /var/oceanbrowser/server.ini.template
RUN mkdir /var/www/data

ADD Docker/bash_history /root/.bash_history

# setup_config is to be run in /var/oceanbrowser
# and use env variable to create server.ini from server.ini.template

CMD /root/divaonweb/Docker/setup_config && chown www-data.www-data -R /var/oceanbrowser/tmp && supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon
