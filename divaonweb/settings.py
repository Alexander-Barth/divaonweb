# Copyright (C) 2008-2010 Alexander Barth <barth.alexander@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''Configurations file containing all settings'''

import os
import logging

max_cputime = 10*60; # in s
max_obs = 100000

# Cache the analysis from Diva
use_cache = True;

# Enable debugging

do_debug = True;
#do_debug = False;

# Cache each image from WMS server

use_pic_cache = True;

# Binary and environement of octave

octave = {
# complete path to octave bin
'bin': 'octave',
          'env': {},
# octave path added with addpath inside octave session
          'path': ''}


#octave = {'bin': '/home/abarth/opt/octave-3.6.4/bin/octave',
#          'env': {'LD_LIBRARY_PATH': '/home/abarth/opt/octave-3.6.4/lib'},
#          'path': ''}

# Directory containing NetCDF files
# such as dynamically created sections or user data
# location on filesystem: 'self.root' + datadir

datadir = 'Data'

# will be overwriten
bathdir = '/var/www/web-vis_bak/'

bath = {}

bath['gebco'] = [
    {'res': 100/120., 'file': os.path.join('Global','gebco_30sec_100.nc')},
    {'res': 32/120., 'file': os.path.join('Global','gebco_30sec_32.nc')},
    {'res': 16/120., 'file': os.path.join('Global','gebco_30sec_16.nc')},
    {'res': 8/120., 'file': os.path.join('Global','gebco_30sec_8.nc')},
    {'res': 4/120., 'file': os.path.join('Global','gebco_30sec_4.nc')},
    {'res': 2/120., 'file': os.path.join('Global','gebco_30sec_2.nc')},
    {'res': 1/120., 'file': os.path.join('Global','gebco_30sec_1.nc')},
    ]

#bath['gebco'] = [{'res': 5/60.,  'file': os.path.join('Global','dbdbv_5min.nc')}]

bath['corsica'] = [
    {'res': 5e-04, 'file': os.path.join('BayOfCalvi','nwcorsica_bathymetry.nc')},
    ]

bath['nobat'] = [
    {'res': 5e-04, 'file': os.path.join('NoBat','nobat.nc')},
    ]

# bathymetries which are global
globalbath = ['gebco','nobat']

# url for WMS server
# will be overwritten by user-configuration

wms_url = ''

# origin for longitude

longitude0 = -180

# supported formats
# mime-type and file extension (for matplotlib)
# distionary is not used here because we want them in this order

supported_formats = (('image/png', 'png'),
                     ('image/svg+xml', 'svg'),
                     ('image/eps', 'eps'),
                     ('application/pdf','pdf'),
                     ('application/vnd.google-earth.kmz', 'kmz'))

# ('pcolor_interp','Interpolated')
supported_styles = (('pcolor_flat','Flat shading'),
                    ('contourf','Filled contours'),
                    ('contour','Contours')
                    )

# True Type Font for error message
fontfile = "/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans.ttf"

# logging of debug and error message

log = logging.Logger('divaonweb')

http_baseurl = None
opendap_baseurl = None

#LocalCDI_baseurl = 'http://emodnet-chemistry.maris2.nl/v_cdi_v3/print_wfs.asp'
LocalCDI_baseurl = 'https://cdi.seadatanet.org/report/edmo/'
#GlobalCDI_baseurl = 'http://seadatanet.maris2.nl/v_cdi_v3/print.asp'
GlobalCDI_baseurl = 'https://cdi.seadatanet.org/report/'
catalogue_url = 'http://www.ifremer.fr/geonetwork/srv/eng/metadata.formatter.html?xsl=mdviewer&style=emodnet&uuid=$(ID)'
catalogue_url_xml = 'https://sextant.ifremer.fr/geonetwork/srv/eng/csw?request=GetRecordById&elementSetName=full&service=CSW&version=2.0.2&id=$(ID)&OutputSchema=http://www.isotc211.org/2005/gmd'
