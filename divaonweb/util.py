# Copyright (C) 2008-2015 Alexander Barth <barth.alexander@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from __future__ import absolute_import

import subprocess
import os
import math
import cgi

try:
    # python3
    from urllib.parse import parse_qs
except:
    # python2
    from cgi import parse_qs


from urllib.request import urlopen, Request

import email.utils as eut
from time import mktime
from datetime import date, datetime, timedelta
from matplotlib.dates import date2num, num2date
import re
import numpy
import traceback
import sys

from urllib.parse import quote

from divaonweb import settings

time_origine = date2num(date(1858,11,17))
datetime_time_origine = datetime(1858,11,17)


date_re = [re.compile('(?P<month>\d{1,2})/(?P<day>\d{1,2})/(?P<year>\d{2,4}) +(?P<hour>[0-9]+):(?P<min>[0-9]+):(?P<sec>[0-9][0-9]).*'),
           re.compile('(?P<year>\d{2,4})-(?P<month>\d{1,2})-(?P<day>\d{1,2}) +(?P<hour>[0-9]+):(?P<min>[0-9]+):(?P<sec>[0-9.]+).*'),
           re.compile('(?P<year>\d{4,4})-(?P<month>\d{2,2})-(?P<day>\d{2,2})'),
           re.compile('(?P<year>\d{4,4})(?P<month>\d{2,2})(?P<day>\d{2,2})')]


class HttpResponse(object):
    def __init__(self,cm = b'', content_type='text/plain',
                 headers = {},
                 time_modified = None
    ):
        "self.content should always be a bytes in python 3"

        self.http_code = '200 OK'

        if isinstance(cm,HttpResponse):
            self.content = cm.content
            self.content_type = cm.content_type
            self.headers = cm.headers
        else:

            if isinstance(cm,tuple):
                self.content_type,self.content = cm
            else:
                self.content = cm

                # if content_type == 'text/xml':

                #     if sys.version_info[0] == 2:
                #         self.content = unicode(cm).encode('utf-8')
                #     else:
                #         self.content = cm.encode('utf-8')

                # else:
                #     self.content = cm

                self.content_type = content_type

            self.headers = headers.copy()
            self.time_modified = time_modified

    def needRevalidation(self,env):
        "returns True if response need to be recomputed"


        if self.time_modified == None:
            # revalidate always if time modified is unknown
            return True


        if env != None:
            if 'HTTP_IF_MODIFIED_SINCE' in env:
                modified_since = parseHTTPdate(env['HTTP_IF_MODIFIED_SINCE'])

                if self.time_modified <= modified_since:
                    print('oceanbrowser: unmodified',self.time_modified, modified_since, file=sys.stderr)
                    self.http_code = '304 Not Modified'
                    return False


        #print('oceanbrowser: recompute',self.time_modified, file=sys.stderr)
        self.headers.update({
            'Cache-Control': 'must-revalidate, max-age=5',
            'Last-Modified': formatHTTPdate(self.time_modified),
            'Vary': 'Accept-Encoding'
        })

        return True

    def getheaders(self):
        if self.http_code == '304 Not Modified':
            return list(self.headers.items())
        else:
            return [('content-type', self.content_type)] + list(self.headers.items())

    def __call__(self,req):
        ''' mod_python entry point '''

        req.content_type = self.content_type

        for (k,v) in self.headers.items():
            req.headers_out[k] = v

        if isinstance(self.content,file):
            from mod_python import apache
            req.sendfile(self.content.name)
            return apache.OK
        else:
            return self.content


def getRequestFullPath(environ):
    """reconstructs the full path for the request (except query string) based on
    https://www.python.org/dev/peps/pep-0333/#url-reconstruction
"""

    url = environ['wsgi.url_scheme']+'://'


    # if OceanBrowser is behind a reverse proxy, then
    # it has use the host name of the reverse proxy
    if environ.get('HTTP_X_FORWARDED_HOST'):
        url += environ['HTTP_X_FORWARDED_HOST']
    elif environ.get('HTTP_HOST'):
        url += environ['HTTP_HOST']
    else:
        url += environ['SERVER_NAME']

        if environ['wsgi.url_scheme'] == 'https':
            if environ['SERVER_PORT'] != '443':
               url += ':' + environ['SERVER_PORT']
        else:
            if environ['SERVER_PORT'] != '80':
               url += ':' + environ['SERVER_PORT']

    url += quote(environ.get('SCRIPT_NAME', ''))
    url += quote(environ.get('PATH_INFO', ''))
    #if environ.get('QUERY_STRING'):
    #   url += '?' + environ['QUERY_STRING']
    return url

def getWMSURL(wms_url,env):
    # env might be {} or None
    # try:
    #     return getRequestFullPath(env)
    # except (KeyError, TypeError) as e:
    #     # the best we can do
    #     return wms_url

    if wms_url != None:
        return wms_url
    else:
        return getRequestFullPath(env)

def run_octave(cmd):
    import os
    '''runs a command in octave'''
    cmd = ("addpath('%s'); pkg load netcdf;" % settings.octave['path']) + cmd

    p = subprocess.Popen([settings.octave['bin'], '--no-history', '--no-gui',
                          '--quiet', '--eval', cmd],
                         stdout=subprocess.PIPE, env=settings.octave['env'])
    p.wait()
    out = p.stdout.read()
    return out



def make_mat(analysis_name_full, varname, outfile):
    '''Produces a MAT file based on a NetCDF file readable in Octave and
    Matlab'''


    cmd = '''
analysis_name_full = '%s';
mat_fname = '%s';
diva2d2mat(analysis_name_full, mat_fname);
''' % (analysis_name_full, outfile)

    run_octave(cmd)



def select_multires(db,res):
    '''select the coarstest resource with resolution finer than res.
    db is a list of hash tables. Each hash table has the field 'res'  '''

    for b in db:
        if res > b['res']:
            return b

    # return finest resolution
    return db[-1]


def reduce(x,r=2):
    x2 = x[range(1,x.shape[0]-1,r),:]
    x3 = x2[:,range(1,x2.shape[1]-1,r)]
    return x3

def mjd_parse(s):

    for dre in date_re:
        g = dre.match(s)

        if g:
            d = g.groupdict()
            year  = int(d['year'])
            month = int(d['month'])
            day   = int(d['day'])
            return date2num(date(year,month,day)) - time_origine

    print('time',s)
    raise BaseException('time not recognized (%s)' % (s,))



def mjd(*args):
        return date2num(date(*args)) - time_origine

def gregd(d,format='%Y-%m-%d'):
    """Convert serial data number to string (slow but works for dates before 1900)"""

    if d < 15020:
        # before 1900-01-01
        # ValueError: year=1860 is before 1900; the datetime strftime() methods require year >= 1900

        dt = num2date(time_origine + d)
        #print('d',d,dt)

        return format.replace('%Y','%04d' %  dt.year).replace('%m','%02d' %  dt.month).replace('%d','%02d' %  dt.day).replace('%H','%02d' %  dt.hour).replace('%M','%02d' %  dt.minute).replace('%S','%02d' %  dt.second)
    else:
        return (timedelta(float(d)) + datetime_time_origine).strftime(format)



def time_units(u):
    ul = u.lower()

    if ul.find('seconds') != -1:
        f = 1./(24*60*60)
    elif ul.find('hours') != -1:
        f = 1./24.
    elif ul.find('days') != -1:
        f = 1.
    elif ul.find('months') != -1:
        f = 365.25/12.
        # f = 30 gives problems with obsid in test-data/WMS/Salinity.19002009.4Danl.nc
        # as time and obstime do not match
        #f = 30.
    elif ul.find('years') != -1:
        f = 365.25
    else:
        raise BaseException('not recognized units %s' % u)

    l = u.find('since')+6

    t0 = mjd_parse(u[l:])
    #print('u',u,t0,f)
    return (f,t0)


# keep only nd significant digits of x

def roundn(x,nd):
    r = 10.0**math.floor(math.log10(abs(x))+1-nd)
    return round(x/r)*r



def ticks(minx,maxx,n):

    # a least 2 ticks
    if n<2: n = 2

    rangex = maxx-minx;
    dt = rangex/n

    # transform dt in "scientific notation"
    # dt = sdt * 10^(log10(base))

    base = 10.** math.floor(math.log10(dt))
    sdt = dt/base

    # pefered increments

    if sdt <= 1.5:
        sdt = 1
    elif sdt < 2.5:
        sdt = 2
    elif sdt <= 4.:
        sdt = 3
    elif sdt <= 7.:
        sdt = 5
    else:
        sdt = 10


    dt = sdt * base

    # the first label will be:  ceil(minx/dt)*dt
    # the last label will be: floor(max/dt)*dt

    t0 = math.ceil(minx/dt)*dt

    # the difference between first and last label
    # gives the number of labels

    nt = int(math.floor(maxx/dt) - math.ceil(minx/dt) +1.5)

    t = list(range(nt))

    for i in range(nt):
        t[i] = t0 + i*dt

        # attempt to remove spurious decimals
        eps = dt
        t[i] = math.floor(t[i]/eps + 0.5)*eps
        #if (math.abs(t[i])<1e-14): t[i]=0


    return t


def obsload(fname):
    obs = numpy.loadtxt(fname)

    if obs.ndim == 1:
        obs = obs.reshape(1,obs.size)

    return obs




def join_path(basename, filename):
    '''joins the basename and filename path. However it verifies that the
    formed path is below basename for security reasons'''

    name = os.path.normpath(os.path.join(basename, filename))

    if not name.startswith(basename):
        raise BaseException('deny access to %s in %s' % (filename, basename))

    if not os.path.exists(name):
        raise BaseException('file %s does not exist in %s' % (filename, basename))

    return name


def join_new_path(basename, filename):
    '''joins the basename and filename path. However it verifies that the
    formed path is below basename for security reasons'''

    name = os.path.normpath(os.path.join(basename, filename))

    if not name.startswith(basename):
        raise BaseException('deny access to %s in %s' % (filename, basename))

    return name

def timemap(t,tu,isclim,t2=None):
    # t is time coordinate of field
    # tu: time units
    # iclim: true if climatology
    # t2: time in mjd where transform is applied (defaults to t with proper convertion)
    # map CF netcdf time in something human readable (hr)

    hr_t = t
    hr_tu = tu
    time_mapping = False

    can_parse_time = False

    if tu:
        try:
            (f,t0) = time_units(tu)
            can_parse_time = True
        except BaseException as error:
            out = str(error)
            out += '\n' + traceback.format_exc()
            #print('timemap unit',tu)
            #print('timemap error',out,file=sys.stderr)
            #sys.stderr.flush()


    if can_parse_time:
        # time in mjd

        time = f*t + t0
        if t2 == None:
            t2 = time


        if isclim and len(time) > 1:
            #print 'can_parse_time ',can_parse_time, time[1] - time[0], f,t0,tu
            time_mapping = True

            #print 'time ',time[1]-time[0],27*4 <= time[1] - time[0] <= 32*4

            # heuristics
            if 27 <= time[1] - time[0] <= 32:
                hr_t = [gregd(t,format='%m') for t in t2]
                hr_tu = 'month'
            elif 27*3 <= time[1] - time[0] <= 32*3 and len(time) <= 4:
                seasonnames = ['winter','spring','summer','autumn']
                hr_t = [
                    seasonnames[  (num2date(t+time_origine).month-1)//3  ] for t in t2]
                hr_tu = 'season'
            elif (27*3 <= time[1] - time[0] <= 32*3):
                seasonnames = ['winter','spring','summer','autumn']
                hr_t = [
                    seasonnames[  (num2date(t+time_origine).month-1)//3  ] + ' ' + gregd(t,format='%Y') for t in t2]
                hr_tu = 'season'
            elif 27*12 <= time[1] - time[0] <= 32*12:
                hr_t = [gregd(t,format='%Y') for t in t2]
                #print('hr_t',hr_t,t2)
                hr_tu = 'years'
            else:
                pass
                #print('here')
        else:
            hr_t = [gregd(t,format='%Y-%m-%dT%H:%M') for t in t2]
            time_mapping = True
            hr_tu = 'yyyy-mm-ddTHH:MM'

    #print('mapping ',hr_t,hr_tu,time_mapping,time[1]-time[0])

    return (hr_t,hr_tu,time_mapping)

def timeselect(nc,t,time_index):
    # time of climatology
    climt = numpy.array(nc.variables['time'][:])
    #print("time_index ",time_index)

    if (27 <= (climt[1] - climt[0]) <= 32) and (len(climt) == 12):
        #print("time_index",time_index)
        # monthly resolution
        # https://stackoverflow.com/a/26895491/3801401
        t_datetime64 = numpy.datetime64(datetime_time_origine) + t.astype("timedelta64[D]")
        month = t_datetime64.astype('datetime64[M]').astype(int) % 12 + 1
        #print("month",month[0:12])
        sel_time = month == (time_index+1)
    elif hasattr(nc.variables['time'],'climatology'):
        cb_name = nc.variables['time'].climatology

        # climatology bounds
        cb = numpy.array(nc.variables[cb_name][:])

        (f,t0) = time_units(nc.variables['time'].units)
        cb_mjd = f*cb + t0

        # time of climatology
        climt = numpy.array(nc.variables['time'][:])

        #print('cb_mjd',cb_mjd[time_index,0],cb_mjd[time_index,1])

        #print('index',climt_hr.index(self.time))

        climt_mjd = f*climt + t0

        sel_time = numpy.logical_and(cb_mjd[time_index,0] <= t , t <= cb_mjd[time_index,1])

        #print(sel_time,numpy.sum(sel_time))
    else:
        # apply the same transformation into human readable way
        (time2,_time_units,time_mapping) = timemap(
                    numpy.array(nc.variables['time'][:]),
                    nc.variables['time'].units,
                    hasattr(nc.variables['time'],'climatology'),
                    t)

        #print('self.time',self.time)
        # make time selection
        sel_time = numpy.array([_ == self.time for _ in time2])

    #print(sel_time,numpy.sum(sel_time))
    return sel_time

def distance(lat1, lon1, lat2, lon2):
    rad = math.pi/180.0

    a = lat1;
    b = lat2;
    C = lon2 - lon1;

    a = a * rad;
    b = b * rad;
    C = C * rad;

    dist = math.acos(math.sin(b) * math.sin(a) + math.cos(b) * math.cos(a) * math.cos(C))

    dist = dist/rad
    return dist



def closest(x,y,lon,lat,maxdist=float('inf')):
    '''index such that (x[index],y[index]) is the closest to (lon,lat);
returns the point which is closest to lon,lat but not farther away than maxdist'''

    cindex = clon = clat = None
    cdist = float('inf')

    for index, (xi,yi) in enumerate(zip(x, y)):
        d = distance(yi,xi,lat,lon)

        if d < maxdist and d < cdist:
            clon = yi
            clat = xi
            cindex = index
            cdist = d

    return cindex,clon,clat


# http://stackoverflow.com/questions/6999726/python-converting-datetime-to-millis-since-epoch-unix-time
def toUnixTime(dt):
    epoch = datetime.utcfromtimestamp(0)
    delta = dt - epoch
    return delta.total_seconds()

def fromUnixTime(delta):
    return datetime.utcfromtimestamp(delta)


# parse HTTP-date
# http://stackoverflow.com/questions/1471987/how-do-i-parse-an-http-date-string-in-python

def parseHTTPdate(text):
    return toUnixTime(datetime(*eut.parsedate(text)[:6]))

# convert  epoch time stamp to HTTPdate string
def formatHTTPdate(stamp):
    return eut.formatdate(
        timeval     = stamp,
        localtime   = False,
        usegmt      = True
    )


def proxy(environ,start_response):
    allowedHosts = ['www.openlayers.org', 'openlayers.org',
                    'labs.metacarta.com', 'world.freemap.in',
                    'prototype.openmnnd.org', 'geo.openplans.org',
                    'sigma.openplans.org', 'demo.opengeo.org',
                    'www.openstreetmap.org', 'sample.azavea.com',
                    'v2.suite.opengeo.org', 'v-swe.uni-muenster.de:8080',
                    'vmap0.tiles.osgeo.org', 'www.openrouteservice.org']

    allowedHosts = None

    # use test/xml for these types
    content_type_map = {
        'application/vnd.ogc.wms_xml': 'text/xml',
        'application/vnd.ogc.se_xml': 'text/xml',
        'application/vnd.ogc.gml': 'text/xml'
    }

    method = environ["REQUEST_METHOD"]

    qs = environ["QUERY_STRING"]
    d = parse_qs(qs)
    if "url" in d:
        url = d["url"][0]
    else:
        url = "http://www.openlayers.org"

    try:
        host = url.split("/")[2]
        if allowedHosts and not host in allowedHosts:
            start_response('502 Bad Gateway',  [('Content-type', 'text/plain')])
            return "This proxy does not allow you to access that location (%s)." % (host,)

        elif url.startswith("http://") or url.startswith("https://"):

            if method == "POST":
                length = int(environ["CONTENT_LENGTH"])
                headers = {"Content-Type": environ["CONTENT_TYPE"]}
                body = environ['wsgi.input'].read(length)
                #print('url',url, body, headers)
                r = Request(url, body, headers)
                y = urlopen(r)
            else:
                y = urlopen(url)


            # set content type header
            i = y.info()

            #if i.has_key("Content-Type"):
            if "Content-Type" in i:
                content_type = i["Content-Type"]
            else:
                content_type = 'text/plain'

            if content_type in content_type_map:
                content_type = content_type_map[content_type]

            start_response('200 OK', [('Content-type', content_type)])

            #return y
            #block_size = 8192
            block_size = 1024
            return iter(lambda: y.read(block_size), b'')

            #data = y.read()
            #y.close()
            #return data


        else:
            start_response('400 Bad Request', [('Content-type', 'text/plain')])
            return 'Illegal request.'

    except Exception as E:
        #import sys, traceback
        #traceback.print_exc(file=sys.stdout)
        start_response('500 Unexpected Error', [('Content-type', 'text/plain')])
        return "Some unexpected error occurred. Error text was:" + str(E)
