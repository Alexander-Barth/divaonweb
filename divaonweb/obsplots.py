#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

import fnmatch
import os

try:
    from mx.DateTime.ISO import ParseDateTimeUTC
except ImportError:
    # python 3
    # works also in python 2, but this routine is very slow and
    # mx.DateTime.ISO is not ported to python 3
    from dateutil.parser import parse as ParseDateTimeUTC

import matplotlib.pyplot as plt
from io import BytesIO

import math

try:
    from urllib.request import urlopen
    from urllib.error import HTTPError
    from urllib.parse import urlencode
except ImportError:
    # python 2
    from urllib2 import urlopen
    from urllib2 import HTTPError
    from urllib import urlencode


import sys
import sqlite3
import mimetypes

try:
    import json
except ImportError:
    # for python 2.5
    import simplejson as json


try:
    from lxml.builder import E, ET
except ImportError:
    from lxml.builder import E
    from lxml import etree as ET


from divaonweb.webmap import base_webmapserver, HttpResponse, webmapserver, folder_description, layer_description, wms_canvas, catalogue_url
from divaonweb import vocab
from divaonweb.util import distance
from divaonweb import settings

def expandBrace(s):
    """expand curly braces similar to bash. Does not do any recursive exansion"""

    i = s.find('{')
    if i == -1:
        return [s]

    j = s.rfind('}',i)
    return [s[:i] + item + s[j+1:] for item in s[i+1:j].split(',')]


def intersectlist(list1,list2):
    return [val for val in list1 if val in list2]


class ParamGroups(object):
    def __init__(self,filename,var):
        self.g = []
        self.v = var

        #print('load',filename,file=sys.stderr)
        f = open(filename,'r')
        for entry in json.load(f):
            # include only groups with elements
            #print "entry2, ",entry[2],var
            #print "inter ",intersectlist(var,entry[2])
            if len(intersectlist(var,entry['codes'])) > 0:
                self.g.append(entry)

        f.close()

        #print "g", self.g
        #self.g = groups
        self._allcodes = [code for entry in self.g for code in entry['codes']]

        othercodes = []
        for v in var:
            if v not in self._allcodes:
                othercodes.append(v)

        if len(othercodes) > 0:
            self.g.append({'name': 'other','title': 'Others','codes': othercodes,'ids': []})


    def __contains__(self,key):
        return key in self.keys()

    def get(self,name,default=(None,[])):
        try:
            return self.__getitem__(self,name)
        except:
            return default

    def keys(self):
        return [entry['name'] for entry in self.g]

    def __getitem__(self,name):
        for entry in self.g:
            if name == entry['name']:
                return (entry['title'],entry['codes'])

    def getcodes(self,name,default=None):
        for entry in self.g:
            if name == entry['name']:
                return entry['codes']

        return default

    def gettitle(self,name):
        for entry in self.g:
            if name == entry['name']:
                return entry['title']

    def getids(self,name,default=[]):
        for entry in self.g:
            if name == entry['name']:
                return entry['ids']

        return default

    def allcodes(self):
        return self._allcodes



class WMSLayers(object):
    def __init__(self):
        pass

    def list(self):
        pass

    def capabilities(self,layer):
        pass

    def map(self,layer):
        pass

    def feature(self,layer):
        pass


def dirwalk(root,dir,pattern):
            "walk a directory tree, using a generator"

            fulldir = os.path.join(root,dir)

            for f in os.listdir(fulldir):
                fullpath = os.path.join(fulldir,f)
                if os.path.isdir(fullpath):
                    for x in dirwalk(root,os.path.join(dir,f),pattern):  # recurse into subdir
                        yield x
                elif fnmatch.fnmatch(f, pattern):
                    yield os.path.join(dir,f)

class ObsPlots(WMSLayers):
    def __init__(self,root,**kwargs):

        super(ObsPlots, self).__init__(**kwargs)

        self.root = root
        self.layers = []

        # Can include any UNIX shell-style wildcards
        pattern = '*.txt'

        for f in dirwalk(self.root,'',pattern):
            self.layers.append(f)
            #print f

    def list(self):
        return self.layers


    def capabilities(self,layer):
        return self.layers






class ObsIndex(object):
    def __init__(self,filename):
        #print "opening "

        if isinstance(filename,str):
            f = open(filename,'r')
            self.f = f.readlines()
            f.close()
        else:
            # assume file like object
            self.f = filename.readlines()

        # lines read
        self.n = 0
        self.stat = {}

    def __del__(self):
        #print "closing "
        #self.f.close()
        pass

    def load(self):
        #self.f.seek(0)

        self.n = 0

        for line in self.f:
            self.n = self.n+1

            if not line.startswith('//'):
                cells = line.split()
                #print cells

                s = {}
                s['longitude'] = float(cells[0])
                s['latitude']  = float(cells[1])
                s['start_depth']  = float(cells[2])
                s['end_depth']  = float(cells[3])
                s['start_time']  = ParseDateTimeUTC(cells[4])
                s['end_time']  = ParseDateTimeUTC(cells[5])
                s['P011_code']  = cells[6]
                s['URL'] = cells[7]
                #s['CDI'] = cells[8]
                # make sure they are sorted
                s['CDI'] = ','.join(sorted(cells[8].split(',')))

                yield s

    def validate(self):


        vocabular = vocab.Vocab('/tmp/')
        valid_cdis = [];
        invalid_cdis = [];
        data = self.load()

        while True:
            try:
                s = data.next()
            except ValueError as error:
                yield (self.n,' unable to parse this line (%s)' % str(error))
                continue
            except StopIteration:
                break

#        for s in self.load():
            if s['latitude'] < -90 or s['latitude'] > 90:
                yield (self.n,' latitude is out side of -90 and 90 degrees')

            if not s['P011_code'].startswith('SDN:P011::'):
                yield (self.n,' P011 code should begin with SDN:P011::')

            try:
                code = vocabular.label(s['P011_code'])
                self.stat[s['P011_code']] = code;
            except ValueError:
                yield (self.n,' unknown P011 code %s ' % s['P011_code'])

            if s['CDI'] != 'not_available':
                for cdi in s['CDI'].split(','):
                    if cdi not in valid_cdis and cdi not in invalid_cdis:

                        if not cdi.startswith('SDN:CDI:'):
                            yield (self.n,' CDI should begin with SDN:CDI:')
                            continue

                        url = settings.GlobalCDI_baseurl + cdi.replace('SDN:CDI:','')

                        try:
                            f = urlopen(url)
                            out = f.read()
                            f.close()

                            if 'WHERE' in out or 'Where' in out:
                                valid_cdis.append(cdi)
                            else:
                                yield (self.n,' unknown CDI %s ' % cdi)
                                invalid_cdis.append(cdi)

                        except urllib2.HTTPError:
                            yield (self.n,' unknown CDI %s ' % cdi)
                            invalid_cdis.append(cdi)




    def variables(self):
        '''list a variables'''
        v = {}
        #self.f.seek(0)

        for line in self.f:
            if not line.startswith('//'):
                cells = line.split()
                #print cells

                code  = cells[6]
                v[code] = None


        return v.keys()


class ObsIndexSelect(ObsIndex):
    def load(self,lon,lat,parameters = []):
        for s in super(ObsIndexSelect, self).load():
            if s['longitude'] != None and s['latitude'] != None:
                if (lon[0] <= s['longitude'] <= lon[1] and
                    lat[0] <= s['latitude'] <= lat[1] and
                    (s['P011_code'] in parameters  or parameters == [])):

                    yield s

    def closest(self,lon,lat,maxdist=float('inf')):
        '''returns the point which is closest to lon,lat but not farther away than maxdist'''

        clon = clat = None
        cdist = float('inf')

        for s in super(ObsIndexSelect, self).load():
            d = distance(s['latitude'],s['longitude'],lat,lon)

            if d < maxdist and d < cdist:
                clon = s['longitude']
                clat = s['latitude']
                cdist = d

        return clon,clat

    def range(self, k, minr=0, parameters = []):
        v0 = float('inf')
        v1 = float('-inf')

        for s in super(ObsIndexSelect, self).load():
            if (s['P011_code'] in parameters  or parameters == []):

                if s[k] > v1:
                    v1 = s[k]

                if s[k] < v0:
                    v0 = s[k]

        if abs(v1-v0) < minr:
            m = (v0+v1)/2.
            v0 = m-minr/2.
            v1 = m+minr/2.


        return (v0,v1)


class obs_webmapserver(webmapserver):
    allparams = "__allplots__"

    def __init__(self, filename, vocabtable, wms_url, service,
                 dbfile, groupsname,
                 title='', **kwargs):
        '''Web Map Server with disk cache if optional arguments use_pic_cache
        is true (default)'''

        super(obs_webmapserver, self).__init__('','',service,**kwargs)
        self.filename = filename
        self.oi = ObsIndexSelect(filename)


        #self.ld = layer_description(name, title, x0, x1, y0, y1, time, time_units,
        #         elevation, elevation_units, attribution, legend, wms_url)

        self.wms_url = wms_url
        self.title = title
        self.feature_info_formats = ['application/vnd.ogc.gml']

        self.vocab = vocab.Vocab(vocabtable)
        self.svocab = vocab.ShortVocab(vocabtable)

        self.dbfile = dbfile
        self.groupsname = groupsname

        # all variables labels
        self.variables = sorted(self.oi.variables(), key = lambda v: self.vocab.label(v))


        # smallest range in degree
        self.minr = 2
        self.groups = ParamGroups(self.groupsname,self.variables)


    def get_capabilities_time_modified(self):
        # a change of one of these file would generate a new
        # modification time

        return math.floor(
            max(os.path.getctime(self.filename),
                os.path.getctime(self.dbfile),
                os.path.getctime(self.groupsname)))

    def obs_layer_description(self,layer,identifier=None):
        time = []
        time_units = None
        elevation = []
        elevation_units = None
        attribution = {}
        legend = None
        stitle = None
        name = layer
        metadata_url = []

        if layer.startswith(self.__class__.allparams):
            title = "All parameters"

            if layer != self.__class__.allparams:
                gname = layer.replace(self.__class__.allparams,"")
                title += " (" + self.groups.gettitle(gname) + ")"

                if identifier != None:
                    metadata_url = [{
                        "type": "TC211",
                        "format": "text/html",
                        "url": catalogue_url(nc.product_id)
                    }]


        else:
            title = self.vocab.label(layer)
            stitle = self.svocab.label(layer)

        parameters = self.get_param(layer)

        (x0,x1) = self.oi.range('longitude',self.minr,parameters = parameters)
        (y0,y1) = self.oi.range('latitude',self.minr,parameters = parameters)

        if stitle:
            item = layer_description(name, stitle, x0, x1, y0, y1, time, time_units,
                                     elevation, elevation_units, attribution, legend, self.wms_url,
                                     abstract = title,metadata_url = metadata_url)
        else:
            item = layer_description(name, title, x0, x1, y0, y1, time, time_units,
                                     elevation, elevation_units, attribution, legend, self.wms_url,
                                     metadata_url = metadata_url)



        return item

    def group_description(self,gname):

        if gname not in self.groups:
            return folder_description(title='Group not found',wms_url=self.wms_url)


        title,codes = self.groups[gname]

        gd = folder_description(title=title,wms_url=self.wms_url)

        identifier = None
        ids = self.groups.getids(gname)
        if len(ids) > 0:
            identifier = ids[0]
            gd.identifier = ids[0]

        # all parameters with a group
        gd.layers.append(self.obs_layer_description(self.__class__.allparams + gname,identifier))

        for code in codes:
            if code in self.variables:
                #print code
                gd.layers.append(self.obs_layer_description(code))

        return gd


    def get_folder_description(self,basedir = ''):

        if basedir != '':
            return self.group_description(basedir)

        fd = folder_description(name="root", title="root")
        fd.layers.append(self.obs_layer_description(self.__class__.allparams))

        # code per group
        for gname in self.groups.keys():
            fd.layers.append(self.group_description(gname))

        return fd

    def default_style(self,style):
        defparam = {'markersize': 7,
                    'marker': 'D',
                    'markerfacecolor': 'cyan',
                    'markeredgecolor': 'black'}

        s = defparam.copy()
        s.update(style)

        if 'method' in s:
            del s['method']
        return s


    def get_param(self,layer):
        """get P011 codes from layer name"""

        # all parameters
        if layer == self.__class__.allparams:
            # special mean: all parameters
            return []
        elif layer.startswith(self.__class__.allparams):

            # all parameters within a group
            return self.groups.getcodes(layer.replace(self.__class__.allparams,''))

        return [layer]


    def get_map(self, **kwargs):
        imgformat = kwargs.get('format', 'image/png')
        layers = kwargs['layers']
        (x0, y0, x1, y1) = kwargs.get('bbox', (-180, -90, 180, 90))

        elevation = kwargs.get('elevation', 0)
        time = kwargs.get('time', 'default')

        styles = kwargs['styles']
        width = kwargs.get('width', 256)
        height = kwargs.get('height', 256)
        decorated = kwargs.get('decorated', False)
        CRS = kwargs.get('crs', 'EPSG:4326')

        style = self.default_style(styles[0])

        wmsc = wms_canvas(layers, x0, y0, x1, y1, width, height,
                          decorated=decorated, CRS=CRS)

        # load an extra 1/20
        buf = max(wmsc.lon1-wmsc.lon0, wmsc.lat1-wmsc.lat0)/20.

        for layer in layers:

            parameters = self.get_param(layer)

            for s in self.oi.load([wmsc.lon0-buf, wmsc.lon1+buf],
                                  [wmsc.lat0-buf, wmsc.lat1+buf],
                                  parameters):
                #print s
                wmsc.m.plot([s['longitude']], [s['latitude']], 'c,', antialiased=False, **style)

        return HttpResponse(wmsc.render(imgformat),imgformat)


    def get_feature_info(self, **kwargs):
        '''gets the value of the field at selected location'''

        # get parameters
        version = kwargs.get('version','1.1.1')
        layers = kwargs['query_layers'].split(',')
        (x0, y0, x1, y1) = kwargs.get('bbox', (-180, -90, 180, 90))

        if version == '1.1.1':
            i = float(kwargs['x'])
            j = float(kwargs['y'])
        else:
            i = float(kwargs['i'])
            j = float(kwargs['j'])

        elevation = kwargs.get('elevation', 0)
        time = kwargs.get('time', 'default')
        width = kwargs.get('width', 256)
        height = kwargs.get('height', 256)

        # TODO take projection into account

        # calculate longitude and latitude of selected point
        x = x0 + i * (x1-x0)/width
        y = y1 + j * (y0-y1)/height

        pixels = 15
        dx = pixels/float(width) * (x1-x0)
        dy = pixels/float(height) * (y1-y0)

        # search for closest station
        cx,cy = self.oi.closest(x,y,max(dx,dy))

        # connections cannot be shared
        conn = sqlite3.connect(self.dbfile)
        cursor = conn.cursor()

        root = E.info()


        for layer in layers:
            #print "layer" ,layer
            parameters = self.get_param(layer)

            plotlist = list(self.oi.load([cx,cx],[cy,cy],parameters))

            # sort according to P011_code
            plotlist = sorted(plotlist, key=lambda s: s['P011_code'])

            if plotlist:
                feature = E.feature(E.longitude(str(cx)),E.latitude(str(cy)))
                root.append(feature)

                for s in plotlist:
                    code = s['P011_code'].replace('SDN:P011::','')
                    format = 'image/html'
                    title = self.vocab.label(code)
                    plot_url = s['URL']
                    query = {}

                    # URL for data download
                    download_url = None
                    if s['CDI'] != 'not_available':
                        combination_id = list(cursor.execute("select id from cdi_combination where cdis == ?",(s['CDI'],)))

                        if len(combination_id) > 0:
                            #baseurl = 'http://emodnet-chemistry.maris2.nl/v_cdi_v3/browse_step.asp?step=013'
                            baseurl = 'https://emodnet-chemistry.maris.nl/search?step=013'
                            download_url = baseurl + str(combination_id[0][0])
                            query['download'] = download_url

                    for plots in expandBrace(s['URL']):
                        #print 'plots ',plots
                        mtype = mimetypes.guess_type(plots)[0]
                        query[mtype] = plots

                    plot_url = 'plots.html?' + urlencode(query)

                    # new format

                    # time or time range
                    if s['start_time'] == s['end_time']:
                        time = s['start_time'].strftime('%Y-%m-%dT%H:%M:%S')
                    else:
                        time = u"%s – %s" % (s['start_time'].strftime('%Y-%m-%dT%H:%M:%S'),
                                             s['end_time'].strftime('%Y-%m-%dT%H:%M:%S'))

                    # depth or depth range
                    if s['start_depth'] == s['end_depth']:
                        depth = str(s['start_depth'])
                    else:
                        depth = u"%s – %s" % (s['start_depth'],
                                              s['end_depth'])


                    DataURL = E.DataURL(
                        E.Title(title),
                        E.Name(code),
                        E.Format(format),
                        E.OnlineResource({'href': plot_url}),
                        E.time(time),
                        E.depth(depth)
                        )

                    if s['CDI'] != 'not_available':
                        DataURL.append(E.CDI(s['CDI']))

                        if download_url:
                            DataURL.append(E.data(download_url))

                    feature.append(DataURL)

        cursor.close()
        return HttpResponse(ET.tostring(root, pretty_print=True, xml_declaration=True),'text/xml')


    def get_legend(self, **kwargs):
        '''Produce a legend (e.g. colorbar)'''
        imgformat = kwargs.get('format', 'image/png')

        layer = kwargs.get('layer','')

        width = kwargs.get('width', 100)
        height = kwargs.get('height', 300)
        color = kwargs.get('color', '#000000')
        bgcolor = kwargs.get('bgcolor', 'none')
        transparent = kwargs.get('transparent', True)
        style = kwargs.get('style',{})
        style = self.default_style(style)

        fig = plt.figure(figsize=(1.*width/self.dpi,
                                    1.*height/self.dpi))

        if transparent:
            fig.figurePatch.set_alpha(0.0)

        ax = fig.add_axes([0., 0., 1., 1.])
        ax.set_facecolor(bgcolor)
        ax.axis('off')

        ax.plot([0],[0],**style)
        ax.set_ylim(-1,1)
        ax.set_xlim(-1,1)

        stream = BytesIO()
        fig.savefig(stream, dpi=self.dpi, facecolor=bgcolor)
        legend = stream.getvalue()
        stream.close()

        return HttpResponse(legend,imgformat)


def listStations():
    f = open('obs_index.txt');
