# Copyright (C) 2008-2014 Alexander Barth <barth.alexander@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from __future__ import absolute_import

import netCDF4
import types
import pylab
import string
import sys
import re
import numpy
import datetime

from divaonweb import util
from divaonweb import interpolation

try:
    import numpy.ma as M
except:
    import matplotlib.numerix.ma as M

import traceback

def _len(x):
    if hasattr(x,'__len__'):
        return len(x)
    else:
        return 1


# cache previous mapped times as this can be quite slow
#timemap_cache = {}

class GenericGeoDataset(object):
    # common dimension names and units names to identify coordinates

    common_dimnames = {'longitude': [re.compile('.*lon.*'),re.compile('^x$')],
                       'latitude': [re.compile('.*lat.*'),re.compile('^y$')],
                       'depth': [re.compile('.*depth.*'),re.compile('^z$')],
                       'time': [re.compile('.*time.*'),re.compile('^t$')] }

    common_units = {'longitude': [re.compile('.*east.*')],
                    'latitude': [re.compile('.*north.*')],
                    'depth': [re.compile('^m$'),re.compile('^meters$')],
                    'time': [re.compile('years?\b.*'), re.compile('months?\b.*'), re.compile('days?\b.*'),
                             re.compile('hours?\b.*'), re.compile('minutes?\b.*'), re.compile('seconds?\b.*')]}



    def __init__(self,filename,varname, hr_time = True):
        self.fileopened = False
        self.hr_time = hr_time

        if sys.version_info[0] == 2:
            # python 2
            string_types = basestring
        else:
            # python 3
            string_types = str

        if isinstance (filename,string_types):
            try:
                self.nc=netCDF4.Dataset(filename,'r')
            except RuntimeError as error:
                print >> sys.stderr, 'Unable to open file',filename
                sys.stderr.flush()
                # re-throw exception
                raise error

            self.fileopened = True
        else:
            self.nc = filename

        self.varname = varname
        self.nv = self.nc.variables[varname]
        self.dims = self.nv.dimensions
        self._coordinates = {}

        self._type = '';

        if hasattr(self.nc,'type'):
            t = self.nc.type

            if t[0:4] == 'ROMS':
                self._type = 'ROMS'

        #print "type ",self._type

        (self.xname,self.yname,self.zname,self.tname) = self.getDimNames()

        #print "zname",varname,self.zname

        self.time_coord_type = None
        self.time_mapping = False


        if self.tname and hr_time:
            nv = self.nc.variables[self.tname]
            if hasattr(nv,'climatology'):
                self.time_coord_type = 'climatology'

            self.t = self._coord('time')
            self.tu = self._coord_units('time')

            # map CF netcdf time in something human readable (hr)
            #print('filename',self.nc,self.tu)

            # cache the result of util.timemap
            # key = (filename,self.tname)
            # if key not in timemap_cache:
            #     timemap_cache[key] = util.timemap(self.t,self.tu,self.time_coord_type == 'climatology')
            #     print('mapping is not in cache',file=sys.stderr)
            # else:
            #     print('mapping is in cache',file=sys.stderr)

            # (self.hr_t,self.hr_tu,self.time_mapping) = timemap_cache[key]

            #print('here',varname,file=sys.stderr)
            (self.hr_t,self.hr_tu,self.time_mapping) = util.timemap(self.t,self.tu,self.time_coord_type == 'climatology')
            #print('there',file=sys.stderr)


    def __del__(self):
        if self.fileopened:
            self.nc.close()


    def getDimNames(self):
        """tries to be smart about the dimensions names in a NetCDF file"""

        xname = self._find_coord('longitude')
        yname = self._find_coord('latitude')
        zname = self._find_coord('depth')
        tname = self._find_coord('time')

        return (xname,yname,zname,tname)


    def _find_coord_old(self,coord):
        dim =  set(self.nv.dimensions)

        if self._type == 'ROMS':
            if coord == 'depth':
                for ncv_name in ('s_rho','s_u','s_v','s_w'):
                    if ncv_name in dim:
                        return ncv_name

        # hack for NEMO
        if coord == 'depth' and self.varname[0:2] == 'so':
            return None


        # all dimensions of ncv must also be a dimension of self.nv
        # scalar can not be a dimension
        candidates = [(nvc_name,nvc) for (nvc_name,nvc) in self.nc.variables.items()
                      if nvc_name != self.varname and len(set(nvc.dimensions) - dim) == 0 and len(nvc.dimensions) > 0 ]

        # test based on dimension name
        for (nvc_name,nvc) in candidates:
            for r in self.__class__.common_dimnames[coord]:
                if r.match(nvc_name):
                    return nvc_name

        # test based on units
        for (nvc_name,nvc) in candidates:
            if hasattr(nvc,'units'):
                unit = nvc.units
                for r in self.__class__.common_units[coord]:
                    if r.match(unit):
                        return nvc_name

        # not found
        return None


    def isacoord(self,nvc_name,coord):
        '''check if ncv_name could a coordinate of type coord'''

        # test based on variable name
        for r in self.__class__.common_dimnames[coord]:
            if r.match(nvc_name):
                return True

        # test based on units
        nvc = self.nc.variables[nvc_name]

        if hasattr(nvc,'units'):
            unit = nvc.units
            for r in self.__class__.common_units[coord]:
                if r.match(unit):
                    return True


    def _find_coord(self,coord):
        '''find coordinate variable for CF files.
        Based on http://cf-pcmdi.llnl.gov/documents/cf-conventions/1.5/ch05.html '''


        dim =  set(self.nv.dimensions)

        if self._type == 'ROMS':
            if coord == 'depth':
                for ncv_name in ('s_rho','s_u','s_v','s_w'):
                    if ncv_name in dim:
                        return ncv_name

        # check for coordinate variables
        for v in self.nv.dimensions:
            if v in self.nc.variables:
                if self.isacoord(v,coord):
                    return v

        # check for the attribute "coordinates" and loop over all candiates
        if hasattr(self.nv,'coordinates'):
            coordinates = self.nv.coordinates.split(' ');

            for v in coordinates:
                if self.isacoord(v,coord):
                    return v



        # not found
        return None



    def FillValue(self):
        if hasattr(self.nv,"_FillValue"):
            fv = getattr(self.nv,"_FillValue")
        elif hasattr(self.nv,"missing_value"):
            fv = getattr(self.nv,"missing_value")
        else:
            # this should disapear
            fv = -9999
        return fv

    def loadzcoord(self):
        """loads the vertical coordinate and honors the positive attribute"""

        if not self.zname:
            return numpy.array([])

        if self._type == 'ROMS':
            hc = self.nc.variables['hc'][:]

            if self.zname == 's_w':
                Cs = self.nc.variables['Cs_w'][:]
            else:
                Cs = self.nc.variables['Cs_r'][:]

            s = self.nc.variables[self.zname][:]
            h = numpy.array(self.nc.variables['h'][:])

            stagger = self.nv.dimensions[-1]

            if stagger == 'xi_u':
                h = (h[:,1:] + h[:,:-1])/2.
            elif stagger == 'xi_v':
                h = (h[1:,:] + h[:-1,:])/2.
            elif stagger == 'xi_psi':
                h = (h[1:,1:] + h[1:,:-1] + h[:-1,1:] + h[:-1,:-1])/4.

            z = numpy.zeros((len(s),h.shape[0],h.shape[1]))

            # keep surface layer to zero
            #for k in range(len(s)):
            for k in range(len(s)-1):
                z[k,:,:] = hc * (s[k]-Cs[k]) + h*Cs[k]

            return z

        nv = self.nc.variables[self.zname]
        z = numpy.array(nv[:])

        if hasattr(nv,"positive"):
            if getattr(nv,"positive") == "down":
                z = -z

        return z

    def selection_ind(self,argname,cname,kwargs,loadcoord=None):


        # variable does have the dimensions
        if not (cname in self.dims):
            return (None,None)

        #if loadcoord is None:
        #    coord = numpy.array(self.nc.variables[cname][:])
        #else:
        #    coord = loadcoord()
        coord = self._coord(argname)

        # is dimension constrained ?
        if argname in kwargs:
            selected_coord = kwargs[argname]

            if selected_coord == None or coord.ndim == 2:
                i = range(len(coord))
            elif type(selected_coord) == list:
                # select by range
                i = [i for i in range(len(coord)) if selected_coord[0] <= coord[i] <= selected_coord[-1]]
            else:
                # find nearest value
                #print 'coord - selected_coord',coord,selected_coord,type(selected_coord)
                d = abs(coord - selected_coord);
                i = numpy.argmin(d)

            #print coord[i]
        else:
            i = list(range(len(coord)))

        #print i
        if isinstance(i,list):
            if len(i) == 1:
                # netCDF interface does not accepts list with single elements as
                # indices
                return (i[0],coord[i[0]])

        return (i,coord[i])

    def coord(self,name):
        if name == 'time' and self.hr_time:
            if self.tname:
                return self.hr_t
            else:
                return None
        else:
            return self._coord(name)


    def coord_range(self,name):
        '''returns a list of possible values for the coordinate. If one element is a tuple, then it mean (first,last,step)'''

        if name == 'time' and self.hr_time:
            if self.tname:
                return self.hr_t
            else:
                return None
        else:
            return self._coord_range(name)

    def _coord_range(self,name):
        c = self._coord(name)
        if c.ndim == 1:
            return c
        else:
            return [(int(c.min()),int(c.max()),1)]


    def _coord(self,name):
        """loads a coordinate: name can be "longitude", "latitude", "depth" or "time". """

        if name in self._coordinates:
            return self._coordinates[name]
        else:
            if name == "longitude":
                if not self.xname:
                    return numpy.array([])

                c = numpy.array(self.nc.variables[self.xname][:])
            elif name == "latitude":
                if not self.yname:
                    return numpy.array([])

                c = numpy.array(self.nc.variables[self.yname][:])
            elif name == "depth":
                c = self.loadzcoord()

            elif name == "time":
                if not self.tname:
                    return numpy.array([])

                c = numpy.array(self.nc.variables[self.tname][:])

            self._coordinates[name] = c
            return c

    def _coord_units(self,name,default=None):
        dims = {'longitude': self.xname,
                'latitude': self.yname,
                'depth': self.zname,
                'time': self.tname}

        nv = self.nc.variables[dims[name]]
        return getattr(nv, 'units', default)

    def coord_units(self,name,default=None):
        if name == 'time' and self.hr_time:
            if self.tname:
                return self.hr_tu
            else:
                return None
        else:
            return self._coord_units(name,default)

    def has_coord(self,name):
        '''returns True if variable the coordinate name. name can be
        'longitude', 'latitude', 'depth' or 'time'. '''

        dims = {'longitude': self.xname,
                'latitude': self.yname,
                'depth': self.zname,
                'time': self.tname}

        if not dims[name]:
            return False

        return True


    def _load(self,**kwargs):
        # treat coordinate equal to None as absent

        for name in ['longitude','latitude','depth','time']:
            if name in kwargs:
                if kwargs[name] == None:
                    del kwargs[name]


        x = self._coord('longitude')
        y = self._coord('latitude')

        vinterp = False

        # lon/lat selection

        if x.ndim == y.ndim == 1:
            (i,x) = self.selection_ind('longitude',self.xname,kwargs)
            (j,y) = self.selection_ind('latitude',self.yname,kwargs)

        elif x.ndim == y.ndim == 2:
            #print 'ij ',len(i),len(j)

            if 'longitude' in kwargs and 'latitude' in kwargs:
                xr = kwargs['longitude']
                yr = kwargs['latitude']


                if _len(xr) == 1:
                    d = (x - xr)**2 + (y - yr)**2
                    ind = d.argmin()
                    j,i = numpy.unravel_index(ind,x.shape)
                    i = numpy.array([i])
                    j = numpy.array([j])
                else:

                    j,i = numpy.nonzero(
                        numpy.logical_and(
                            numpy.logical_and(xr[0] <= x,x <= xr[-1]),
                            numpy.logical_and(yr[0] <= y,y <= yr[-1])
                            ))

                if len(i) == 0 or len(j) == 0:
                    i = j = x = y = numpy.array([])
                else:
                    i = numpy.arange(i.min(),i.max()+1)
                    j = numpy.arange(j.min(),j.max()+1)

                    #print 'ii ',len(i),len(j),x.shape,i.shape,xr,yr,i

                    # bug in numpy?
                    # x = x[j,i] does not work:
                    # ValueError: shape mismatch: objects cannot be broadcast to a single shape

                    x = x[j,:][:,i]
                    y = y[j,:][:,i]

                    #print 'i, j ',i,j,x,y

            else:
                i = range(numpy.size(x,1))
                j = range(numpy.size(x,0))


        else:
            raise BaseException('geodataset: dimension layout not supported: lon/lat %d,%d' % (x.ndim,y.ndim))


        if _len(i) == 0 or _len(j) == 0:
            v = numpy.array([])
            return (v,v,v,v,v)


        # time selection

        t = self._coord('time')

        if t.ndim == 1:
            (n,t) = self.selection_ind('time',self.tname,kwargs)
        else:
            raise BaseException('geodataset: dimension layout not supported: time %d' % (t.ndim))

# should we convert to MJD?
#        if t != None:
#            # convert to MJD
#            (f,t0) = util.time_units(self.tu)
#            print 't ',t
#            t = f*t + t0
#            print 't ',t


        #print 'n ',n,t,kwargs
        #if len(n) == 0:
        #    v = numpy.array([])
        #    return (v,v,v,v,v)


        # depth selection

        z = self._coord('depth')

        if z.ndim == 1:
            (k,z) = self.selection_ind('depth',self.zname,kwargs,self.loadzcoord)
        elif z.ndim == 3:
            #_z = z[:,j,i]
            # inconsistency in numpy?
            # zeros((10,4))[1,[]]  : works
            # zeros((10,4))[1,array([])]  : does not work

            k = range(z.shape[0])
            _z = z[:,j,:][:,:,i]

            if 'depth' in kwargs:
                z = kwargs['depth']
                vinterp = True
            else:
                z = _z

        else:
            raise BaseException('geodataset: dimension layout not supported: depth %d' % (z.ndim))


        #print >> sys.stderr, 'i,j,k,n ',self.varname,i,j,k,n,kwargs.get('longitude','no range')
        #sys.stderr.flush()

        try:
            if self.zname in self.dims and self.tname in self.dims:
                v = self.nv[n,k,j,i]
                # why is this necessary???
                #v = v.reshape((_len(n),_len(k),_len(j),_len(i)))

            elif self.tname in self.dims:
                v = self.nv[n,j,i]
            elif self.zname in self.dims:
                v = self.nv[k,j,i]
            else:
                v = self.nv[j,i]

        except IndexError as error:
            # maybe triggered if i or j is empty

            if False:
                print >> sys.stderr, 'i,j,k,n ',self.varname,i,j,k,n
                print >> sys.stderr, 'shape ',self.nv.shape
                out = str(error)
                out += '\n' + traceback.format_exc()
                print >> sys.stderr, 'geodataset ',out
                sys.stderr.flush()

            v = []

        fv = self.FillValue()
        # v might be already a numpy.ma.core.MaskedArray
        v = M.array(v, mask = v == fv, fill_value=fv)

        # not necessary anymore
        #if hasattr(self.nv,'scale_factor'):
        #    v = v * self.nv.scale_factor

        #if hasattr(self.nv,'add_offset'):
        #    v = v + self.nv.add_offset


        # interpolation

        if vinterp:
            #print 'v shape',v.shape,_z.shape,n,kwargs['time']
            sz = _z.shape
            v = v.reshape((_len(n),sz[0],sz[1],sz[2]))
            #print 'v shape',v.shape,_z.shape,n,kwargs['time']

            v = interpolation.vinterp(v,_z,z)

        #print 'v.shape ',v.shape,kwargs

        return (x,y,z,t,v)

    def _convert_time(self,hr_t):

        time = hr_t

        #print 'comp' ,hr_t == str(self.hr_t[0]),type(hr_t),str(self.hr_t[0]),self.time_mapping
        #print 'comp' ,hr_t

        if not self.tname:
            return None

        if hr_t == 'default':
            time = self.t[0]
        else:
            if self.time_mapping and self.hr_time:
                for (hr_ti,ti) in zip(self.hr_t,self.t):
                    if hr_ti == hr_t:
                        time = ti
                        break

        if isinstance(time,str):
            time = float(hr_t)

        #print 'time convert ',time,hr_t,type(time),self.hr_t
        return time


    def _convert_to_hrtime(self,t):

        hr_t = t

        #print 'comp' ,hr_t == str(self.hr_t[0]),type(hr_t),str(self.hr_t[0]),self.time_mapping
        #print 'comp' ,hr_t

        if not self.tname:
            return None

        if self.time_mapping and self.hr_time:
            for (hr_ti,ti) in zip(self.hr_t,self.t):
                if ti == t:
                    hr_t = hr_ti
                    break

        return hr_t


    def load(self,**kwargs):
        kw = kwargs.copy()
        #print 'kw1 ',kw,self.hr_t,self.t
        if 'time' in kw:
            if kw['time'] != None:
                kw['time'] = self._convert_time(kw['time'])
        #print('kw ',kw)

        (x,y,z,t,v) = self._load(**kw)

        try:
            # if iterable
            t = [self._convert_to_hrtime(time) for time in t]
        except TypeError:
            # not iterable
            t = self._convert_to_hrtime(t)

        return (x,y,z,t,v)

    def load_range(self, no_data_range = (0.,0.), **kwargs):
        ''' load a range of a variable '''

        #print("kwargs ",kwargs)
        if (kwargs.get('longitude',None) == None and kwargs.get('latitude',None) == None and kwargs.get('time',None) == None
            and kwargs.get('depth',None) == None):


            # load from attribute if present
            if hasattr(self.nv,'actual_range'):
                #print("load actual range")
                r = self.nv.actual_range
                return (r[0],r[1])
            else:
                #print("load per time slices")
                # load all per time slices
                def varrange(a):
                    return a.min(),a.max();

                ranges = [varrange(self.nv[n]) for n in range(self.nv.shape[0])]
                vmax = numpy.ma.array([_[1] for _ in ranges]).max()
                vmin = numpy.ma.array([_[0] for _ in ranges]).min()

                if numpy.ma.isMaskedArray(vmin):
                    return no_data_range
                else:
                    return (vmin,vmax)

        else:
            v = self.load(**kwargs)[-1]

            # handle case when all values are masked
            if numpy.ma.isMaskedArray(v):
                # print >> sys.stderr, "count ", v.count()
                # sys.stderr.flush()

                if v.count() == 0:
                    return no_data_range

            #print 'vmin ',v
            vmin = v.min()
            vmax = v.max()

            return (vmin, vmax)



class WRFDataset(GenericGeoDataset):
    def __init__(self,filename,varname, hr_time = True):
        super(WRFDataset,self).__init__(filename,varname,hr_time)
        self.gridname = '/home/abarth/geo_em.d01.nc'
        self.gridnc = netCDF4.Dataset(self.gridname,'r')

        Times = self.nc.variables['Times'][:]

        # time
        self.t = []
        # time units
        self.tu = 'days since 1858-11-17 00:00:00'

        for t in Times:
            s = ''.join(t)
            # args is vector of year,month,day,...
            args = [int(f) for f in re.split(re.compile('[-_:]'),s)]

            self.t.append(netCDF4.date2num(datetime.datetime(*args),self.tu))


        self.tname = 'Time'
        self.zname = 'bottom_top'

        # humand readable time
        self.hr_t = [util.gregd(t,format='%Y-%m-%dT%H:%M') for t in self.t]
        self.time_mapping = True
        self.hr_tu = 'yyyy-mm-ddTHH:MM'


    def __del__(self):
        super(WRFDataset,self).__del__()
        self.gridnc.close()


    @staticmethod
    def isvalid(filename):
        '''check if filename is a WRF file'''

        if sys.version_info[0] == 2:
            # python 2
            string_types = basestring
        else:
            # python 3
            string_types = str

        if isinstance (filename,string_types):
            nc = netCDF4.Dataset(filename,'r')
        else:
            nc = filename

        type = ''

        if hasattr(nc,'TITLE'):
            type = nc.TITLE

        if isinstance (filename,string_types):
            nc.close()

        return 'WRF' in type



    def _coord(self,name):
        if name == 'longitude':
            c = numpy.squeeze(self.gridnc.variables['XLONG_M'][:])
        elif name == 'latitude':
            c = numpy.squeeze(self.gridnc.variables['XLAT_M'][:])
        elif name == 'time':
            c = numpy.array(self.t)
        elif name == 'depth':
            n = 0
            PHB = self.nc.variables['PHB'][n,:,:,:]
            PH = self.nc.variables['PH'][n,:,:,:]
            g = 9.8
            H = (PH + PHB)/g

            # vertical staggering
            c = (H[:-1,:,:] + H[1:,:,:])/2

        return c


    def _coord_units(self,name,default=None):
        if name == 'depth':
            return 'm'

        return super(WRFDataset,self)._coord_units(name,default)



def GeoDataset(filename,varname, hr_time = True):

    if WRFDataset.isvalid(filename):
        return WRFDataset(filename,varname, hr_time)
    else:
        return GenericGeoDataset(filename,varname, hr_time)
