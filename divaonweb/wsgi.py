# Copyright (C) 2008-2010 Alexander Barth <barth.alexander@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from __future__ import absolute_import

import sys
import site
import os
import mimetypes

from divaonweb.divaserver import DivaServer, HttpResponse
from paste.request import parse_formvars

def application2(environ, start_response):
    status = '200 OK'
    output = 'Hello World!'

    response_headers = [('Content-type', 'text/plain'),
                        ('Content-Length', str(len(output)))]
    start_response(status, response_headers)

    return [output]




def make_app(global_conf, root, **kwargs):
    return DivaServer(root, **kwargs)
    #print >> sys.stderr, 'here make_app'
    #return application2

# application = DivaServer()

def run_test_server():
    from paste import httpserver
    basepath = '/home/abarth/workspace/test-server/'
    root = os.path.join(basepath,'data')
    kwargs = {'log': os.path.join(basepath,'divaonweb.log'),
              'bathdir': os.path.join(basepath,'DivaData'),
              'wms_url': 'http://localhost/web-vis-test/Python/web/wms?',
              'tmpdir': os.path.join(basepath,'tmp')}

    application = DivaServer(root, **kwargs)
    httpserver.serve(application, host='127.0.0.1', port='8080')

def profile_server():
    from repoze.profile.profiler import AccumulatingProfileMiddleware
    middleware = AccumulatingProfileMiddleware(
        application,
        log_filename='/tmp/divaserver.log',
        cachegrind_filename='/tmp/cachegrind.out.bar',
        discard_first_request=True,
        flush_at_shutdown=True,
        path='/__profile__'
        )

    from paste import httpserver
    httpserver.serve(middleware, host='127.0.0.1', port='8080')


if __name__ == '__main__':
    run_test_server()
    #profile_server()
