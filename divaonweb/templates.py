# Copyright (C) 2008-2010 Alexander Barth <barth.alexander@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#                                                                           
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import os
from paste.script import templates

class DivaServerTemplate(templates.Template):

    summary = "A Diva-on-web and OceanBrowser server deployed through paste.deploy"    
    _template_dir = 'paster_templates'

    def post(self, command, output_dir, vars):

        if command.verbose:
            print('*'*72)
            print('* Run "paster serve %s/server.ini" to run' % output_dir)
            print('* the Diva server on http://localhost:8080')
            print('*'*72)
