# Copyright (C) 2008-2010 Alexander Barth <barth.alexander@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#                                                                           
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pylab
import matplotlib

def plot_covafit(ax,covar_name,covarfit_name):

    covar = pylab.loadtxt(covar_name)
    covarfit = pylab.loadtxt(covarfit_name)    

    xmax = 1.2*max(covarfit[:,0])

    # limit data to xmax
    i = pylab.find(covar[:,0] > 1.1*xmax)[0]
    covar = covar[0:i,:]

    ax.plot(covar[:,0],covar[:,1],'b-',label='sampled covariance')

    x = pylab.concatenate((covar[:,0],covar[::-1,0]))
    y = pylab.concatenate((covar[:,1]-covar[:,3],covar[::-1,1] + covar[::-1,3]))

    ax.fill(x,y,facecolor='#BBBBFF',edgecolor="none",label="uncertainty")



    #ax.plot(covar[:,0],covar[:,1]+covar[:,3],'g',label='error interval on covariance')
    #ax.plot(covar[:,0],covar[:,1]-covar[:,3],'g')

    #ax.errorbar(covar[:,0],covar[:,1],yerr=covar[:,3])


    ax.plot(covarfit[:,0],covarfit[:,1],'bo-',label='covariance used for fitting',ms=3)
    ax.plot(covarfit[:,0],covarfit[:,2],'r',label='fitted Bessel covariance')

    #ax.grid(color='k');
    ax.axhline(y=0, linestyle=':',color='k')

    ax.set_xlabel('distance [degrees]')
    ax.set_ylabel('covariance')

    font = matplotlib.font_manager.FontProperties(size='medium');
    ax.legend(prop=font)

    #ax.set_ylim(-4,6)
    #ax.set_xlim(0,1.2*max(covarfit[:,0]))
    ax.set_xlim(0,xmax)


