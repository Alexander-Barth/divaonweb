# Copyright (C) 2008-2010 Alexander Barth <barth.alexander@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#                                                                           
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Export Diva analysis as static images"""

import sys

try:
    from matplotlib.toolkits.basemap import Basemap, shiftgrid
except ImportError:
    from mpl_toolkits.basemap import Basemap, shiftgrid

import pylab

try:
    import numpy.ma as M
except:
    import matplotlib.numerix.ma as M



def choose_bathymetry(bath,database,x0,x1,y0,y1):
    # determine which bathymetry to take
    bathymetry = bath[database][-1]["file"];

    res = min(x1-x0,y1-y0)/300
    for b in bath[database]:
        if res > b["res"]:
            return b

    return b
