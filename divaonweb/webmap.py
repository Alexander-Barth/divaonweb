#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2008-2014 Alexander Barth <barth.alexander@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''This module implements a Web Map Service'''

from __future__ import print_function
from __future__ import absolute_import

import hashlib
import random
import sys
import os
import string
import posixpath
from datetime import datetime
from wsgiref.handlers import format_date_time
import traceback
import netCDF4
import numpy
import zipfile
from PIL import Image, ImageDraw, ImageFont
import textwrap
import fcntl
import time
import re
import math
import json
from distutils.version import StrictVersion

try:
    from urllib.request import pathname2url
except ImportError:
    # python 2
    from urllib import pathname2url

try:
    import configparser
except ImportError:
    # python 2
    import ConfigParser as configparser


import matplotlib
import matplotlib.cbook
matplotlib.use('Agg')  # force the antigrain backend

try:
    from matplotlib.toolkits.basemap import Basemap
except ImportError:
    from mpl_toolkits.basemap import Basemap

import matplotlib.pyplot as plt

try:
    import numpy.ma as M
except:
    import matplotlib.numerix.ma as M

from io import BytesIO
import subprocess
import fnmatch
import unittest
import logging

try:
    from lxml.builder import E, ET
except ImportError:
    from lxml.builder import E
    from lxml import etree as ET

from lxml.etree import Element

from divaonweb import settings
from divaonweb import geodataset
from divaonweb.export_image import choose_bathymetry
from divaonweb.vertical_section import  vertical_section
from divaonweb.util import HttpResponse, select_multires, roundn, obsload, join_path, fromUnixTime, toUnixTime, parseHTTPdate, formatHTTPdate
from divaonweb import util


__author__ = 'Alexander Barth <a.barth@ulg.ac.be>'
__version__ = (1,9,1)

#escape_layer_names = True
escape_layer_names = False

# default encoding for netcdf files
netcdf_encoding = 'iso-8859-1'

# separator in layer name
#layersep = '#'
layersep = '*'

class UserCancel(Exception):
    '''class for exceptions trigged when the user wants to cancel.'''
    pass

class WMSError(Exception):
    '''class for exceptions trigged when WMS error occurs.'''
    pass



def WMSStyle(style):
    '''Parse style string'''

    if type(style) == dict:
        return style
    elif type(style) == str:
        s = {}

        if ':' in style:
            for arg in style.split('+'):
                key,val = arg.split(':')

                if key in ['vmin','vmax','scale','reduce','space']:
                    s[key] = float(val)
                elif key in ['ncontours','markersize']:
                    s[key] = int(val)
                elif key in ['inverted']:
                    s[key] = val.lower() == 'true'
                else:
                    s[key] = val
        else:
            all_styles = [suported_style[0] for suported_style in wms_canvas.supported_styles]
            all_styles.extend(["","scatter","max_contourf"])
            if style not in all_styles:
                raise WMSError("unsupported style '%s'" % (style,))

            s['method'] = style

        return s
    else:
        return [WMSStyle(s) for s in style]


def WMSStyleEncode(style):
    return '+'.join([key + ':' +str(style[key]) for key in style])

    # else type(style) == list:
    #     return ','.join([WMSStyleEncode(_) for _ in style])
    # else:
    #     return style



def list_param(style):
    params = [
        {'id': 'cmap',
         'type': 'imageselector',
         'label': 'Colormap',
         'options': [('jet','img/mini_colormap_jet.png'),
                     ('hsv','img/mini_colormap_hsv.png'),
                     ('gray','img/mini_colormap_gray.png'),
                     ('RdBu','img/mini_colormap_RdBu.png'),
                     ('Paired','img/mini_colormap_Paired.png'),
                     ]},

        {'id': 'inverted',
         'type': 'boolean',
         'label': 'Invert colormap'},

        {'id': 'method',
         'type': 'string',
         'label': 'Plotting style',
         'options': [('pcolor_flat','Flat shading'),
                     ('contourf','Filled contours'),
                     ('contour','Contours'),
                    ],
         'description': 'Flat shading will produce interpolated colors'},

        {'id': 'scale',
         'type': 'float',
         'label': 'Scale',
         'description': 'Length in pixel of a vector of length 1'},

        {'id': 'space',
         'type': 'float',
         'label': 'Space',
         'description': 'space in pixels between arrows (order of magnitude)'},

        {'id': 'color',
         'type': 'color',
         'label': 'Color',
         'options': [('k','black'),
                     ('r','red'),
                     ('g','green'),
                     ('b','blue'),
                     ('c','cyan'),
                     ('m','magenta'),
                     ('y','yellow'),
                    ]},

        {'id': 'markerfacecolor',
         'type': 'color',
         'label': 'Color',
         'options': [('k','black'),
                     ('r','red'),
                     ('g','green'),
                     ('b','blue'),
                     ('c','cyan'),
                     ('m','magenta'),
                     ('y','yellow'),
                    ]},

        {'id': 'markeredgecolor',
         'type': 'color',
         'label': 'edge color',
         'options': [('k','black'),
                     ('r','red'),
                     ('g','green'),
                     ('b','blue'),
                     ('c','cyan'),
                     ('m','magenta'),
                     ('y','yellow'),
                    ]},

        {'id': 'markersize',
         'type': 'int',
         'label': 'Marker size'},

        {'id': 'marker',
         'type': 'color',
         'label': 'marker',
         'options': [('D','diamont'),
                     ('s','square'),
                    ]},


        {'id': 'vmin',
         'type': 'float',
         'label': 'Minimum color-bar range'},

        {'id': 'vmax',
         'type': 'float',
         'label': 'Maximum color-bar range'},

        {'id': 'ncontours',
         'type': 'int',
         'label': 'Number of contour-lines'},


        ]


    out = []

    for p in params:
        k = p['id']
        if k in style:


            attrib = 'id="%s" type="%s" default="%s" label="%s" ' % (k,p['type'],str(style[k]),p['label'])

            if 'description' in p:
                attrib += ' description="%s"' % (p['description'])

            out += ['<param %s>' % (attrib)]


            if 'options' in p:
                for val, label in p['options']:
                    out += ['<option value="%s">%s</option>' % (val,label) ]

            out += ['</param>']


    return out



def get_colormap(style):
    '''Get Matplotlib colormap from style'''

    # lookup colormap

    inverted = style.get('inverted',False)
    cmap_name = style.get('cmap', 'jet')

    if inverted:
        cmap_name += '_r'

    try:
        cmap = matplotlib.cm.__getattribute__(cmap_name)
        assert isinstance(cmap, matplotlib.colors.LinearSegmentedColormap)
    except (AttributeError, AssertionError):
        raise WMSError("unknown colormap (cmap=%s) " % cmap_name)

    return cmap


# translate mimetype format in to a file extension for matplotlib
def get_mpl_format(imgformat):

    mpl_format = [ i[1] for i in wms_canvas.supported_formats
                       if i[0] == imgformat ]

    if len(mpl_format) == 0:
        raise WMSError('InvalidFormat', 'supported formats are: ' +
                       ', '.join([ i[0]
                                   for i in wms_canvas.supported_formats]))

    return mpl_format[0]



def time_modified(root,layer):
    '''return the time when the underlying files where modified last time. If a layer depends on multiple files then the newest time is choosen.'''

    if layer.startswith('vector:'):
        (type,layer_u,layer_v) = layer.split(':')
        [fieldname_u, varname_u] = layer_u.split(layersep)
        [fieldname_v, varname_v] = layer_v.split(layersep)

        filename_u = join_path(root, fieldname_u)
        filename_v = join_path(root, fieldname_v)

        return math.floor(max(
            os.path.getctime(filename_u),
            os.path.getctime(filename_v))
        )

    # all other layers depend only on one file

    if layer.startswith('point:'):
        [fieldname, varname] = layer.replace('point:','').split(layersep)
        filename = join_path(root, fieldname)
    elif layer.find(layersep) > 0:
        [fieldname, varname] = layer.split(layersep)
        filename = join_path(root, fieldname)
    else:
        filename = join_path(root, layer + '.dat')

    return math.floor(os.path.getctime(filename))

def layers_time_modified(root,layers):
    return max([time_modified(root,layer) for layer in layers])

class WMSList():
    """a list for WMS dimensions, possibly stored in compressed form
    r = [1,2,3]
    r = [(0,10,2)] # from 0 to 10 by step 2
    r = [1,2,(0,6,2)] # [1,2,0,2,4,6]
"""
    def __init__(self,r):
        self.r = r

    def expand(self):
        l = []
        for item in self.r:
            if isinstance(item,tuple):
                # start, end, step
                # inclusive end
                if len(item) == 3:
                    l += range(item[0],item[1]+item[2],item[2])
                else:
                    raise WMSError('cannot expand continuous range')
            else:
                l += [item]

        return l

    def __str__(self):
        vals = []

        for item in self.r:
            if isinstance(item,tuple):
                if len(item) == 3:
                    vals.append('%s/%s/%s' % (str(item[0]),str(item[1]),str(item[2])))
                else:
                    vals.append('%s/%s' % (str(item[0]),str(item[1])))
            else:
                vals.append(str(item))

        return ','.join(vals)

    def min(self):
        return min(self.expand())

    def max(self):
        return max(self.expand())

    def closestTo(self,val):
        close = {}

        # http://stackoverflow.com/questions/11987358/why-nested-functions-can-access-variables-from-outer-functions-but-are-not-allo
        def check(v):
            if 'val' not in close or abs(v - val) < close['dist']:
                close['val'] = v
                close['dist'] = abs(v - val)

        for item in self.r:
            if isinstance(item,tuple):
                minval = item[0]
                maxval = item[1] # might be more complicated !!!

                if minval <= val <= maxval:
                    return val

                check(minval)
                check(maxval)
            else:
                check(item)

        return close['val']

#        tmp = numpy.array(self.expand())
#        index = abs(tmp - val).argmin()
#        return tmp[index]

    def __getitem__(self,index):
        """indexing self[index]"""
        return self.expand()[index]

    def first(self):
        item = self.r[0]

        if isinstance(item,tuple):
            return item[0]
        else:
            return item

class layer_description(object):
    '''metadata of layer from which WMS GetCapabilities request or a KML file
    can be formed'''

    def __init__(self, name, title, x0, x1, y0, y1, time, time_units,
                 elevation, elevation_units, attribution, legend, wms_url, abstract=None,
                 metadata_url=[], default_time = None,
                 identifier = None, identifier_doi = None,
                 styles = None):
        self.name = name
        self.title = title
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1
        self.t = time
        self.z = elevation
        self.tu = time_units
        self.zu = elevation_units
        self.attribution = attribution
        self.legend = legend
        self.wms_url = wms_url
        self.abstract = abstract
        self.metadata_url = metadata_url
        self.identifier = identifier
        self.identifier_doi = identifier_doi
        self.queryable = True
        self.default_time = default_time
        #print('default_time',default_time, file=sys.stderr)


        if styles == None:
            self.styles = wms_canvas.supported_styles
        else:
            self.styles = styles

    def wmscap_dim(self, version, t, dimname, units):
        '''return a description of a dimensions'''

        # if empty t array then return empty string
        if len(t)==0:
            return ''

        wmslist = WMSList(t)

        if dimname == 'elevation':
            # closest value to zero (surface)
            default_val = wmslist.closestTo(0)
        else:
            if self.default_time != None:
                default_val = self.default_time
            else:
                # first value for time
                default_val = wmslist.first()

        default_val = str(default_val)

        if version == '1.1.1':
            dim = ['<Extent name="%s" default="%s">' % (dimname, default_val),
                   str(wmslist),
                   '</Extent>\n']
        else:
            dim = ['<Dimension name="%s" units="%s" default="%s">' %
                   (dimname, units, default_val),
                   str(wmslist),
                   '</Dimension>\n']


        return dim

    def wmscap(self, env, version='1.3.0'):
        '''Return a description of the layer (WMS getCapabilities)'''

        wms_url = util.getWMSURL(self.wms_url,env)
        if '?' not in wms_url:
            wms_url = wms_url + '?'

        if self.queryable:
            out  = ['<Layer queryable="1">\n']
        else:
            out  = ['<Layer>\n']


        # previous version use URL encoding for the layer name
        # this has changed
        if escape_layer_names:
            out += ['<Name>%s</Name>\n' % pathname2url(self.name)]
        else:
            out += ['<Name>%s</Name>\n' % self.name]

        out += ['<Title><![CDATA[%s]]></Title>\n' % self.title]

        if self.abstract:
            out += ['<Abstract><![CDATA[%s]]></Abstract>\n' % self.abstract]

        bbox = (self.x0, self.y0, self.x1, self.y1)
        if version == '1.1.1':
            out += ['<SRS>EPSG:4326</SRS>\n']
            out += ['<LatLonBoundingBox minx="%g" miny="%g" maxx="%g" maxy="%g" />\n' % bbox]
            out += ['<BoundingBox SRS="EPSG:4326" minx="%g" miny="%g" maxx="%g" maxy="%g" />\n' % bbox]
        else:
            out += ['<CRS>CRS:84</CRS>\n']
            out += ['<CRS>EPSG:4326</CRS>\n']
            out += ['<CRS>EPSG:54004</CRS>\n']
            out += ['<CRS>EPSG:3857</CRS>\n']
            out += ['''
              <EX_GeographicBoundingBox>
                <westBoundLongitude>%g</westBoundLongitude>
                <eastBoundLongitude>%g</eastBoundLongitude>
                <southBoundLatitude>%g</southBoundLatitude>
                <northBoundLatitude>%g</northBoundLatitude>
              </EX_GeographicBoundingBox>
        ''' % (self.x0, self.x1, self.y0, self.y1)]
            out += ['<BoundingBox CRS="CRS:84" minx="%g" miny="%g" maxx="%g" maxy="%g" />\n' % bbox]
            # note x and y are reversed for EPSG:4326 for 1.3.0!
            # see http://gher-diva.phys.ulg.ac.be/web-vis-test/Python/web/bluemarble?request=GetCapabilities&service=WMS&version=1.3.0&_proxy_url=http%3A%2F%2Fgeo.vliz.be%2Fgeoserver%2Fwms
            out += ['<BoundingBox CRS="EPSG:4326" miny="%g" minx="%g" maxy="%g" maxx="%g" />\n' % bbox]


        if version == '1.1.1':
            if len(self.t) > 0:
                out += ['<Dimension name="%s" units="%s" />'  % ('time', self.tu)]
            if len(self.z) > 0:
                out += ['<Dimension name="%s" units="%s" />'  % ('elevation', self.zu)]

        # time and elevation dimension
        out.extend(self.wmscap_dim(version, self.t, 'time', self.tu))
        out.extend(self.wmscap_dim(version, self.z, 'elevation', self.zu))

        if 'title' in self.attribution:
            out += ['<Attribution>']
            out += ['<Title>' + self.attribution['title'] + '</Title>\n']

            if 'url' in self.attribution:
                out += ['''
                <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink"
                                xlink:type="simple"
                    xlink:href="%s" />\n''' % self.attribution['url']]

            if 'logo' in self.attribution:
                out += ['''
                <LogoURL width="126" height="92">
                  <Format>image/gif</Format>
                  <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink"
                                  xlink:type="simple"
                                  xlink:href="%s" />
                </LogoURL>\n''' % self.attribution['logo']]

            out += ['</Attribution>\n']

        # URL for meta data

        for metadata_url in self.metadata_url:
#             out += ['''
#         <MetadataURL type="TC211">
#             <Format>text/html</Format>
#             <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink"
#              xlink:type="simple"
#              xlink:href="%s" />
#          </MetadataURL>''' % self.metadata_url]



            xlink_ns = 'http://www.w3.org/1999/xlink'
            NSMAP = {"xlink" : xlink_ns}

            MetadataURL = E.MetadataURL(type=metadata_url["type"])
            MetadataURL.append(E.Format(metadata_url["format"]))

            OnlineResource = Element("OnlineResource", nsmap = NSMAP)
            OnlineResource.attrib['{%s}type' % xlink_ns] = 'simple'
            OnlineResource.attrib['{%s}href' % xlink_ns] = metadata_url["url"]
            MetadataURL.append(OnlineResource)

            metadata_xml = ET.tostring(MetadataURL, pretty_print=True)

            if sys.version_info[0] == 3:
                metadata_xml = metadata_xml.decode("utf-8")

            out += [metadata_xml]


        if self.identifier:
            out += ['''<Identifier authority="seadatanet"><![CDATA[%s]]></Identifier>''' % self.identifier]

        if self.identifier_doi:
            out += ['''<Identifier authority="doi"><![CDATA[%s]]></Identifier>''' % self.identifier_doi]

        # URL for data download (netCDF files)

        if layersep in self.name:
            path = self.name.split(layersep)[0]

            if settings.http_baseurl:
                out += ['''
            <DataURL>
                <Format>application/x-netcdf</Format>
                <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink"
                 xlink:type="simple"
                 xlink:href="%s" />
             </DataURL>''' % (settings.http_baseurl + path)]

            if settings.opendap_baseurl:
                out += ['''
            <DataURL>
                <Format>text/html</Format>
                <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink"
                 xlink:type="simple"
                 xlink:href="%s" />
             </DataURL>''' % (settings.opendap_baseurl + path + '.html')]


        # style and legend

        if self.name.startswith('point:'):
            out += ['<Style><Name>%s</Name>\n' % 'scatter']
            out += ['<Title>%s</Title>\n' % 'scatter']
            out += ['</Style>\n']
        elif self.legend:
            for style in self.styles:
                self.legend['style'] = style[0]

                # must use semicolones instead of ampersand for XML validity
                legend_url = wms_url + 'request=GetLegendGraphic;service=WMS;version=1.3.0;' + \
                    ';'.join([k + '=' + pathname2url(str(v))  for k, v in self.legend.items()])

                out += ['<Style><Name>%s</Name>\n' % self.legend['style']]
                out += ['<Title>%s</Title>\n' % style[1]]

                out += ['<LegendURL width="%g" height="%g">\n' % (
                    self.legend['width'], self.legend['height'])]
                out += ['<Format>%s</Format>\n' % self.legend['format']]
                out += ['''<OnlineResource
                         xmlns:xlink="http://www.w3.org/1999/xlink"
                         xlink:type="simple"
                         xlink:href="%s" />\n
                      </LegendURL>\n</Style>\n''' % legend_url]
        else:
            # for observations
            # should be better way
            out += ['<Style><Name>%s</Name>\n' % 'scatter']
            out += ['<Title>%s</Title>\n' % 'scatter']
            out += ['</Style>\n']



        out += ['</Layer>']
        return out

    def kml(self, env):
        '''Return a description of the layer (KMS)'''

        wms_url = util.getWMSURL(self.wms_url,env)
        bbox = (self.x0, self.y0, self.x1, self.y1)

        kml  = '<Folder>\n'
        kml += '<name>' + self.title + '</name>\n'
        kml += '<visibility>0</visibility>\n'

        for z in self.z[::-1]:
#        for z in self.z[-2:-1]:
            req = {'request': 'GetMap',
                   'layers': self.name,
                   'styles': '',
                   'bbox': ','.join([str(b) for b in bbox]),
                   'format': 'image/png',
                   'version': '1.3.0',
                   'crs': 'CRS:84',
                   'elevation': z}

            url = wms_url + \
                ';'.join([k + '=' + pathname2url(str(v))  for k, v in req.items()])


            kml += '<GroundOverlay>\n'
            kml += '<name>%s (%g %s)</name>\n' % (self.title, z, self.zu)
            kml += '<description>' + self.title + '</description>\n'
            kml += '<altitudeMode>absolute</altitudeMode>\n'
            kml += '<altitude>%g</altitude>\n' % z

            kml += '<Icon><href>%s</href></Icon>\n' % url
            kml += '''
                  <LatLonBox>
                    <north>%.6f</north>
                    <south>%.6f</south>
                    <east>%.6f</east>
                    <west>%.6f</west>
                    <rotation>0</rotation>
                  </LatLonBox>
              ''' % (self.y1, self.y0, self.x1, self.x0)
            kml += '</GroundOverlay>\n'

        if self.legend:
            # must use semicolones instead of ampersand for XML validity
            legend_url = wms_url + 'request=GetLegendGraphic;service=WMS;version=1.3.0;' + \
                ';'.join([k + '=' + pathname2url(str(v))  for k, v in self.legend.items()])

            kml += '''
                  <ScreenOverlay>
                    <name>Colorbar</name>
                    <description>Colorbar</description>
                    <Icon>
                      <href>%s</href>
                    </Icon>
                    <overlayXY x="0" y="1" xunits="fraction" yunits="fraction"/>
                    <screenXY x="0" y="1" xunits="fraction" yunits="fraction"/>
                    <rotationXY x="0" y="0" xunits="fraction" yunits="fraction"/>
                    <size x="0" y="0" xunits="fraction" yunits="fraction"/>
                  </ScreenOverlay>\n''' % legend_url

        kml  += '</Folder>\n'

        return kml


class MemCache(object):
    def __init__(self):
        self._c = {}
        self._list = []
        self.max = 10

    def get(self,fun,s,*args,**kwargs):

        #s = str((args,kwargs))

        if s in self._c:
            res = self._c[s]
            print('from cache ',s)
        else:
            print('not in cache ',s)
            res = fun(*args,**kwargs)
            self._c[s] = res
            self._list.append(s)


        if len(self._list) > max:
            # get 'oldest' element
            key = self._list.pop(0)
            print('delete ',key)
            del self._c[key]

        return res

#MCache = MemCache()

class WMSField(object):
    SCALAR = 0
    VECTOR = 1

    def __init__(self,layer,root):
        self.layer = layer
        self.root = root

        if layer.startswith('vector:'):
            self.type = self.VECTOR
        else:
            self.type = self.SCALAR


    def hsection(self,elevation=None,time=None):
        return WMSHSection(self,elevation,time)

    def vsection(self,section,time=None):
        return WMSVSection(self,section,time)


class WMSSection(object):
    def get_layer_range(self):
        '''Maximum and minimum of layer from the WMS server'''

        if self.field.layer.startswith('point:'):
            return (-1,1)

        return self._scalar_load_range()

    def _scalar_load_range(self):
        """load range of a scalar (min and max)"""

        v = self._scalar_load()[-1]

        # handle case when all values are masked
        if M.isMaskedArray(v):
            # print >> sys.stderr, "count ", v.count()
            # sys.stderr.flush()

            if v.count() == 0:
                return (0.,0.)

        vmin = v.min()
        vmax = v.max()

        return (vmin, vmax)


    def scalar_default_style(self,style = {}):
        '''normalize style for scalar fields and add possibly missing fields using default values'''

        if 'method' not in style:
            style['method'] = 'pcolor_flat'

        if 'ncontours' not in style:
            style['ncontours'] = 40

        if 'inverted' not in style:
            style['inverted'] = False

        if 'cmap' not in style:
            style['cmap'] = 'jet'

        vmin = style.get('vmin', None)
        vmax = style.get('vmax', None)


        # if colorbar-range is not defined take the full range

        if (vmin == None or vmax == None) and style['method'] != 'scatter':
            (vmin, vmax) = self.get_layer_range()

        if 'vmin' not in style:
            style['vmin'] = vmin

        if 'vmax' not in style:
            style['vmax'] = vmax


    def vector_default_style(self, style = {}):
        '''normalize style for vector fields and add possibly missing fields using default values'''

        # number of pixels for a vector of length 1
        if 'scale' not in style:
            #domain_size = numpy.sqrt((x1-x0)**2 + (y1-y0)**2)

            (x, y, z, t, vel) = self._vector_load()
            u = vel[0]
            v = vel[1]

            norm = u**2 + v**2
            valid = numpy.logical_not(norm.mask)
            norm[valid] = numpy.sqrt(norm[valid])

            # produces RuntimeWarning: invalid value encountered in sqrt
            # http://modb.oce.ulg.ac.be/mediawiki/index.php/Prgramming_oddities#Python
            # norm = numpy.sqrt(u**2 + v**2)

#            import pdb
#            pdb.set_trace()
            max_vel = norm.max()
            #print 'max_vel ',max_vel

            # determine from data???
            #max_vel = 1
            # 20 pixel for maximum velicity
            style['scale'] = 200 * 20 / max_vel

        if 'color' not in style:
            #style['color'] = 'k'
            style['color'] = 'm'

        # space in pixels between arrows (order of magnitude)
        if 'space' not in style:
            style['space'] = 10

        #print "style",style

    def point_default_style(self,style):
        defparam = {'markersize': 7,
                    'marker': 'D',
                    'markerfacecolor': 'c',
                    'markeredgecolor': 'k'}

        for k in defparam.keys():
            if k not in style:
                style[k] = defparam[k]

        # style can be {'method': 'scatter'}
        # remove method
        if "method" in style:
            del style["method"]

    def default_style(self,style = {}):
        if self.field.type == self.field.SCALAR:
            if self.field.layer.startswith('point:'):
                return self.point_default_style(style)
            else:
                return self.scalar_default_style(style)
        else:
            return self.vector_default_style(style)

    def _load(self,*args,**kwargs):
        if self.field.type == self.field.SCALAR:
            res = self._scalar_load(*args,**kwargs)
        else:
            res = self._vector_load(*args,**kwargs)

        #print 'request in _load' ,self.what()

        return res


    def load(self,*args,**kwargs):
        #key = str((self.what(),args,kwargs))
        #return MCache.get(self._load,key,*args,**kwargs)
        return self._load(*args,**kwargs)

class WMSHSection(WMSSection):
    def __init__(self,field,elevation=None,time=None):
        self.field = field
        self.elevation = elevation
        self.time = time

    def what(self):
        return {'layer': self.field.layer, 'elevation': self.elevation, 'time': self.time}

    def _scalar_load_range(self, longitude=None, latitude=None, maxdist=None):
        layer = self.field.layer
        [fieldname, varname] = layer.split(layersep)
        filename = join_path(self.field.root, fieldname)

        an = geodataset.GeoDataset(filename, varname)
        return an.load_range(longitude=longitude, latitude=latitude,
                                      depth=self.elevation, time=self.time)

    def _scalar_load(self, longitude=None, latitude=None, maxdist=None):
        '''Load a layer from the WMS server'''

        layer = self.field.layer

        if layer.startswith('point:'):
            [fieldname, varname] = layer.replace('point:','').split(layersep)
            filename = join_path(self.field.root, fieldname)

            nc = netCDF4.Dataset(filename, 'r')
            x = numpy.array(nc.variables['obslon'][:])
            y = numpy.array(nc.variables['obslat'][:])
            z = -numpy.array(nc.variables['obsdepth'][:])
            t = numpy.array(nc.variables['obstime'][:])
            ids = nc.variables['obsid'][:]
            #print 'ids ',ids.shape,ids[1,:]
            tunits = nc.variables['obstime'].units

            # convert to mjd
            (f,t0) = util.time_units(tunits)
            t = f*t + t0

            if self.elevation != None:
                #depth_tol = 1e-5
                depth_tol = 1.
                sel = numpy.abs(z - self.elevation) < depth_tol
            else:
                sel = numpy.ones(x.shape,dtype=bool)

            if self.time != None:
                # time of climatology
                climt = numpy.array(nc.variables['time'][:])

                (climt_hr,time_units,time_mapping) = util.timemap(
                        climt,
                    nc.variables['time'].units,
                    hasattr(nc.variables['time'],'climatology'))

                time_index = climt_hr.index(self.time)

                sel_time = util.timeselect(nc,t,time_index)

                sel = numpy.logical_and(sel,sel_time)
                #print(sel,numpy.sum(sel))

            x = x[sel]
            y = y[sel]
            z = z[sel]
            t = t[sel]
            ids = ids[sel,:]

            if longitude != None and latitude != None and x.size != 0:
                cindex,cx,cy = util.closest(x,y,longitude,latitude,maxdist)
                cindex = [cindex]
                #print('toto',x[cindex])
                return (x[cindex], y[cindex], z[cindex], t[cindex], ids[cindex,:])
            else:
                return (x, y, z, t, ids)


        elif layer.find(layersep) > 0:
            [fieldname, varname] = layer.split(layersep)
            filename = join_path(self.field.root, fieldname)

            an = geodataset.GeoDataset(filename, varname)
            (x, y, z, t, v) = an.load(longitude=longitude, latitude=latitude,
                                      depth=self.elevation, time=self.time)

            return (x, y, z, t, v)

        else:
            fname = join_path(self.field.root, layer + '.dat')
            data = obsload(fname)
            x = data[:, 0]
            y = data[:, 1]
            v = data[:, 2]

            return (x, y, None, None, v)


    def _vector_load(self):
        layer = self.field.layer

        try:
            (type,layer_u,layer_v) = layer.split(':')
            [fieldname_u, varname_u] = layer_u.split(layersep)
            [fieldname_v, varname_v] = layer_v.split(layersep)
        except ValueError:
            raise WMSError('Invalid layer (layer=%s)' %
                           layer)

        filename_u = join_path(self.field.root, fieldname_u)
        uds = geodataset.GeoDataset(filename_u, varname_u)
        (ux, uy, uz, ut, u) = uds.load(depth=self.elevation, time=self.time)

        filename_v = join_path(self.field.root, fieldname_v)
        vds = geodataset.GeoDataset(filename_v, varname_v)
        (vx, vy, vz, vt, v) = vds.load(depth=self.elevation, time=self.time)

        #print 'vx.shape',vx.shape,vx.ndim

        if vx.ndim == vy.ndim == 1:
            [ux,uy] = numpy.meshgrid(ux,uy)
            [vx,vy] = numpy.meshgrid(vx,vy)

        if ux.shape != vx.shape:
            # assume C-grid
            x   = (ux[:-1,:] + ux[1:,:])/2
            y   = (uy[:-1,:] + uy[1:,:])/2

            if u.ndim == 2:
                u_r = (u[:-1,:] + u[1:,:])/2
                v_r = (v[:,:-1] + v[:,1:])/2
            elif u.ndim == 3:
                u_r = (u[:,:-1,:] + u[:,1:,:])/2
                v_r = (v[:,:,:-1] + v[:,:,1:])/2
            elif u.ndim == 4:
                u_r = (u[:,:,:-1,:] + u[:,:,1:,:])/2
                v_r = (v[:,:,:,:-1] + v[:,:,:,1:])/2
        else:
            # assume A-grid
            x = ux
            y = uy
            u_r = u
            v_r = v

        #print ' x',x[0,0],x[1,0]

        # rotate vector for curvilinear grid
        a = 0

        #print 'u_r.shape,v_r.shape',u_r.shape,v_r.shape,x.shape,y.shape
        ux =  numpy.cos(a) * u_r - numpy.sin(a) * v_r;
        vx =  numpy.sin(a) * u_r + numpy.cos(a) * v_r;


        return (x,y,None,None,(ux,vx,None))



class WMSVSection(WMSSection):
    def __init__(self,field,section,time = None):
        self.field = field
        self.section = section
        self.time = time
        self.tmpdir = '/tmp'

    def what(self):
        return {'layer': self.field.layer, 'section': self.section, 'time': self.time}

    def _scalar_load(self):
        section = self.section
        time = self.time
        [fieldname, varname] = self.field.layer.split(layersep)

        sx = []
        sy = []
        for coord in section.split('|'):
            p = coord.split(',')
            sx.append(float(p[0]))
            sy.append(float(p[1]))

        section_name = 'section_' + section + '_' + varname + '_' + str(time) + '.nc'
        section_fullname = os.path.join(self.tmpdir,section_name)
        cache_section = False
        #cache_section = True

        if os.path.exists(section_fullname) and cache_section:
            f = open(section_fullname,'r')
            #self.log.info('get shared lock ' + section_fullname)
            fcntl.lockf(f,fcntl.LOCK_SH)
            #self.log.info('got shared lock ' + section_fullname)


            # load section from disk
            nc = netCDF4.Dataset(section_fullname, 'r')

            secz = numpy.array(nc.variables['depth'][:])
            distance = numpy.array(nc.variables['distance'][:])
            sec = numpy.array(nc.variables['section'][:])
            #fv = getattr(nc.variables['section'], '_FillValue')
            fv = 99999
            sec = M.masked_where(sec == fv, sec)

            fcntl.lockf(f,fcntl.LOCK_UN)
            f.close()
        else:
            #self.log.info('creating ' + section_fullname)
            # calculate section
            an = geodataset.GeoDataset(os.path.join(self.field.root, fieldname),
                                       varname)
            (x, y, z, dummyt, v) = an.load(time=time)

            if x.ndim == 2:
                x = x[0,:]
                y = y[:,0]

            (sec, distance) = vertical_section(x, y, v, sx, sy)

            if z.ndim == 3:
                (secz, distance) = vertical_section(x, y, z, sx, sy)
                sz = secz.shape
                distance = distance.reshape((1,sz[1])).repeat(sz[0],0)
            else:
                secz = z

            fv = 99999
            sec.set_fill_value(fv)

            # test again if file exists?

            if not os.path.exists(section_fullname) and cache_section:
                try:
                    f = open(section_fullname,'w')
                    #self.log.info('get ex lock ' + section_fullname)
                    fcntl.lockf(f,fcntl.LOCK_EX)

                    # save section
                    nc = netCDF4.Dataset(section_fullname, 'w')
                    nc.createDimension('depth', sec.shape[0])
                    nc.createDimension('distance', sec.shape[1])

                    nc_section = nc.createVariable('section', 'd', ('depth', 'distance',))
                    #setattr(nc_section, '_FillValue', fv)

                    if secz.ndim == 1:
                        nc_depth = nc.createVariable('depth', 'd', ('depth',))
                        nc_distance = nc.createVariable('distance', 'd', ('distance',))
                    else:
                        nc_depth = nc.createVariable('depth', 'd', ('depth','distance'))
                        nc_distance = nc.createVariable('distance', 'd', ('depth','distance'))

                    #print 'secz.ndim ', secz.ndim,nc_depth.shape,secz.shape

                    nc_depth[:] = secz
                    nc_distance[:] = distance
                    nc_section[:] = sec.filled()
                    nc.close()

                    fcntl.lockf(f,fcntl.LOCK_UN)
                    f.close()
                except IOError:
                    print('unable to write %s to cache' % section_fullname, file=sys.stderr)

        vmin = sec.min()
        vmax = sec.max()

        # handle case when all values are masked
        if M.isMaskedArray(vmin):
            vmin = 0.
        if M.isMaskedArray(vmax):
            vmax = 0.

        #return (distance,secz,sec,vmin,vmax,section_name)
        return (distance,secz,sec)


def catalogue_url(id):
    return settings.catalogue_url.replace('$(ID)',pathname2url(id))

def catalogue_url_xml(id):
    return settings.catalogue_url_xml.replace('$(ID)',pathname2url(id))

class folder_description(object):
    '''Object contains all metadata of a variable'''

    def __init__(self, name=None, title=None,wms_url = '',log=None,authority=None):
        self.name = name
        self.title = title
        self.layers = []
        self.wms_url = wms_url;
        self.log = log
        self.authority = authority
        self.identifier = None
        self.identifier_doi = None

    def load_nc(self, basepath, path):
        '''load metadata from netCDF file'''

        vectors = []
        ignore_variables = []
        subfolders = []
        default_time = None

        filename = os.path.join(basepath, path)

        # read configuration file if present
        if os.path.exists(filename + '.config'):
            cfg = configparser.ConfigParser()

            # prevent converting key to lower-case
            cfg.optionxform = str

            cfg.read(filename + '.config')

            if cfg.has_option('main','ignore'):
                ignore_variables = cfg.get('main','ignore').split()

            if cfg.has_option('main','vectors'):
                vectors = [tuple(i.split(',')) for i in cfg.get('main','vectors').split()]

            if cfg.has_section('subfolders'):
                for name,val in cfg.items('subfolders'):
                    #print 'subfolders opt ',name,val
                    subfolders.append((name,val.split()))

            if cfg.has_option('main','default_time'):
                default_time = cfg.get('main','default_time')

        elif os.path.exists(filename + '.json'):
            with open(filename + '.json') as cfgfile:
                cfg = json.load(cfgfile)
                ignore_variables = cfg.get('ignore_variables',[])
                default_time = cfg.get('default_time',None)

                if 'vectors' in cfg:
                    vectors = [tuple(_) for _ in cfg['vectors']]

                if 'subfolders' in cfg:
                    subfolders = [(_['name'],_['variables']) for _ in cfg['subfolders']]


        #print('subfolders',subfolders, file=sys.stderr)

        subfolders_description = {}
        for title,vars in subfolders:
            subfolders_description[title] = folder_description(title=title,wms_url=self.wms_url,log = self.log)

        #self.log.info('opening %s ' % filename)
        nc = netCDF4.Dataset(filename, 'r')

        try:
            # try to make a list of variables names in same order than
            # NetCDF file

            try:
                d = {}
                for k, v in nc.variables.items():
                    d[v._varid] = k

                varids = d.keys()
                varids.sort()
                names = [d[id] for id in varids]
            except AttributeError:
                names = nc.variables.keys()


            attribution = {}

            try:
                attribution['title'] = getattr(nc, 'institution')
            except AttributeError:
                pass

            try:
                attribution['url'] = getattr(nc, 'institution_url')
            except AttributeError:
                pass

            try:
                attribution['logo'] = getattr(nc, 'institution_logo_url')
            except AttributeError:
                pass


            # get title (first global NetCDF parameter_keyword(s) attribute, then title,
            # then description, then filename)
            orig_title = getattr(nc, 'parameter_keyword',
                                 getattr(nc, 'parameter_keywords',
                                         getattr(nc, 'title',
                                                 getattr(nc, 'description', filename))))

            # strip "DIVA 4D analysis of "
            self.title = orig_title.replace('DIVA 4D analysis of ','')
            self.title = self.title.replace('DIVA 4D seasonal analysis of ','')
            self.title = self.title.replace('DIVAnd analysis of ','')
            self.title = self.title.replace('_',' ')

            self.name = None

            if hasattr(nc,'product_id'):
                self.identifier = nc.product_id
                metadata_url = [{
                    "type": "TC211",
                    "format": "text/html",
                    "url": catalogue_url(nc.product_id)
                }]

                if settings.catalogue_url_xml:
                    metadata_url.append({
                        "type": "TC211",
                        "format": "text/xml",
                        "url": catalogue_url_xml(nc.product_id)
                    })
            else:
                metadata_url = []

            if hasattr(nc,'doi'):
                self.identifier_doi = nc.doi

            vector_names = []
            scalar_names = []

            for name in names:
                if name in ignore_variables:
                    continue

                isobsid = name == 'obsid'
                # no time mapping for obsid
                hr_time = not isobsid
                vv = geodataset.GeoDataset(nc,name,hr_time)

                # keep only variables having a lon and lat dimension
                if vv.has_coord('longitude') and vv.has_coord('latitude'):

                    isvector = False
                    skip = False
                    layer_name = path + layersep + name

                    # ignore long_name if it is an empty string
                    v = vv.nv
                    title = getattr(v, 'long_name', '')
                    # replace _ by space
                    title = title.replace('_',' ')

                    for v in vectors:
                        if name == v[0] and v[1] in names:
                            isvector = True
                            layer_name = 'vector:' + path + layersep + v[0] + ':' + path + layersep + v[1]
                            title = re.sub('u-momentum component','momentum',title)
                            break
                        elif name == v[1] and v[0] in names:
                            skip = True
                            break

                    # skip this variables, it is part of a vector field
                    if skip:
                        continue

                    if isobsid:
                        layer_name = 'point:' + path + layersep + name
                        title = 'Interpolated observations'

                    x = vv.coord('longitude')
                    y = vv.coord('latitude')

                    if not title:
                        title = name

                    legend = {'width': 100, 'height': 300,
                              'format': 'image/png',
                              'layer': layer_name,
                              'style': ''
                              }

                    if hasattr(v,'units'):
                        legend['label'] = v.units

                    time = elevation = []
                    time_units = elevation_units = ''
                    abstract = []

                    if isobsid:
                        # for observation identifier use time axis of the climatology

                        (time,time_units,self.time_mapping) = util.timemap(
                            numpy.array(nc.variables['time'][:]),
                            nc.variables['time'].units,
                            hasattr(nc.variables['time'],'climatology'))

                    else:
                        if vv.has_coord('time'):
                            time = vv.coord_range('time')
                            time_units = vv.coord_units('time','ISO8601')

                    if isobsid:
                        # for observation identifier use depth axis of the climatology
                        elevation = -nc.variables['depth'][:]
                        elevation_units = vv.coord_units('depth','m')
                        #elevation = list(set(elevation))
                    else:
                        if vv.has_coord('depth'):
                            elevation = vv.coord_range('depth')
                            elevation_units = vv.coord_units('depth','m')


                    if hasattr(v,'units'):
                        units = v.units

                        if sys.version_info[0] == 2:
                            # python 2
                            if type(units) != unicode:
                                # before netCDF4 1.0
                                units = units.decode(netcdf_encoding)

                        abstract.append('Units: ' + units)

                    if orig_title.startswith('DIVA'):
                        abstract.append('Method: spatial interpolation produced with DIVA (Data-Interpolating Variational Analysis)')
                        abstract.append('URL: http://modb.oce.ulg.ac.be/DIVA')

                    if 'comment' in nc.ncattrs():
                        if nc.comment != 'No comment':
                            abstract.append('Comment: ' + nc.comment)

                    # concatenate
                    if len(abstract) > 0:
                        abstract = '\n'.join(abstract)
                    else:
                        abstract = None

                    x0 = x.min()
                    x1 = x.max()
                    y0 = y.min()
                    y1 = y.max()


                    ld = layer_description(layer_name, title, x0, x1, y0, y1,
                                           time, time_units, elevation,
                                           elevation_units, attribution, legend,
                                           self.wms_url,abstract,metadata_url,
                                           default_time = default_time
                    )

                    insubfolder = False
                    for title,vars in subfolders:
                        if name in vars:
                            # add to subfolder
                            subfolders_description[title].layers.append(ld)
                            insubfolder = True

                    # not a variable that must be added in subfolder, so add it to top-level
                    if not insubfolder:
                        self.layers.append(ld)

            # add a subfolder (if not empty) to list
            for title,vars in subfolders:
                if len(subfolders_description[title].layers) > 0:
                    self.layers.append(subfolders_description[title])

        except IOError as error:
            self.log.error('Error %s' % str(error))
            self.log.error(traceback.format_exc())
            self.log.error('unable to read file %s ' % filename)
            raise RuntimeError('unable to read %s' % filename)
        finally:
            nc.close()


    def valid(self, path, filemask = '*'):
        '''check if file or directory should be listed'''

        return (fnmatch.fnmatch(path, filemask) and
                not (path.endswith('.config') or path.endswith('.json')))  or os.path.isdir(path)

    def load_obs(self,basepath,epath):
        filename = os.path.join(basepath, epath)

        self.log.info('opening ' + filename)

        try:
            data = obsload(filename)
            x = data[:, 0]
            y = data[:, 1]
        #v = data[:, 2]

            legend = None
            attribution = {}

            ld = layer_description(epath[:-4], "Observation", x.min(), x.max(), y.min(), y.max(),
                                   [], '',
                                   [], '',
                                   attribution, legend,
                                   self.wms_url)
        except:
            raise RuntimeError('unable to read %s' % filename)

        return ld


    def load_dir(self, basepath, path='', filemask='*'):
        '''loads recursively all files maching filemask under basepath
        basepath remains constant during all recursive calls.
        subpath are added to the variable path
        '''

        p = os.path.join(basepath, path)

        if os.path.isdir(p):
            for entry in sorted(os.listdir(p)):
                epath = os.path.join(path, entry)

                if self.valid(epath, filemask):
                    try:
                        if epath.endswith('.dat'):
                            item = self.load_obs(basepath,epath)
                        else:
                            title = entry.replace('_',' ')
                            item = folder_description(title=title,wms_url=self.wms_url,log = self.log)
                            item.load_dir(basepath, epath, filemask)

                        self.layers.append(item)
                    # error in netCDF files may trigger RuntimeError
                    except RuntimeError as error:
                        self.log.error('Error %s' % str(error))
                        self.log.error(traceback.format_exc())
                        self.log.error('unable to read file %s ' % epath)

        else:
            # list only maching files

            if self.valid(path, filemask):
                try:
                    self.load_nc(basepath, path)
                except IOError as error:
                    self.log.error('Error %s' % str(error))
                    self.log.error(traceback.format_exc())
                    self.log.error('unable to read file %s ' % p)


    def wmscap(self, env, version):
        '''Return a description of directory (WMS getCapabilities)'''

        out  = ['<Layer>\n']

        if self.title:
            out += ['<Title>%s</Title>\n' % self.title]

        if self.authority:
            # is root layer
            out += ['''<AuthorityURL name="%s">
                <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink"
                                xlink:type="simple"
                    xlink:href="%s" /></AuthorityURL>\n''' % (self.authority['name'],self.authority['url'])]

        if self.identifier:
            out += ['''<Identifier authority="seadatanet"><![CDATA[%s]]></Identifier>''' % self.identifier]

        if self.identifier_doi:
            out += ['''<Identifier authority="doi"><![CDATA[%s]]></Identifier>''' % self.identifier_doi]


        if self.name:
            # previous version use URL encoding for the layer name
            # this has changed
            if escape_layer_names:
                # encode special characters such as # and spaces
                out += ['<Name>%s</Name>\n' % pathname2url(self.name)]
            else:
                out += ['<Name>%s</Name>\n' % self.name]

        for l in self.layers:
            out.extend(l.wmscap(env, version))

        out += ['</Layer>']
        return out

    def kml(self,env):
        '''Return a description of directory (KML)'''

        out  = '<Folder>\n'
        if self.title:
            out += '<name>%s</name>\n' % self.title
            out += '<description>%s</description>\n' % self.title

        for layer in self.layers:
            out += layer.kml(env)

        out += '<styleUrl>#radioFolder</styleUrl>\n'
        out += '</Folder>'
        return out


    def find_layer(self,name):
        '''recursively finds a layer by name'''

        for layer in self.layers:
            #print "layer ",layer.name
            if hasattr(layer,'find_layer'):
                tmp = layer.find_layer(name)
                if tmp:
                    return tmp
            elif layer.name == name:
                return layer

        return None



# Basic WMS class implements functionality shared by all WMS server

class base_webmapserver(object):
    '''Main Wep Map Server instance delegates the two request
    types: GetMap and GetCapabilities'''

    def __init__(self, fontfile = None, log = None):
        '''Initialize the WMS instance'''
        self.cap = None
        self.fontfile = fontfile
        self.fontsize = 10
        self.dpi = wms_canvas.dpi

        if log:
            self.log = log
        else:
            self.log = logging.Logger(self.__class__.__name__)
            self.log.addHandler(logging.StreamHandler(sys.stdout))


    def parse_args(self, kwargs):
        '''Parse and normalizes keyword arguments'''

        kw = {}
        # save original arguments for proxy
        kw['original_arguments'] = kwargs

        # parameters in GET and POST are not case sensitive

        for k in kwargs:
            kw[k.lower()] = kwargs[k]


        # parse bbox
        if 'bbox' in kw:
            try:
                kw['bbox'] = [float(s) for s in kw['bbox'].split(',')]
            except:
                raise WMSError('invalid bounding box (bbox=%s)'% kw['bbox'])

        # parse layers and styles
        if 'layers' in kw:
            kw['layers'] = kw['layers'].split(',')

            # handle old layer separation
            kw['layers'] = [l.replace('#',layersep) for l in kw['layers']]


            # parse styles
            try:
                styles = kw['styles']
            except:
                raise WMSError('no style')

            if styles:
                # if styles is not an empty string slit at commas
                kw['styles'] = styles.split(',')
            else:
                # if styles is an empty string, make a list of
                # empty string (one for each layer)
                # layers must be defined if styles is
                kw['styles'] = ['' for l in kw['layers']]

            kw['styles'] = WMSStyle(kw['styles'])

        if 'style' in kw:
            kw['style'] = WMSStyle(kw['style'])



        # ncWMS extension
        # http://www.resc.rdg.ac.uk/trac/ncWMS/wiki/WmsExtensions

        if 'colorscalerange' in kw:
            if kw['colorscalerange'] != 'auto':
                range = [float(s) for s in kw['colorscalerange'].split(',')]

                def set_range(s):
                    if 'vmin' not in s:
                        s['vmin'] = range[0]

                    if 'vmax' not in s:
                        s['vmax'] = range[1]


                if 'styles' in kw:
                    for s in kw['styles']:
                        set_range(s)

                if 'style' in kw:
                    set_range(kw['style'])

        # parse numerical arguments
        # make convertion of time?

        for arg in ['vmin', 'vmax', 'rate']:
            if arg in kw:
                try:
                    kw[arg] = float(kw[arg])
                except:
                    raise WMSError('invalid %s (%s=%s)'% (arg, arg, kw[arg]))


        arg = 'elevation'
        if arg in kw:
            try:
                if '/' in kw[arg]:
                    kw[arg] = [float(_) for _ in  kw[arg].split('/')]
                else:
                    kw[arg] = float(kw[arg])
            except:
                raise WMSError('invalid %s (%s=%s)'% (arg, arg, kw[arg]))

        # parse integer arguments (discart fractional part)

        for arg in ['width', 'height']:
            if arg in kw:
                try:
                    kw[arg] = int(float(kw[arg]))
                except:
                    raise WMSError('invalid %s (%s=%s)'% (arg, arg, kw[arg]))


        # parse boolean

        for arg in ['transparent', 'decorated']:
            if arg in kw:
                ls = kw[arg].lower()
                if ls in ['true', 'false']:
                    kw[arg] = ls == 'true'
                else:
                    raise WMSError('invalid %s (%s=%s)'% (arg, arg, kw[arg]))

        # Spatial Reference System

        if 'srs' in kw:
            kw['crs'] = kw['srs']

        return kw

    def request(self, **kwargs):
        '''Handles the requests for the WMS. Arguments are all parameters
        passed by the HTTP GET or POST request.'''


        kw = {}
        try:
            kw = self.parse_args(kwargs)

            service = kw.get("service","").lower()
            if  service != "wms":
                raise WMSError('unknown service (service=%s)' % service)

            language = kw.get("language","eng").lower()
            if  language != "eng":
                raise WMSError('unknown language (language=%s)' % language)

            if "request" not in kw:
                raise WMSError("unspecified request")

            request = kw['request']

            if request != "GetCapabilities" and "version" not in kw:
                raise WMSError('unspecified version')

            # default version
            version = '1.3.0'

            v = StrictVersion(kw.get('version', version))
            # version negotiation for GetCapabilities
            # see 6.2.4 Version number negotiation, page 10
            if v >= StrictVersion('1.3.0'):
                version = '1.3.0'
            elif v <= StrictVersion('1.1.1'):
                version = '1.1.1'
            else:
                version = v
            kw["version"] = version


            if request == 'GetMap':
                resp = self.get_map(**kw)
            elif request == 'GetCapabilities':
                resp = self.get_capabilities(**kw)
            elif request == 'GetKML':
                resp = self.get_kml(**kw)
            elif request == 'GetFeatureInfo':
                resp = self.get_feature_info(**kw)
            elif request == 'GetLegendGraphic'  \
                    or request == 'GetLegendRequest':
                resp = self.get_legend(**kw)
            elif request == 'GetStats':
                resp = self.get_stats(**kw)
            else:
                raise WMSError('unknown request (request=%s)' % request)

        except BaseException as error:

            if isinstance(error,WMSError):
                out = str(error)
            else:
                out = 'Operation not supported'


            self.log.error(traceback.format_exc())
            self.log.error('Error %s' % str(error))
            self.log.error('Arguments %s' % str(kwargs))

            version = kw.get("version","1.3.0")
            resp = self.error(out, version, kw)


        # remove optional parameters from mime-type
        # e.g. "text/xml; charset=utf-8" to "text/xml"
        ct = resp.content_type.split(';')[0]

        if ct not in ["image/png","text/xml","image/svg+xml","text/html"]:
            # propose browser to download
            import mimetypes
            mimetypes.add_type('image/eps','.eps')
            mimetypes.add_type('application/vnd.google-earth.kml','.kml')

            ext = mimetypes.guess_extension(ct)
            if not ext:
                self.log.warn("unknown mimtype %s " % ct)
            else:
                resp.headers["Content-Disposition"] = 'attachment;filename="seadatanet%s"' % ext

        return resp


    def error(self, s, version, kw):
        '''Format the error to be send to the client'''

        if 'exceptions' not in kw:
            # default exception type
            if version == '1.1.1':
                kw['exceptions'] = 'application/vnd.ogc.se_xml'
            else:
                kw['exceptions'] = 'text/xml'


        if (kw['exceptions'] == 'application/vnd.ogc.se_inimage' or
            kw['exceptions'] == 'application/vnd.ogc.se_blank'):

            width = kw.get('width',256)
            height = kw.get('height',256)

            # transparent background
            im = Image.new('RGBA', (width, height), (1,1,1,1))


            # write error message
            if kw['exceptions'] == 'application/vnd.ogc.se_inimage':
                d = ImageDraw.Draw(im)
                f = ImageFont.truetype(self.fontfile, self.fontsize)

                j = 0

                for line in s.splitlines():
                    for bline in textwrap.wrap(line, 50):
                        d.text((0, j),  bline, font=f, fill='black')
                        j += self.fontsize+2

            stream = BytesIO()
            im.save(stream, 'PNG')
            pic =  stream.getvalue()
            stream.close()

            return HttpResponse(pic,'image/png')


        out  = '''<ServiceExceptionReport version='%s'>''' % (version)
        out += '''<ServiceException><![CDATA[%s]]></ServiceException>''' % (s)
        out += '''</ServiceExceptionReport>'''

        return HttpResponse(''.join(out).encode('utf-8'),kw['exceptions'])


    def save_request(self, s, filename):
        '''performs a request and save the results to a file'''
        try:
            # new
            from urllib.parse import parse_qs
        except ImportError:
            # old
            from cgi import parse_qs

        if isinstance(s,str):
            # parse string if necessary
            kw = parse_qs(s)

            for k in kw:
                kw[k]=kw[k][0]

            print(kw)
        else:
            kw = s


        hr = self.request(**kw)
        print(hr.content_type)

        f = open(filename,"wb")
        f.write(hr.content)
        f.close()


    def get_capabilities(self, **kwargs):
        '''Return an inventory of all layers'''
        pass

    def get_legend(self, **kwargs):
        '''Produce a legend (e.g. colorbar)'''
        imgformat = kwargs.get('format', 'image/png')

        layer = kwargs.get('layer','')
        label = kwargs.get('label','')

        width = kwargs.get('width', 100)
        height = kwargs.get('height', 300)
        color = kwargs.get('color', '#000000')
        bgcolor = kwargs.get('bgcolor', 'none')
        transparent = kwargs.get('transparent', True)
        decorated = kwargs.get('decorated', True)
        style = kwargs.get('style',{})
        fontsize = kwargs.get('fontsize', 8)
        fontweight = kwargs.get('fontweight', 'bold')

        if width <= height:
            default_orientation = 'vertical'
        else:
            default_orientation = 'horizontal'

        orientation = kwargs.get('orientation', default_orientation)

        fig = plt.figure(figsize=(1.*width/self.dpi,
                                    1.*height/self.dpi))

        section = self.section(**kwargs)
        section.default_style(style)

        # vector plots
        if layer.startswith('vector:'):
            ax = fig.add_axes([0., 0., 1., 1.])
            ax.set_facecolor(bgcolor)
            ax.axis('off')

            # style['scale']: number of pixel of a vector of unit length
            qscale = width/style['scale']

            z = numpy.zeros((1,1))
            u = numpy.ones((1,1))

            #print 'scale ',qscale, width,style['scale']

            # draw a vector whose length is about a half of the image width
            u[0,0] = roundn(qscale/2,1)

            ax.quiver(z,z,u,z,scale=qscale,color=style['color'])


            #print 'u',str(u[0,0])
            #ax.quiver(0,0,1,0,scale=style['scale'],color=style['color'])
            #ax.arrow(0,2,.8,0,head_width = .2)
            ax.set_ylim(-1,1)
            ax.set_xlim(-1,3)

            units = 'm/s'
            legend = str(u[0,0]) + units
            ax.annotate(legend, xy=(0, 0.1))
        elif layer.startswith('point:'):
            ax = fig.add_axes([0., 0., 1., 1.])
            ax.set_facecolor(bgcolor)
            ax.axis('off')

            ax.plot([0],[0],**style)
            ax.set_ylim(-1,1)
            ax.set_xlim(-1,1)

        # scalar plots
        else:
            method = style['method']

            if (method == 'max_contourf'):
                # error mask
                max_error = float(kwargs.get('max_error', .1))
            elif (method == 'scatter'):
                # dot means observations
                pass
            else:
                ncontours = style['ncontours']
                cmap = get_colormap(style)
                vmin = style['vmin']
                vmax = style['vmax']
                norm = None # default linear scaling
                legendformat = '%g'

                if style.get('norm',None) == 'log':
                    legendformat = None
                    norm = matplotlib.colors.LogNorm(vmin = vmin, vmax = vmax)

                # Dummy axis to plot simething with the right range
                # These axes will not be visible
                ax = fig.add_axes([0., 0.05, 1, 0.9])
                ax.set_facecolor(bgcolor)
                ax.axis('off')
                ax.set_visible(False)

                if decorated:
                    if orientation == "vertical":
                        cax = fig.add_axes([0, 0.05, .3, .9])
                    else:
                        #cax = fig.add_axes([0.05, 0.5, .9, .3])
                        # needed to add more space for log colorbars
                        cax = fig.add_axes([0.075, 0.5, .85, .3])

                else:
                    cax = fig.add_axes([0, 0, 1, 1])

                if hasattr(vmin, 'mask') and vmin.mask or hasattr(vmax, 'mask') \
                        and vmax.mask:
                    # no data
                    vmin = vmax = 0

                if vmin == vmax:
                    vmin -= 1
                    vmax += 1

                # some arbitrary data with the right range
                a = numpy.zeros((2,2))
                a[0,0] = vmin
                a[0,1] = vmin
                a[1,0] = vmax
                a[1,1] = vmax

                if method == 'contour':
                    item = ax.contour(a, ncontours, cmap=cmap)
                elif method == 'contourf':
                    item = ax.contourf(a, ncontours, cmap=cmap)
                else:
                    item = ax.pcolor(a, cmap=cmap, norm=norm)

                if orientation == 'horizontal' and norm == None:
                    # reduce number of labels
                    # assume tick label are 60 pixels wide max.
                    nticks = int(width / 60. + .5)
                    nticks = max(nticks,2)
                    ticks = util.ticks(vmin,vmax,nticks)
                else:
                    ticks = None

                # draw colorbar
                c = fig.colorbar(item,cax=cax,format=legendformat,orientation=orientation,ticks=ticks)

                # get label from unit if not provided
                if label == "":
                    if not layer.startswith('point:') and not layer.startswith('vector:'):
                        if layersep in layer:
                            try:
                                [fieldname, varname] = layer.split(layersep)
                                filename = join_path(self.root, fieldname)
                                with netCDF4.Dataset(filename, 'r') as nc:
                                    label = nc.variables[varname].units
                            except:
                                pass

                if label != "":
                    c.set_label(label)

                for l in c.ax.get_yticklabels():
                    l.set_color(color)
                    l.set_fontweight(fontweight)
                    l.set_fontsize(fontsize)

                for l in c.ax.get_xticklabels():
                    l.set_color(color)
                    l.set_fontweight(fontweight)
                    l.set_fontsize(fontsize)


        stream = BytesIO()
        fig.savefig(stream, dpi=self.dpi, facecolor=bgcolor,
                    transparent = transparent,
                    format=get_mpl_format(imgformat))
        legend =  stream.getvalue()
        stream.close()

        return HttpResponse(legend,imgformat)


class caching_webmapserver(base_webmapserver):
    '''Web Map Server with disk cache'''

    def __init__(self, use_pic_cache = True, tmpdir = None, **kwargs):
        '''Web Map Server with disk cache if optional arguments use_pic_cache
        is true (default)'''

        super(caching_webmapserver, self).__init__(**kwargs)

        self.use_pic_cache = use_pic_cache

        if tmpdir:
            self.tmpdir = tmpdir
        else:
            self.tmpdir = os.environ.get('TMP','/tmp')


    def check_cache(self, make_plot, default_mime, tmp, **kwargs):
        '''return a plot either from cache or render it using make_plot'''

        args = str(kwargs['original_arguments'])
        args = args.encode('utf-8')
        hash = hashlib.md5(args).hexdigest()

        caller = self.__class__.__name__
        prefix = os.path.join(self.tmpdir,caller,tmp)

        # subdirectory based on layers
        dir = 'Others'

        if 'layers' in kwargs:
            layers = kwargs['layers']

            # a list as layers combines multiple layer
            if not isinstance(layers,list):
                dir = layers


        dir = os.path.normpath(os.path.join(prefix, dir))
        if not dir.startswith(prefix):
            raise BaseException('deny access to %s in %s' % (dir, prefix))

        if not os.path.isdir(dir):
            os.makedirs(dir)

        filename = os.path.join(dir,hash)

        # cached files from old scheme
        filename_old = os.path.join(self.tmpdir,
                                'cache_' + caller + '_' +
                                hash + '.png')

        if os.path.exists(filename_old):
            os.rename(filename_old,filename)


        if os.path.exists(filename) and self.use_pic_cache:
            f = open(filename, 'rb')
            # get shared lock
            fcntl.lockf(f,fcntl.LOCK_SH)
            pic = f.read()
            # release shared lock
            fcntl.lockf(f,fcntl.LOCK_UN)
            f.close()

            self.log.debug('%s from cache' % filename)

            return HttpResponse(pic,default_mime)

        #self.log.debug('%s not cached %s' % (filename,self.use_pic_cache))

        resp = make_plot(**kwargs)

        if self.use_pic_cache:
            try:
                f = open(filename, 'wb')
                # get exclusive lock
                fcntl.lockf(f,fcntl.LOCK_EX)
                f.write(resp.content)
                # release exclusive lock
                fcntl.lockf(f,fcntl.LOCK_UN)
                f.close()
            except IOError:
                self.log.error('unable to write %s to cache' % filename)

        return resp


    def make_anim(self, **kwargs):
        '''Return a animation'''

        if sys.version_info.major == 2:
            hash = hashlib.md5(str(kwargs['original_arguments'])).hexdigest()
        else:
            hash = hashlib.md5(str(kwargs['original_arguments']).encode("ascii")).hexdigest()

        time = kwargs['time'].split(',')
        rate = kwargs.get('rate',1)
        title = kwargs.get('title','')
        animformat = kwargs.get('format', 'video/webm')

        kw = kwargs.copy()
        kw['format'] = 'image/png'
        kw['transparent'] = False

        # temporary directory
        tmpdir = os.path.join(self.tmpdir,'video')
        if not os.path.isdir(tmpdir):
            os.makedirs(tmpdir)

        fnamefmt = os.path.join(tmpdir,hash + '-%05d.png')
        moviename = os.path.join(tmpdir,hash + '-oceanbrowser.')

        #print time

        for i in range(len(time)):
            kw['time'] = time[i]
            kw['title'] = title + ' time: ' + time[i]

            pic = self.make_simple_plot(**kw)
            fname = fnamefmt % i
            #print 'fname ',fname
            f = open(fname,'wb')
            f.write(pic.content)
            f.close()

        cmd = ['ffmpeg','-y','-r',('%s' % rate),'-i',fnamefmt]
        #cmd = ['avconv','-y','-r',('%s' % rate),'-i',fnamefmt]

        if animformat == 'video/webm':
            moviename += 'webm'
            cmd.extend([moviename])
            p = subprocess.Popen(cmd,
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                 cwd=tmpdir)
        else:
            moviename += 'mp4'
            cmd.extend(['-vcodec','libx264'])
            if cmd[0] == "avconv":
                cmd.extend(['-pre','medium'])
            cmd.extend([moviename])
            p = subprocess.Popen(cmd,
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                 cwd=tmpdir)

        (stdoutdata, stderrdata) = p.communicate()
        returncode = p.wait()

        #print "returncode ",returncode
        if returncode != 0:
            print("cmd ",cmd,file=sys.stderr)
            print("stdout ",stdoutdata,stderrdata,file=sys.stderr)
            raise WMSError("animation failed")

        with open(moviename,"rb") as f:
            return HttpResponse(f.read(),kwargs['format'])


    def make_plot(self, **kwargs):
        '''render an image for a GetMap request.'''
        pass

    def get_map(self, **kwargs):
        '''implement the GetMap request'''
        #disable cache at it is now done in other layer
        #return self.check_cache(self.make_plot,kwargs.get('format', 'image/png'),'GetMap',**kwargs)
        return self.make_plot(**kwargs)

    def get_capabilities(self, **kwargs):
        '''implement the GetCapabilities request'''
        # cached version is not faster
        #return self.check_cache(self.make_capabilities,'text/xml','GetCapabilities',**kwargs)

        return self.make_capabilities(**kwargs)



class webmapserver(caching_webmapserver):
    '''Main Wep Map Server instance delegates the two request types: GetMap and
    GetCapabilities'''

    def __init__(self, root, directory, service, filemask='*',
                 wms_url = '', **kwargs):
        '''Initialize the WMS instance
        root: is the path to the directory containing the Layers and sublayers.
              should not be visible from the user
        directory: is the part of the path added to root that can be visible to
              the user'''

        super(webmapserver, self).__init__(**kwargs)

        self.root = root
        self.directory = directory
        self.wms_url = wms_url

        # cache for scanning all NetCDF files
        self.enable_cache = True
        self.cache_time = -1
        self.response_xml = ''
        self.filemask = filemask
        self.dpi = wms_canvas.dpi

        self.service = service
        self._folder_description = {}

        self.feature_info_formats = ['application/vnd.ogc.gml','text/xml','text/html']


    def section(self, **kwargs):
        """return a horizontal section object according to
        elevation and time """

        layer = kwargs["layer"]
        elevation = kwargs.get("elevation", None)
        time = kwargs.get("time", None)
        section = WMSField(layer,self.root).hsection(elevation, time)
        return section

    def get_stats(self, **kwargs):
        '''Compute basic statistics about a field. It rerurns an XML file
        containing the range of the field and boundary box'''

        layer = kwargs["layer"]
        elevation = kwargs.get('elevation', None)
        time = kwargs.get('time', None)
        env = kwargs.get('_env')

        resp = HttpResponse(
            '','text/xml',
            time_modified = time_modified(self.root,layer))

        if not resp.needRevalidation(env):
            return resp

        section = WMSField(layer,self.root).hsection(elevation, time)
        style = {}
        section.default_style(style)
        (x, y, z, t, v) = section.load()

        if layer.startswith('vector:'):
            # vector field
            # fix me???
            v = v[0]

        if layer.startswith('point:'):
            v = numpy.array([-1,1])

        vmin = v.min()
        vmax = v.max()
        warning = ''

        if (hasattr(vmin,'count') and vmin.count() == 0 or
            hasattr(vmax,'count') and vmax.count() == 0 or
            x.size == 0 or y.size == 0):
            vmax = 1
            vmin = -1
            warning = 'The layer does not contain any data'

            if elevation != None or time != None:
                warning += ' (at '

                if elevation != None or time != None:
                    warning += 'elevation=' + str(elevation) + ' '

                if time != None:
                    warning += 'time=' + str(time)

                warning += ')'


        out  = ['<?xml version="1.0"?>']
        out += ['<stats>']

        # preferred range for scalar
        if not layer.startswith('vector:') and not layer.startswith('point:'):
            [fieldname, varname] = layer.split(layersep)
            filename = join_path(self.root, fieldname)
            #print('filename',filename,file=sys.stderr)

            if os.path.exists(filename + '.json'):
                with open(filename + '.json') as cfgfile:
                    cfg = json.load(cfgfile)
                    preferred_range = cfg.get('preferred_range',[])
                    if varname in preferred_range:
                        vrange = preferred_range[varname]
                        out += ["<vmin_preferred>%g</vmin_preferred>" % vrange[0]]
                        out += ["<vmax_preferred>%g</vmax_preferred>" % vrange[1]]

                        # crop range
                        style["vmin"] = max(style["vmin"],vrange[0])
                        style["vmax"] = min(style["vmax"],vrange[1])


        out.extend(list_param(style))

        if x.size == 0:
            out += ["<xmin></xmin>"]
            out += ["<xmax></xmax>"]
        else:
            out += ["<xmin>%g</xmin>" % x.min()]
            out += ["<xmax>%g</xmax>" % x.max()]

        if y.size == 0:
            out += ["<ymin></ymin>"]
            out += ["<ymax></ymax>"]
        else:
            out += ["<ymin>%g</ymin>" % y.min()]
            out += ["<ymax>%g</ymax>" % y.max()]

        out += ["<vmin>%g</vmin>" % vmin]
        out += ["<vmax>%g</vmax>" % vmax]

        if warning:
            out += ["<warning><![CDATA[%s]]></warning>" % warning]

        out += ["</stats>"]

        resp.content = '\n'.join(out).encode('utf-8')
        return resp


    def make_kmz(self, **kwargs):
        '''Return a KMZ file'''

        layers = kwargs['layers']
        styles = kwargs['styles']
        (x0, y0, x1, y1) = kwargs.get('bbox', (-180, -90, 180, 90))

        # create zip in memory
        outfile = BytesIO()
        kmz = zipfile.ZipFile(outfile, 'w')

        kml = '''<?xml version='1.0' encoding='UTF-8'?>
        <kml xmlns='http://www.opengis.net/kml/2.2'>
          <Document>'''

        i = 0

        for layer, style in zip(layers, styles):
            i += 1
            kw = kwargs.copy()
            kw['format'] = 'image/png'
            kw['layers'] = [layer]
            kw['styles'] = [style]
            kw['decorated'] = False

            pic = self.make_simple_plot(**kw)

            ld = self.find_layer(layer)

            if not ld:
                raise WMSError('Invalid layer (layer=%s)' %
                               str(layer))

            kml += '''
            <Folder>
            <name>%s</name>
            <description>Diva Analysis</description>
            <GroundOverlay>
              <name>Anlysis</name>
              <description>Gridded field</description>
              <Icon>
                <href>%s</href>
              </Icon>
              <LatLonBox>
                <north>%.6f</north>
                <south>%.6f</south>
                <east>%.6f</east>
                <west>%.6f</west>
                <rotation>0</rotation>
              </LatLonBox>
            </GroundOverlay>
          ''' % (ld.title, 'field' + str(i) + '.png', y1, y0, x1, x0)

            kmz.writestr('field' + str(i) + '.png', pic.content)

            # some figures don't have a legend associated

            if style['method'] != "max_contourf" and style['method'] != "scatter":

                kml += '''
              <ScreenOverlay>
                <name>Colorbar</name>
                <description>Colorbar</description>
                <Icon>
                  <href>%s</href>
                </Icon>
                <overlayXY x="0" y="1" xunits="fraction" yunits="fraction"/>
                <screenXY x="0" y="1" xunits="fraction" yunits="fraction"/>
                <rotationXY x="0" y="0" xunits="fraction" yunits="fraction"/>
                <size x="0" y="0" xunits="fraction" yunits="fraction"/>
              </ScreenOverlay>''' % ('colorbar' + str(i) + '.png')

                kw = kwargs.copy()
                kw['layer'] = layer
                kw['style'] = style
                kw['width'] = 100
                kw['height'] = 300
                kw['decorated'] = True
                kw['color'] = '#cccccc'
                kw['format'] = 'image/png'

                pic_cb = self.get_legend(**kw)

                kmz.writestr('colorbar' + str(i) + '.png', pic_cb.content)

            kml += '</Folder>'


        kml += '</Document></kml>'

        kmz.writestr('doc.kml', kml.encode('utf-8'))
        kmz.close()

        pic =  outfile.getvalue()
        outfile.close()

        return HttpResponse(pic,'application/vnd.google-earth.kmz')


    def make_plot(self, **kwargs):
        '''make a map'''

        imgformat = kwargs.get('format', 'image/png')

        if imgformat == 'application/vnd.google-earth.kmz':
            return self.make_kmz(**kwargs)
        elif imgformat == 'video/webm' or imgformat == 'video/mp4':
            return self.make_anim(**kwargs)
        else:
            return self.make_simple_plot(**kwargs)

    def vector_make_plot(self, wmsc, layer, style, x0,x1, y0,y1, buf, elevation, time):
        # TODO take projection into account

        #print 'style ',style,type(style)
        section = WMSField(layer,self.root).hsection(elevation, time)
        section.default_style(style)

        (x, y, z, t, vel) = section.load()
        ux = vel[0]
        vx = vel[1]


        minx = (x[:,1:] - x[:,:-1]).min()
        miny = (y[1:,:] - y[:-1,:]).min()


        if 'reduce' in style:
            r = style['reduce']
        else:
            arrow_every_pixel = style['space']
            #print 'minx ',minx,miny
            reduce_x = (x1-x0) / (wmsc.width * minx)
            reduce_y = (y1-y0) / (wmsc.height * miny)
            r = min(reduce_x,reduce_y)*arrow_every_pixel
            r = max(1,int(round(r)))


        if r != 1:
            x = util.reduce(x,r)
            y = util.reduce(y,r)
            ux = util.reduce(ux,r)
            vx = util.reduce(vx,r)


        #print "reduce",r
        color = style['color']

        #print "style['scale']",style['scale']

        qscale = wmsc.width/style['scale']

        wmsc.m.quiver(x,y,ux,vx,scale=qscale,color=color)

    def point_make_plot(self, wmsc, layer, style, x0,x1, y0,y1, buf, elevation, time):
        section = WMSField(layer,self.root).hsection(elevation, time)
        section.default_style(style)
        (x, y, z, t, dummy) = section.load()

        #print 'x,y',x,y,x0,x1,y0,y1,style
        wmsc.m.plot(x, y, 'k,', antialiased=False, **style)
        #wmsc.m.plot([41.0495], [43], 'k,', antialiased=False, **style)
        #wmsc.m.plot([42.0495], [43], 'k,', antialiased=False, **style)
        #wmsc.m.plot([41.05], [43], 'k,', antialiased=False, **style)


    def make_simple_plot(self, **kwargs):
        '''make a map using matplotlib'''

        title = kwargs.get('title', '')
        layers = kwargs['layers']

        try:
            (x0, y0, x1, y1) = kwargs['bbox']
            width = kwargs['width']
            height = kwargs['height']
            CRS = kwargs['crs']
            imgformat = kwargs['format']
        except KeyError as e:
            raise WMSError("Error with: " + str(e))


        elevation = kwargs.get('elevation', 0)
        time = kwargs.get('time', 'default')

        styles = kwargs['styles']
        decorated = kwargs.get('decorated', False)
        basemap = kwargs.get('basemap', 'fillcontinents')
        transparent = kwargs.get('transparent', True)
        #CRS = kwargs.get('crs', 'EPSG:4326')
        version = kwargs['version']
        env = kwargs.get('_env')

        resp = HttpResponse(
            '',imgformat,
            time_modified = layers_time_modified(self.root,layers))

        if not resp.needRevalidation(env):
            return resp

        if CRS == 'EPSG:4326' and version == '1.3.0':
            # swap lon/lat
            x0, y0, x1, y1 = y0, x0, y1, x1

        wmsc = wms_canvas(
            layers, x0, y0, x1, y1, width, height,
            decorated=decorated, CRS=CRS,
            transparent=transparent,
            basemap=basemap
        )

        # load an extra 1/20
        buf = max(wmsc.lon1-wmsc.lon0, wmsc.lat1-wmsc.lat0)/20.

        # loop over all layers
        # the result should be a composite

        for layer, style in zip(layers, styles):
            if title != '':
                wmsc.ax.set_title(title)

            # continents
            if layer == '_cont':
                wmsc.ax.set_facecolor('1')
                wmsc.m.drawlsmask((200, 200, 200, 255), (0, 0, 255, 0))

            # vector plots
            elif layer.startswith('vector:'):
                self.vector_make_plot(wmsc, layer, style, x0,x1, y0,y1, buf, elevation, time)
            # cloud of point plots
            elif layer.startswith('point:'):
                self.point_make_plot(wmsc, layer, style, x0,x1, y0,y1, buf, elevation, time)


            # scalar plots
            else:
                # if elevation and time is not specified, then get full range
                # for all time and elevations
                # This is constitent with get_legend

                z = kwargs.get('elevation', None)
                t = kwargs.get('time', None)

                section = WMSField(layer,self.root).hsection(z, t)
                section.default_style(style)

                # if elevation and time is not specified, then get plot default
                # time and elevations

                section = WMSField(layer,self.root).hsection(elevation, time)

                if (style['method'] == 'max_contourf'):
                    max_error = float(kwargs.get('max_error', .1))
                    [fieldname, varname] = layer.split(layersep)

                    filename = join_path(self.root, fieldname)
                    erro = geodataset.GeoDataset(filename, varname)
                    (x, y, dummyz, dummyt, err) = erro.load(
                        longitude=[wmsc.lon0-buf, wmsc.lon1+buf],
                        latitude=[wmsc.lat0-buf, wmsc.lat1+buf],
                        depth=elevation, time=time)

                    if (wmsc.within(x, y)):
                        X, Y = numpy.meshgrid(x, y)
                        wmsc.m.contourf(X, Y, err, [max_error, 1], colors='#eeeeee')


                elif (style['method'] == 'scatter'):
                    (x, y, dummyz, dummyt, v) = section.load()

        #          if (wmsc.within(x, y)):
                    if (True):
                        wmsc.m.plot(x, y, 'k,', antialiased=False)
        #              wmsc.m.plot(x, y, 'k.')
        #              wmsc.m.plot(x, y, 'k.', markersize=3, antialiased=False)
                else:

                    if style['method'] == 'contour' or style['method'] == 'contourf' :
                  #load all to avoid contour lines at different levels
                        #print "load all (fix me)"
                        (x, y, dummyz, dummyt, v) = section.load()
                    else:
                        (x, y, dummyz, dummyt, v) = section.load(
                            longitude=[wmsc.lon0-buf, wmsc.lon1+buf],
                            latitude=[wmsc.lat0-buf, wmsc.lat1+buf])

                    wmsc.contourf(x, y, v, style)


        resp.content = wmsc.render(imgformat)
        return resp


    def get_feature_info(self, **kwargs):
        '''gets the value of the field at selected location'''

        # get parameters
        version = kwargs.get('version','1.1.1')
        layers = kwargs['query_layers'].split(',')
        (x0, y0, x1, y1) = kwargs.get('bbox', (-180, -90, 180, 90))

        if version == '1.1.1':
            i = float(kwargs['x'])
            j = float(kwargs['y'])
        else:
            i = float(kwargs['i'])
            j = float(kwargs['j'])

        elevation = kwargs.get('elevation', 0)
        time = kwargs.get('time', 'default')
        width = kwargs.get('width', 256)
        height = kwargs.get('height', 256)
        info_format = kwargs.get('info_format', 'text/html')
        CRS = kwargs.get('crs', 'EPSG:4326')


        # calculate coordinate of selected point in map projection
        x = x0 + i * (x1-x0)/width
        y = y1 + j * (y0-y1)/height

        # take projection into account
        if CRS == "CRS:84":
            x,y = y,x
        else:
            epsg = int(CRS.replace('EPSG:',''))
            proj = Basemap(epsg = epsg)

            if epsg == 3857:
                x,y = proj(x + (proj.urcrnrx-proj.llcrnrx)/2,
                           y + (proj.urcrnry-proj.llcrnry)/2, inverse = True)
            else:
                x,y = proj(x,y, inverse = True)

        print("x,y",x,y,file=sys.stderr)
        # load the value at the selected location
        section = WMSField(layers[0],self.root).hsection(elevation, time)

        maxdist = None
        if layers[0].startswith('point:'):
            pixels = 15
            dx = pixels/float(width) * (x1-x0)
            dy = pixels/float(height) * (y1-y0)
            maxdist = max(dx,dy)

        (sx, sy, sz, st, v) = section.load(x,y,maxdist)

        if hasattr(v,'count'):
            if v.count() == 0:
                # no data
                v = ''

        if info_format == 'text/xml':
            if hasattr(v,'dtype'):
                if v.dtype == numpy.dtype('S1'):
                    v = ','.join([''.join(_) for _ in v])

            out = ['<?xml version="1.0" encoding="UTF-8"?>',
                   '<info>',
                   '<longitude>' + str(x) + '</longitude>',
                   '<latitude>' + str(y) + '</latitude>',
                   '<value>' + str(v) + '</value>',
                   '</info>']
        elif info_format == 'application/vnd.ogc.gml':

            #print('s*',sx,sy,sz,st,v,type(v),v.dtype,v.dtype == '|S1')
            # ensure that all s* flat list
            # 123 becomes [123]
            # [[1,2],[3,4]] becomes [1,2,3,4]

            sx = numpy.array([sx]).flatten()
            sy = numpy.array([sy]).flatten()
            sz = numpy.array([sz]).flatten()
            st = numpy.array([st]).flatten()

            dtype = None
            if type(v) == numpy.ndarray:
                dtype = v.dtype

            if dtype  != '|S1':
                # do this unless v is a list of strings
                #print('here',dtype,dtype  != '|S1')
                v  = numpy.array([ v]).flatten()

            root = E.info()
            #print('s*',sx,sy,sz,st,v,type(v),v.dtype)

            for px,py,pz,pt,pv in zip(sx,sy,sz,st,v):
                feature = E.feature(
                    E.longitude(str(px)),
                    E.latitude(str(py)),
                    )


                DataURL = E.DataURL()

                if pz != None:
                    DataURL.append(E.depth(str(pz)))

                if pt != None:
                    # mapping is already done in geodataset
                    #DataURL.append(E.time(util.gregd(pt,format='%Y-%m-%dT%H:%M')))
                    DataURL.append(E.time(str(pt)))

                if layers[0].startswith('point:'):
                    #print('pv',pv)
                    EDMO,CDI = ''.join(pv).split('-',1)

                    #data = settings.LocalCDI_baseurl + '?popup=yes&edmo=' + EDMO  + '&identifier=' + CDI
                    data = settings.LocalCDI_baseurl + EDMO  + '/' + CDI
                    DataURL.append(E.data(data))
                else:
                    DataURL.append(E.value(str(pv)))

                feature.append(DataURL)
                root.append(feature)

            info_format = 'text/xml'

            outxml = ET.tostring(root, pretty_print=True, xml_declaration=True)
            if sys.version_info[0] == 3:
                outxml = outxml.decode("utf-8")

            out = [outxml]
        else:
            if isinstance(v,str):
                if v == '':
                    v = 'no value'

            if hasattr(v,'dtype'):
                if v.dtype.char == 'S':
                    v = [''.join(v[i,:].data) for i in range(v.shape[0])];
                    v = ','.join(v)

            out = ['<div>',
                   '<table>',
                   '<tbody>',
                   '<tr><td>Longitude</td><td>' + str(x) + '</td></tr>',
                   '<tr><td>Latitude</td><td>' + str(y) + '</td></tr>',
                   '<tr><td>Value</td><td>' + str(v) + '</td></tr>',
                   '</tbody>',
                   '</table>',
                   '</div>'
                   ]


        return HttpResponse('\n'.join(out).encode('utf-8'),info_format)


    def get_folder_description(self,basedir = ''):
        '''load metadata from all files under basedir. basedir is a
        directory relative to os.path.join(self.root,self.directory)'''

        # use the time of the toplevel directory
        # so that when this directory is "touched"
        # all sub-directories (with basedir) are reloaded too
        time = math.floor(os.path.getctime(
            os.path.join(self.root, self.directory)))

        if time != self.cache_time or not self.enable_cache or not (basedir in self._folder_description):
            self.log.info('reload: current ctime %s old ctime %s'  % (str(time),str(self.cache_time)))
            self.cache_time = time

            path = os.path.join(self.directory, basedir)
            authority = {'name': 'seadatanet', 'url': 'http://www.seadatanet.org'}

            self._folder_description[basedir] = folder_description(
                title="Layers", wms_url=self.wms_url, log = self.log,authority = authority)

            self._folder_description[basedir].load_dir(self.root, path, filemask=self.filemask)
            self.log.info('load dir "%s" under "%s"'  % (path,self.root) )

        return self._folder_description[basedir]


    def find_layer(self,name):
        return self.get_folder_description().find_layer(name)

    def _getCapabilities(self, env, basedir=''):
        '''create reply to GetCapabilities request from metadata'''

        wms_url = util.getWMSURL(self.wms_url,env)

        def xml_tag(name,key,dict):
            if key in dict:
                if dict[key]:
                    return '<%s><![CDATA[%s]]></%s>' % (name,dict[key],name)

            return ''


        out = ['<?xml version="1.0" encoding="UTF-8"?>']


        if self.version == '1.1.1':
            out += ['<!DOCTYPE WMT_MS_Capabilities SYSTEM ']
            out += ['"http://schemas.opengis.net/wms/1.1.1/WMS_MS_Capabilities.dtd">\n']
            out += ['<WMT_MS_Capabilities version="1.1.1">']
            cap_format = 'application/vnd.ogc.wms_xml'
            exception_format = 'application/vnd.ogc.se_xml'
        else:
            out += ['''
    <WMS_Capabilities version="1.3.0" xmlns="http://www.opengis.net/wms"
                      xmlns:xlink="http://www.w3.org/1999/xlink"
                      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                      xmlns:inspire_common="http://inspire.ec.europa.eu/schemas/common/1.0"
                      xmlns:inspire_vs="http://inspire.ec.europa.eu/schemas/inspire_vs/1.0"
                      xsi:schemaLocation="http://inspire.ec.europa.eu/schemas/inspire_vs/1.0 http://inspire.ec.europa.eu/schemas/inspire_vs/1.0/inspire_vs.xsd">''']

            cap_format = 'text/xml'
            exception_format = 'text/xml'


        title = self.service['title']

        # add basedir to the title if defined
        if basedir:
            title += ' (' + basedir + ')'

        if self.version == '1.1.1':
            out += ['<Service><Name>OGC:WMS</Name>']
        else:
            out += ['<Service><Name>WMS</Name>']

        out += ['<Title><![CDATA[%s]]></Title>' % (title)]

        # description of layers (abstract)

        out += xml_tag('Abstract','abstract',self.service)
        out += xml_tag('AccessConstraints','accessconstraints',self.service)
        out += xml_tag('Fees','fees',self.service)

        # keywords

        out += ['<KeywordList>']

        for k in self.service.get('keywords',[]):
            out += ['<Keyword><![CDATA[%s]]></Keyword>' % k]

        out += ['''
        </KeywordList>
        <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink" xlink:type="simple"
                        xlink:href="%s" />
''' %(wms_url)]


        out += ['<ContactInformation><ContactPersonPrimary>']
        out += [xml_tag('ContactPerson','contact_person',self.service)]
        out += [xml_tag('ContactOrganization','contact_organization',self.service)]
        out += [xml_tag('ContactPosition','contact_position',self.service)]
        out += ['</ContactPersonPrimary>']
        out += [xml_tag('ContactElectronicMailAddress','email',self.service)]
        out += ['</ContactInformation></Service>']

        # check Format
        out += ['''<Capability>
        <Request>
          <GetCapabilities>
            <Format>%s</Format>
            <DCPType>
              <HTTP>
                <Get>
                  <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink"
                                  xlink:type="simple"
                                  xlink:href="%s" />
                </Get>
              </HTTP>
            </DCPType>
          </GetCapabilities>''' % (cap_format, wms_url)]

        # GetMap
        out += ["<GetMap>"]

        for imgformat in wms_canvas.supported_formats:
            out += ['<Format>' + imgformat[0] + '</Format>\n']

        out += ['''
            <DCPType>
              <HTTP>
                <Get>
                  <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink"
                                  xlink:type="simple"
                                  xlink:href="%s" />
                </Get>
              </HTTP>
            </DCPType>
          </GetMap>''' % (wms_url)]

        # GetFeatureInfo

        out += ['<GetFeatureInfo>\n']

        for format in self.feature_info_formats:
            out += '<Format>' + format + '</Format>\n'

        out += ['''
      <DCPType>
        <HTTP>
          <Get><OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink" xlink:type="simple" xlink:href="%s" /></Get>
        </HTTP>
      </DCPType>
    </GetFeatureInfo>''' % (wms_url)]


        out += ['''</Request>
        <Exception>
          <Format>%s</Format>
        </Exception>''' % (exception_format)]

        if self.version == '1.3.0':
            inspire_metadata_url = self.service.get("inspire_metadata_url","none")

            if inspire_metadata_url.lower() != "none":
                out += ['''
<inspire_vs:ExtendedCapabilities>
<inspire_common:MetadataUrl xsi:type="inspire_common:resourceLocatorType">
<inspire_common:URL><![CDATA[%s]]></inspire_common:URL>
<inspire_common:MediaType>application/vnd.ogc.csw.GetRecordByIdResponse_xml</inspire_common:MediaType>
</inspire_common:MetadataUrl>
<inspire_common:SupportedLanguages>
<inspire_common:DefaultLanguage>
<inspire_common:Language>eng</inspire_common:Language>
</inspire_common:DefaultLanguage>
<inspire_common:SupportedLanguage>
<inspire_common:Language>eng</inspire_common:Language>
</inspire_common:SupportedLanguage>
</inspire_common:SupportedLanguages>
<inspire_common:ResponseLanguage>
<inspire_common:Language>eng</inspire_common:Language>
</inspire_common:ResponseLanguage>
</inspire_vs:ExtendedCapabilities>''' % (inspire_metadata_url)]



        f = self.get_folder_description(basedir)

        # don't display Root layer
        #for layer in f.layers:
        #    out.extend(layer.wmscap(self.version))
        out.extend(f.wmscap(env, self.version))

        out += ["</Capability>"]


        if self.version == '1.1.1':
            out += ["</WMT_MS_Capabilities>"]
        else:
            out += ["</WMS_Capabilities>"]

        #print('test',out[177],out[170:180])
        return ''.join(out)

    def get_capabilities_time_modified(self):
        return math.floor(os.path.getctime(
            os.path.join(self.root, self.directory)))

    def make_capabilities(self, **kwargs):
        '''return XML file representing the capabilities of a WMS server.
        Request is cached'''

        self.version = kwargs.get("version", "1.3.0")
        basedir = kwargs.get("basedir","")
        env = kwargs.get("_env")

        if not self.version in ['1.1.1', '1.3.0']:
            raise BaseException("unsupported WMS version (%s)" % self.version)

        time = self.get_capabilities_time_modified()

        resp = HttpResponse('','text/xml',time_modified = time)
        if resp.needRevalidation(env) or time == None:
            response_xml = self._getCapabilities(env, basedir)
            resp.content = response_xml.encode('utf-8')

        return resp


    def get_kml(self, **kwargs):
        '''return KML file representing the capabilities of a WMS server.
        Request is cached'''

        basedir = kwargs.get("basedir","")
        env = kwargs.get('_env')

        kml  = '<?xml version="1.0" encoding="UTF-8"?>\n'
        kml += '<kml xmlns="http://www.opengis.net/kml/2.2">\n'
        kml += '<Document>\n'

        kml += '''<Style id="radioFolder">
<ListStyle>
  <listItemType>radioFolder</listItemType>
</ListStyle>
 </Style>'''

        f = self.get_folder_description(basedir)

       # don't display Root layer
        for layer in f.layers:
            kml += layer.kml(env)

        kml += '<styleUrl>#radioFolder</styleUrl>\n'
        kml +=  '</Document>\n</kml>\n'
        kml = ''.join(kml).encode('utf-8')

        return HttpResponse(kml,'application/vnd.google-earth.kml')




class vertsection_webmapserver(caching_webmapserver):
    '''WMS for vectial sections'''

    def __init__(self, root, **kwargs):
        '''Initialize the WMS instance'''
        super(vertsection_webmapserver, self).__init__(**kwargs)

        self.root = root

    def section(self, **kwargs):
        """return a horizontal section object according to
        elevation and time """

        layer = kwargs["layer"]
        section = kwargs.get('section', '')
        time = kwargs.get("time", None)
        section_ = WMSField(layer,self.root).vsection(section,time)
        return section_

    def make_simple_plot(self, **kwargs):
        '''returns a vertical plot according to the information in kwargs'''
        scale = 1

        layers = kwargs.get('layers')[0]
        styles = kwargs['styles']
        title = kwargs.get('title', '')
        ratio = float(kwargs.get('ratio', 1))
        (x0, y0, x1, y1) = kwargs.get('bbox', (0, -3000, 360, 10))

        x0 = x0 * ratio * scale
        x1 = x1 * ratio * scale
        y0 = y0 * scale
        y1 = y1 * scale

        section = kwargs.get('section', '')
        time = kwargs.get('time', 'default')
        imgformat = kwargs.get('format', 'image/png')
        width = kwargs.get('width', 256)
        height = kwargs.get('height', 256)
        decorated = kwargs.get('decorated', False)
        transparent = kwargs.get('transparent', True)


        wmsc = wms_canvas(layers, x0, y0, x1, y1, width, height,
                          decorated=decorated, CRS='vertical',
                          transparent=transparent)


        section_ = WMSField(layers,self.root).vsection(section,time)
        distance,z,sec = section_.load()

        style = styles[0]

        # only plot if there is some data
        if sec.count() > 0:
            wmsc.contourf(distance, z, sec, style)
            wmsc.ax.set_xlim(x0, x1)
            wmsc.ax.set_ylim(y0, y1)
            if title != '':
                wmsc.ax.set_title(title)

        pic = wmsc.render(imgformat)
        return HttpResponse(pic,imgformat)

    def make_plot(self, **kwargs):
        '''make a map'''

        imgformat = kwargs.get('format', 'image/png')

        if imgformat == 'video/webm' or imgformat == 'video/mp4':
            return self.make_anim(**kwargs)
        else:
            return self.make_simple_plot(**kwargs)


    def get_stats(self,**kwargs):
        '''Extracting a vertical section'''

        layer = kwargs['layer']
        section = kwargs.get('section', '0, 0|360, 0')
        time = kwargs.get('time', 'default')


        section_ = WMSField(layer,self.root).vsection(section,time)
        style = {}

        distance,z,sec = section_.load()
        section_.default_style(style)

        vmin = style['vmin']
        vmax = style['vmax']

        out  = ['<?xml version="1.0"?>']
        out += ['<layers>']
        out += ['<stats>']
        out.extend(list_param(style))
        out += ['<xmin>%g</xmin>' % distance.min()]
        out += ['<xmax>%g</xmax>' % distance.max()]
        out += ['<ymin>%g</ymin>' % z.min()]
        out += ['<ymax>%g</ymax>' % z.max()]
        out += ['<vmin>%g</vmin>' % vmin]
        out += ['<vmax>%g</vmax>' % vmax]
        out += ['</stats>']
        out += ['</layers>']

        content_type = 'text/xml'
        return HttpResponse('\n'.join(out).encode('utf-8'),content_type)


    def get_feature_info(self, **kwargs):
        '''gets the value of the field at selected location'''

        # get parameters
        version = kwargs.get('version','1.1.1')
        layers = kwargs['query_layers'].split(',')
        (x0, y0, x1, y1) = kwargs['bbox']
        section = kwargs.get('section', '0, 0|360, 0')
        time = kwargs.get('time', 'default')

        if version == '1.1.1':
            i = float(kwargs['x'])
            j = float(kwargs['y'])
        else:
            i = float(kwargs['i'])
            j = float(kwargs['j'])

        width = kwargs.get('width', 256)
        height = kwargs.get('height', 256)
        info_format = kwargs.get('info_format', 'text/html')
        # for now
        info_format = 'text/html'

        section_ = WMSField(layers[0],self.root).vsection(section,time)
        distance,z,sec = section_.load()

        # calculate distance and depth of selected point
        x = x0 + i * (x1-x0)/width
        y = y1 + j * (y0-y1)/height

        #print('x',x,y)
        #print('distance',distance)

        # indice in grid
        d = abs(distance - x);
        gridi, = numpy.where(d == min(d))
        gridi = gridi[0]

        d = abs(z - y);
        gridj, = numpy.where(d == min(d))
        gridj = gridj[0]

        v = sec[gridj,gridi]

        #print('sec',v)
        #print('distance',x,distance[gridi])
        #print('z',y,z[gridj])


        out = [
            '<div>',
            '<table>',
            '<tbody>',
            '<tr><td>Distance</td><td>' + str(distance[gridi]) + '</td></tr>',
            '<tr><td>Depth</td><td>' + str(z[gridj]) + '</td></tr>',
            '<tr><td>Value</td><td>' + str(v) + '</td></tr>',
            '</tbody>',
            '</table>',
            '</div>'
        ]


        return HttpResponse('\n'.join(out).encode('utf-8'),info_format)


class continents_webmapserver(caching_webmapserver):
    '''Web Map Server for drawing continents'''

    def __init__(self, bathdir, **kwargs):
        '''Initialize the WMS instance'''
        super(continents_webmapserver, self).__init__(**kwargs)

        self.bathdir = bathdir
        self.bath = settings.bath

    def make_plot(self, **kwargs):
        '''render an image for a GetMap request.'''
        layers = kwargs.get('layers')[0]
        (x0, y0, x1, y1) = kwargs.get('bbox', (-180, -90, 180, 90))

        imgformat = kwargs.get('format', 'image/png')
        width = kwargs.get('width', 256)
        height = kwargs.get('height', 256)
        database = kwargs.get('db', 'gebco')
        decorated = kwargs.get('decorated', False)
        transparent = kwargs.get('transparent', True)

        CRS = kwargs.get('crs', 'EPSG:4326')
        version = kwargs.get('version','1.3.0')

        if CRS == 'EPSG:4326' and version == '1.3.0':
            # swap lon/lat
            x0, y0, x1, y1 = y0, x0, y1, x1

        #y1 = min(y1, 90)

        #print >> sys.stderr, 'x0', layers, x0, y0, x1, y1, width, height
        #sys.stderr.flush()

        wmsc = wms_canvas(layers, x0, y0, x1, y1, width, height,
                          decorated=decorated, CRS=CRS,
                          transparent=transparent)

        if (layers == 'cont'):
            #wmsc.m.drawcoastlines(color='0.8', antialiased=0)
            #wmsc.m.fillcontinents()
            wmsc.ax.set_facecolor('1')

            #print >> sys.stderr, 'coord', x0, y0, x1, y1
            #sys.stderr.flush()
            wmsc.m.drawlsmask((200, 200, 200, 255), (0, 0, 255, 0))
        elif (layers == 'contour0'):
            # determine which bathymetry to take

            b = choose_bathymetry(self.bath,database, x0, x1, y0, y1)
            bathymetry = os.path.join(self.bathdir, b["file"])
            varname = 'bat'

            # load 10 pixels more for contours
            buf = max(10*b['res'], max(x1-x0, y1-x0)/20)

            bat = geodataset.GeoDataset(bathymetry, varname)

            (x, y, bz, bt, err) = bat.load(longitude=[x0-buf, x1+buf],
                                           latitude=[y0-buf, y1+buf])
            if len(err) != 0:
                err = err.filled(fill_value = 1)

                X, Y = numpy.meshgrid(x, y)
                X, Y = wmsc.m(X, Y)
                wmsc.m.contourf(X, Y, err, [0, numpy.inf], colors='#aaaaaa')
                wmsc.m.contour(X, Y, err, [0], colors='#222222')

        pic = wmsc.render(imgformat)

        return HttpResponse(pic,imgformat)


class wms_canvas:
    '''Off-screen rendering canvas for WMS server based on basemap'''

    # supported projections
    projections = {'EPSG:4326': 'cyl',
                   'EPSG:54004': 'merc'}

    # Resolution of basemap coastline
    basemap_resolution = 'c'

    # Resolution drops off by roughly 80% between datasets.

    available_resolutions = [
        {'res': 0.2**0 * 2., 'code': 'c'},
        {'res': 0.2**1 * 2., 'code': 'l'},
        {'res': 0.2**2 * 2., 'code': 'i'},
        {'res': 0.2**3 * 2., 'code': 'h'},
    ]

    dpi = 100

    # supported formats
    # mime-type and file extension (for matplotlib)
    # distionary is not used here because we want them in this order

    supported_formats = (('image/png', 'png'),
                         ('image/svg+xml', 'svg'),
                         ('image/eps', 'eps'),
                         ('application/pdf','pdf'),
                         ('application/vnd.google-earth.kmz', 'kmz'),
                         ('video/webm', 'webm'),
                         ('video/mp4', 'mp4'),
                         )

    supported_styles = (('pcolor_flat','Flat shading'),
                        ('contourf','Filled contours'),
                        ('contour','Contours')
                        )

    # basemap is on one 'fillcontinents','bluemarble','shadedrelief'
    def __init__(self, layers, x0, y0, x1, y1, width, height, decorated=False,
                 CRS='EPSG:4326', transparent=True, basemap = 'fillcontinents'):
        '''If decorated is true, than axis and colorbar are drawn.'''
        self.layers = layers
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1
        self.decorated = decorated
        self.height = height
        self.width = width
        self.dpi = self.__class__.dpi
        self.basemap = basemap
        self.transparent = transparent
        #self.basemap_resolution = self.__class__.basemap_resolution

        target_resolution = max(x1-y0,y1-y0)/100.

        #print 'target_resolution ',target_resolution
        if decorated or '_cont' in self.layers:
            self.basemap_resolution = select_multires(self.__class__.available_resolutions,
                                                      target_resolution)['code']
        else:
            self.basemap_resolution = None

        #print 'self.basemap_resolution ',self.basemap_resolution

        self.fig = plt.figure(figsize=(1.*width/self.dpi,
                                       1.*height/self.dpi))

        self.item = None
        self.CRS = CRS

        bottom = 0
        height = 1


        #print('CRS',CRS,x0,y0,x1,y1)
        if CRS == 'vertical':
            # vertical section
            self.m = None
            self.lon0 = x0
            self.lat0 = y0
            self.lon1 = x1
            self.lat1 = y1
        else:
            # horizontal section
            if CRS == 'EPSG:4326' or CRS == 'CRS:84':
                self.y1l = min(float(y1),  90.)
                self.y0l = max(float(y0), -90.)
                self.lon0 = x0
                self.lat0 = y0
                self.lon1 = x1
                self.lat1 = y1

                # latitude cannot exceed +/- 90 degrees

                # height and bottom of figure for non-decorated figures
                height = (self.y1l-self.y0l)/(y1-y0)
                bottom = (self.y0l - y0)/(y1-y0)

                proj = dict(projection='cyl', llcrnrlon=x0, llcrnrlat=self.y0l,
                            urcrnrlon=x1, urcrnrlat=self.y1l)
            elif CRS == 'EPSG:54004':

                bmap = Basemap(projection='merc', llcrnrlon=0, llcrnrlat=0,
                              urcrnrlon=180, urcrnrlat=90, lat_ts=0)

                self.lon0, self.lat0 = bmap(x0, y0, inverse=True)
                self.lon1, self.lat1 = bmap(x1, y1, inverse=True)

                #print "lon0, lat0 ", self.lon0, self.lat0
                #print "lon1, lat1 ", self.lon1, self.lat1

                proj = dict(projection='merc',
                            llcrnrlon=self.lon0, llcrnrlat=self.lat0,
                            urcrnrlon=self.lon1, urcrnrlat=self.lat1, lat_ts=0)
            # this should replace previous code
            elif CRS in ['EPSG:4326','EPSG:54004','EPSG:3857']:
                epsg = CRS.replace('EPSG:','')
                bmap = Basemap(epsg = epsg, llcrnrlon=0, llcrnrlat=0,
                              urcrnrlon=180, urcrnrlat=90, lat_ts=0)

                self.lon0, self.lat0 = bmap(x0, y0, inverse=True)
                self.lon1, self.lat1 = bmap(x1, y1, inverse=True)

                #print "lon0, lat0 ", self.lon0, self.lat0
                #print "lon1, lat1 ", self.lon1, self.lat1

                proj = dict(epsg = epsg,
                            llcrnrlon=self.lon0, llcrnrlat=self.lat0,
                            urcrnrlon=self.lon1, urcrnrlat=self.lat1, lat_ts=0)
            else:
                raise WMSError("unsupported projection","CRS=%s" % CRS)





        #print('bottom, 1, height ',bottom, 1, height, proj,self.basemap_resolution)

        if not decorated:
            # axis use entire figure
            self.ax = self.fig.add_axes([0., bottom, 1, height])
            #self.ax = self.fig.add_axes([0., bottom, 1, height])
            #self.ax = self.fig.add_axes([0.00001, bottom, 1, height])
            self.ax.axis('off')
        else:
            # axis leaves some room for labels and colorbar
            self.ax = self.fig.add_axes([0.1, 0.1, 0.7, 0.8])

        if CRS != 'vertical':
            self.m = Basemap(resolution=self.basemap_resolution,
                             suppress_ticks=False, ax=self.ax, **proj)
        else:
            self.m = self.ax

        #print 'x0,x1,y0,y1 ',x0,x1,y0,y1
        self.ax.set_facecolor('none')
        self.ax.set_xlim(x0, x1)
        self.ax.set_ylim(y0, y1)

        self.empty = True

    def within(self, lon, lat):
        '''check if (lon, lat) is within the canvas'''

#        if len(lon) == 0 or len(lat) == 0:
#            return False

        if lon.size == 0 or lat.size == 0:
            return False

        if float(lat.min()) > self.lat1 or float(lat.max()) < self.lat0:
            #print 'outside', lon, lat
            return False

        if float(lon.min()) > self.lon1 or float(lon.max()) < self.lon0:
            #print 'outside', lon, lat
            return False

        return True

    def contourf(self, lon, lat, v, style={'method': 'contourf'}):
        '''draws a field in the specified style'''

        if (not self.within(lon, lat)):
            return

        # if v is not a masked array, mask a NaN values

        if not hasattr(v, 'mask'):
            v = M.masked_where(numpy.isnan(v), v)


        # stop if all elemenents are masked
        if v.count() == 0:
            return

        self.empty = False

        # range
        vmin = style.get('vmin',None)
        vmax = style.get('vmax',None)

        # lookup colormap
        cmap = get_colormap(style)

        ncontours = int(style.get('ncontours',40))

        if (vmin != None and vmax != None):
            #v[v < vmin] = vmin
            #v[v > vmax] = vmax
            # for old version of numpy
            v[numpy.logical_and((v > vmax).data,
                                numpy.logical_not(v.mask))] = vmax
            v[numpy.logical_and((v < vmin).data,
                                numpy.logical_not(v.mask))] = vmin

        if self.CRS == 'vertical':
            if lon.ndim == 1:
                Xp, Yp = numpy.meshgrid(lon, lat)
            else:
                Xp, Yp = (lon, lat)

        else:
            # apply projection
            if lon.ndim == 1:
                Xp, Yp = self.m(*numpy.meshgrid(lon, lat))
            else:
                Xp, Yp = self.m(lon,lat)

        if (style['method'] == 'contourf'):
            if (vmin != None and vmax != None):
                levels = numpy.linspace(vmin,vmax,ncontours)
                self.item = self.m.contourf(Xp, Yp, v, levels=levels, cmap=cmap)
            else:
                self.item = self.m.contourf(Xp, Yp, v, ncontours, cmap=cmap)

        elif (style['method'] == 'contour'):

            # handle case when all values are masked
            if not M.isMaskedArray(v) or v.count() > 0:
                # print('Xp,Yp,v',Xp.shape,Yp.shape,v.shape)
                # print('Xp,Yp,v',v.count())
                # numpy.savetxt("/tmp/v.npy",v)
                # numpy.savetxt("/tmp/Xp.npy",Xp)
                # numpy.savetxt("/tmp/Yp.npy",Yp)
                try:
                    if (vmin != None and vmax != None):
                        levels = numpy.linspace(vmin,vmax,ncontours)
                        self.item = self.m.contour(Xp, Yp, v, levels=levels, cmap=cmap)
                    else:
                        self.item = self.m.contour(Xp, Yp, v, ncontours, cmap=cmap)
                except ValueError:
                    # unfortunately, matplotlib raises an exception wthen nothing is to plot
                    # ValueError: zero-size array to reduction operation minimum which has no identity
                    self.item = None

        else:
            # default style
            # style == 'pcolor_flat'

            #print('Xp, Yp, v', Xp.shape, Yp.shape, v.shape,v.min(),v.max(),type(v))

            #self.item = self.m.pcolor(Xp, Yp, v, shading='flat', cmap=cmap)
            self.item = self.m.pcolor(Xp, Yp, v, edgecolors='none', cmap=cmap)

        if (vmin != None and vmax != None and self.item != None):
            self.item.set_clim(vmin, vmax)


    def render(self, imgformat='image/png'):
        '''return canvas as image'''

        # force the map to fill the rectange
        # procudes streched figure with possible wrong aspect ratio as
        # the WMS standard requires. It is up to the client program to display
        # the map correctly

        if not self.decorated:
            self.ax.set_aspect('auto')
        else:
            if self.item:
                bbox = self.ax.get_position()

                try:
                    # required for matplotlib 0.98.3-4
                    l = bbox.x0
                    b = bbox.y0
                    w = bbox.width
                    h = bbox.height
                except AttributeError:
                    l, b, w, h = bbox

                cax = self.fig.add_axes([l+w+0.05, b, .03, h])

                self.fig.colorbar(mappable=self.item, cax=cax)

            if self.CRS != 'vertical':
                if self.basemap in ['fillcontinents','bluemarble','shadedrelief']:
                    getattr(self.m,self.basemap)()

                #self.m.drawcoastlines(color='0.8', antialiased=0)
                #self.m.fillcontinents()

                #self.m.bluemarble()
                #self.m.shadedrelief()

        # render figure in BytesIO (python 3) or StringIO (python 2)
        stream = BytesIO()
        self.fig.savefig(stream, dpi=self.dpi, format=get_mpl_format(imgformat),
                         transparent = self.transparent
        )
        pic =  stream.getvalue()
        stream.close()

        plt.close(self.fig)
        return pic
