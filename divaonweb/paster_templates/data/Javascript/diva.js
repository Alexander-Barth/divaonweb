/******************************************************************************
 *                                                                            *
 *  Copyright (C) 2008-2014 Alexander Barth <barth.alexander@gmail.com>.      *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU Affero General Public License as published  *
 *  by the Free Software Foundation, either version 3 of the License, or      *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                            *
 ******************************************************************************/

// no set_styles, set_data_range
// call set_stat
// 

// making this a global variable so that it is accessible for
// debugging/inspecting in Firebug

var diva = null;
var debug = false;
var dinputs = new Util.DocumentInputs();

function Diva(){
    this.error_params_old = null;
    this.url_wms = "Python/web/diva_on_web";
    this.url_analysis = 'Python/web/make_analysis';
    this.url_cancel = 'Python/web/cancel';
    this.url_download = 'Python/web/download';
    this.url_fit = 'Python/web/fit';
    this.url_save = 'Python/web/save';
    this.url_bug = 'Python/web/send_bug';

    this.base_zindex = 200;
    this.current_step = 1;

    this.map = new OpenLayers.Map('map',{tileSize: new OpenLayers.Size(512,512),
					 theme: 'css/theme/default/style.css'});
    var options = {
	resolutions: [1.40625,0.703125,0.3515625,0.17578125,0.087890625,0.0439453125,0.02197265625,0.010986328125,0.0054931640625,0.00274658203125,0.00137329101]
    };

    this.ol_wms = new OpenLayers.Layer.WMS( "Metacarta",
					    "http://labs.metacarta.com/wms/vmap0?", 
					    {layers: 'basic'},
					    options);

    this.cont = new OpenLayers.Layer.WMS( "Land-sea mask",
					  "Python/web/continents?", 
					  {layers: 'cont', format: 'image/png', styles: "max_contourf"});

    this.contour0 = new OpenLayers.Layer.WMS("0 m isobath",
					     "Python/web/continents?", 
					     {layers: 'contour0', format: 'image/png', styles: "max_contourf"});

    /*    this.bm = new OpenLayers.Layer.WMS( "NASA Blue marble",
					    "http://wms.jpl.nasa.gov/wms.cgi?", {layers: 'BMNG',
										 format: 'image/png'},
					    {isBaseLayer: true});

    */
    this.bm = new OpenLayers.Layer.WMS( "NASA Blue marble",
					"Python/web/bluemarble?", 
					{layers: 'BMNG', format: 'image/png'},
					{isBaseLayer: true});


    this.analysis_wms = new OpenLayers.Layer.WMS( "Analysis",
						  this.url_wms,
						  {layers: "" ,
						   transparent: "true", format: "image/png", styles: "contourf"},
						  {minResolution: 0.00001,
						   maxResolution: 0.703125});

    this.mesh_wms = new OpenLayers.Layer.WMS( "Mesh",
					      "Python/web/mesh",
					      {layers: "" ,
					       transparent: "true", format: "image/png"},
					      {minResolution: 0.00001,
					       maxResolution: 0.703125});
    this.mesh_wms.setVisibility(false);

    this.obs_location_wms = new OpenLayers.Layer.WMS( "Location of observations",
						      this.url_wms,
						      {layers: "obs_location" ,
						       transparent: "true", format: "image/png", styles: "scatter"},
						      {minResolution: 0.00001,
						       maxResolution: 0.703125});


    this.error_wms = new OpenLayers.Layer.WMS( "Error mask",
					       this.url_wms,
					       {layers: "" , styles: "max_contourf",
						transparent: "true", format: "image/png"},
					       {minResolution: 0.00001,
						maxResolution: 0.703125});

    this.map.addLayers([
	new OpenLayers.Layer.WMS("GEBCO",
                                 "https://www.gebco.net/data_and_products/gebco_web_services/web_map_service/mapserv?" , 
                                 {layers: 'GEBCO_Latest'},
				 {resolutions: 
				  [360/Math.pow(2,8),360/Math.pow(2,9),360/Math.pow(2,10),
				   360/Math.pow(2,11),360/Math.pow(2,12),360/Math.pow(2,13),
				   360/Math.pow(2,14),360/Math.pow(2,15),360/Math.pow(2,16)],
				  isBaseLayer: true}),
	this.bm,this.ol_wms,this.cont,this.contour0]);

    this.map.addControl(new OpenLayers.Control.LayerSwitcher({activeColor: "#0078AE"}));
    this.map.addControl(new OpenLayers.Control.MousePosition());

    this.map.zoomToMaxExtent();

    this.colorbar_container = document.getElementById('map_colorbars');

    var layer = new LayerInfo(null,
                          {wms_url: this.url_wms,
                           WMS_extensions: "OceanBrowser",
                           bounding_box: [this.x0,this.y0,this.x1,this.y1],
                          });

    // figure properties
    var obj = this;
    this.figprop = new FigureProperties("map","map",layer,function() { obj.request_analysis(); },
                                        {colorbarclass: 'colorbar2',
					 colorbar_container: this.colorbar_container,
					 add_style_button: true,
					 colorbar_height: 300
					}
                                        );
    /*
    this.figprop.set_styles([
			     {name: 'contourf',title: 'Filled contours'},
			     {name: 'pcolor_flat',title: 'Flat shading'},
			     {name: 'contour',title: 'Contours'}
			     ]);
    */
}



Diva.prototype.update_stats2 = function(xmldoc_stats,overwrite) {
	//dinputs.enable();
	$("#loading2").hide();

	var error = xmldoc_stats.getElementsByTagName("error");
	if (error.length !== 0) {
	    alert("Error: " + error[0].firstChild.nodeValue);
	    return;
	}

	var warning = xmldoc_stats.getElementsByTagName("warning");
	if (warning.length !== 0) {
	    alert("Warning: " + warning[0].firstChild.nodeValue);
	}

	var debug = xmldoc_stats.getElementsByTagName("debug");
	if (debug.length !== 0) {
	    document.getElementById("debug").firstChild.data = debug[0].firstChild.nodeValue;
	}

	var stats = xmldoc_stats.getElementsByTagName("stat");

	for (var i=0;i<stats.length;i++) {
	    var id = stats[i].getAttribute("id");
	    if (id != "debug") {
		var val = stats[i].firstChild.nodeValue;
		var img  = document.getElementById(id);
		
		if (img) {
		    if (img.nodeName === "INPUT") {
			if (img.value === "" || overwrite)  {
			    img.value = val;
			}
		    }
		    else {
			img.innerHTML = val;
		    }
		}
	    }
	    
	}

	progress.finished();
	console.log("receiving stat");
};


Diva.prototype.update = function(xmldoc) {
	

	var error = xmldoc.getElementsByTagName("error");
	if (error.length !== 0) {
	    alert("Analysis failed :-(\n" + error[0].firstChild.data);
	    return;
	}

	this.update_stats2(xmldoc,true);

	var result = xmldoc.getElementsByTagName("result");
	this.fieldname = result[0].getAttribute("src");
	document.getElementById("fieldname").firstChild.data = this.fieldname;
    // update Layer name for colorbar
    this.figprop.Layer = new LayerInfo(null,
                          {name: this.fieldname + Util.layersep + "analyzed_field",
						   wms_url: this.url_wms,
                           WMS_extensions: "OceanBrowser",
                           bounding_box: [this.x0,this.y0,this.x1,this.y1]
                          }); 
						  
	// data range
	var vmin;
	var vmax;

	vmin = parseFloat(document.getElementById("vmin").firstChild.data);
	vmax = parseFloat(document.getElementById("vmax").firstChild.data);

	if (vmin == vmax) {
	    vmin--;
	    vmax++;
	}

	this.figprop.set_data_range(vmin,vmax);
	
	//this.figprop.set_stat(xmldoc);

	// download links

	var max_error = document.getElementById("max_error").value;
	var obsname = document.getElementById("sessionid").firstChild.data;
	var len = document.getElementById("len").value;

	// for linking and embedding

	var b = this.map.getExtent();
        var mydiva_params = '?analysis=' + this.fieldname +
	      '&max_error=' + max_error +
  	      "&xmin=" + b.left +
	      "&xmax=" + b.right +
  	      "&ymin=" + b.bottom +
	      "&ymax=" + b.top +
  	      "&vmin=" + vmin  +
   	      "&vmax=" + vmax;

	// url for mydiva
	var mydiva_url = window.location.href.replace('diva.html','mydiva-1.0.html') + mydiva_params;

	document.getElementById("embed_link").value = mydiva_url;
	document.getElementById("embed_code").value = '<iframe src ="' + mydiva_url + 
	             '" width="620" height="500" frameborder="0"></iframe>';

	document.getElementById("button_download").disabled = false;
	document.getElementById("button_embed").disabled = false;


	var analysis_params = {
	    styles: this.figprop.get_style(),
	    layers: this.fieldname + Util.layersep + "analyzed_field"
	};

	this.analysis_wms.mergeNewParams(analysis_params);

	var error_params = {
	    max_error: document.getElementById("max_error").value,
	    layers: this.fieldname + Util.layersep + "error_field"
	};

	if (error_params != this.error_params_old) {
	    console.log("updating error field");
	    this.error_wms.mergeNewParams(error_params);
	}

	this.mesh_wms.mergeNewParams({"layers": obsname, "len": len});

	if (this.map.getLayersByName("Analysis").length === 0) {
	    this.map.addLayer(this.analysis_wms);
	    this.analysis_wms.setZIndex(this.base_zindex);
	}
	else {
	    this.analysis_wms.redraw();
	}

	if (this.map.getLayersByName("Error").length === 0) {
	    this.map.addLayer(this.error_wms);
	    this.error_wms.setZIndex(this.base_zindex+1);
	}
	else {
	    this.error_wms.redraw();
	}
	if (this.map.getLayersByName("Mesh").length === 0) {
	    this.map.addLayer(this.mesh_wms);
	    this.mesh_wms.setZIndex(this.base_zindex+2);
	}
	else {
	    this.mesh_wms.redraw();
	}

	document.getElementById("use_posteriori_stn").disabled = false;	

};

Diva.prototype.request_analysis = function() {
    var parameters  = ["sessionid","x0","x1","dx","y0","y1","dy","len","stn","level","max_error","db","adv","diff","vel_md5"];
	var params = Util.getVals(parameters);
	$("#loading_cancel").show();
	dinputs.disable();
	document.getElementById("cancel").disabled = false;

	var obj = this;

	Util.ajax(this.url_analysis,params,function (xmldoc) { 
		dinputs.enable();
		$("#loading_cancel").hide();
		obj.update(xmldoc);
		    }
		);
};


Diva.prototype.cancel = function() {
	var parameters  = ["sessionid"];    
        var params = Util.getVals(parameters);
	dinputs.enable();
	$("#loading_cancel").hide();
	document.getElementById("cancel").disabled = true;

	Util.ajax(this.url_cancel,params,function (xmldoc) { 
		dinputs.enable();
		$("#loading_cancel").hide();
		document.getElementById("cancel").disabled = false;
	    }
	    );

};


Diva.prototype.remove_analysis = function() {
	if (this.map.getLayersByName("Analysis").length !== 0) {
	    this.map.removeLayer(this.analysis_wms);
	}
};

Diva.prototype.remove_error = function() {
	if (this.map.getLayersByName("Error mask").length !== 0) {
	    this.map.removeLayer(this.error_wms);
	}
};

Diva.prototype.remove_mesh = function() {
	if (this.map.getLayersByName("Mesh").length !== 0) {
	    this.map.removeLayer(this.mesh_wms);
	}
};

Diva.prototype.remove_obs_location = function() {
	if (this.map.getLayersByName("Location of observations").length !== 0) {
	    this.map.removeLayer(this.obs_location_wms);
	}

};

Diva.prototype.remove_velocity = function() {
	if (this.map.getLayersByName("Velocity").length !== 0) {
	    this.map.removeLayer(this.vel_wms);
	}
};

Diva.prototype.next = function() {
    //this.change_step(this.current_step+1);
    var $tabs = $('#step').tabs();
//    $tabs.tabs('select', $tabs.tabs('option', 'selected') + 1); 
    $tabs.tabs('option', 'active', $tabs.tabs('option', 'active') + 1); 
};

Diva.prototype.previous = function() {
    //	this.change_step(this.current_step-1);
    var $tabs = $('#step').tabs();
    $//tabs.tabs('select', $tabs.tabs('option', 'selected') - 1); 
    $tabs.tabs('option', 'active', $tabs.tabs('option', 'active') - 1); 
};

Diva.prototype.change_step = function(new_step) {
    //var $tabs = $('#step').tabs();
    //    var new_step = $tabs.tabs('option', 'selected') + 1;

    //alert("new_step " + new_step);
        var bounds;

	switch (new_step) {
	case 1:
	this.remove_analysis();
	this.remove_error();
	this.remove_mesh();
	this.remove_obs_location();
	document.getElementById("button_download").disabled = true;	
	document.getElementById("button_embed").disabled = true;	
	document.getElementById("button_stat").disabled = true;	

	document.getElementById("dx").value = "";
	document.getElementById("dy").value = "";
	document.getElementById("x0").value = "";
	document.getElementById("y0").value = "";
	document.getElementById("x1").value = "";
	document.getElementById("y1").value = "";

	break;
	case 2:
	document.getElementById("button_download").disabled = true;	
	document.getElementById("button_embed").disabled = true;	
	document.getElementById("button_stat").disabled = false;	

	if (new_step > this.current_step) {
	    // set sensible default values

	    this.x0 = parseFloat(document.getElementById("obs_x0").firstChild.data);
	    this.x1 = parseFloat(document.getElementById("obs_x1").firstChild.data);
	    this.y0 = parseFloat(document.getElementById("obs_y0").firstChild.data);
	    this.y1 = parseFloat(document.getElementById("obs_y1").firstChild.data);
	    this.N = parseFloat(document.getElementById("obs_count").firstChild.data);

	    // make sure that x0 != x1 and y0 != y1
	    
	    if (this.x0 == this.x1) {
		this.x0 -= 0.5;
		this.x1 += 0.5;
	    }

	    if (this.y0 == this.y1) {
		this.y0 -= 0.5;
		this.y1 += 0.5;
	    }

	    this.dx = Util.round(Math.sqrt((this.x1-this.x0)*(this.y1-this.y0)/5000),1);
	    //alert(this.dx);

	    if (document.getElementById("dx").value === "") {
		document.getElementById("dx").value = remove_spurious_decimals(this.dx);
	    }

	    if (document.getElementById("dy").value === "") {
		document.getElementById("dy").value = remove_spurious_decimals(this.dx);
	    }

	    var len = Math.sqrt((this.x1-this.x0)*(this.y1-this.y0)/Math.min(100,this.N));
	    len = Util.round(len,1);
	    if (document.getElementById("len").value === "") {
		document.getElementById("len").value = remove_spurious_decimals(len);
	    }

	    if (document.getElementById("x0").value === "") {
		document.getElementById("x0").value = this.x0;
		document.getElementById("x1").value = this.x1;
		document.getElementById("y0").value = this.y0;
		document.getElementById("y1").value = this.y1;
		this.round_coordinates();
	    }

	    bounds = new OpenLayers.Bounds(this.x0,this.y0,this.x1,this.y1);
	    this.map.zoomToExtent(bounds);
	}


	var obs_location_params = {};
	obs_location_params.layers = document.getElementById("sessionid").firstChild.data;
	this.obs_location_wms.mergeNewParams(obs_location_params);

	if (this.map.getLayersByName("Location of observations").length === 0) {
	    this.map.addLayer(this.obs_location_wms);
	    this.obs_location_wms.setZIndex(this.base_zindex+2);
	}
	else {
	    this.obs_location_wms.redraw();
	}

	break;
	case 3:
	this.remove_analysis();
	this.remove_mesh();
	this.remove_error();

	// zoom to choosen grid
	var gx0 = document.getElementById("x0").value;
	var gx1 = document.getElementById("x1").value;
	var gy0 = document.getElementById("y0").value;
	var gy1 = document.getElementById("y1").value;
	
	bounds = new OpenLayers.Bounds(gx0,gy0,gx1,gy1);
	this.map.zoomToExtent(bounds);

	break;


	default:
	    // should not happen
	    break;
	}

	/*
	// hide all divs
	$("#main_interface > div").hide();
	$("#navigation > span").removeClass("navigation_current");

	$("#step-" + new_step).show();
	$("#nav" + new_step).addClass("navigation_current");
	*/
	this.current_step = new_step;

};


Diva.prototype.start_over = function() {
	//document.getElementById("dx").value = "";
	//document.getElementById("dy").value = "";
	document.getElementById("len").value = "";

	this.change_step(1);
};

Diva.prototype.divafit = function() {
	document.getElementById("divafit_quality").style.visibility = "visible";
        var params = Util.getVals(["sessionid"]);

	$("#loading2").show();
	dinputs.disable();

	var obj = this;
	Util.ajax(this.url_fit,params,function (xmldoc) { 
		dinputs.enable();
		obj.update_stats2(xmldoc,true);

		var figname = xmldoc.getElementsByTagName("plot")[0].firstChild.data;
		document.getElementById("divafit_img").src = figname;
		$("#divafit_dialog").dialog('open');
	    }
	    );

	progress.start("fit");
	console.log("updating stat");    
};

Diva.prototype.get_coordinates = function() {
	/*
	var x0 = parseFloat(document.getElementById("obs_x0").firstChild.data);
	var x1 = parseFloat(document.getElementById("obs_x1").firstChild.data);

	var y0 = parseFloat(document.getElementById("obs_y0").firstChild.data);
	var y1 = parseFloat(document.getElementById("obs_y1").firstChild.data);
	*/

	var dx = parseFloat(document.getElementById("dx").value);
	var dy = parseFloat(document.getElementById("dy").value);

	var x0n = Math.floor(this.x0/dx)*dx;
	var x1n = Math.ceil(this.x1/dx)*dx;

	var y0n = Math.floor(this.y0/dy)*dy;
	var y1n = Math.ceil(this.y1/dy)*dy;

	document.getElementById("x0").value = remove_spurious_decimals(x0n);
	document.getElementById("x1").value = remove_spurious_decimals(x1n);
	document.getElementById("y0").value = remove_spurious_decimals(y0n);
	document.getElementById("y1").value = remove_spurious_decimals(y1n);

	//   alert("here " + x0);

};



Diva.prototype.save = function() {
	var params = Util.getVals(["sessionid","fieldname","embed_name","embed_email"]);

	Util.ajax(this.url_save,params,function (xmldoc) { 
		$("#embed_ok").show();
		    }
		);


};

Diva.prototype.send_bug = function() {
    var params = Util.getVals(["sessionid","fieldname","bug_name","bug_email","bug_description"]);

	Util.ajax(this.url_bug,params,function (xmldoc) { 		
		// check if error
		var error = xmldoc.getElementsByTagName("error");
		
		if (error.length === 0) {
		    // every thing is fine
		    $("#bug_ok").show();
		    $("#bug_dialog").dialog('close');
		} 
		else {
		    // error
		    alert(error[0].firstChild.data);
		}

	    });
};


Diva.prototype.update_db = function() {
    this.contour0.mergeNewParams({"db": document.getElementById("db").value});
    this.contour0.redraw();
};

Diva.prototype.round_coordinates = function() {
    var x0 = document.getElementById("x0").value;
    var x1 = document.getElementById("x1").value;

    var y0 = document.getElementById("y0").value;
    var y1 = document.getElementById("y1").value;

    var dx = parseFloat(document.getElementById("dx").value);
    var dy = parseFloat(document.getElementById("dy").value);

    var x0n = Math.floor(x0/dx)*dx;
    var x1n = Math.ceil(x1/dx)*dx;

    var y0n = Math.floor(y0/dy)*dy;
    var y1n = Math.ceil(y1/dy)*dy;

    document.getElementById("x0").value = remove_spurious_decimals(x0n);
    document.getElementById("x1").value = remove_spurious_decimals(x1n);
    document.getElementById("y0").value = remove_spurious_decimals(y0n);
    document.getElementById("y1").value = remove_spurious_decimals(y1n);
};




Diva.prototype.download = function(format) {
    var params = {};
    var range = this.figprop.plotting_range();
    var vmin = range.ca0;
    var vmax = range.ca1;
    
    var max_error = document.getElementById("max_error").value;
    var obsname = document.getElementById("sessionid").firstChild.data;
    var url;
    

    if (format == 'nc' || format == 'mat') {
        // get netcdf metadata
        $('#metadata_dialog').find('input[type=text]').each(function () { 
            params[this.id] = $(this).val(); 
        });
        params['type'] = format;
        params['fieldname'] = this.fieldname;
	url =  Util.append_param(this.url_download,params);
    }
    else if (format == 'image' || format == 'kml') {
	// http://localhost:8080/Python/web/diva_on_web?LAYERS=1234-672b7b61e2_analysis_diva_gebco_1_2_27_41.8_0.1_40.3_46.8_0.1.nc%23analyzed_field,1234-672b7b61e2&TRANSPARENT=true&FORMAT=image%2Fpng&STYLES=method%3Apcolor_flat%2Bncontours%3A40%2Binverted%3Afalse%2Bcmap%3Ajet%2Bvmin%3A-5.64409%2Bvmax%3A26.5889,scatter&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&SRS=EPSG%3A4326&BBOX=22.5,33.75,33.75,45&WIDTH=256&HEIGHT=256&decorated=true


	// http://localhost:8080/Python/web/diva_on_web?layers=1234-3664d8f704_analysis_diva_gebco_1_2_27_41.8_0.1_40.3_46.8_0.1.nc%23analyzed_field%2C1234-3664d8f704_analysis_diva_gebco_1_2_27_41.8_0.1_40.3_46.8_0.1.nc%23error_field&request=GetMap&width=748.9828571428571&height=512&bbox=23.15%2C35.85957%2C45.65%2C51.24043&transparent=true&decorated=true&styles=method%3Acontourf%2Bncontours%3A40%2Binverted%3Afalse%2Bcmap%3Ajet%2Bvmin%3A-5.64409%2Bvmax%3A26.5889%2Cmax_contourf&format=application%2Fvnd.google-earth.kmz&db=gebco&max_error=1

	var image_format;

	if (format == 'image') {
	    image_format = document.getElementById('image_format').value;
	}
	else {
	    image_format = "application/vnd.google-earth.kmz";
	}

	var db = document.getElementById("db").value;
	var extent = this.map.getExtent();

	// same aspect ratio
	var height = 512;
	var width = extent.getWidth() * height / extent.getHeight();

	params = 
     	  {layers: this.fieldname + Util.layersep + "analyzed_field",
	   request: "GetMap",
           service: "WMS",
	   width: width,
	   height: height,
	   bbox: extent.toBBOX(),
	   transparent: true,
	   decorated: true,
	   crs: 'CRS:84',
	   version: '1.3.0',
	   styles: this.figprop.get_style(),
	   format: image_format,
	   db: db // currently not used
	  };

	if (max_error) {
	    params.layers +=  ',' + this.fieldname + Util.layersep + "error_field";
	    params.styles += ',max_contourf';
	    params.max_error = max_error;
	}

	if (this.obs_location_wms.visibility) {
	    params.layers +=  ',' + obsname;
	    params.styles += ',scatter';
	}

	
	url =  Util.append_param(this.url_wms,params);
    }
    else if (format == 'bat-nc') {
	params = Util.getVals(["x0","x1","dx","y0","y1","dy","db"]);
	params["type"] = "bat-nc";
	url =  Util.append_param(this.url_download,params);
    }

    window.open(url,'Image');    
    return false;
};


function ajaxFileUpload()
{

    $("#loading2")
	.ajaxStart(function(){
		//dinputs.disable();
		$(this).show();
	    })
	.ajaxComplete(function(){
		//dinputs.enable();
		$(this).hide();
	    });

    $.ajaxFileUpload
	(
	 {
	     url:Util.append_param('Python/web/upload',Util.getVals(["columnsep","decimalsep"])),
		 secureuri:false,
		 fileElementId: 'data',
		 dataType: 'xml',
		 success: function (data, status) {

		 var xmldoc_stats = data;
		 diva.update_stats2(xmldoc_stats,true);

		 var error = xmldoc_stats.getElementsByTagName("error");
		 if (error.length === 0) {
		     diva.next();
		 }

	     },
		 error: function (data, status, e)
		 {
		     alert(e);
		 }
	 }
	 );
		
	return false;    
}


function ajaxFileUploadODV()
{

    $("#loading2")
	.ajaxStart(function(){
		//dinputs.disable();
		$(this).show();
	    })
	.ajaxComplete(function(){
		//dinputs.enable();
		$(this).hide();
	    });

    $.ajaxFileUpload
	(
	 {
	     url:Util.append_param('Python/web/upload',Util.getVals(["variable","depth","t0","t1"])) + "&type=odv4",
		 secureuri:false,
		 fileElementId: 'data_odv',
		 dataType: 'xml',
		 success: function (data, status) {

		 xmldoc_stats = data;
		 diva.update_stats2(xmldoc_stats,true);

		 var error = xmldoc_stats.getElementsByTagName("error");
		 if (error.length === 0) {
		     diva.next();
		 }

	     },
		 error: function (data, status, e)
		 {
		     alert(e);
		 }
	 }
	 );
		
	return false;    
}



function ajaxFileUploadVel()
{

    $.ajaxFileUpload
	(
	 {
	     url:Util.append_param('Python/web/uploadvel',Util.getVals(["sessionid","uname","vname"])),
		 secureuri:false,
		 fileElementId: 'data_vel',
		 dataType: 'xml',
		 success: function (data, status) {

		 xmldoc_stats = data;
		 diva.update_stats2(xmldoc_stats,true);

		 var error = xmldoc_stats.getElementsByTagName("error");
		 if (error.length === 0) {
		     $('#uploadvel_status').html('upload sucessful');

                     var result = xmldoc_stats.getElementsByTagName("result");
	             var velname = result[0].getAttribute("src");

		     diva.remove_velocity();
		     var layername = 'vector:' + velname + Util.layersep + 'u' + ':' + velname + Util.layersep + 'v';

                     diva.vel_wms = new OpenLayers.Layer.WMS('Velocity',diva.url_wms,
                                                           {layers: layername,
                                                            format: 'image/png'},
							   {isBaseLayer: false});
                     diva.map.addLayer(diva.vel_wms);
		     diva.vel_wms.setZIndex(this.base_zindex+3);

		 }

	     },
		 error: function (data, status, e)
		 {
		     alert(e);
		 }
	 }
	 );
		
	return false;    
}


$(document).ready(function(){
	// get query parameters
	var query = new Util.query(window.location.search.substring(1));

	// set default database
	var db = query.get("db","gebco");
	var options = document.getElementById("db").options;	
	for (var j=0;j<options.length;j++) {
	    options[j].selected = options[j].value == db;
	}		
	// enable debugging if requested
	debug = query.has("debug");


       	$("#loading2").hide();
	$("#loading_cancel").hide();
	$("#bug_ok").hide();
	$("#embed_ok").hide();

	if (!debug) {
	    $("#debug_info").hide();
	    document.getElementById("sessionid").parentNode.style.display = "none";
	    document.getElementById("vmin").parentNode.style.display = "none";
	    document.getElementById("vmax").parentNode.style.display = "none";
	    document.getElementById("fieldname").parentNode.style.display = "none";
	    document.getElementById("vel_md5").parentNode.style.display = "none";
	}

	$("#advanced_settings").hide();

	$("#advanced_settings_header").click(function(event){
		$("#advanced_settings").toggle("slow");
	    });

	diva = new Diva();

	//diva.change_step(1);
	$("#step").tabs();
	$('#step').on('tabsactivate', function(event, ui) {
		ui.index = $(this).tabs('option','active');
		diva.change_step(ui.index+1);
        });


	$("#format").tabs();
	$('#format').on('tabsactivate', function(event, ui) {
		event.stopImmediatePropagation();
        });


	// jQuery UI dialogs
	$("#divafit_dialog").dialog({ autoOpen: false, width: 611, zIndex: 3999 }); 
	$("#statistics_dialog").dialog({ autoOpen: false, width: 611, zIndex: 3999 }); 
	$("#download_dialog").dialog({ autoOpen: false, width: 611, zIndex: 3999 }); 
	$("#embed_dialog").dialog({ autoOpen: false, width: 611, zIndex: 3999 }); 
	$("#bug_dialog").dialog({ autoOpen: false, width: 611, zIndex: 3999 });

        // metadata dialog
 	$("#metadata_dialog").dialog({ autoOpen: false, width: 611, zIndex: 3999 }); 
        //$("#metadata_dialog").dialog('open');

        $('#metadata_dialog form').submit(function() {
           $("#metadata_dialog").dialog('close');
           diva.download('nc');
           return false;
        });

        $('#download_nc').click(function() {
          $("#metadata_dialog").dialog('open');
        });

	// event handers

	//document.getElementById("download_nc").onclick    = function () { return diva.download('nc'); };
	document.getElementById("download_mat").onclick   = function () { return diva.download('mat'); };
	document.getElementById("download_kml").onclick   = function () { return diva.download('kml'); };
	document.getElementById("download_image").onclick = function () { return diva.download('image'); };
	document.getElementById("download_bat_nc").onclick    = function () { return diva.download('bat-nc'); };

	document.getElementById("button_stat").onclick    = function () { $("#statistics_dialog").dialog('open'); };
	document.getElementById("button_download").onclick    = function () { $("#download_dialog").dialog('open'); };
	document.getElementById("button_embed").onclick    = function () { $("#embed_dialog").dialog('open'); };
	//document.getElementById("button_startover").onclick    = function() { diva.start_over() };
	document.getElementById("button_help").onclick    = function () {
	    window.open('http://modb.oce.ulg.ac.be/mediawiki/index.php/Using_Diva_on_web','mywindow'); 
	};

	// open link in new window
	document.getElementById("embed_info").onclick    = function () {
	    window.open(this.href,'mywindow'); 
	    return false;
	};

	document.getElementById("button_bug").onclick    = function () { $("#bug_dialog").dialog('open'); };

	document.getElementById("embed_save").onclick = function() { diva.save(); };
	document.getElementById("bug_send").onclick = function() { diva.send_bug(); };

	document.getElementById("use_posteriori_stn").onclick = function() {
  	    document.getElementById("stn").value = document.getElementById("stat_posteriori_stn").firstChild.data;
	};


    $("#loading2")
	.ajaxStart(function(){
		//dinputs.disable();
		$(this).show();
	    })
	.ajaxComplete(function(){
		//dinputs.enable();
		$(this).hide();
	    });


	
    });

