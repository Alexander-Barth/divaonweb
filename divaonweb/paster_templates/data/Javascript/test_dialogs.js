var SERVERS = [
	       { name: 'local',
		 title: 'localhost',
		 url: 'http://localhost/emodnet/Python/web/wms',
		 version: '1.3.0',
		 autoload: true,
	         extension: 'OceanBrowser' },
	       { name: 'sdn',
		 title: 'SeaDataNet climatologies',
		 url: 'http://gher-diva.phys.ulg.ac.be/web-vis/Python/web/wms',
		 version: '1.3.0',
		 autoload: false,
	         extension: 'OceanBrowser' },
	       { name: 'emodnet-bio',
		 title: 'EMODNet (biological data)',
		 url: 'http://gher-diva.phys.ulg.ac.be/wms/emodnet-bio.xml',
		 version: '1.1.1',
		 autoload: false,
	         extension: 'none' },
	       { name: 'emodnet-hydro',
		 title: 'EMODNet (hydrographic data)',
		 url: 'http://portal.emodnet-hydrography.eu/geoserver/wms',
		 version: '1.3.0',
		 autoload: false,
	         extension: 'none' },
	       { name: 'emodnet-chemical',
		 title: 'EMODNet (chemical data)',
		 url: 'http://gher-diva.phys.ulg.ac.be/emodnet/Python/web/wms',
		 version: '1.3.0',
		 autoload: false,
	         extension: 'OceanBrowser' },
	       { name: 'maris',
		 title: 'SeaDataNet CDI WMS Server - EMODnet Chemistry',
		 url: 'http://gher-diva.phys.ulg.ac.be/wms/wms-maris.xml',
		 version: '1.1.1',
		 autoload: false,
	         extension: 'box-select' },
	       { name: 'ifremer-swe',
		 title: 'IFREMER',
		 url: 'http://www.ifremer.fr/services/wms/swe',
		 version: '1.1.1',
		 autoload: false,
	         extension: 'none' },
	       /*{ name: 'Metacarta',
		 title: 'Metacarta',
		 url: 'http://labs.metacarta.com/wms/vmap0',
		 version: '1.1.1',
		 autoload: false,
	         extension: 'none' },*/
	       { name: 'demis',
		 title: 'Demis World Maps',
		 url: 'http://www2.demis.nl/wms/wms.asp?wms=WorldMap',
		 version: '1.3.0',
		 autoload: false,
	         extension: 'none' }
	       ];



function test_chooseServer() {
	var d = new OceanBrowser.UI.chooseServer('test',SERVERS);
	//var d = new OceanBrowser.UI.chooseServer('test',[]);
	d.events.register('serverSelected',null, function(s) { alert(s.url) });
	d.open();

}

function test_about() {
    var d = new OceanBrowser.UI.About('test');
    d.open();
}

function test_settings() {
    var d = new OceanBrowser.UI.Settings('test');
    d.open();
}


function test_chooseLayer() {
    var baseurl = 'http://localhost/emodnet';

    var baselayer = {url: baseurl + '/' + 'Python/web/continents?',
		     name: 'cont',
		     format: 'image/png'};


    d = new OceanBrowser.UI.chooseLayer('test',SERVERS,
					{baselayer: baselayer,
					 showSectionType: false,
					 showPreview: false});

    d.events.register('layerSelected',null, function(s) { alert(s.field.name) });
    d.open();
}

function test_layerList() {
    /*
	    $('#totoLayerList').dialog({ autoOpen: false, width: 300, height: 500});
	    $('#totoLayerList').dialog('open');

	    $('.LegendButton').click(function() {

		    $(this).parent().find('.LayerListLegend').toggle("slow");
		    $(this).find('.ui-icon').toggleClass("ui-icon-triangle-1-e");
		    $(this).find('.ui-icon').toggleClass("ui-icon-triangle-1-s");

		});
    */
    var server = "Python/web/wms";
    var cap = new OceanBrowser.UI.LayerTree("chooser",server,
					{show_title: true
					});

    var d = new OceanBrowser.UI.LayerList('lala');

    cap.events.register('loaded', null, function() {
	    
	    //alert('toto' + cap.layer_infos[4].name);
	    //add_layer(cap.layer_infos[4]);
	    d.open();
	    d.add(cap.layer_infos[4]);

	});
    cap.load();


}
function test_layerdownload() {

    var server = "Python/web/wms";
    var cap = new OceanBrowser.UI.LayerTree("chooser",server,
					{show_title: true
					});


    cap.events.register('loaded', null, function() {
	    
	    //add_layer(cap.layer_infos[4]);
	    var d = new OceanBrowser.UI.LayerDownload('lala',cap.layer_infos[4],
						      function() { 
							  return {left: -180, right: 180, bottom: -90, top: 90} 
						      });
	    d.open();

	    d.events.register('download', null, function(param) {
		    console.log(param);
		});
	});
    cap.load();

}
$(document).ready(function() {
	//test_chooseLayer();
	//test_about();
	test_settings();
	//test_layerList();
	//test_layerdownload();

    });