o3djs.require('o3djs.util');
o3djs.require('o3djs.math');
o3djs.require('o3djs.quaternions');
o3djs.require('o3djs.rendergraph');
o3djs.require('o3djs.primitives');
o3djs.require('o3djs.arcball');
o3djs.require('o3djs.io');
o3djs.require('o3djs.material');



var g = {
  EARTH_RADIUS: 25,
};

g.camera = {
     eye: [0, 0, 75],
    //eye: [-58.86641754160468, 37.49999999999999, -31.696369630552457],
     target: [0, 0, 0],
};

var g_finished = false;  // for selenium.
var dragging = false;

function startDragging(e) {
  g.lastRot = g.thisRot;
  g.aball.click([e.x, e.y]);
  dragging = true;
}

function drag(e) {
  if (dragging) {
    rotationQuat = g.aball.drag([e.x, e.y]);
    var rot_mat = g.quaternions.quaternionToRotation(rotationQuat);
    g.thisRot = g.math.matrix4.mul(g.lastRot, rot_mat);

    var m = g.root.localMatrix;
    g.math.matrix4.setUpper3x3(m, g.thisRot);
    g.root.localMatrix = m;
  }
}

function stopDragging(e) {
  dragging = false;
}

function updateViewFromCamera() {
  var target = g.camera.target;
  var eye = g.camera.eye;
  var up = [0, 1, 0];
  g.viewInfo.drawContext.view = g.math.matrix4.lookAt(eye, target, up);
  g.eyePosParam.value = eye;
}

function setEye(lon,lat) {
    var lonr = (90-lon)*Math.PI/180; 
    var latr = lat*Math.PI/180; 

    rot_mat = g.math.matrix4.identity();
    rot_mat = g.math.matrix4.mul(o3djs.math.matrix4.rotationX(latr),rot_mat);
    rot_mat = g.math.matrix4.mul(o3djs.math.matrix4.rotationY(lonr),rot_mat);

    g.thisRot = rot_mat;

    var m = g.root.localMatrix;
    g.math.matrix4.setUpper3x3(m, rot_mat);
    g.root.localMatrix = m;
}


function scrollMe(e) {
  if (e.deltaY > 0) {
    g.camera.eye[0] *= 11 / 12;
    g.camera.eye[1] *= 11 / 12;
    g.camera.eye[2] *= 11 / 12;

  } else {
    g.camera.eye[0] *= (1 + 1 / 12);
    g.camera.eye[1] *= (1 + 1 / 12);
    g.camera.eye[2] *= (1 + 1 / 12);
  }
  updateViewFromCamera();
}


function setClientSize() {
  var newWidth = g.client.width;
  var newHeight = g.client.height;

  if (newWidth != g.o3dWidth || newHeight != g.o3dHeight) {
    g.o3dWidth  = newWidth;
    g.o3dHeight = newHeight;

    // Create a perspective projection matrix
    g.viewInfo.drawContext.projection = g.math.matrix4.perspective(
      g.math.degToRad(45), g.o3dWidth / g.o3dHeight, 0.1, 5000);

    // Sets a new area size for arcball.
    g.aball.setAreaSize(g.o3dWidth, g.o3dHeight);
  }
}

function onRender() {
  setClientSize();
}

// A geo is a float where the integer part is in degrees and the fractional
// part is in 60ths
function geoToRad(geo) {
  var sign = geo >= 0 ? 1 : -1;
  geo = Math.abs(geo);
  var integerPart = Math.floor(geo);
  var fractionalPart = (geo % 1) * 100;
  fractionalPart = fractionalPart / 60;
  return g.math.degToRad(integerPart + fractionalPart);
}


/**
 * Creates the client area.
 */
function init() {
  o3djs.util.makeClients(initStep2);
 
  init3D();
}
/**
 * Initializes o3d
 * @param {Array} clientElements Array of o3d object elements.
 */
function initStep2(clientElements) {
  var path = window.location.href;
  var index = path.lastIndexOf('/');

  g.o3dElement = clientElements[0];
  g.o3d = g.o3dElement.o3d;
  g.math = o3djs.math;
  g.quaternions = o3djs.quaternions;
  g.client = g.o3dElement.client;

  g.pack = g.client.createPack();

  // Create the render graph for a view.
  g.viewInfo = o3djs.rendergraph.createBasicView(
      g.pack,
      g.client.root,
      g.client.renderGraphRoot);

  // Set the background color to black.
  g.viewInfo.clearBuffer.clearColor = [0, 0, 0, 0];

  // Set states for shards.
  g.viewInfo.zOrderedState.getStateParam('CullMode').value =
      g.o3d.State.CULL_NONE;
  g.viewInfo.zOrderedState.getStateParam('DestinationBlendFunction').value =
      g.o3d.State.BLENDFUNC_ONE;
  g.viewInfo.zOrderedState.getStateParam('ZWriteEnable').value = false;

  g.viewInfo.performanceDrawPass.sortMethod = g.o3d.DrawList.BY_PRIORITY;

  g.lastRot = g.math.matrix4.identity();
  g.thisRot = g.math.matrix4.identity();

  var root = g.client.root;

  // Create a param for the sun and eye positions that we can bind
  // to auto update a bunch of materials.
  g.globalParams = g.pack.createObject('ParamObject');
  g.eyePosParam = g.globalParams.createParam('eyePos', 'ParamFloat3');

  updateViewFromCamera();

  g.aball = o3djs.arcball.create(100, 100);
  setClientSize();

  g.client.setRenderCallback(onRender);


  // Create Materials.
  var effectNames = [
    "dayOnly",
    ];
  g.materials = [];

  for (var ii = 0; ii < effectNames.length; ++ii) {
    var effectName = effectNames[ii];
    var effect = g.pack.createObject('Effect');
    effect.loadFromFXString(document.getElementById(effectName).value);

    // Create a Material for the effect.
    var material = g.pack.createObject('Material');

    // Apply our effect to this material. The effect tells the 3D hardware
    // which shader to use.
    material.effect = effect;

    // Set the material's drawList
    material.drawList = g.viewInfo.performanceDrawList;

    // This will create the effects's params on the material.
    effect.createUniformParameters(material);


    // Save off the material.
    g.materials.push(material);
  }

  g.dayOnlyMaterial = g.materials[0];

  // create samplers
  g.samplers = [];
  for (var ii = 0; ii < 4; ++ii) {
    var sampler = g.pack.createObject('Sampler');
    g.samplers[ii] = sampler;
  }

  g.daySampler = g.samplers[0];
  g.BaseSampler = g.samplers[1];

  // set the material samplers.
  g.dayOnlyMaterial.getParam('daySampler').value = g.daySampler;
  g.dayOnlyMaterial.getParam('BaseSampler').value = g.BaseSampler;


  // Setup counters to fade in textures.
  // WMS layer

  // Create a sphere at the origin for the earth.
  var earth = o3djs.primitives.createSphere(g.pack,
                                            o3djs.material.createBasicMaterial(g.pack, g.viewInfo, [.3,.3,1.,1]),
                                            25,
                                            50,
                                            50);


  var plane = o3djs.primitives.createPlane(
      g.pack,
      o3djs.material.createBasicMaterial(g.pack, g.viewInfo, [.3,.3,1.,1]),
      100,      // Width.
      1.618,  // Depth.
      3,      // Horizontal subdivisions.
      3);     // Vertical subdivisions.

  // Get a the element so we can set its material later.
  g.earthPrimitive = earth.elements[0];


  g.root = g.pack.createObject('Transform');
  g.root.parent = g.client.root;
  g.earth = g.pack.createObject('Transform');
  g.earth.addShape(earth);
  //g.earth.addShape(plane);
  g.earth.parent = g.root;

  o3djs.event.addEventListener(g.o3dElement, 'mousedown', startDragging);
  o3djs.event.addEventListener(g.o3dElement, 'mousemove', drag);
  o3djs.event.addEventListener(g.o3dElement, 'mouseup', stopDragging);
  o3djs.event.addEventListener(g.o3dElement, 'wheel', scrollMe);

  url = base_url + 'Python/web/bluemarble?LAYERS=BMNG&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&SRS=EPSG%3A4326&BBOX=-180,-90,180,90&WIDTH=2048&HEIGHT=1024';

  url = base_url + 'Python/web/continents?LAYERS=cont&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&SRS=EPSG%3A4326&BBOX=-180,-90,180,90&WIDTH=2048&HEIGHT=1024';

  //alert(url);
  loadDayTexture(base_url + 'img/blank.png');
  loadBaseTexture(url);

  setEye(0,0);
}


function loadDayTexture(url) {

  o3djs.io.loadTexture(g.pack, url, function(texture, exception) {
      if (exception) {
        alert(exception);
      } else {
         g.daySampler.texture = texture;
         g.earthPrimitive.material = g.dayOnlyMaterial;
         //g.flatToDayCounter.running = true;
      }
    });

}


function loadBaseTexture(url) {

  o3djs.io.loadTexture(g.pack, url, function(texture, exception) {
      if (exception) {
        alert(exception);
      } else {
	  console.log('loaded' + texture);
         g.BaseSampler.texture = texture;
         g.earthPrimitive.material = g.dayOnlyMaterial;
         //g.flatToDayCounter.running = true;
      }
    });

}



function uninit() {
  // TODO: We should clean up any counters that have callbacks here.
  if (g.client) {
    g.client.cleanup();
  }
}

