var EmodnetChemistry = {
    //WPSODV_baseurl: 'http://dtvirt5.deltares.nl/wps'
    //WPSODV_baseurl: 'http://emodnet02.cineca.it/wps'
    WPSODV_baseurl: 'http://emodnet01.cineca.it/wps'
};


EmodnetChemistry.parseParamList = function(p) {
    var params = [];
    
    for (var key in p) {
        if (p.hasOwnProperty(key)) {
            params.push({
                title: p[key][2],
                altTitle: p[key][1],                        
                aggregated_param: p[key][0],
                codes: [p[key][0]],
                p36_code: p[key][3],
                p36_title: p[key][5],
                t0: new Date(p[key][6]),
                t1: new Date(p[key][7])
            });
            
        }
    }
    
    console.log(params);
    return params;
    
};


EmodnetChemistry.groupParameters = function(layerTree,params,ft,path,isTimeSeries) {

    var tree = [];

    for (var i=0; i < params.length; i++) {
        var starttime = Util.ISODateString(new Date(params[i].starttime),'YYYY-MM-DD');
        var endtime = Util.ISODateString(new Date(params[i].endtime),'YYYY-MM-DD');
        
        //var WFSLayer = new WFSLayerInfoColor(
        //var WFSLayer = new WFSLayerInfo(
        var WFSLayer = new WFSLayerInfoColorDensity(
            ft, layerTree,
            {'title':  params[i].prefTitle, 'parameters': [params[i].codes],
             'name': params[i].codes, 'featureTypeName': ft.name,
             'isTimeSeries': isTimeSeries,
             'fullpath': path.concat([params[i].p36_title,params[i].prefTitle])
            });

        WFSLayer.dimensions.time.range.lower.min = starttime;
        WFSLayer.dimensions.time.range.lower.max = endtime;
        WFSLayer.dimensions.time.range.upper.min = starttime;
        WFSLayer.dimensions.time.range.upper.max = endtime;

        var item = {
            text: params[i].prefTitle, 
            code: params[i].aggregated_param,
            data: WFSLayer
        }


        // look if P36 is already in the tree of parameters
        var found = -1; 
        for (var j=0; j < tree.length; j++) {
            if (tree[j].p36_code === params[i].p36_code) {
                found = j;
                break;
            }
        }


        if (found === -1) {
            // new group
            tree.push({
                text: params[i].p36_title,
                p36_code: params[i].p36_code,
                children: [item]
            });
        }
        else {
            // add entry to existing P36 group
            tree[found].children.push(item);
        }

    }

    return tree;
};

EmodnetChemistry.LayerTree = function LayerTree(featureTypes) {

    var tree = [], ft, that = this;
    //console.log('ft',featureTypes,this);
    var featureTypeName = 'emodnet:observed_cindex';
    ft = featureTypes.filter(function(_) { return _.name === featureTypeName});
    
    if (ft.length === 0) {
        alter('feature type ' + featureTypeName + ' not found');
        return;
    }

    if (0) {
        var WPSserver = new OceanBrowser.WPS.server(EmodnetChemistry.WPSODV_baseurl);
        WPSserver.execute2(
            'bbox_get_parameters',{bbox: '-180,180,-90,90'},
            function(_) { 
                console.log(_);
                /*
                var index = 0; d = JSON.parse(_.data);
                params = []; 
                // make array
                while (d[index]) { 
                    params.push(d[index++]) 
                };  
                */

                var ad = _['all_observations'].data;
                var tree = EmodnetChemistry.groupParameters(that,ad,ft[0],['All observations']);
                console.log('tree ',tree);                         
                
                that.tree = tree;
                that.events.triggerEvent('treeReady',that.tree);
            } 
        );
    }
    else {
        var url = 'Javascript/emodnet_chemistry_varlist.json';

        var request = OpenLayers.Request.GET({
            url: url,
            //params: params,
            success: function(request) {
                console.log(request);

                //var params = EmodnetChemistry.parseParamList(JSON.parse(request.responseText));
                //var params = EmodnetChemistry.varlist;
                var params = JSON.parse(request.responseText);
                var tree = EmodnetChemistry.groupParameters(that,params,ft[0],['All observations']);

                console.log('tree ',tree);                         

                that.tree = tree;
                that.events.triggerEvent('treeReady',that.tree);

            },
            failure: function(request) {                    
                var msg = 'Error while connecting to ' +  url;
                alert(msg);
            }
        });       
    }
  
};


EmodnetChemistry.pesentFeature = function(feature,layerinfo) {
    var template = [
         '<div class="WFS feature_content" id="{{feature_id}}" >',
         /*'<h3><span class="P01">{{param_id}}</span> ',
         '[<span class="Units">{{param_units}}</span>]</h3>',*/
         //'<div style="width: 815px"><iframe width="815" height="590" src="{{url}}" style="border-width: 0px"></iframe></div>',
         '<div><iframe width="815" height="590" src="{{url}}" style="border-width: 0px"></iframe></div>',
         /*
         '<h4>Metadata:</h4>',
         '<div class="metadata_list">{{links}}</div>',
         */
         '</div>',        
        ].join('\n');
    
    var center = feature.geometry.getCentroid();

    // number of pixel representing a click
    var npixels = 5;
    // degree for a click (divided by 2)
    var eps = hsection.map.getResolution() * npixels/2;
    //  W,E,S,N
    var bbox =  [center.x-eps,center.x+eps,center.y-eps,center.y+eps];

    // Metadata list
    var bounds = new OpenLayers.Bounds(bbox[0],bbox[2],bbox[1],bbox[3]);
    
    var timerange = layerinfo.figprops.get_dimension('time');
    var elevationrange = layerinfo.figprops.get_dimension('elevation');
    var format = 'YYYY-MM-DD';

    var url = Util.append_param('view.html',{
        bbox: bbox.join(','),
        //edmo: feature.data.edmo,
        //LOCAL_CDI_ID: feature.data.local_cdi_id,
        parameter: feature.data.p35_id,
        starttime: Util.ISODateString(timerange.lower,format),
        endtime: Util.ISODateString(timerange.upper,format),
        zlim0: elevationrange.lower,
        zlim1: elevationrange.upper
    });
    var feature_id = 'feature_' + feature.data.id;
    var vocab_url = 'http://vocab.nerc.ac.uk/collection';
    /*
    var request = OpenLayers.Request.GET({
        url: 'http://vocab.nerc.ac.uk/collection/P35/current/' + feature.data.p35_id + '/',
        success: function (request) {
            var skos = 'http://www.w3.org/2004/02/skos/core#';
            var prefLabel = request.responseXML.getElementsByTagNameNS(skos,'prefLabel')[0].firstChild.data;
            $('#' + feature_id).find('.P01').text(prefLabel);     
        },
        failure: function(request) {
            $('#' + feature_id).find('.P01').text(feature.data.p35_id);
        }
    });
    */


    var html = OceanBrowser.render(template,
                               {
                                   feature_id: feature_id,
                                   param_id: feature.data.p35,
                                   param_units: feature.data.unit,                                              
                                   datetime: feature.data.datetime,
                                   edmo: feature.data.edmo,
                                   quality: feature.data.quality,
                                   local_cdi_id: feature.data.local_cdi_id,
                                   lon: center.x,
                                   lat: center.y,
                                   type: feature.data.platform_type,
                                   url: url
                               });
     console.log('html',html);
     return html;
};


EmodnetChemistry.selectBox = function(map,layerinfo,bbox) {
    console.log(bbox);

    var timerange = layerinfo.figprops.get_dimension('time');
    var elevationrange = layerinfo.figprops.get_dimension('elevation');
    var format = 'YYYY-MM-DD';

    //var bounds = new OpenLayers.Bounds(bbox[0],bbox[2],bbox[1],bbox[3]);


    var url = Util.append_param('view.html',{
        bbox: bbox.join(','),        
        parameter: layerinfo.parameters[0],
        starttime: Util.ISODateString(timerange.lower,format),
        endtime: Util.ISODateString(timerange.upper,format),
        zlim0: elevationrange.lower,
        zlim1: elevationrange.upper,
        cdis: cdis
     });
                  
     window.open(url,'data');
}; 
