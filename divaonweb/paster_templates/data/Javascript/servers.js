// hide: if true, it is not shown in the list (add extrnal layers)
// all fields will be available in OceanBrowser.UI.LayerTree object

var SERVERS =
    [
        {
            name: 'sdn',
            title: 'SeaDataNet - Map Server',
            url: 'http://sdn.oceanbrowser.net/web-vis/Python/web/wms',
            version: '1.3.0',
            service: 'WMS',
            autoload: false,
            extension: 'OceanBrowser'
        },

        {
            name: 'oceanotron',
            title: 'SeaDataNet - Oceanotron (in situ observations)',
            //url: 'http://oceanotrondemo0.ifremer.fr/oceanotron/WMS/wms',
	    url: 'http://www.ifremer.fr/oceanotron/WMS/wms',
            version: '1.3.0',
            service: 'WMS',
            //autoload: window.location.href.indexOf('web-vis') > 0,
            autoload: false,
            extension: 'ncWMS',
            default_dim: function(varname,dimname) {
                var dims = {
                        elevation: [0, 10],
                        time: ['2000-01-01T00:00:00.000Z', '2001-01-01T00:00:00.000Z']
                };

                return dims[dimname];
            },
            WMS: {
                layers: {
                    keep: function(layer) {
                        // keep layer that containt SDN (or its parant layers)
                        return layer.FullPath().filter(
                            function(_) {
                                return (_.indexOf('SDC') !== -1);
                                ;}
                        ).length > 0;
                    }
                }
            }
        },

        {
            name: 'emodnet-chemical',
            title: 'EMODNET Chemistry - Eutrophication',
            url: 'http://ec.oceanbrowser.net/emodnet/Python/web/wms',
            version: '1.3.0',
            service: 'WMS',
            autoload:  (window.location.href.indexOf('test-1') > 0) || (window.location.href.indexOf('emodnet') > 0),
            extension: 'OceanBrowser',
            download_form: true,
            present_dim: function(values,dimname,layer_name) {
		if (layer_name.indexOf("6-year running") > 0) {
		    //DIVA
                    return Util.present_dim_timeaverage(values,dimname,layer_name,3,2);
		    //DIVAnd
                    //return Util.present_dim_timeaverage(values,dimname,layer_name,2,3);
		}
		else {
		    return values;
		}
            }
        },

        {
            name: 'emodnet-chemical-all',
            title: 'EMODNET Chemistry - Eutrophication',
            url: 'https://ec.oceanbrowser.net/emodnet-projects/Python/web/wms',
            version: '1.3.0',
            service: 'WMS',
            autoload:  false,
            download_form: true,
            extension: 'OceanBrowser',
            present_dim: function(values,dimname,layer_name) {
		console.log("layer_name ",layer_name);
		if (layer_name.indexOf("Phase-2") == 0) {
                    return Util.present_dim_timeaverage(values,dimname,layer_name,5,4);
		}
		else {
                    return Util.present_dim_timeaverage(values,dimname,layer_name,3,2);
		}
            }
        },

        {
            name: 'emodnet-chemical-obs-dyn',
	    title: 'EMODNET Chemistry - Plots',
            url: 'Python/web/wpswms',
            version: '1.3.0',
            service: 'WMS',
            autoload: window.location.href.indexOf('enable-plots') > 0,
/*            autoload: (window.location.href.indexOf('emodnet') > 0 ||
                       window.location.href.indexOf('test-1') > 0)
                      && !(window.location.href.indexOf('emodnet-combined') > 0)
                      && !(window.location.href.indexOf('emodnet-projects') > 0),*/
            extension: 'OceanBrowser',
            default_dim:  function(varname,dimname) {
                var dims;

                if (['EPC00105', // chlorophyll-a
                     'EPC00002', // dissolved oxygen
                     'EPC00004', // nitrate
                     'EPC00005', // nitrate plus nitrite
                     'EPC00006', // nitrite
                     'EPC00007', // phosphate
                     'EPC00009', // ammonium
                     'EPC00134', // total nitrogen
                     'EPC00135', // total phosphorus
                     'EPC00008'  // silicate
                ].indexOf(varname) !== -1) {
                   dims = {
                        elevation: [0, 10],
                        time: ['2000-01-01T00:00:00.000Z', '2001-01-01T00:00:00.000Z']
                   };
                }
                else {
                   // maximum range
                   dims = {
                        elevation: [0, 10],
                        time: ['1900-01-01T00:00:00.000Z', new Date().toISOString()]
                   };
                }

                return dims[dimname];
            },
            popup: 'large'
        },

        {
            name: 'emodnet-chemistry-geoserver',
            title: 'EMODNET Chemistry - Litter',
            url: 'https://sextant.ifremer.fr/services/wms/emodnet_chemistry2',
            version: '1.3.0',
            service: 'WMS',
            /*autoload:  window.location.href.indexOf('emodnet') > 0,*/
            autoload:  (window.location.href.indexOf('litter') > 0) || (window.location.href.indexOf('emodnet') > 0),
            extension: 'none',
            featureinfo_formats: ['text/html'],
            download_form: true,
            download_URL: function(layer) {
                var url = Util.append_param("http://sextant.ifremer.fr/services/wfs/emodnet_chemistry2",{
                        REQUEST: "GetFeature",
                        SERVICE: "WFS",
                        VERSION: "1.1.0",
                        TypeName: layer.name,
                        OUTPUTFORMAT: "SHAPEZIP"});
                console.log("download_URL ",url);
                return {
                    format: "application/x-shapefile+zip",
                    url: url};
            },

            disclaimer: "WARNING: Data have been homogenized and filtered in order to allow comparisons among countries. Thus, EMODnet products might not be comparable with source data accessible through other platforms. The absence of data on the map doesn't necessarily mean that they don't exist, but that no information has been entered in the Marine Litter Database for this area.",
            WMS: {
                single_tile: function(layer) {
                    return true;
                }
            }
        },


        {
            name: 'emodnet-chemistry-geoserver-ogs',
            title: 'EMODNET Chemistry - Contaminants',
            url: 'https://nodc.inogs.it/geoserver/Contaminants/wms',
            version: '1.3.0',
            service: 'WMS',
            autoload:  window.location.href.indexOf('emodnet') > 0,
            /*autoload:  (window.location.href.indexOf('contaminants') > 0),*/
            extension: 'none',
            download_form: true,
            featureinfo_formats: ['text/html']
        },

/*
        {
            name: 'emodnet-chemistry-geoserver',
            title: 'EMODNET Chemistry - Litter',
            url: 'http://www.emodnet-chemistry.eu/geoserver/ows',
            version: '1.3.0',
            service: 'WMS',
            autoload:  window.location.href.indexOf('emodnet') > 0,
            extension: 'none',
            featureinfo_formats: ['text/html'],
            WMS: {
                layers: {
                    keep: function(layer) {
                        // keep layer that containt SDN (or its parant layers)
                        // Nodc:microlitter_survey_transects_180322
                        return layer.FullPath().filter(function(_) { return _.indexOf('litter') !== -1;}).length > 0;
                    }
                }
            }
        },
            */
        {
            name: 'maris',
            title: 'EMODNET Chemistry - Data access',
            url: 'https://geo-service.maris.nl/emodnet_chemistry/wms',
            //http://geoservice.maris2.nl/wms/seadatanet/emodnet_chemistry',
            version: '1.3.0',
            service: 'WMS',
            autoload: false,
            extension: 'box-select'
        },


        {
            name: 'maris2',
            title: 'EMODNET Chemistry - Distribution of CDI observations per data category (P36) and MSFD sea regions',
            //url: 'http://geoservice.maris2.nl/wms/project/emodnet_chemistry_service',
            url: "https://geo-service.maris.nl/emodnet_chemistry_p36/wms",
            version: '1.3.0',
            service: 'WMS',
            autoload: window.location.href.indexOf('emodnet') > 0 && !(window.location.href.indexOf('emodnet-projects') > 0),
            featureinfo_formats: ['text/html']
        },


        {
            name: 'emodnet-bio',
            title: 'EMODNET Biology',
            url: 'https://geo.vliz.be/geoserver/Emodnetbio/wms',
            version: '1.3.0',
            service: 'WMS',
            autoload: false,
            extension: 'none'
        },

/*
        {
            name: 'emodnet-bio-wfs',
            title: 'EMODNET Biology - WFS',
            url: 'http://geo.vliz.be/geoserver/wfs',
            version: '1.0.0',
            service: 'WFS',
            attribution: {
                title: "EMODNET Biology",
                href: "http://www.emodnet-biology.eu/"
            },
            WFS: {}
        },
*/
        {
            name: 'emodnet-bath',
            title: 'EMODNET Bathymetry',
            url: 'http://ows.emodnet-bathymetry.eu/wms',
            version: '1.3.0',
            service: 'WMS',
            autoload: false,
            extension: 'none'
        },
/*
        {
          name: 'emodnet-phys',
          title: 'EMODNET Physics',
          url: 'http://151.1.25.219:8080/cgi-bin/mapserv.exe?map=C:/ms4w/apps/gisclient-3.5/map/ett/stazioni.map',
          version: '1.0.0',
          service: 'WFS',
          autoload: false,
          extension: 'none',
          attribution: {
              title: "EMODNET Physics",
              href: "http://www.ettsolutions.com/"
          },
          WFS: {
              pesentFeature: function(feature) {
                  var template =
                      ['<table class="feature_content">',
                       '<tr>',
                       '  <td>Layer</td>',
                       '  <td>{{layer}}</td>',
                       '</tr>',
                       '<tr>',
                       '  <td>Identifier</td>',
                       '  <td>{{id}}</td>',
                       '</tr>',
                       '<tr>',
                       '  <td>Longitude</td>',
                       '  <td>{{lon}}</td>',
                       '</tr>',
                       '<tr>',
                       '  <td>Latitude</td>',
                       '  <td>{{lat}}</td>',
                       '</tr>',
                       '<tr>',
                       '  <td>Type</td>',
                       '  <td>{{type}}</td>',
                       '</tr>',
                       '<tr>',
                       '  <td>Plot</td>',
                       '  <td><a href="{{url}}" target="_blank">Time serie</a></td>',
                       '</tr>',
                       '</table>'].join('\n');

                  var center = feature.geometry.getCentroid();

                  return OceanBrowser.render(template,
                                             {layer: feature.layer.name,
                                              id: feature.data.platform_id,
                                              lon: center.x,
                                              lat: center.y,
                                              type: feature.data.platform_type,
                                              url: feature.data.platform_info});
              },
              presentTitle: function(feature) {
                  if (feature.title !== undefined) {
                      return feature.title.replace(/_/g,' ');
                  }
                  else {
                      return feature.name;
                  }
              },
              featureList: {
                  selected:  ['parameters_mooring.water_temperature_MO'],
                  ignore: ['acque.acque','regioni.mari','stazioni.black','stazioni.north',
                           'stazioni.baltic','stazioni.med','stazioni.atlantic','stazioni.arctic',
                           'stazioni.stazioni']
                  //keep: [...]
              }
          }
        },
*/

        /*
          { name: 'emodnet-physics',
          title: 'EMODNET (physical data)',
          url: 'http://151.1.25.219:8080/gisclient/services/ows.php?PROJECT=ett&MAP=stazioni&GISCLIENT_MAP=1',
          version: '1.3.0',
          service: 'WMS',
          autoload: true,
          extension: 'none' },
        */


        /*{ name: 'Metacarta',
          title: 'Metacarta',
          url: 'http://labs.metacarta.com/wms/vmap0',
          version: '1.1.1',
          service: 'WMS',
          extension: 'none' },*/

        /*
        {
            name: 'demis',
            title: 'Demis World Maps',
            url: 'http://www2.demis.nl/wms/wms.asp?wms=WorldMap',
            version: '1.3.0',
            service: 'WMS',
            autoload: false,
            extension: 'none'
        },
*/

/*
        { name: 'maris2',
          title: 'SeaDataNet CDI WMS Server 2',
          url: 'http://gher-diva.phys.ulg.ac.be/wms/wms-maris2.xml',
          version: '1.1.1',
          service: 'WMS',
          autoload: false,
          extension: 'box-select-wfs' },
*/

/*
        {
            name: 'vliz',
            title: 'Flanders Marine Institute (VLIZ) - WMS',
            url: 'http://geo.vliz.be/geoserver/wms',
            version: '1.1.1',
            service: 'WMS',
            autoload: false,
            extension: 'none'
        },
*/

/*
        {
            title: 'SeaDataNet CDI',
            url: 'http://geoservice.maris2.nl/wfs/seadatanet/cdi_v2/simorc',
            version: '1.0.0',
            service: 'WFS',
            autoload: false,
            extension: 'none',
            attribution: {
                html: '<a href="http://www.seadatanet.org/" target="_blank" class="OceanBrowserAttributionImage"><img src="seadatanet-logo.png" alt="logo" height="50"></a>',
                title: "SeaDataNet",
                href: "http://www.seadatanet.org/"
            },
            WFS: {
                pesentFeature: function(feature) {
                    var template =
                        ['<table class="feature_content">',
                         '<tr>',
                         '  <td>Layer</td>',
                         '  <td>{{layer}}</td>',
                         '</tr>',
                         '<tr>',
                         '  <td>Identifier</td>',
                         '  <td>{{id}}</td>',
                         '</tr>',
                         '<tr>',
                         '  <td>Longitude</td>',
                         '  <td>{{lon}}</td>',
                         '</tr>',
                         '<tr>',
                         '  <td>Latitude</td>',
                         '  <td>{{lat}}</td>',
                         '</tr>',
                         '<tr>',
                         '  <td>Description</td>',
                         '  <td>{{dataname}}</td>',
                         '</tr>',
                         '<tr>',
                         '  <td>Metadata</td>',
                         '  <td><a href="{{url}}" target="_blank">Link</a></td>',
                         '</tr>',
                         '</table>'].join('\n');

                    var center = feature.geometry.getCentroid();

                    return OceanBrowser.render(template,
                                               {layer: feature.layer.name,
                                                id: feature.data.n_code,
                                                lon: center.x,
                                                lat: center.y,
                                                dataname: feature.data.dataname,
                                                url: feature.data.url.replace('v_cdi_v2','v_cdi_v3')});
                }
            }
        },
*/

/*
        {
            title: 'ECOOP Database - WFS',
            url: 'http://geoservergisweb2.hrwallingford.co.uk:8080/geoserver/wfs',
            version: '1.0.0',
            service: 'WFS',
            WFS: {
                featureList: {
                    keep: ['Hydrology', 'TimeSeriesParameterDubaiCoast', 'TimeSeriesParameterECOOP', 'TimeSeriesPointDubaiCoast', 'TimeSeriesPointECOOP']
                }
            }
        },

        {
            title: 'NOAA Atlantic/Caribbean Hurricane',
            url: 'http://gis.srh.noaa.gov/arcgis/services/AtStormViewer/MapServer/WFSServer',
            version: '1.0.0',
            service: 'WFS',
            WFS: {}
        }
*/

    ];
