/******************************************************************************
 *                                                                            *
 *  Copyright (C) 2008-2010 Alexander Barth <barth.alexander@gmail.com>.      *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU Affero General Public License as published  *
 *  by the Free Software Foundation, either version 3 of the License, or      *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                            *
 ******************************************************************************/

"use strict";


/*

map = new WMSGL.Map('id');
layer = new WMSGL.Layer(title,name,WMSparam,options);

map.addLayer(layer);

var topo = new EarthGL.Topo(64,12);

LayerInfo = LayerInfo(xml,options);
LayerInfo.figprop

// fun calls  figprop.set_stat(xml);
LayerInfo.onload = function fun(xml);

// param: additional parameters for GetStat request
LayerInfo.loadStat(param);


method of OceanBrowser.UI.viewer3D respond to user events

EarthScene = new OceanBrowser.UI.viewer3D('id');
EarthScene.addLayer(layerinfo);
EarthScene.removeLayer(layerinfo);


*/

var EarthScene = null;
var cap;

/*var baseurl = 'http://gher-diva.phys.ulg.ac.be/web-vis';
var baseurl = 'http://localhost:8081';
var baseurl = 'http://localhost/diva-on-web';
var baseurl = 'http://localhost/emodnet';*/
var baseurl = '.';

//var baseurl = 'http://gher17.phys.ulg.ac.be:8081';


var baselayer = {url: baseurl + '/' + 'Python/web/continents?',
		 name: 'cont',
		 format: 'image/png'};

var SERVERS = [
	       { name: 'local',
		 title: 'localhost',
		 url: 'Python/web/wms',
		 version: '1.3.0',
		 autoload: true,
	         extension: 'OceanBrowser' },
	       { name: 'sdn',
		 title: 'SeaDataNet climatologies',
		 url: 'http://gher-diva.phys.ulg.ac.be/web-vis/Python/web/wms',
		 version: '1.3.0',
		 autoload: false,
	         extension: 'OceanBrowser' },
	       { name: 'emodnet-bio',
		 title: 'EMODNet (biological data)',
		 url: 'http://gher-diva.phys.ulg.ac.be/wms/emodnet-bio.xml',
		 version: '1.1.1',
		 autoload: false,
	         extension: 'none' },
	       { name: 'emodnet-hydro',
		 title: 'EMODNet (hydrographic data)',
		 url: 'http://portal.emodnet-hydrography.eu/geoserver/wms',
		 version: '1.3.0',
		 autoload: false,
	         extension: 'none' },
	       { name: 'emodnet-chemical',
		 title: 'EMODNet (chemical data)',
		 url: 'http://gher-diva.phys.ulg.ac.be/emodnet/Python/web/wms',
		 version: '1.3.0',
		 autoload: false,
	         extension: 'OceanBrowser' },
	       { name: 'maris',
		 title: 'SeaDataNet CDI WMS Server - EMODnet Chemistry',
		 url: 'http://gher-diva.phys.ulg.ac.be/wms/wms-maris.xml',
		 version: '1.1.1',
		 autoload: false,
	         extension: 'box-select' },
	       { name: 'ifremer-swe',
		 title: 'IFREMER',
		 url: 'http://www.ifremer.fr/services/wms/swe',
		 version: '1.1.1',
		 autoload: false,
	         extension: 'none' },
	       /*{ name: 'Metacarta',
		 title: 'Metacarta',
		 url: 'http://labs.metacarta.com/wms/vmap0',
		 version: '1.1.1',
	         extension: 'none' },*/
	       { name: 'demis',
		 title: 'Demis World Maps',
		 url: 'http://www2.demis.nl/wms/wms.asp?wms=WorldMap',
		 version: '1.3.0',
		 autoload: false,
	         extension: 'none' }
	       ];


        

FigureProperties.prototype.getLayer = function(param,options) {
    var p2 = this.get_layer_param();
    var p = p2.param;

    p.layers = this.Layer.name;
    p.styles = p2.style;    	
    EarthGL.Util.merge(p,param);

    if (this.constraints.type === 'lon-lat') {    
	return new EarthGL.Layer(this.Layer.title,this.wms_url,p,options);
    }
    else {
	var lon = this.constraints.lon;
	var lat = this.constraints.lat;
	var z = [-5000,0];
	console.log('section',lon,lat);
	return new EarthGL.Section(this.Layer.title,this.wms_url,
				   lon,lat,z,
				   p);
    }
};



FigureProperties.prototype.loadStat = function(param){
    var that = this;
    var params = {};

    var p = this.get_layer_param();

    params.layer = this.Layer.name;
    params.request = 'GetStats';
    params.service = "WMS";
    params.version = "1.3.0";

    // add parameters
    if (param) {
	for (p in param) {
	    params[p] = param[p];
	}
    }

    var fun = function(xmldoc) {
	if (xmldoc) {
	    that.stat = {};
	    that.stat.xmldoc = xmldoc;
	    that.stat.xmin = parseFloat(xmldoc.getElementsByTagName("xmin")[0].firstChild.nodeValue);
	    that.stat.xmax = parseFloat(xmldoc.getElementsByTagName("xmax")[0].firstChild.nodeValue);
	    that.stat.ymin = parseFloat(xmldoc.getElementsByTagName("ymin")[0].firstChild.nodeValue);
	    that.stat.ymax = parseFloat(xmldoc.getElementsByTagName("ymax")[0].firstChild.nodeValue);

	    
	    if (that) {
		console.log('set stat ',that.Layer.title,that.Layer.name);
		that.set_stat(xmldoc);
	    }
	}

	if (that.onload) {
	    that.onload(xmldoc);
	}
    };

    Util.proxy_ajax(this.Layer.wms_url,params,null,fun,{proxy: this.Layer.use_proxy});
};




(function() {
    $(document).ready(function() {
	    $('#warning_no_webgl').hide();

	    try {
		EarthScene = new OceanBrowser.UI.viewer3D('example',
							  {servers: SERVERS,
							   previewBaselayer: baselayer});
	    }
	    catch (err) {
		$('#warning_no_webgl').show();
	    }

	    EarthScene.addLayerControl.trigger();
	});
})();