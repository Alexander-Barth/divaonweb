/******************************************************************************
 *                                                                            *
 *  Copyright (C) 2008-2014 Alexander Barth <barth.alexander@gmail.com>.      *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU Affero General Public License as published  *
 *  by the Free Software Foundation, either version 3 of the License, or      *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                            *
 ******************************************************************************/


// WMS ranges
// decode intervales such as
// 2007-12-01/2008-03-20/P1D

function WMSRange(s,options) {
    option = options | {};

    this.units = options.units;
    this.format = options.format || 'YYYY-MM-DD';

    var list = s.split(',');
    var range = [], r;

    for (var i=0; i<list.length; i++) {

        // remove leading and trailing whitespaces
        list[i] = list[i].replace(/^\s+|\s+$/g,"");

        if (list[i].indexOf('/') == -1) {
            // no decoding
            range.push(list[i]);
        }
        else {
            r = list[i].split('/');
            var subrange = {};

            subrange.min = this.decode_dim(r[0]);
            subrange.max = this.decode_dim(r[1]);

            if (r.length == 3) {
                subrange.step = this.decode_step(r[2]);
            }
            range.push(subrange);
        }
    }

    this.range = range;
};


// returns a list of strings. Each elements is the
// dimension encoded

WMSRange.prototype.list = function() {
    var list = [];

    for (var i=0; i<this.range.length; i++) {

       if (this.range[i].min === undefined) {
           // no parsing in constructor, so no encoding
           //list.push(this.encode_dim(this.range[i]));
           list.push(this.range[i]);
       }
       else if (this.range[i].step !== undefined || this.range[i].step === 0) {
           var x = this.range[i].min;

           while (x <= this.range[i].max) {
                list.push(this.encode_dim(x));
                x += this.range[i].step;
           }
       }
       else {
           throw "Cannot generate a list from continous range";
       }
    }

    return list;
};

WMSRange.prototype.decode_dim = function(s) {
        if (this.units == "ISO8601") {
            return Util.mjd(s);
        }
        else {
            return parseFloat(s);
        }
};


WMSRange.prototype.encode_dim = function(s) {
        if (this.units == "ISO8601") {
            return Util.gregd(s,this.format);
        }
        else {
            return "" + s;
        }
};

// decode internally the interval step
WMSRange.prototype.decode_step = function(s) {

        if (this.units == "ISO8601") {
            if (s == 'P1D') {
                return 1;
            }
        }

        return parseFloat(s);
};

// return minimum value

WMSRange.prototype.min = function() {
    var vals = [];

    for (var i=0; i<this.range.length; i++) {
       if (this.range[i].min === undefined) {
           vals.push(this.range[i]);
       }
       else {
           vals.push(this.range[i].min);
       }
    }

    return this.encode_dim(Math.min.apply(null, vals));
};

// return maximum value

WMSRange.prototype.max = function() {
    var vals = [];

    for (var i=0; i<this.range.length; i++) {
       if (this.range[i].max === undefined) {
           vals.push(this.range[i]);
       }
       else {
           vals.push(this.range[i].max);
       }
    }

    return this.encode_dim(Math.max.apply(null, vals));
};


// if any subrange does not have a defined step, then the range is continous
// and cannot be used to generate a list

WMSRange.prototype.iscontinous = function() {

    for (var i=0; i<this.range.length; i++) {
       if (this.range[i].max !== undefined) {
           if (this.range[i].step === undefined || this.range[i].step === 0) {
               return true;
           }
       }
    }

    return false;
};

WMSRange.prototype.toString = function() {
    var vals = [];

    for (var i=0; i<this.range.length; i++) {
       if (this.range[i].max === undefined) {
           vals.push(this.range[i]);
       }
       else if (this.range[i].step === undefined) {
           vals.push(this.range[i].min + '/' + this.range[i].max);
       }
       else {

       }
    }

    return this.encode_dim(Math.max.apply(null, vals));
};


function WMSDimToString(dim,timeformat) {
    timeformat = timeformat || 'YYYY-MM-DDThh:mm:ss.sssZ';

    if (dim instanceof Date) {
        return Util.ISODateString(dim,timeformat);
    }
    else {
        return dim.toString();
    }
}

function WMSDimRangeToString(dim,timeformat) {

    if (dim.upper !== undefined) {
        return WMSDimToString(dim.lower,timeformat) + '/' + WMSDimToString(dim.upper,timeformat);
    }
    else {
        return WMSDimToString(dim,timeformat);
    }
}

function WMSParams(param) {
    if (param.time) {
        param.time = WMSDimRangeToString(param.time);
    }

    if (param.elevation) {
        param.elevation = WMSDimRangeToString(param.elevation);
    }

    return param;
}

function test_WMSRange() {
   var r = new WMSRange('2007-12-21/2008-01-3/P1D',{units: 'ISO8601'});

   var r2 = new WMSRange('10/20/1',{units: 'm'});

   console.assert(r2.list().length === 11,'check length');
   console.assert(r2.min() === "10",'check min');
   console.assert(r2.max() === "20",'check max');
   console.assert(r2.iscontinous() === false,'check continous');

   var r2 = new WMSRange('10/20',{units: 'm'});

   console.assert(r2.min() === "10",'check min');
   console.assert(r2.max() === "20",'check max');
   console.assert(r2.iscontinous() === true,'check continous');


   var rc = new WMSRange('2012-03-24T12:00:00.000Z/2012-03-26T01:00:00.000Z',
      {units: 'ISO8601', format: 'YYYY-MM-DDThh:mm:ss.sssZ'});

   console.assert(rc.min() === "2012-03-24T12:00:00.000Z",'check min (ISO date input)');
   console.assert(rc.iscontinous() === true,'check continous');

   var rc = new WMSRange('2012-03-24/2012-03-26',
      {units: 'ISO8601', format: 'YYYY-MM-DDThh:mm:ss.sssZ'});

   console.assert(rc.min() === "2012-03-24T00:00:00.000Z",'check min (convert input)');
   console.assert(rc.iscontinous() === true,'check continous');


    var d = WMSDimRangeToString({lower: 12, upper: 20});
    console.assert(d === "12/20",'check WMSDimRangeToString scalar');

    // month is zero-based (month = 0 -> January, ...)
    var d = WMSDimRangeToString(new Date(Date.UTC(2000,12-1,31,23,59,59)));
    console.assert(d === "2000-12-31T23:59:59.000Z",'check WMSDimRangeToString date');

    var d = WMSDimRangeToString({
        lower: new Date(Date.UTC(2000,12-1,31,23,59,59)),
        upper: new Date(Date.UTC(2001,12-1,31,23,59,59))});

    console.assert(d === "2000-12-31T23:59:59.000Z/2001-12-31T23:59:59.000Z",'check WMSDimRangeToString date');


    var d = WMSDimRangeToString(new Date(Date.UTC(2000,12-1,31,23,59,59)),'YYYY-MM-DD');
    console.assert(d === "2000-12-31",'check WMSDimRangeToString date with format');

}

// representation of a dataset
// description come from the XML section of the GetCapability request
// loadStat() performs additional request
// options:
//   wms_url ('')
//   use_proxy (null)


LayerInfo = OpenLayers.Class({

    initialize: function (layer_xml,options) {
        var fmt, url;

        this.layer_xml = layer_xml;
        this.name = '<no name>';
        this.title = '<no title>';
        this.abstract_info = null;
        this.wms_url = '';
        this.use_proxy = null;
        // OceanBrowser.UI.LayerTree
        this.cap = null;
        this.WMS_extensions = null;
        this.stat = {};
        this.figprops = null;
        this.parentLayer = null;
        this.identifiers = [];
        this.dimensions = {};
        this.attribution = {};
        this.data_url = [];
        this.metadata_url = null;

        if (this.layer_xml) {
            if (options.cap) {
                this.cap = options.cap;
            }

            this.parseXML();
            this.parseXMLBB();
            this.parseXMLAttribution();
            this.parseXMLStyles();
            this.parseXMLDimensions();
        }


        this.id = 'layer' + this.name;

        // options
        if (options) {
            for (var p in options) {
                this[p] = options[p];
            }
        }

        if (this.cap) {
            if (this.cap.download_URL) {
		var data_url = this.cap.download_URL(this);
		console.log("this.cap.download_URL",url);
		this.data_url.push(data_url);
            }
	}
    },


    parseXML: function() {
        var item = this.layer_xml.firstChild;

        while (item !== null) {

            // ignore empty elements
            if (item.firstChild) {
                if (item.tagName == "Name") {
                    this.name = item.firstChild.data;
                }
                else if (item.tagName == "Title") {
                    this.title = item.firstChild.data;
                }
                else if (item.tagName == "Abstract") {
                    this.abstract_info = item.firstChild.data;
                }
                else if (item.tagName == "MetadataURL") {
                    fmt = item.getElementsByTagName("Format")[0].firstChild.data;
                    url = item.getElementsByTagName("OnlineResource")[0].getAttribute("xlink:href");

                    if (fmt == 'text/html') {
                        this.metadata_url = url;
                    }

                    // get HTML URL from XML URL for emodnet-chemistry
                    if (fmt.startsWith("text/xml") && (this.metadata_url === null)) {
                        if (url.startsWith("https://sextant.ifremer.fr/geonetwork/srv/eng/csw")) {
                            var q = new Util.query(url);
                            this.metadata_url = "http://www.emodnet-chemistry.eu/products/catalogue#/metadata/" + q.get("id");
                        }
                    }
                }
                else if (item.tagName == "DataURL") {
                    fmt = item.getElementsByTagName("Format")[0].firstChild.data;
                    url = item.getElementsByTagName("OnlineResource")[0].getAttribute("xlink:href");

                    this.data_url.push({format: fmt, url: url});

                    if (fmt == 'application/x-netcdf') {
                        this.http_url = url;
                    }
                    else {
                        this.opendap_url = url;
                    }
                }
                else if (item.tagName == "Identifier") {
                    this.identifiers.push({'authority': item.getAttribute('authority'),
                                           'id': item.firstChild.data});

                    //console.log('identifiers ',this.name,this.identifiers);

                }

            }

            item = item.nextSibling;
        }
    },

    parseXMLBB: function() {
        // parse bounding box

        // default boundary box
        var b = [-180,-90,180,90];

        // for version 1.3.0
        var bb = Util.get_tags(this.layer_xml,'EX_GeographicBoundingBox')[0];

        if (bb) {
            b = [
                parseFloat(bb.getElementsByTagName('westBoundLongitude')[0].firstChild.data),
                parseFloat(bb.getElementsByTagName('southBoundLatitude')[0].firstChild.data),
                parseFloat(bb.getElementsByTagName('eastBoundLongitude')[0].firstChild.data),
                parseFloat(bb.getElementsByTagName('northBoundLatitude')[0].firstChild.data)];

        }
        else {
            // for version 1.1.1
            bb = Util.get_tags(this.layer_xml,'LatLonBoundingBox')[0];

            if (bb) {
                b = [
                    parseFloat(bb.getAttribute("minx")),
                    parseFloat(bb.getAttribute("miny")),
                    parseFloat(bb.getAttribute("maxx")),
                    parseFloat(bb.getAttribute("maxy"))];
            }
        }

        this.bounding_box = b;

    },

    get_bounding_box: function() {
        return this.bounding_box;
    },


    get_bounds: function() {
        var b = this.get_bounding_box();
        return new OpenLayers.Bounds(b[0],b[1],b[2],b[3]);
    },




    /*

      <Dimension name="time" units="ISO8601"/>
      <Extent name="time">2007-12-01/2010-03-20/P1D</Extent>
    */
    parseXMLDimensions: function() {
        function parseRange(s,units,default_value,server_default) {
            var format = 'YYYY-MM-DDThh:mm:ss.sssZ';
            var r = new WMSRange(s,{units: units, format: format});
            var defaults;

//            if (default_value == "" && server_default) {
            if (server_default) {
               defaults = server_default;
            }
            else {
                defaults = [default_value, default_value];
            }

            if (r.iscontinous()) {
                return {
                    lower: {min: r.min(), max: r.max(), default_value: defaults[0]},
                    upper: {min: r.min(), max: r.max(), default_value: defaults[1]}
               };
            }
            else {
                return r.list();
            }

        }


        var dimensions = Util.get_tags(this.layer_xml,"Dimension");
        var units = '';
        var range = [];
        var default_value = null;
        var i, extent, name;

        if (this.cap.version == '1.3.0') {
            for (i=0; i < dimensions.length; i++) {
                name = dimensions[i].getAttribute("name");
                units = dimensions[i].getAttribute("units");
                range = dimensions[i].firstChild.data;
                //range = wms_range(range,units);

                default_value = dimensions[i].getAttribute('default');

                var server_default;
                if (this.cap.default_dim) {
                    if (typeof this.cap.default_dim === "function") {
                        server_default = this.cap.default_dim(this.name,name);
                        //console.log(this.name,this.title,server_default);
                    }
                    else {
                        server_default = this.cap.default_dim[name];
                    }
                }

                range = parseRange(range,units,default_value,server_default);
                //console.log('range ',range,name);

                this.dimensions[name] =
                    {range: range, units: units, default_value: default_value};
            }
        }
        else {
            // version 1.1.1
            for (i=0; i < dimensions.length; i++) {
                name = dimensions[i].getAttribute("name");
                units = dimensions[i].getAttribute("units");

                extent = Util.get_tags(this.layer_xml,"Extent");
                for (i=0; i < extent.length; i++) {
                    if (extent[i].getAttribute("name") == name) {
                        if (extent[i].firstChild) {
                            range = extent[i].firstChild.data;
                            default_value = extent[i].getAttribute('default');
                            range = parseRange(range,units,default_value);

                            this.dimensions[name] =
                                {range: range, units: units, default_value: default_value};
                        }
                        break;
                    }
                }
            }

        }

        for (var name in this.dimensions) {
            if (this.dimensions.hasOwnProperty(name)) {
                if (this.cap.present_dim) {
                    this.dimensions[name].range_label = this.cap.present_dim(this.dimensions[name].range,name,this.name);
                }
                else {
                    this.dimensions[name].range_label = this.dimensions[name].range;
                }

              //console.log("this.dimensions[name]",name,this.dimensions[name]);
           }
        }

        /*
            // for debugging
            this.dimensions = {
            'elevation': {
                //'range': ['0-10','10-20','20-30','30-50','50-100','100-200'],
                //'range': {min: 0, max: Infinity},
                'range': {upper: {min: 0, max: Infinity}, lower: {min: 0, max: Infinity}},

                'units': 'm',
                'default_value': '10-20'
            }

        };
*/


    },

    get_dimension: function(name) {
        return this.dimensions[name];
    },

    has_dimension: function(name) {
        return this.dimensions[name] !== undefined;
    },

    parseXMLAttribution: function() {
        var attr = Util.get_tags(this.layer_xml,"Attribution")[0];

        if (!attr) {
            return;
        }
        var tmp = attr.getElementsByTagName("Title");
        if (tmp.length > 0) {
            this.attribution.title = tmp[0].firstChild.data;
        }

        tmp = attr.getElementsByTagName("OnlineResource");
        if (tmp.length > 0) {
            this.attribution.url = tmp[0].getAttribute("xlink:href");
        }

        tmp = attr.getElementsByTagName("LogoURL");
        if (tmp.length > 0) {
            tmp = tmp[0].getElementsByTagName("OnlineResource");
            if (tmp.length > 0) {
                this.attribution.logo = tmp[0].getAttribute("xlink:href");
            }
        }
    },

    get_attribution: function() {
        return this.attribution;
    },

    // get list of possible style

    parseXMLStyles: function() {
        // TODO inherit style
        var styles = this.layer_xml.getElementsByTagName('Style');
        var s = [];
        var legend_url, j, name, title;

        for (j=0; j < styles.length; j++) {
            try {
                legend_url = styles[j].getElementsByTagName("OnlineResource")[0].getAttribute("xlink:href");
            }
            catch (err) {
                legend_url = null;
            }

	    
            try {
		name = styles[j].getElementsByTagName("Name")[0].firstChild.data;
	    }
	    catch (err) {
		name = "_default_"
	    }

            try {
                title = styles[j].getElementsByTagName("Title")[0].firstChild.data;
            }
            catch (err) {
                title = name;
            }

            s[j] = {name: name,
                    title: title,
                    legend_url: legend_url
                   };
        }

        this.styles = s;
        //console.log(this.name,this.styles);
    },

    get_styles: function() {
        return this.styles;
    },


    /**
     * make a getStat request and call onload when finished
     */
    // depreciated
    // LayerInfo should not have a figprop method, because it prevent to use
    // multiple sections (horizontal and vertical on the same map)
    // loadStat is now a method of FigureProperties

    loadStat: function(param) {
        var that = this;
        var params = {}, p;

        if (this.figprops) {
            p = this.figprops.get_layer_param();
            params = p.param;
        }

        params.layer = this.name;
        params.request = 'GetStats';
        params.service = "WMS";
        params.version = "1.3.0";

        // add parameters
        if (param) {
            for (p in param) {
                params[p] = param[p];
            }
        }

        var fun = function(xmldoc) {
            if (xmldoc) {
                that.stat.xmldoc = xmldoc;
                that.stat.xmin = parseFloat(xmldoc.getElementsByTagName("xmin")[0].firstChild.nodeValue);
                that.stat.xmax = parseFloat(xmldoc.getElementsByTagName("xmax")[0].firstChild.nodeValue);
                that.stat.ymin = parseFloat(xmldoc.getElementsByTagName("ymin")[0].firstChild.nodeValue);
                that.stat.ymax = parseFloat(xmldoc.getElementsByTagName("ymax")[0].firstChild.nodeValue);


                if (that.figprops) {
                    //console.log('set stat ',that.title,that.name);
                    that.figprops.set_stat(xmldoc);
                }
            }

            if (that.onload) {
                that.onload(xmldoc);
            }
        };

        Util.proxy_ajax(this.wms_url,params,null,fun,{proxy: this.use_proxy});
    },


    getLegendGraphic: function(style_param,width,height) {
        var s, url = null;
        //console.log('getLegendGraphic 2',style_param,width,height);

        if (this.WMS_extensions === 'OceanBrowser') {
            s = '';
            var val, id;
            for (var id in style_param) {
                val = style_param[id];

                if (val !== undefined) {
                    s +=  id + ':' + val + '+';
                }
                else {
                    console.log('id ',id,' undefined');
                }

            }


            // remove last plus
            s = s.slice(0,s.length-1);
        }
        else {
            var styles = this.get_styles();
            var name = style_param.name;

            for (var j=0; j < styles.length; j++) {
                if (this.styles[j].name == name) {
                    url = styles[j].legend_url;
                    break;
                }
            }

            s = style_param.name;

            // draw ticks around
        if (this.WMS_extensions === 'ncWMS') {
            var len = 128;

            url = Util.append_param(this.wms_url,{
                request: 'GetLegendGraphic',
                service: "WMS",
		version: "1.3.0",
                palette: style_param.name.split('/')[1],
                colorbaronly: true,
                width: 16,
                height: len});

            var orientation = 'horizontal'; // or 'vertical'
          //orientation = 'vertical';
          var container = $('<div/>').addClass('WFSColorBarContainer').addClass(orientation).append(
            elem = $('<div/>').addClass('WFSColorBar').addClass(orientation),
            axis = $('<div/>').addClass('LabelContainer')
          );


          if (orientation === 'horizontal') {

               elem.append($('<div/>').css({
                'background-image': 'url(' + url + ')',
                'width': 16,
                'height': len,
                'transform': 'rotate(90deg)',
                'margin-top': -(len/2 - 16/2),
                'margin-bottom': -(len/2 - 16/2),
                'margin-left': (len/2 - 16/2)
                }));

            this.axis = new Axis(axis.get(0),"x",
                                 {tickslabelclass: "xtickslabel", ticksclass: "xticks"});

             this.axis.def_len = function() { return this.len = len-2; };
          }
          else {
            elem.append($('<div/>').css({
                'background-image': 'url(' + url + ')',
                'width': 16,
                'height': len
                }));

            this.axis = new Axis(axis.get(0),"y",
                                 {tickslabelclass: "ytickslabel", ticksclass: "yticks"});

            // jQuery cannot determine the length of the div as it is not drawn jet
            this.axis.def_len = function() { return this.len = len-2; };
          }
           this.axis.set_range(style_param.vmin,style_param.vmax);

           return {elem: container.get(0), style: style_param.name};
        }

        }

        if (!url) {

            var params = {
                request: "GetLegendGraphic",
                service: "WMS",
		version: "1.3.0",
                width: width,
                height: height,
                transparent: true,
                decorated: true,
                style: s,
                color: '#000000',
                format: 'image/png'
            };

            params.layer = this.name;
            url = Util.append_param(this.wms_url,params);
        }

        return {url: url, style: s};
    },

    // negociate info_formats
    infoFormat: function(preferred_info_formats) {
        var available_info_formats = this.cap.requests.GetFeatureInfo.formats;
        // override with settings from server.js
        if (this.cap.featureinfo_formats) {
            available_info_formats = this.cap.featureinfo_formats;
        }

        for (var i in preferred_info_formats) {
            for (var j in available_info_formats) {
                if (preferred_info_formats[i] == available_info_formats[j]) {
                    return preferred_info_formats[i];
                }
            }
        }

        return null;
    },


    FullPath: function() {

        if (this.fullpath !== undefined) {
            return this.fullpath;
        }

        var path = [];
        if (this.parentLayer) {
            path = this.parentLayer.FullPath();
        }

        path.push(this.title);
        return path;
    },

    FullTitle: function(sep) {
        sep = sep || ' <span class="separator">&#9656;</span> ';
        return this.FullPath().join(sep);
    },

    hitCount: function(q) {
        var hits = 0, i, kw;

        kw = q.keywords;
        //console.log('q ',q);

        for (i in kw) {
            hits += this.title.toLowerCase().indexOf(kw[i].toLowerCase()) >= 0 ? 1 : 0;
        }

        if (this.parentLayer) {
            // do this more efficiently
            hits += this.parentLayer.hitCount(q);
        }

        return hits;
    },

    // search score for a query

    searchScore: function(q) {
        var inside, hitc = 0;

        if (q.point !== undefined) {

            inside =
                (this.bounding_box[0] <= q.point[0] && q.point[0] <= this.bounding_box[2]) &&
                (this.bounding_box[1] <= q.point[1] && q.point[1] <= this.bounding_box[3]);

            if (!inside) {
                return -1;
            }
            hitc = 1;
        }

        if (this.name === '<no name>') {
            return -1;
        }
        else {
            return hitc + this.hitCount(q);
        }

    },

    // get ID of this layer (or parent layer)
    // returns [] if none found

    get_ids: function() {
        if (this.identifiers.length !== 0)  {
            return this.identifiers;
        }

        if (this.parentLayer) {
            // do this more efficiently
            return this.parentLayer.get_ids();
        }

        return [];
    },

    get_uuid: function() {
        var ids = this.get_ids();
        for (var i = 0; i < ids.length; i++) {
            if (ids[i]["authority"] == "seadatanet") {
                return ids[i]["id"];
            }
        }
        return null
    },

    // get a layer that can be added by OpenLayer map.addLayer function
    // option.styles
    // option.param

    get_layer: function(options) {
        // id will be reencoded by OpenLayers

        var singleTile = false;
        if (this.cap.WMS) {
            if (this.cap.WMS.single_tile) {
                singleTile = this.cap.WMS.single_tile(this);
            }
        }

        var layer = new OpenLayers.Layer.WMS(this.title,
                                             this.wms_url,
                                             {layers: this.name,
                                              styles: options.style,
                                              transparent: "true",
                                              format: "image/png"},
                                             {minResolution: 0.00001,
                                              maxResolution: 1,
                                              singleTile: singleTile,
                                              buffer: 0 });

        // options.param.time (and .elevation) can be an object with lower and upper attributes
        // make simple strings

        options.param = WMSParams(options.param);

        layer.mergeNewParams(options.param);
        this.layer = layer;
        return layer;
    },

    describe: function() {
        return Util.ncfile(this.name);
    }
});


WFSLayerInfoBase = OpenLayers.Class(LayerInfo, {
    initialize: function(feature,server) {
        this.feature = feature;
        // should disapear
        this.server = server;
        // OceanBrowser.UI.LayerTree
        this.cap = server;

        this.id = 'feature-' + feature.name;
        this.name = feature.name;
        this.title = feature.title;
        this.identifiers = [];

        //console.log('WFS id',this.id,this.name);

        this.bounding_box = [this.feature.minx,this.feature.miny,this.feature.maxx,this.feature.maxy];
        this.styles = [];
        this.dimensions = {};
        this.layer = null;

        this.style = OpenLayers.Feature.Vector.style["default"];
    },

    getLegendGraphic: function(style_param,width,height) {
        var url, that = this;

        //console.log('getLegendGraphic',style_param);

        // use style from options
        if (style_param.fillColor !== undefined) {
            this.style.fillColor = style_param.fillColor;
            this.style.strokeColor = style_param.fillColor;
        }
        if (style_param.fillOpacity !== undefined) {
            this.style.fillOpacity = style_param.fillOpacity;
        }

        if (style_param.pointRadius !== undefined) {
            this.style.pointRadius = style_param.pointRadius;
        }

//color #ee9900
        var elem = $(OceanBrowser.render(
            '<svg width="50" height="50"><circle cx="25" cy="25" r="{{pointRadius}}" fill="{{fillColor}}" fill-opacity="{{fillOpacity}}" stroke="{{fillColor}}" stroke-opacity="1" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-dasharray="none" pointer-events="visiblePainted" cursor="inherit"> </circle></svg>',
            this.style));

        // keep reference to legend elem
        this.elem = elem[0];
        return {elem: elem[0], style: style_param};
    },

    get_layer: function(options) {
        var that = this, server = this.server;
        var layer, style, color, v;

        var title = this.feature.title;
        var attribution;
        //title = server.WFS.presentTitle(feature);
        if (server.attribution) {
            if (server.attribution.html) {
                attribution = server.attribution.html;
            }
            else {
                attribution = OceanBrowser.render('<a href="{{href}}" target="_blank" class="OceanBrowserAttribution">{{title}}</a>',server.attribution);
            }
        }


        //return 'rgb(' + Math.round(255*c[0]) + ',' + Math.round(255*c[1]) + ',' + Math.round(255*c[2]) + ')';


        var parts = this.name.split(':');
        var featureNS = parts[0];
        var featureType = parts[1];
        var geometryName = 'geom';

        if (this.server.directConnection) {
            var saveProxy = OpenLayers.ProxyHost;
            OpenLayers.ProxyHost = null;
        }

        layer = new OpenLayers.Layer.Vector(title, {
            strategies: [new OpenLayers.Strategy.BBOX()],
            visibility: true,
            protocol: new OpenLayers.Protocol.WFS({
                version: "1.1.0",
                url:  server.url,
                geometryName: geometryName,
                featureType: featureType,
                featureNS: featureNS
            }),
            style: this.style,
            attribution: attribution
        });

        // restore proxy settings
        if (this.server.directConnection) {
            OpenLayers.ProxyHost = saveProxy;
        }

        var cmap = this.cmap;

        layer.events.register('loadend', null, function (e) {
            if (e.response.priv) {
                var exception = e.response.priv.responseXML.getElementsByTagName('ServiceException');
                if (exception.length > 0) {
                    alert('This layer cannot be displayed.\n\nServer:\n' + server.url + '\n\nreturns the following error message:\n ' + exception[0].firstChild.data);

                    // remove layer and make checkbox unchecked
                    //    that.removeLayer(layer);
                }
            }

            $('#map_loading').hide();
        });


        this.layer = layer;
        return layer;
    },

    presentFeature: function(feature) {
        var html;
        var server = this.server;
        var WFS = server.WFS || {};

        if (WFS.pesentFeature) {
            html = WFS.pesentFeature(feature,this);
        }
        else {
            html = '<table class="feature_content">';
            html += '<tr><td>' + 'Layer' + '</td><td>' + feature.layer.name + '</td></tr>';

            for (var key in feature.data) {
                if (feature.data[key] === null) {
                    // ignore values which are null
                    continue;
                }
                var value = feature.data[key];
                if (value.indexOf('http') === 0) {
                    value = '<a href="' + value + '" target="_blank">Link</a>';
                }

                html += '<tr><td>' + key + '</td><td>' + value + '</td></tr>';
            }
            html += '</table>';
        }

        return html;
    }

});


WFSLayerInfo = OpenLayers.Class(WFSLayerInfoBase, {
    initialize: function(feature,server,options) {
        WFSLayerInfoBase.prototype.initialize.apply(this, [feature,server]);

        this.dimensions = {
            'elevation': {
                //'range': ['0-10','10-20','20-30','30-50','50-100','100-200'],
                'range': {upper: {min: 0, max: Infinity, default_value: 10}, lower: {min: 0, max: Infinity, default_value: 0}},
                //'range': {min: 0, max: Infinity},
                'units': 'm',
            },
            'time': {
                'range': {
                    /*lower: {min: 0, max: Infinity, default_value: '1998-01-01'},
                    upper: {min: 0, max: Infinity, default_value: '2015-01-31'}*/
                    lower: {min: 0, max: Infinity, default_value: '2000-01-01'},
                    upper: {min: 0, max: Infinity, default_value: '2000-12-31'}
                    },
                'units': 'YYYY-MM-DD',
            }

        };

        this.style.fillColor = 'orange';
        this.style.strokeColor = 'orange';
        this.style.fillOpacity = 0.4;

        this.parameters = ['PHOSZZXX'];
        //this.isTimeSeries = true;
        //this.parameter = 'AMONZZXX';

        // overwrite default options
        if (options) {
            for (var p in options) {
                this[p] = options[p];
            }
        }

        // featureTypeName defaults to name
        if (this.featureTypeName === undefined) {
            this.featureTypeName = this.name;
        }


        // filter out only the paramters in the list this.parameters
        var filters_parameters = [];

        for (var i=0; i < this.parameters.length; i++) {
            filters_parameters.push(new OpenLayers.Filter.Comparison({
                type: OpenLayers.Filter.Comparison.EQUAL_TO,
                property: "p35_id",
                value: this.parameters[i]
            }));
        }

        this.filter = new OpenLayers.Filter.Logical({
            type: OpenLayers.Filter.Logical.OR,
            filters: filters_parameters
        });


        // filter on time series if provided
        if (this.isTimeSeries !== undefined) {
            this.filter = new OpenLayers.Filter.Logical({
                type: OpenLayers.Filter.Logical.AND,
                filters: [
                    this.filter,
                    new OpenLayers.Filter.Comparison({
                        type: OpenLayers.Filter.Comparison.EQUAL_TO,
                        property: "istimeseries",
                        value: this.isTimeSeries
                    })
                ]
            });

        }

        /*
        this.filter = new OpenLayers.Filter.Comparison({
            type: OpenLayers.Filter.Comparison.EQUAL_TO,
            property: "p01_id",
            value: this.parameters[0]
        });
        */

    },

    transformFeature: function(features,options) {
        // do nothing

        if (this.figprops) {
             if (this.initFigProp === undefined) {
                // reset only at the first call

            this.figprops.create_prop_ui(
                [
                    {
                        'label': "Marker color",
                        'default': 'orange',
                        'id': "fillColor",
                        'description': "Select the color of the markers",
                        'type': "string",
                        'opt': [
                            ['blue', 'blue'],
                            ['orange', 'orange'],
                            ['green', 'green']
                        ]
                    },
                    {
                        'label': "Marker size",
                        'default': '6',
                        'id': "pointRadius",
                        'description': "size in pixels of marker",
                        'type': "float",
                    },
                    {
                        'label': "Marker opacity",
                        'default': '0.4',
                        'id': "fillOpacity",
                        'description': "value between 0 (transparent marker) and 1 (opaque marker)",
                        'type': "float",
                    },
                    {
                        'label': "Quality flags",
                        'default': '1, 2, 6',
                        'id': "quality_flags",
                        'description': "separated by commas",
                        'type': "string"
                    }
                ]
            );


                this.figprops.reset();
                this.initFigProp = true;
            }
        }
    },

    // get a layer that can be added by OpenLayer map.addLayer function
    // option.styles
    // option.param

    get_layer: function(options) {
        var that = this, server = this.server;
        var featureNS = 'http://mapserver.gis.umn.edu/mapserver';
        var layer, style, color, v;

        var elevation_interval = options.param.elevation;
        var time_interval = options.param.time;


        var title = this.feature.title;
        var attribution;

        if (server.attribution) {
            if (server.attribution.html) {
                attribution = server.attribution.html;
            }
            else {
                attribution = OceanBrowser.render('<a href="{{href}}" target="_blank" class="OceanBrowserAttribution">{{title}}</a>',server.attribution);
            }
        }


        var parts = this.featureTypeName.split(':');
        var featureNS = parts[0];
        var featureType = parts[1];
        var geometryName = 'geom';
        //geometryName = 'shape';

        //console.log('options get_layer',options);

        // filter on parameter, depth and time
        var filters = [
            this.filter,
            new OpenLayers.Filter.Comparison({
                        type: OpenLayers.Filter.Comparison.BETWEEN,
                        property: "z",
                        lowerBoundary: elevation_interval.lower,
                        upperBoundary: elevation_interval.upper
                    }),
            new OpenLayers.Filter.Comparison({
                        type: OpenLayers.Filter.Comparison.BETWEEN,
                        property: "datetime",
                        lowerBoundary: time_interval.lower,
                        upperBoundary: time_interval.upper
                    }),

            ];

        // filter on quality flags
        if (options.style.quality_flags) {
            var flags = options.style.quality_flags.split(',');
            var filters_flags = [];

            for (var i=0; i < flags.length; i++) {
                filters_flags.push(new OpenLayers.Filter.Comparison({
                        type: OpenLayers.Filter.Comparison.EQUAL_TO,
                        property: "quality",
                        value: flags[i].trim()
                 }));
            }

            filters.push(new OpenLayers.Filter.Logical({
                type: OpenLayers.Filter.Logical.OR,
                filters: filters_flags
            }));
        }


        var filter = new OpenLayers.Filter.Logical({
                type: OpenLayers.Filter.Logical.AND,
                filters: filters
                });

        if (this.server.directConnection) {
            var saveProxy = OpenLayers.ProxyHost;
            OpenLayers.ProxyHost = null;
        }

        layer = new OpenLayers.Layer.Vector(title, {
            strategies: [new OpenLayers.Strategy.BBOX()],
            visibility: true,
            protocol: new OpenLayers.Protocol.WFS({
                version: "1.1.0",
                url:  server.url,
                geometryName: geometryName,
                featureType: featureType,
                //outputFormat: "GML2",
                featureNS: featureNS
            }),
            filter: filter,
            style: this.style,
            attribution: attribution
        });

        // restore proxy settings
        if (this.server.directConnection) {
            OpenLayers.ProxyHost = saveProxy;
        }


        //$('#map_loading').show();
        layer.events.register('beforefeaturesadded', this, function (e) {
            this.transformFeature(e.features,options);

            if (e.features.length === 0) {
                alter('No observations that satisfy the criterias.')
            }
        })


        layer.events.register('loadstart', null, function (e) {
            // show indetermined progressbar
            $('#progressbar').progressbar({value: false});
        });

        layer.events.register('loadend', null, function (e) {
            // hide progressbar if it was setup
            if ($('#progressbar').data('ui-progressbar')) {
                $('#progressbar').progressbar('destroy');
            }

            //$('#map_loading').hide();

            if (e.response && e.response.priv) {
                if (e.response.priv.responseXML) {
                  var exception = e.response.priv.responseXML.getElementsByTagName('ServiceException');
                  if (exception.length > 0) {
                    alert('This layer cannot be displayed.\n\nServer:\n' + server.url + '\n\nreturns the following error message:\n ' + exception[0].firstChild.data);

                    // remove layer and make checkbox unchecked
                    //    that.removeLayer(layer);
                  }
                }
            }


        });


        this.layer = layer;
        return layer;
    }


});

WFSLayerInfoColor = OpenLayers.Class(WFSLayerInfo, {
    initialize: function(feature,server,options) {
        WFSLayerInfo.prototype.initialize.apply(this, [feature,server,options]);

        // with transparent marker we can mix colors
        // and create "black" which is not the colormap
        this.style.fillOpacity = 1;

        this.cmap = OceanBrowser.colormaps['jet'].map(
            function(c) { return 'rgb(' + Math.round(255*c[0]) + ',' + Math.round(255*c[1]) + ',' + Math.round(255*c[2]) + ')'; });

        //this.colorindex = 'colorindex';
        this.colorindex = 'obsdensity';
        this.scalename = '<span title="Number of observations per location">Observation density</span>';

    },

    getLegendGraphic: function(style_param,width,height) {
        var url, that = this, i, elem, axis;
        var orientation = 'horizontal'; // or 'vertical'
        //orientation = 'vertical';
        var container = $('<div/>').addClass('WFSColorBarContainer').addClass(orientation).append(
            elem = $('<div/>').addClass('WFSColorBar').addClass(orientation),
            axis = $('<div/>').addClass('LabelContainer')
        );


        if (orientation === 'horizontal') {
            for (i = 0; i < this.cmap.length; i++) {
                elem.append($('<div/>').css({'background-color': this.cmap[i]}));
            }

            this.axis = new Axis(axis.get(0),"x",
                                 {tickslabelclass: "xtickslabel", ticksclass: "xticks"});
        }
        else {
            for (i = this.cmap.length-1; i >= 0; i--) {
                elem.append($('<div/>').css({'background-color': this.cmap[i]}));
            }

            this.axis = new Axis(axis.get(0),"y",
                                 {tickslabelclass: "ytickslabel", ticksclass: "yticks"});
        }



        /*
          this.cmap.forEach(function(_) {
          elem.append($('<div/>').css({'background-color': _, 'width': '40px', 'height': '2px'}));
          });*/

        // keep reference to legend elem
        this.elem = container.get(0);
        return {elem: container.get(0), style: style_param};
    },


    // include color based on options
    extractValues: function(features) {
        var values = [];

        for (var i=0, len=features.length; i<len; i++) {
            if (this.colorindex === 'colorindex') {
                maxindex = parseFloat(features[i].data.maxindx);
                minindex = parseFloat(features[i].data.minindx);

                // approximate value
                values[i] = (maxindex - minindex) *
                    (features[i].data.colorindex-1)/nval + minindex;
            }
            else {
                values[i] = parseFloat(features[i].data[this.colorindex]);
            }
        }

        return values;
    },

    // include color based on options
    transformFeature: function(features,options) {
        var min, max, indices, that = this;
        // number of entries in global colorbar
        var nval = 3 * 256, values = [];
        var cmap = this.cmap;


        values = this.extractValues(features);

        if (values === undefined) {
            // no colorindex to change color
            return;
        }

        min = options.style.vmin;
        max = options.style.vmax;
        //console.log('options',options);

        values_min = Math.min.apply(null,values);
        values_max = Math.max.apply(null,values);


        if (min === undefined || max === undefined ) {
            /*
            indices = features.map(function(_) { return _.data.colorindex; });
            min = Math.min.apply(null,indices);
            max = Math.max.apply(null,indices);
            */

            min = values_min;
            max = values_max;

            if (this.figprops) {
                if (this.scalename) {
                   $(this.figprops.colorbar_div).after(this.scalename)
                }


                this.figprops.create_prop_ui([
                    {'default': min,
                     'id': 'vmin',
                     'label': "Minimum color-bar range",
                     'type': "float"
                    },
                    {'default': max,
                     'id': "vmax",
                     'label': "Maximum color-bar range",
                     'type': "float"},
                    {'default': '1, 2, 6',
                     'id': "quality_flags",
                         'label': "Quality flags",
                     'description': "separated by commas",
                     'type': "string"},
                ]
                                            );
                this.figprops.reset();
            }
        }
        else {
            // if min/max was not changed by the user then set it to new bounds

            function get(id) {
               return that.figprops.uidata.filter(function(_) { return _.id === id })[0];
            }
            var prefix = this.figprops.namespace + "_style_param_";

            if (get('vmax').default ===  options.style.vmax) {
                get('vmax').default = values_max;
                max = values_max;
                //this.figprops.reset({(prefix + 'vmin'): 1, (prefix + 'quality_flags'): 1});
                var protect = {};
                protect[prefix + 'vmin'] = protect[prefix + 'quality_flags'] = 1;
                this.figprops.reset(protect);
            }

            if (get('vmin').default ===  options.style.vmin) {
                get('vmin').default = values_min;
                min = values_min;
                var protect = {};
                protect[prefix + 'vmax'] = protect[prefix + 'quality_flags'] = 1;
                this.figprops.reset(protect);
            }
        }
        //console.log('min/max',min,max,options);

        this.axis.set_range(min,max);

        /*if (this.elem) {
          $(this.elem).parent().append('min=' + min + '/max=' + max);
          } */

        for (var i=0, len=features.length; i<len; i++) {
            // add color to the feature based on attributes
            style = OpenLayers.Util.extend({}, OpenLayers.Feature.Vector.style['default']);
            //var index = Math.floor(cmap.length * (parseInt(features[i].data.colorindex) - min) / (max - min));
            var index = Math.floor(cmap.length * (values[i] - min) / (max - min));
            //console.log('index',index);
            index = Math.min(index,cmap.length-1);
            index = Math.max(index,0);

            var color = cmap[index];
            style.strokeColor = color;
            style.fillColor = color;
            style.pointRadius = 4;

            //console.log('feature',features[i]);
            features[i].style = style;
        }

    }

});




WFSLayerInfoColorDensity = OpenLayers.Class(WFSLayerInfoColor, {
    // density of observations
    extractValues: function(features) {
        var values = [], len=features.length, p1, p2;

        // unique elements
        // need to optimize
        // http://stackoverflow.com/questions/15052702/count-unique-elements-in-array-without-sorting

        /*
        for (var i=0; i<len; i++) {
            values[i] = 0;
            p1 = features[i].geometry;

            for (var j=0; j<len; j++) {
                p2 = features[j].geometry;
                if (p1.x === p2.x && p1.y === p2.y) {
                    values[i]++;
                }
            }
        }
        */

        var keys = features.map(function(_) { return _.geometry.toString(); });
        var counts = {};
        for (var i = 0; i < keys.length; i++) {
            counts[keys[i]] = 1 + (counts[keys[i]] || 0);
        }
        for (var i=0; i<len; i++) {
            values[i] = counts[keys[i]];
        }

        return values;
    }
});
