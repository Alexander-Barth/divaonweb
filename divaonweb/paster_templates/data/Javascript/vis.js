/******************************************************************************
 *                                                                            *
 *  Copyright (C) 2008-2010 Alexander Barth <barth.alexander@gmail.com>.      *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU Affero General Public License as published  *
 *  by the Free Software Foundation, either version 3 of the License, or      *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                            *
 ******************************************************************************/


// Progress object

function progress_start(message) {
    this.n += 1;

    if (this.n == 1) {
	$("#loading").show();

	var inputs = document.getElementsByTagName("input");

	for (var i=0;i<inputs.length;i++) {
	    if (!inputs[i].disabled) {
		this.disabled_inputs.push(inputs[i]);
		inputs[i].disabled = true;
	    }
	}

    }
}

function progress_finished() {
    this.n -= 1;

    if (this.n === 0) {

	while (this.disabled_inputs.length > 0) {
	    this.disabled_inputs.pop().disabled = false;
	}

	$("#loading").hide();
    }

}


function Progress() {
    this.n = 0;
    this.start = progress_start;
    this.finished = progress_finished;
    this.disabled_inputs = new Array();


    $("#loading").hide();
}


var progress = new Progress();


