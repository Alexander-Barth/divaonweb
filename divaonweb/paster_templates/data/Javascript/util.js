/******************************************************************************
 *                                                                            *
 *  Copyright (C) 2008-2015 Alexander Barth <barth.alexander@gmail.com>.      *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU Affero General Public License as published  *
 *  by the Free Software Foundation, either version 3 of the License, or      *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                            *
 ******************************************************************************/

// TODO:
// rename 'anim' to 'ViewSection'

// disable console login if firebug is not running

    if ( typeof console == "undefined" ) {
        var console = {};
        console.log = function() {};
    }


var Util = {};

// separator in layer name
Util.layersep = '#';
Util.layersep = '*';

Util.ncfile = function(layername) {
    return layername.split(Util.layersep)[0];
}

Util.round = function(x,nd) {
    // keep only nd significant digits of x
    var r = Math.pow(10.0,Math.floor(log10(Math.abs(x))+1-nd) );
    x = Math.round(x/r)*r;
    return x;
};


Util.removeChildNodes = function (elem) {
    while (elem.hasChildNodes()) {
        elem.removeChild(elem.lastChild);
    }
};

Util.DocumentInputs = function() {
    this.disabled_inputs = [];

    this.disable = function() {
        var inputs = document.getElementsByTagName("input");
        var i;

        for (i=0;i<inputs.length;i++) {
            if (!inputs[i].disabled) {
                this.disabled_inputs.push(inputs[i]);
                inputs[i].disabled = true;
            }
        }


        inputs =  document.getElementsByTagName("select");

        for (i=0;i<inputs.length;i++) {
            if (!inputs[i].disabled) {
                this.disabled_inputs.push(inputs[i]);
                inputs[i].disabled = true;
            }
        }

    };

    this.enable = function() {
        while (this.disabled_inputs.length > 0) {
            this.disabled_inputs.pop().disabled = false;
        }
    };
};

Util.query = function(s) {

    this.param = {};

    var vars = s.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        //alert(pair[0]);
        this.param[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
    }

    this.get = function(kw,def) {
        if (kw in this.param) {
            return this.param[kw];
        }
        else {
            return def;
        }
    };

    this.has = function(kw) {
        return (kw in this.param);
    };
};


Util.getVals = function(param_id) {
    var parameters = {}, d, i, elem;

    for (i=0; i<param_id.length; i++) {
        elem = document.getElementById(param_id[i]);

        if (elem.nodeName == "TD") {
            d = elem.firstChild.data;
        }
        else {
            d = elem.value;
        }

        if (d.length !== 0) {
            parameters[param_id[i]] = d;
        }
    }

    return parameters;
}


// return GET parameter string from the hash table param

Util.encodeparam = function(param) {
    var parameters = "";

    for (var name in param) {
        parameters += (parameters.length !==0 ? "&" : "") + name + "=" + encodeURIComponent(param[name]);
    }

    return parameters;
};


Util.append_param = function(url,vars) {

    if (typeof vars == 'object') {
        vars = Util.encodeparam(vars);
    }

    if (url.indexOf('?') == -1) {
        return url + '?' + vars;
    }
    else {
        return url + '&' + vars;
    }

};

Util.ajax = function(url, vars, callbackFunction,options) {
    options = options || {};
    var req_type = options.request || 'GET';


    // Provide the XMLHttpRequest class for IE 5.x-6.x:
    if( typeof XMLHttpRequest == "undefined" ) {
        XMLHttpRequest = function() {
            try {
                return new ActiveXObject("Msxml2.XMLHTTP.6.0");
            }
            catch(e) {
                try {
                    return new ActiveXObject("Msxml2.XMLHTTP.3.0");
                }
                catch(e) {
                    try {
                        return new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    catch(e) {
                        alert( "This browser does not support XMLHttpRequest." );
                    }
                }
            }
            return 0;
        };
    }

    var request =  new XMLHttpRequest();


    if (req_type == 'POST') {
        request.open("POST", url, true);
    }
    else {
        url = Util.append_param(url,vars);
        request.open("GET", url, true);
    }

    request.onreadystatechange = function() {
        var done = 4, ok = 200, cType;

        if (request.readyState == done && request.status == ok) {
            cType = this.getResponseHeader("Content-Type");

            if (cType.substring(0,9) === 'text/html' || cType.substring(0,10) === 'text/plain') {
                callbackFunction(request.responseText);
            }
            else {
                callbackFunction(request.responseXML);
            }
        }
    };

    if (req_type == 'POST') {
        request.setRequestHeader("Content-Type",
                                 "application/x-www-form-urlencoded");

        if (typeof vars == 'object') {
            vars = Util.encodeparam(vars);
        }

        request.send(vars);
    }
    else {
        request.send(null);
    }
};

Util.proxy_ajax = function(url, params, obj, callback_method, options) {

    var fun = function (xmldoc) {
        //console.log('obj ',obj);
        callback_method.call(obj,xmldoc);
    };

    if (options.proxy) {
        params._proxy_url=url;
        Util.ajax(options.proxy, params, fun, options);
    }
    else {
        Util.ajax(url, params, fun, options);
    }

};


Util.loadXMLDoc = function(dname) {
    var xhttp;

    if (window.XMLHttpRequest)  {
        xhttp=new XMLHttpRequest();
    }
    else {
        xhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhttp.open("GET",dname,false);
    xhttp.send("");
    return xhttp.responseXML;
};


Util.XSLTransform = function(xml,xsl,node) {
    var resultDocument, ex, xsltProcessor;

    // code for IE
    if (window.ActiveXObject)  {
        ex=xml.transformNode(xsl);
        node.innerHTML=ex;
    }
    // code for Mozilla, Firefox, Opera, etc.
    else if (document.implementation && document.implementation.createDocument) {
        xsltProcessor=new XSLTProcessor();
        xsltProcessor.importStylesheet(xsl);
        resultDocument = xsltProcessor.transformToFragment(xml,document);
        node.appendChild(resultDocument);
    }
};


// get elements by tags. Looks also in parent node

Util.get_tags = function(elem,name,list) {
    // if list argument is not given start with empty list
    list = list || [];

    var item = elem.firstChild;

    while (item !== null) {
        if (item.tagName == name) {
            list.push(item);
        }

        item = item.nextSibling;
    }

    // look in parent node
    if (elem.parentNode !== null) {
        list = Util.get_tags(elem.parentNode,name,list);
    }

    return list;
};


// reverse a list

Util.reverse = function(l) {
    var r = [];

    for (var i=0;i<l.length;i++) {
        r[l.length-i-1] = l[i];
    }

    return r;
};

// intersect of two lists

Util.intersect = function(l1,l2) {
    var r = [];

    for (var i=0;i<l1.length;i++) {
        for (var j=0;j<l2.length;j++) {
            if (l1[i] === l2[j]) {
                r[r.length] = l1[i];
            }
        }
    }

    return r;
};


Util.mjd = function(str) {
    var t;
    // split date and time
    var parts = str.split('T');

    var r = parts[0].split('-');
    var year  = parseFloat(r[0]);
    var month = 1;
    var day = 1;

    if (r.length > 1) {
        month = parseFloat(r[1]);
        if (r.length > 2) {
          day  = parseFloat(r[2]);
        }
    }

    if (parts.length == 1) {
        t = Date.UTC(year,month-1,day,0,0,0);
    }
    else {
        // remove trailling Z if present
        var time = parts[1].split('Z')[0];
        r = time.split(':');
        var hour  = parseFloat(r[0]);
        var minute = parseFloat(r[1]);
        var sec   = parseFloat(r[2]);
        t = Date.UTC(year,month-1,day,hour,minute,sec);
    }

    var t0 = Date.UTC(1858,11-1,17,0,0,0);
    var mjd = (t - t0)/(24*60*60*1000);
    return mjd;
};


Util.gregd = function(t,format) {
    format = (format === undefined ? 'YYYY-MM-DD' : format);

    var t0 = Date.UTC(1858,11-1,17,0,0,0);
    var d1 = new Date();
    d1.setTime(24*60*60*1000 * t + t0 );

    return Util.ISODateString(d1,format);
/*
    var str =
        (d1.getFullYear()).toString() + "-" +
        (d1.getMonth()+101).toString().substr(1,2) + "-" +
        (d1.getDate()+100).toString().substr(1,2);

    return str;*/

};


// format is one of
// YYYY-MM-DD
// YYYY-MM-DDThh:mm:ssZ
// YYYY-MM-DDThh:mm:ss.sssZ

Util.ISODateString = function(d,format){
    function pad(n) { return n<10 ? '0'+n : n }

    if (format === 'YYYY-MM-DD') {
        return d.getUTCFullYear()+'-'
            + pad(d.getUTCMonth()+1)+'-'
            + pad(d.getUTCDate());
    }
    else if (format === 'YYYY-MM-DDThh:mm:ss.sssZ') {
        return d.toISOString();
    }
    else {
        return d.getUTCFullYear()+'-'
            + pad(d.getUTCMonth()+1)+'-'
            + pad(d.getUTCDate())+'T'
            + pad(d.getUTCHours())+':'
            + pad(d.getUTCMinutes())+':'
            + pad(d.getUTCSeconds())+'Z';
    }

};


Util.encodesec = function(slon,slat) {
    var section = "";
    for (var i=0; i < slon.length; i++) {
        section += slon[i] + "," + slat[i] + "|";
    }

    section = section.substring(0,section.length-1);
    return section;
};

Util.deencodesec = function(url_sec) {
    var coord = url_sec.split('%7C'), i;

    var slon = [];
    var slat = [];

    for (i = 0; i < coord.length; i++) {
        var v = coord[i].split('%2C');
        slon.push(parseFloat(v[0]));
        slat.push(parseFloat(v[1]));
    }
    return {lon: slon, lat: slat};
};


// escape regular expression
Util.escapeRegExp = function(str) {
    return str.replace(/[-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
};


Util.unique = function(a) {
    return a.reduce(function(p, c) {
        if (p.indexOf(c) < 0) p.push(c);
        return p;
    }, []);
};

Util.figureTitle = function(layer,params) {
    var title = [];

    if (layer.abstract_info) {
        if (layer.abstract_info.indexOf('Units') !== -1 || layer.abstract_info.indexOf('units') !== -1) {
            // temporary disable
            //title.push(layer.abstract_info);
        }
    }

    if (params.elevation !== undefined) {
        title.push('depth: ' + params.elevation + ' ' + layer.get_dimension('elevation').units);
    }

    // include time unless it is a video
    if (params.format.slice(0,5) !== 'video') {
      if (params.time !== undefined) {
        title.push('time: ' + params.time + ' ' + layer.get_dimension('time').units);
      }
    }

    // new line after the parameter name
    return layer.title + '\n' + title.join(', ');
};

// anim class

// derived class must provide
// get_wms_layer()
// analysis_wms

function anim(mapid,colorbar_container,anim_button,select_time) {
    this.mapid = mapid;
    this.colorbar_container = colorbar_container;
    this.animation_running = false;
    this.animation_delay = 2000;
    //this.animation_smooth_transition = true;
    this.animation_smooth_transition = false;
    //alert('in anim' + p);

    this.anim_button = anim_button;
    this.select_time = select_time;
    this.analysis_wms_new = {};
    this._loadend = {};

    // hash table
    // each element is one layer
    this.analysis_wms = {};
    this.figprops = {};
    this.Layers = {};
    this.fields = {};
    this.ncolorbar = 0;
    this.layer_num = 0;
    this.popup = null;
    this.featureInfo = {'popup': null,
                        'latlon': null,
                        'content': []};

    this.downloadFormats = ['image/png', 'application/pdf', 'image/eps', 'image/svg+xml',
                            'video/webm','video/mp4'];

}

// class methods

anim.prototype.addLayer = function(Layer) {
    var id = Layer.id;
    this.Layers[id] = Layer;
    this.fields[id] = {};
    var obj = this;

    // default: no download button
    var download_callback = null;
    var metadata_url = Layer.metadata_url;

    if (Layer.WMS_extensions == 'OceanBrowser') {
        this.fields[id].downloadDialog = new OceanBrowser.UI.LayerDownload('download_' + id,Layer,
                                                                           function() {
                                                                               return obj.getCurrentExtent();
                                                                           },
                                                                           this.downloadFormats);

        this.fields[id].downloadDialog.events.register('download', null, function(param) {
            //console.log(param);
            obj.download(id,param);
        });

        download_callback = function() {
            obj.fields[id].downloadDialog.open();
        };
    }
    else if (Layer.WMS_extensions == 'box-select') {
        download_callback = function() {
            // redefine call-back later
        };
    }
    else if (Layer.data_url) {
        console.log("Layer.data_url", Layer.data_url);
        this.fields[id].downloadDialog = new OceanBrowser.UI.LayerSimpleDownload('download_' + id,Layer);
        this.fields[id].downloadDialog.events.register('download', null, function(param) {
            //console.log(param);
            obj.download(id,param);
        });

        download_callback = function() {
            obj.fields[id].downloadDialog.open();
        };
    }

    if (Layer.WMS_extensions == 'box-select-wfs') {
        metadata_url = 'dummy';
    }

    var remove_callback = function() {
        // will trigger remove_layer(Layer);
        //this.Layer.cap.deselectLayer(this.Layer);
        remove_layer(this.Layer);
    };

    // style of layer
    // add only style button if any styles are defined or if it is a WFS layer
    var s = Layer.get_styles();
    var add_style_button = s.length > 0 || Layer instanceof WFSLayerInfoBase;


    this.figprops[id] = new FigureProperties(
        this.mapid,this.mapid + this.layer_num,
        Layer,
        //function() { obj.update(); },
        function() { obj.update_layer(id); },
        {
            colorbarclass: 'colorbar2',
            colorbar_container: this.colorbar_container,
            title: Layer.title,
            abstract_info: Layer.abstract_info,
            metadata_url: metadata_url,
            add_style_button: add_style_button,
            download_callback: download_callback,
            colorbar_height: 50,
            colorbar_width: 150,
            //getCurrentExtent: function() { return obj.getCurrentExtent(); },
            remove_callback: remove_callback,
            update_after_dim_change_callback: function() {
                return obj.update_all();
            }
        });

    if (Layer.WMS_extensions !== 'OceanBrowser' && !(Layer instanceof WFSLayerInfoBase)) {
        this.figprops[id].set_styles(s);
    }

    this.ncolorbar++;
    this.layer_num++;
};

anim.prototype.removeLayer = function(Layer) {
    var id = Layer.id;

    if (this.figprops[id]) {
       this.figprops[id].remove();
       delete this.figprops[id];
       delete this.Layers[id];

       this.ncolorbar--;
    }
};


anim.prototype.get_bounds = function() {
    var bounds = null;

    for (var id in this.Layers) {
        if (!bounds) {
            bounds = this.Layers[id].get_bounds();
        }
        else {
            bounds.extend(this.Layers[id].get_bounds());
        }
    }

    return bounds;
};

anim.prototype.getCurrentExtent = function() {
    return this.map.getExtent();
};


anim.prototype.layer_info_width = function() {
    var w = 0;
    var i = 0;

    for (var id in this.Layers) {
        w += $(this.figprops[id].layer_info_div).outerWidth(true);
        i += 1;

        // put only two-colorbars side-by-side
        if (i == 2) {
            break;
        }
    }

    //return w;

    // width of scroll-bar
    w += 20;

    //w = Math.min(w,370);

    $(this.colorbar_container).width(w);

    return w;
};

anim.prototype.request_feature_info = function (ev) {
    var that = this;

    //console.log('event',ev,'that',that);

    for (var id in this.analysis_wms) {
        // limit this only for WMS layers

        if (this.Layers[id].wms_url) {
            //console.log('layer info',id);
            //this.figprops[id].set_info('Loading...');
            // formats that we understand
            //var preferred_info_formats = ['application/vnd.ogc.gml','text/html','text/xml'];
            var preferred_info_formats = ['application/vnd.ogc.gml','text/xml','text/html'];
            var info_format = this.Layers[id].infoFormat(preferred_info_formats);

            var url = this.analysis_wms[id].getFullRequestString({
                REQUEST: "GetFeatureInfo",
                EXCEPTIONS: "application/vnd.ogc.se_xml",
                BBOX: this.analysis_wms[id].map.getExtent().toBBOX(),
                X: Math.round(ev.xy.x),
                Y: Math.round(ev.xy.y),
                INFO_FORMAT: info_format,
                QUERY_layers: this.analysis_wms[id].params.LAYERS,
                WIDTH: this.analysis_wms[id].map.size.w,
                HEIGHT: this.analysis_wms[id].map.size.h});

            // closure on id
            var callback = function(id,map,latlon,info_format) {
                return function(resp) {
                    that.show_feature_info(resp,id,latlon,info_format);
                };
            }(id,this.map,this.map.getLonLatFromPixel(ev.xy),info_format);

            //Util.proxy_ajax(url,{},null,callback,{proxy: this.Layers[id].use_proxy});

            // absolute URL
            if (!url.startsWith("http")) {
                url = new URL(url,window.location.href).href
                console.log("url",url)
            }

            Util.ajax(OpenLayers.ProxyHost.replace("url=",""),{url: url},callback);
        }
    }
};

anim.prototype.show_feature_info = function (html,id,latlon) {
    var i, all='';

    if (this.featureInfo.popup) {
        // remove previous popup
        this.map.removePopup(this.featureInfo.popup);
    }

    if (!latlon.equals(this.featureInfo.latlon)) {
        // new position -> empty all previous content
        this.featureInfo.content = [];
        this.featureInfo.latlon = latlon;
    }

    this.featureInfo.content.push({'title': this.Layers[id].title,
                                   'html': html});

    all  = '<div class="OceanBrowserFeatureInfo">';

    // assemble content for popup
    for (i in this.featureInfo.content) {
        all += '<h4>' + this.featureInfo.content[i].title + '</h4>';
        all += this.featureInfo.content[i].html;
    }

    all  += '</div>';

    this.featureInfo.popup = new OpenLayers.Popup.FramedCloud(
        //new OpenLayers.Popup.AnchoredBubble(
        "FeatureInfo",
        latlon,
        new OpenLayers.Size(350,200),
        //new OpenLayers.Size(920,550),
        all,
        null,
        true);
    // test
    //this.featureInfo.popup.autoSize = false;

    this.map.addPopup(this.featureInfo.popup);
};

// AnimLayer class

// derived class must provide
// get_wms_layer()
// analysis_wms

function AnimLayer(anim_button,select_time,viewer,id) {
    this.viewer = viewer;
    this.id = id;

    this.animation_running = false;
    this.animation_delay = 2000;
    //this.animation_smooth_transition = true;
    this.animation_smooth_transition = false;
    //alert('in anim' + p);

    this.anim_button = anim_button;
    this.select_time = select_time;
    this.analysis_wms_new = null;


    var that = this;
    this.anim_button.onclick = function() { that.animate(); };
}


AnimLayer.prototype.animate_transition = function() {
    var opacity = this.analysis_wms_new.opacity;

    var opacity_inc = 0.1;

    if (this.nextframe === 0) {
        // hurry up!
        opacity = 1;
    }
    else {
        // ok, we have time to make a nice transition
        opacity += opacity_inc;
    }

    this.analysis_wms_new.setOpacity(opacity);

    var obj = this;
    //console.log("op " + opacity);


    if (opacity > 1-opacity_inc/2) {
        // remove previous layer
        // calling destroy instead of removeLayer helps
        // to avoid memory leaks

        var zindex = this.viewer.analysis_wms[this.id].getZIndex();
        this.viewer.analysis_wms[this.id].destroy();


        // switch WMS layer
        this.viewer.analysis_wms[this.id] = this.analysis_wms_new;
        this.viewer.figprops[this.id].layerWMS = this.analysis_wms_new;
        this.viewer.analysis_wms[this.id].setZIndex(zindex);

        this.animate_next();
    }
    else {
        var timeout = setTimeout(function () { obj.animate_transition(); },50);
    }
};


AnimLayer.prototype.animate_loadend = function() {
    // remove event handler
    this.analysis_wms_new.events.unregister("loadend",this,this.animate_loadend);

    //console.log("load end " + this.id);

    if (this.animation_running) {

        /*

          var num = 0;
          for (var i=0; i < this.map.layers.length; i++) {
          num += this.map.layers[i].numLoadingTiles;
          }

          // do nothing if all maps are not ready
          if (num > 0) {
          return;
          }

          console.log("load end all loaded " + this.id);
        */

        // increment time index
        this.select_time.selectedIndex = (this.select_time.selectedIndex+1)  % this.select_time.options.length;

        // determine time needed to load this frame
        var d = new Date();
        // loadtime time to load frame in ms
        var loadtime = d.getTime() - this.animation_time;
        // time we have before we should show next frame
        this.nextframe = Math.max(this.animation_delay-loadtime,0);


        if (this.animation_smooth_transition) {
            this.animate_transition();
        }
        else {

            // make visible
            this.analysis_wms_new.setOpacity(1);
            // remove previous layer
            // calling destroy instead of removeLayer helps
            // to avoid memory leaks
            //this.map.removeLayer(this.analysis_wms);
            //console.log("remove layer " + this.id);

            this.viewer.analysis_wms[this.id].destroy();

            // switch WMS layer
            this.viewer.analysis_wms[this.id] = this.analysis_wms_new;
            this.viewer.figprops[this.id].layerWMS = this.analysis_wms_new;

            //console.log("Load time " + loadtime + "; next frame at: " + this.nextframe);

            // schedule next frame refresh
            var obj = this;
            var timeout = setTimeout(function () { obj.animate_next(); },this.nextframe);
            //timeout = setTimeout(function () { obj.animate_prepare_next2(); },this.nextframe);
        }
    }
    else {
        // clean-up
        this.viewer.map.removeLayer(this.analysis_wms_new);
    }
};

AnimLayer.prototype.animate_next = function() {

    if (this.animation_running) {
        var d = new Date();
        this.animation_time = d.getTime();

        var time_index = (this.select_time.selectedIndex+1)  % this.select_time.options.length ;
        var time = this.select_time.options[time_index].value;
        var obj = this;

        var zindex = this.viewer.analysis_wms[this.id].getZIndex();

        this.analysis_wms_new = this.viewer.get_wms_layer(this.id,{time: time});
        this.analysis_wms_new.setZIndex(zindex);

        this.analysis_wms_new.events.register("loadend",this,this.animate_loadend);

        // make invisible but will still load when added to map
        this.analysis_wms_new.setOpacity(0);
        //this.analysis_wms_new.setOpacity(1);
        this.viewer.map.addLayer(this.analysis_wms_new);


        this.analysis_wms_new.setZIndex(zindex);



        // schedule next event
        //var obj = this;
        //timeout = setTimeout(function () { obj.animate_next() },this.animation_delay);

    }
};



AnimLayer.prototype.animate = function() {
    //alert('anim');

    this.animation_running = !this.animation_running;

    if (this.animation_running) {
        this.anim_button.value = "Stop";
        this.animate_next();
    }
    else {
        this.anim_button.value = "Animate";
    }


};


// progress bar for OpenLayers

function MapProgressbar(id,map) {
    this.id = id;
    this.elem = $('#' + id);
    this.tiles_to_load = 0;
    this.map = map;
    this.tasknames = [];
    this.visible = false;

    map.events.register("addlayer",this,this.addlayer);
    map.events.register("removelayer",this,this.removelayer);
  /*  map.events.register("preaddlayer",this,function(event) {
        console.log('preaddlayer',event);
        if (event.layer.protocol instanceof OpenLayers.Protocol.WFS.v1) {
            // show indetermined progressbar
            $('#progressbar').progressbar({value: false});
        }
    });*/
}

MapProgressbar.prototype.start = function(taskname) {
    //console.log('start task ',taskname);
    if (this.tasknames.length === 0 && ~this.visible) {
        this.elem.progressbar({"value": 0});
        this.visible = true;
    }
    this.tasknames.push(taskname);
};

MapProgressbar.prototype.end = function(taskname) {
    //console.log('end task ',taskname);
    this.tasknames = this.tasknames.filter(function(_) { return _ !== taskname; });

    if (this.tasknames.length === 0) {
        this.elem.progressbar('destroy');
        this.visible = false;
    }
};

// register event handler
MapProgressbar.prototype.addlayer = function(event) {
    if (event.layer.protocol instanceof OpenLayers.Protocol.WFS.v1) {
            // show indetermined progressbar
         //$('#progressbar').progressbar('destroy');
    }
    else {
        event.layer.events.register("tileloaded",this,this.update);
        event.layer.events.register("loadstart",this,function(event) {
            var layer = event.layer;
            if (!layer) {
                layer = event.object;
            }
            this.start('getmap-' + layer.url + '-' + layer.name);
            this.update();
        });
        event.layer.events.register("loadend",this,function(event) {
            var layer = event.layer;
            if (!layer) {
                layer = event.object;
            }
            this.end('getmap-' + layer.url + '-' + layer.name);
        });
    }
};

// unregister event handler
MapProgressbar.prototype.removelayer = function(event) {
    event.layer.events.unregister("tileloaded",this,this.update);
};

// count number to tiles yet to be loaded and set progressbar accordingly
MapProgressbar.prototype.update = function(event) {
    var num = 0;
    var tmp;
    for (var i=0; i < this.map.layers.length; i++) {
        tmp = this.map.layers[i].numLoadingTiles;
        if (tmp !== undefined) {
            num += tmp;
        }
    }
    //console.log('tiles_to_load ',num,this.tiles_to_load);

    if (num > this.tiles_to_load) {
        this.tiles_to_load = num;

        // initialize progressbar
        $("#" + this.id).progressbar({ value: 0 });
        this.visible = true;

    }
    else  {
        $("#" + this.id).progressbar({value: 100*(1-num/this.tiles_to_load) });
        /*
        if (num === 0) {
            // finish
            this.tiles_to_load = 0;
            $("#" + this.id).progressbar('destroy');
            this.visible = false;
        }*/

    }

};



// options used:
// add_style_button
// colorbar_container
// Layer

function FigureProperties(mapid,namespace,Layer,update,options) {
    this.namespace = namespace;
    this.mapid = mapid;
    this.info = null;

    // options might be overwritten by options
    this.colorbarclass = "colorbar";
    this.add_style_button = false;
    this.colorbar_container = document.getElementById(mapid);
    this.title = null;
    this.units = null;
    this.abstract_info = null;
    this.layer_info_class = 'layer_info';
    this.dimensions = {};
    this.download_callback = null;
    this.remove_callback = null;
    this.update_after_dim_change_callback = null;
    this.colorbar_height = 150;
    this.colorbar_width = 80;
    this.Layer = Layer;
    this.update = update;

    // include this figure properties in Layer so that the layer can set the min/max value for
    // WFS layers
    this.Layer.figprops = this;
    var that = this;

    // overwrite default options
    if (options) {
        for (var p in options) {
            this[p] = options[p];
        }
    }

    var obj = this;

    var ext = 'OceanBrowser';
    if (this.Layer) {
        ext = this.Layer.WMS_extensions;
    }

    var oLabel;

    var dom = OceanBrowser.UI.makedom;

    this.layer_info_div = dom("div",{"class": this.layer_info_class})

    if (this.Layer.title) {
        this.layer_info_div.appendChild(dom('h3',{
            title: this.Layer.FullTitle(' ▸ ')
        },[this.title]));
    }

    // add colorbar
    this.colorbar_div = dom('div',{
        'id': namespace + "_colorbar",
        'class': this.colorbarclass});
    this.layer_info_div.appendChild(this.colorbar_div);


    // get units if present in abstract_info
    if (!this.units) {
	if (Layer.abstract_info) {
            abstract_lines = Layer.abstract_info.split('\n');
            for (var i=0; i < abstract_lines.length; i++) {
		if (abstract_lines[i].indexOf("Units: ") == 0) {
                    this.units = abstract_lines[i].split("Units: ")[1]
		}
            }
	}
    }
    // display units
    if (this.units) {
        var units_div = document.createElement("div");
        units_div.style.clear = "both";
        units_div.className = "units";
        units_div.appendChild(document.createTextNode("Units: " + this.units));
        this.layer_info_div.appendChild(units_div);
    }


    this.input_field_div = dom("div",{"class": "clearfix dimensions"});
    this.layer_info_div.appendChild(this.input_field_div);

    this.buttons_div = dom("div",{"class": "buttons"});
    this.layer_info_div.appendChild(this.buttons_div);

    if (this.add_style_button) {
        var button_open = dom('input',{
            'type': 'button',
            'class': "settings",
            'title': "settings",
            'id': namespace + "config_button"});

        this.buttons_div.appendChild(button_open);

        button_open.onclick = function () {
            obj.open();
        };
    }

    // info/metadata button
    var cap = this.Layer.cap || {};
    if (cap.service === 'WMS' || this.metadata_url) {
        this.InfoDialog = new OceanBrowser.UI.InfoLayer(this.namespace,this.Layer);
        var button_metadata = dom("input", {
            'type': 'button',
            'class': "information",
            'title': "Information",
            'id': namespace + "config_info"});

        // metadata_url might change
        button_metadata.onclick = function () {
            if (obj.metadata_url) {
                window.open(obj.metadata_url,'Metadata');
            }
            else {
                obj.InfoDialog.open();

            }
        };

        this.buttons_div.appendChild(button_metadata);
        this.button_metadata = button_metadata;
    }


    // download button
    if (this.download_callback) {
        var button_download = dom("input", {
            'type': 'button',
            'class': "download",
            'title': "Download",
            'id':  namespace + "config_download"});
        // this.download_callback might change
        button_download.onclick = function() { obj.download_callback(); };
        this.buttons_div.appendChild(button_download);
        this.button_download = button_download;
    }


    if (ext == 'box-select' || ext == 'box-select-wfs' || cap.selectBox) {
        var br = dom("br",{"clear": 'both'});
        this.layer_info_div.appendChild(br);
        this.box_select = document.createElement("input");
        this.box_select.id = namespace + "_box_select";
        this.box_select.type = "checkbox";
        // IE requires to set first type then to add into DOM
        // https://connect.microsoft.com/IE/feedback/details/332453/cannot-change-an-inputs-type-after-youve-placed-it-in-the-page
        this.layer_info_div.appendChild(this.box_select);

        oLabel=this.layer_info_div.appendChild(document.createElement("label"));
        oLabel.htmlFor= namespace + "_box_select";
        oLabel.appendChild (document.createTextNode("select domain"));
    }

    // add disclaimer if present
    if (cap.disclaimer) {
        var disclaimer = document.createElement("div");
        disclaimer.style.clear = "both";
        disclaimer.className = "disclaimer";
        disclaimer.appendChild(document.createTextNode(cap.disclaimer));
        this.layer_info_div.appendChild(disclaimer);
    }

    if (this.colorbar_container) {
       // install colorbars
       this.colorbar_container.appendChild(this.layer_info_div);
    }

    // Properties Dialog

    this.dialog_element = document.createElement("div");
    this.dialog_element.id = namespace + "config_dialog";
    this.dialog_element.title="Configuration";
    var oForm = this.dialog_element.appendChild(document.createElement("form"));
    oForm.className = "FigurePropertiesForm"
    this.oForm = oForm;

    this.oDiv = this.oForm.appendChild(document.createElement("div"));

    this.button_list = dom('div',{'class': 'button_list'});
    this.oForm.appendChild(this.button_list);

    var button_reset = document.createElement("input");
    button_reset.type="button";
    button_reset.value="Reset to default";
    button_reset.id = namespace + "config_button_reset";

    button_reset.onclick = function () {
        obj.reset();
    };

    // input element cannot be changed in IE after added to the DOM
    var button_close = document.createElement("input");
    button_close.type = "submit";
    button_close.value = "Done";
    button_close.id = namespace + "config_button_close";

    this.button_list.appendChild(button_close);
    this.button_list.appendChild(button_reset);


    // add to DOM
    document.body.appendChild(this.dialog_element);

    // create dialog
    $(this.dialog_element).dialog({ autoOpen: false, width: 400, height: 350, zIndex: 3999 });



    // more visible remove button
    if (this.remove_callback) {
        //this.layer_info_div.appendChild(document.createElement("br"));
        this.button_remove = dom("input",{
            "type": "button",
            "value": "Remove",
            "class": "remove",
            "title": "remove"});
        this.buttons_div.appendChild(this.button_remove);
        // this.remove_callback might change
        this.button_remove.onclick = function() { obj.remove_callback(); };
    }

    if (this.update_after_dim_change_callback) {
        // update button
        this.button_update = dom("input",{
            "type": "button",
            "value": "Update",
            "class": "update",
            "title": "update"});
        this.buttons_div.appendChild(this.button_update);
        this.button_update.onclick = function() { obj.update_after_dim_change_callback(); };
    }

    // events

    this.colorbar_div.onclick = function () {
        obj.open();
    };

    this.oForm.onsubmit = function (ev) {
        ev.preventDefault();
        $(obj.dialog_element).dialog('close');

        if (obj.update) {
            obj.update();
        }

    };

}

FigureProperties.prototype.open = function() {
    $(this.dialog_element).dialog('open');
};

FigureProperties.prototype.set_styles = function(styles) {
    // style
    this.styles = styles;

    //var oP = this.dialog_element.appendChild(document.createElement("p"));
    var oDiv = this.oDiv;
    var oLabel, oSelect, oOption;

    // plotting method

    oDiv.appendChild(document.createElement("br"));
    oLabel=oDiv.appendChild(document.createElement("label"));
    oLabel.htmlFor = this.namespace + "select_style";

    oLabel.appendChild (document.createTextNode("Style: "));
    oSelect=oDiv.appendChild(document.createElement("select"));
    oSelect.id = this.namespace + "select_style";

    oOption=oSelect.appendChild(document.createElement("option"));
    oDiv.appendChild(document.createElement("br"));


    var sel_style = oSelect;

    while (sel_style.hasChildNodes()) {
        sel_style.removeChild(sel_style.lastChild);
    }

    for (var j=0; j < styles.length; j++) {
        var option = document.createElement('option');

        option.value = styles[j].name;
        option.appendChild(document.createTextNode(styles[j].title));

        sel_style.appendChild(option);
    }

};


FigureProperties.prototype.parseXMLstat = function(uidata) {

    if (uidata.documentElement) {
        uidata = $(uidata).find('param').map(function(_) {
            var options = $(this).find('option').map(function(_) {
                return [[$(this).attr('value'), $(this).text()]];
            });

            if (!options) {
                options = [];
            }

            return {'type': $(this).attr('type'),
                    'id': $(this).attr('id'),
                    'default': $(this).attr('default'),
                    'label': $(this).attr('label'),
                    'description': $(this).attr('description'),
                    'opt': options
                   };
        });

    }
    return uidata;

};

FigureProperties.prototype.create_prop_ui = function(uidata) {

    if (this.uidata) {
        // ignore multiple call to this function
        return;
    }

    this.uidata = this.parseXMLstat(uidata);

    this.style_param_id = [];
    this.style_param_id_modif = {};
    this.style_params = [];

    var oLabel, elem;
    var oOption;
    var obj = this;

    var oDiv = this.oDiv;
    //this.dialog_element.appendChild(oDiv);

    for (var i=0; i<this.uidata.length; i++) {
        var type = this.uidata[i]['type'];
        var id = this.uidata[i]['id'];
        var def = this.uidata[i]['default'];
        var label = this.uidata[i]['label'];
        var description = this.uidata[i]['description'];
        var opt = this.uidata[i]["opt"];

        var html_id = this.namespace + "_style_param_" + id;
        this.style_param_id.push(id);

        oLabel = document.createElement("label");
        oLabel.htmlFor = html_id;
        oLabel.appendChild(document.createTextNode(label +": "));

        if (description) {
            oLabel.title = description;
        }

        oDiv.appendChild(oLabel);

        if (opt && opt.length > 0) {
            // user can select among different options
            elem = oDiv.appendChild(document.createElement("select"));

            for (var j = 0; j < opt.length; j++) {
                oOption = elem.appendChild(document.createElement("option"));
                oOption.value = opt[j][0];

                if (type == 'imageselector') {
                    oOption.setAttribute('rel','<img src="' + opt[j][1] + '" />');
                }
                else {
                    oOption.appendChild(document.createTextNode(opt[j][1]));
                }

                // should be in reset
                //oOption.selected = def == oOption.value;
                //oOption.selected = (def == oOption.value ? 'selected' : 'unselected');
                if (def == oOption.value) {
                    oOption.selected = true;
                }

            }


            if (type == 'imageselector') {
                // id must be defined before jListbox is initiated
                elem.id = html_id;
                $(elem).jListbox();
            }

        }
        else {
            if (type == 'boolean') {
                elem = document.createElement("input");
                elem.type="checkbox";
            }

            else if (type == 'float' || type == 'string' || type == 'int') {
                elem = document.createElement("input");
                elem.type='text';
                elem.size='7';
            }
        }


        elem.id = html_id;
        elem.name = html_id;
        oDiv.appendChild(elem);

        $(elem).change(function() {
            obj.style_param_id_modif[this.id] = true;
            this.style.background = '#eef';                                                                                                             });

        oDiv.appendChild(document.createElement("br"));
    }

};


// reset the values in the style form except the protected onces
// typically modified by the user

FigureProperties.prototype.reset = function(protect) {

    protect = protect || {};
    var elem;

    for (var i=0; i<this.uidata.length; i++) {
        var type = this.uidata[i]['type'];
        var id = this.uidata[i]['id'];
        var def = this.uidata[i]['default'];
        var opt = this.uidata[i]["opt"];

        var html_id = this.namespace + "_style_param_" + id;

        elem = document.getElementById(html_id);

        // reset only not projected fields
        if (! (html_id in protect )) {
            elem.style.background = 'white';

            if (opt && opt.length > 0) {
                // user can select among different options


                // does not work with jListbox

                if (type != 'imageselector') {
                    for (var j = 0; j < opt.length; j++) {
                        opt[j].selected = def == opt[j][0];
                    }
                }
            }
            else {
                if (elem.type == "checkbox") {
                    elem.checked = def == 'True';
                }
                else if (elem.type == "text") {
                    elem.value = String(def);
                }
            }

        }

    }
};

FigureProperties.prototype.set_stat = function(uidata) {
    if (!this.uidata) {
        this.create_prop_ui(uidata);
    }

    //this.uidata = uidata;
    this.uidata = this.parseXMLstat(uidata);

    this.reset(this.style_param_id_modif);

    // check if uidata is an XML document
    if (uidata.documentElement) {
      var warning = uidata.getElementsByTagName("warning");
      if (warning.length !== 0) {
          alert("Warning: " + warning[0].firstChild.nodeValue);
      }
    }

};


FigureProperties.prototype.get_style = function() {
    var ext = 'OceanBrowser';
    var service = 'WMS';
    // encode style
    var s = "";
    var url = null;
    var style_param = {};

    if (this.Layer) {
        if (this.Layer.cap) {
            ext = this.Layer.cap.WMS_extensions;
            service = this.Layer.cap.service;
        }
    }

    if (ext === 'OceanBrowser' || service === 'WFS') {
        style_param = this.get_style_param();
    }
    else {
        var elem = document.getElementById(this.namespace + "select_style");
        if (elem) {
            style_param.name = elem.value;
        }

        if (ext === 'ncWMS') {
            style_param = jQuery.extend(style_param, this.get_style_param());
        }
    }


    var out = this.Layer.getLegendGraphic(style_param,
                                          this.colorbar_width,
                                          this.colorbar_height);
    s = out.style;

    if (out.url) {
        url = out.url;

        // once image is loaded set size of div
        var obj = this;
        var img = new Image();
        img.onload = function() {
            $(obj.colorbar_div).width(img.width);
            $(obj.colorbar_div).height(img.height);
        };
        img.src = url;

        var colorbar_style = "url('" + url + "')";
        this.colorbar_div.style.backgroundImage = colorbar_style;
    }
    else {
        $(this.colorbar_div).empty();
        this.colorbar_div.appendChild(out.elem);
    }
    return s;
};


FigureProperties.prototype.plotting_range = function() {

    return {ca0: parseFloat(document.getElementById(this.namespace + "_style_param_vmin").value),
            ca1: parseFloat(document.getElementById(this.namespace + "_style_param_vmax").value)};

};


FigureProperties.prototype.get_style_param = function() {
    var sp = {}, id, val, elem;

    if (!this.uidata) {
        return {};
    }

    for (var i=0; i<this.uidata.length; i++) {
        id = this.uidata[i].id;

    //for (var i in this.style_param_id) {
        //id = this.style_param_id[i];

        elem = document.getElementById(this.namespace + "_style_param_" + id);

        if (elem) {
            if (this.uidata[i].type === 'boolean') {
                // for checkbox input
                val = elem.checked;
            }
            else if (this.uidata[i].type === 'float') {
                val = parseFloat(elem.value);
            }
            else if (this.uidata[i].type === 'int') {
                val = parseInt(elem.value,10);
            }
            else {
                val = elem.value;
            }

            sp[id] = val;
        }
    }

    return sp;
};


FigureProperties.prototype.set_data_range = function(vmin,vmax) {
    //console.log('call to set_data_range',vmin,vmax);
   /*
    var s = '';

    s += '<?xml version="1.0"?>';
    s += '<stats>';
    s += '<param id="cmap" type="imageselector" default="jet" label="Colormap" >';
    s += '<option value="jet">img/mini_colormap_jet.png</option>';
    s += '<option value="hsv">img/mini_colormap_hsv.png</option>';
    s += '<option value="gray">img/mini_colormap_gray.png</option>';
    s += '<option value="RdBu">img/mini_colormap_RdBu.png</option>';
    s += '<option value="Paired">img/mini_colormap_Paired.png</option>';
    s += '</param>';
    s += '<param id="inverted" type="boolean" default="False" label="Invert colormap" >';
    s += '</param>';
    s += '<param id="method" type="string" default="pcolor_flat" label="Plotting style"  description="Flat shading will produce interpolated colors">';
    s += '<option value="pcolor_flat">Flat shading</option>';
    s += '<option value="contourf">Filled contours</option>';
    s += '<option value="contour">Contours</option>';
    s += '</param>';
    s += '<param id="vmin" type="float" default="' + vmin + '" label="Minimum color-bar range" >';
    s += '</param>';
    s += '<param id="vmax" type="float" default="' + vmax + '" label="Maximum color-bar range" >';
    s += '</param>';
    s += '<param id="ncontours" type="int" default="40" label="Number of contour-lines" >';
    s += '</param>';
    s += '</stats>';

    var xmldoc = OpenLayers.parseXMLString(s);
    this.set_stat(xmldoc);
    */

    var data = [
        {'type': "imageselector",
         'default': "jet",
         'id': "cmap",
         'label': "Colormap",
         'opt': [
             ["jet", "img/mini_colormap_jet.png"],
             ["hsv", "img/mini_colormap_hsv.png"],
             ["gray", "img/mini_colormap_gray.png"],
             ["RdBu", "img/mini_colormap_RdBu.png"],
             ["Paired", "img/mini_colormap_Paired.png"]
         ]
        },
        {'type': "boolean",
         'default': "False",
         'id': "inverted",
         'label': "Invert colormap"
        },
        {'default': "pcolor_flat",
         'description': "Flat shading will produce interpolated colors",
         'id': "method",
         'label': "Plotting style",
         'opt': [
             ["pcolor_flat","Flat shading"],
             ["contourf", "Filled contours"],
             ["contour", "Contours"]
         ],
         'type': "string"
        },
        {'default': vmin,
         'id': 'vmin',
         'label': "Minimum color-bar range",
         'type': "float"
        },
        {'default': vmax,
         'id': "vmax",
         'label': "Maximum color-bar range",
         'type': "float"}
    ];


    this.set_stat(data);
};

FigureProperties.prototype.input_field = function(name, title, units, range, range_label, default_value) {
        //var div = this.layer_info_div;
        var div = this.input_field_div;
        var br = div.appendChild(document.createElement("br"));

        if (range_label === null) {
           range_label = range;
        }

        br.style.clear = 'both';

        var label = document.createElement("label");
        label.htmlFor = this.namespace + "_select_" + name;

        if (units) {
            label.appendChild(document.createTextNode(title + ": " + '[' + units + ']'));
        }
        else {
            label.appendChild(document.createTextNode(title + ": "));
        }

        div.appendChild(label);

        if (range.min === undefined) {
             // range is a list of values

            sel = div.appendChild(document.createElement("select"));

             // set values in range

        for (j=0; j < range.length; j++) {
            option = document.createElement('option');

            option.value = "" + range[j];

            if (range[j] == default_value) {
                option.selected = range[j] == default_value;
            }

            option.appendChild(document.createTextNode("" + range_label[j]));

            sel.appendChild(option);
        }
        }
        else {
            // range is a object {min: ..., max: ...}
            sel = div.appendChild(document.createElement("input"));

            if (name === 'time') {
                sel.type = "text";
            }
            else {
                sel.type = "number";
                sel.min = range.min;
                sel.max = range.max;
            }

            if (range.default_value !== undefined) {
                sel.value = range.default_value;
            }

            if (name === 'time') {
                sel.type = "text";
                $(sel).datepicker({dateFormat: 'yy-mm-dd' });
            }

        }

        sel.title = title;
       //sel.id = this.namespace + "_select_" + name;

      return sel;

};

FigureProperties.prototype.set_dimension = function(name,title,opt) {
    var sel,
    range = opt.range,
    range_label = opt.range_label,
    interval = opt.interval,
    units = opt.units,
    default_value = opt.default_value,
    default_upper_value = opt.default_upper_value || opt.default_value,
    default_lower_value = opt.default_lower_value || opt.default_value,
    j;


    if (!(name in this.dimensions)) {
        this.dimensions[name] = {};
        this.dimensions[name].name = title;

/*
        this.layer_info_div.appendChild(document.createElement("br"));
        var label = document.createElement("label");
        label.htmlFor = this.namespace + "_select_" + name;
        label.appendChild(document.createTextNode(title + ": "));
        this.layer_info_div.appendChild(label);

        if (range.min === undefined) {
             // range is a list of values

            sel = this.layer_info_div.appendChild(document.createElement("select"));
        }
        else {
            // range is a object {min: ..., max: ...}
            sel = this.layer_info_div.appendChild(document.createElement("input"));
            sel.type = "number";
            sel.min = range.min;
            sel.max = range.max;
        }

        sel.id = this.namespace + "_select_" + name;
        sel.title = title;

        this.dimensions[name].sel = sel;
        this.dimensions[name].label = label;*/

        if (range.upper === undefined) {
            this.dimensions[name].sel = this.input_field(name,title,units,range, range_label, default_value);
        }
        else {
            this.dimensions[name].sel0 = this.input_field(name,'min ' + title,units,range.lower, null, default_lower_value);
            this.dimensions[name].sel1 = this.input_field(name,'max ' + title,units,range.upper, null, default_upper_value);

            if (name === 'time') {
                this.dimensions[name].sel0.title =  'date between ' + range.lower.min + ' and ' + range.lower.max;
                this.dimensions[name].sel1.title =  'date between ' + range.upper.min + ' and ' + range.upper.max;
            }

        }

    }


    if (name == 'time' && range.upper === undefined) {
        this.layer_info_div.appendChild(document.createElement("br"));
        this.button_anim = OceanBrowser.UI.makedom("input", {
            "type": "button",
            "value": "Animate",
            "class": "animate",
            "title": "animate",
            "id": this.namespace + "config_button"});
        this.buttons_div.appendChild(this.button_anim);
    }

};


FigureProperties.prototype.get_dimension = function(name) {
    function getdate($elem) {
        // bug in datepicker?
        // http://stackoverflow.com/questions/28174855/jquery-ui-datepicker-getdate-returns-current-date-if-the-format-has-been-set-to

        /*
        var d = $elem.datepicker('getDate');

        var utcd = new Date();
        utcd.setUTCFullYear(d.getFullYear());
        utcd.setUTCMonth(d.getMonth());
        utcd.setUTCDate(d.getDate());
        utcd.setUTCHours(d.getHours());
        utcd.setUTCMinutes(d.getMinutes());
        utcd.setUTCSeconds(d.getSeconds());
        utcd.setUTCMilliseconds(d.getMilliseconds());
        return utcd;
        */
        var d = new Date($elem.val());
        return d;
    }


    if (name in this.dimensions) {
        if (this.dimensions[name].sel) {
            return this.dimensions[name].sel.value;
        }
        else {
            if (name === 'time') {
               return {
                   lower: getdate($(this.dimensions[name].sel0)),
                   upper: getdate($(this.dimensions[name].sel1))
               }
            }
            else {
               return {lower: this.dimensions[name].sel0.value,
                    upper: this.dimensions[name].sel1.value};
            }
        }
    }

    return '';
};


// display feature info
FigureProperties.prototype.set_info = function(str) {
    if (!this.info) {
        this.info = document.createElement("div");
        this.info.appendChild (document.createTextNode(""));
        this.layer_info_div.appendChild(this.info);
    }

    if (typeof(str) === "string") {
        this.info.firstChild.data = str;
    }
    else {
        $(this.info).empty();
        $(this.info).append(str);
    }
};

FigureProperties.prototype.get_layer_param = function() {
    //console.log('get_layer_param',id);
    var analysis_params = {};
    var ext = '';

    if (this.Layer) {
        if (this.Layer.cap) {
            ext = this.Layer.cap.WMS_extensions;
        }
    }

    var depth = this.get_dimension('elevation');
    var time = this.get_dimension('time');
    var style = this.get_style();

    if (depth !== "") {
        analysis_params.elevation = depth;
    }

    if (time !== "") {
        analysis_params.time = time;
    }

    //console.log('param + style',analysis_params,style);

    if (ext === 'ncWMS') {

        var style_param = this.get_style_param();
        analysis_params.COLORSCALERANGE = style_param.vmin + ',' + style_param.vmax;
    }

    return {param: analysis_params, style: style};
};



FigureProperties.prototype.remove = function() {
    var root = document.body;
    //root.removeChild(this.dialog_element);
    this.colorbar_container.removeChild(this.layer_info_div);
};



// give nice names to time ranges
Util.present_dim_timeaverage = function(values,dimname,layer_name,dyear0,dyear1) {
    var season;

    if (dimname === "time") {
        var range_label = [];
        for (var i = 0; i < values.length; i++) {
            // can be "spring 1965" or "2000"
            parts = values[i].split(" ")
            var year = parseInt(parts[parts.length-1]);

            season = null;
            if (parts.length > 1) {
                season = parts[0];
            }
            else {
                try {
                    layer_name_splitted = layer_name.split('/');
                    season = layer_name_splitted[layer_name_splitted.length-2].split(" ")[0].toLowerCase();
                    if (["winter","spring","summer","autumn"].indexOf(season) == -1) {
                        season = null;
                    }
                }
                catch(e) {
                    season = null;
                }
            }


            if (season) {
                range_label[i] = season + ",  " + (year-dyear0) + "-" + (year+dyear1);
            }
            else {
                range_label[i] = (year-dyear0) + "-" + (year+dyear1);
            }
        }
        return range_label;
    }
    else {
        return values;
    }
};



Util.make_https = function(server_url) {
    // if page is loaded with https then all other page must be loaded over https

    if (window.location.protocol == "https:") {
        // check if URL API is available
        if (typeof URL === "function") {
            try {
                var url = new URL(server_url);

                if (url.protocol == "http:") {
                    url.protocol = "https:";
                }
                if (url.port == "80") {
                    url.port = "443";
                }

                server_url = url.toString();
            }
            catch {}
        }
        else  {
	        server_url = server_url.replace("http://","https://")
        }
    }
    return server_url;
}
