/******************************************************************************
 *                                                                            *
 *  Copyright (C) 2008-2014 Alexander Barth <barth.alexander@gmail.com>.      *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU Affero General Public License as published  *
 *  by the Free Software Foundation, either version 3 of the License, or      *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                            *
 ******************************************************************************/


// TODO: remove section_name

function log(message) {
    document.getElementById("log").firstChild.data += "" + message + "\n";
}


// VerticalSection extends anim class

VerticalSection.prototype = new anim();
VerticalSection.prototype.constructor = VerticalSection;

function VerticalSection() {
    anim.call(this,
	      "vert_map",
	      document.getElementById('vert_map_colorbars')
	      );

    this.control = null;
    this.xaxis = new Axis("xaxis","x",{tickslabelclass: "xtickslabel", ticksclass: "xticks"});
    this.yaxis = new Axis("yaxis","y",{tickslabelclass: "ytickslabel", ticksclass: "yticks"});
    this.ratio = 1/100;
    this.vectors = null;
    this.map_selection = null;
    this.mini_wms = {};
    this.mini_baselayer_wms = null;
    this.analysis_wms = {};
    this.Layers = {};
    this.map = null;
    this.section = '';
    this.positionMarker  = null; // marker on map_selection
    this.sectionCoord = [];

    var scale = 1;

    var xmin = 0/(scale*this.ratio);
    var xmax = 120/(scale*this.ratio);
    var ymin = -10000/(scale);
    var ymax = 360/(scale);

    if (this.map) {
	this.map.destroy();
    }

    if (this.map_selection) {
	this.map_selection.destroy();
    }

    this.map_selection = new OpenLayers.Map("vert_map_selection",
					    {maxResolution: 180/128,
					     maxExtent: new OpenLayers.Bounds(-180, -90, 180, 90),
					     theme: null
					    });

    var options = {
	resolutions: [1.40625,0.703125,0.3515625,0.17578125,0.087890625,0.0439453125,0.02197265625,0.010986328125,0.0054931640625,0.00274658203125,0.00137329101]
    };

    this.mini_baselayer_wms = new OpenLayers.Layer.WMS("Continents",baselayer_url,
						       {layers: baselayer_name,
							styles: "max_contourf",
							format: baselayer_format},
						       options);

	// style for finished paths
    var style = OpenLayers.Util.applyDefaults({strokeWidth: 2, strokeColor: "black"},
					  OpenLayers.Feature.Vector.style["default"]);

	// style for path currently drawn
    var styletemp = OpenLayers.Util.applyDefaults({strokeWidth: 2, pointRadius: 0, strokeColor: "black"},
					  OpenLayers.Feature.Vector.style["default"]);


    this.vectors = new OpenLayers.Layer.Vector("Vector Layer", {style: style});
    this.markerLayer = new OpenLayers.Layer.Vector("Marker Layer", {style: style});
    this.markerLayer.setZIndex(this.map_selection.Z_INDEX_BASE.Feature+1);

    this.control = new OpenLayers.Control.DrawFeature(
		this.vectors,
		OpenLayers.Handler.Path,
		{ handlerOptions: {style: styletemp},
		  callbacks: {point: function(e) {
			  // remove current selection if defined
 			  if (that.sectionCoord) {
				  that.clear_map_selection();
			  }
		  }}
			});

    this.map_selection.addLayers([this.mini_baselayer_wms,this.vectors,this.markerLayer]);
    this.vectors.setZIndex(this.map_selection.Z_INDEX_BASE.Feature);

    this.map_selection.addControl(this.control);

    var that = this;
    // how to prevent default action such as selection (in Chrome)
    // after featureadded event?
    this.vectors.events.register("featureadded", null, function(event) {
		// extract section in "draw" mode
		// otherwise, wait that the user select a contour line

		if ($('#vertical_mode_choose_type').val() == "draw") {
			that.update();
		}
	});

    this.vectors.events.register("sketchcomplete", this, function(event) {
		console.log('event completed',event);
	});

    var bounds = new OpenLayers.Bounds(xmin,ymin,xmax,ymax);

    this.map = new OpenLayers.Map('vert_map', {
	    projection: "EPSG:4326" ,
	    maxExtent: bounds,
	    numZoomLevels: 70,
	    //	    maxScale: 2,
	    maxResolution: 10000/256, // maximum depth / height of map
	    units: 'm',
	    theme: null
                });

    this.map.events.register("dragend", null, function() { that.draw_axis(); });
    this.map.events.register("moveend", null, function() { that.draw_axis(); });
    this.map.events.register("zoomend", null, function() { that.draw_axis(); });
    this.map.events.register('mousemove',this,function(evt) {
        var pos, xy;
        pos = this.map.getLonLatFromPixel(evt.xy);

        if (!pos) {
            // map has not yet been properly initialized
            return;
        }
        xy = this.secpos(pos.lon * this.ratio);
        this.showPos(xy);
        //console.log('x,y ',xy);
    });

    this.baselayer = new OpenLayers.Layer.WMS( "Base layer",
					       "img/blank.gif?", {layers: 'BMNG',
								   format: 'image/png'},
					{isBaseLayer: true});

    this.map.addLayers([this.baselayer]);

    this.map.addControl(new OpenLayers.Control.LayerSwitcher());
    this.map.addControl(new OpenLayers.Control.CustomNavToolbar(this));

    //alert(bounds);
    this.map.fractionalZoom = true;

    //document.getElementById("ratio").value = this.ratio;
    this.map.events.register('click',this,this.request_feature_info);

    var that = this;
    this.manageSection = new OceanBrowser.UI.manageSection(DefinedSection,defaults.ContourExtractor_baseurl);
    this.manageSection.events.register('newSection',this,this.addSection);

    $('#manage_section_button').click(function() {
        var bb = that.map_selection.getExtent();
        that.manageSection.getSection(that.sectionCoord,bb);
    });

    $('#vertical_mode_choose_type').change(function() {
        that.change_select_section();
    })
    this.change_select_section();


    $('#vertical_mode_form').submit(function(event) {
        event.preventDefault();
        that.generateIsoline();
        return false;
    });

}

VerticalSection.prototype.addSection = function(coord) {
    console.log('coord ',coord);

    // compare value not reference
    if (JSON.stringify(coord) !== JSON.stringify(this.sectionCoord)) {
        var points = coord.map(function(p) { return new OpenLayers.Geometry.Point(p[0],p[1]); });
        var feature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.LineString(points));
        this.clear_map_selection();
        this.vectors.addFeatures(feature);
    }
};

VerticalSection.prototype.addLayer = function(Layer) {
    if (!Layer.has_dimension('elevation') || (Layer.WMS_extensions !== 'OceanBrowser')) {
	// do nothing if layer does not have a vertical dimension
	return;
    }


    anim.prototype.addLayer.call(this,Layer);
    var id = Layer.id;

    var display_param = hsection.get_layer_param(id);
    console.log(display_param.style);

    this.change_select_section();

    this.mini_wms[id] = new OpenLayers.Layer.WMS(Layer.title,
						 Layer.wms_url,
					     {layers: Layer.name, styles: display_param.style,
					      transparent: "true", format: "image/png"});

    this.mini_wms[id].mergeNewParams(display_param.param);
    this.map_selection.addLayer(this.mini_wms[id]);
    // make sure that mini_wms is under the vector layer
    this.mini_wms[id].setZIndex(this.map_selection.Z_INDEX_BASE.BaseLayer + this.layer_num);

    this.map_selection.zoomToExtent(this.get_bounds(),true);


    // query time dimension and add to dialog if appropriate

    var time = Layer.get_dimension('time');

    if (time) {
    if (time.range.length > 0) {

	this.figprops[id].set_dimension('time','time',time);

	this.Layers[id].anim = new AnimLayer(
					     this.figprops[id].button_anim,
					     this.figprops[id].dimensions['time'].sel,
					     this,id);
    }
    }

    // if section is already defined request the same sector for the variable
    if (this.section) {
	this.request_stat(id);
    }
};



VerticalSection.prototype.removeLayer = function(Layer) {
    /*if (!Layer.has_dimension('elevation')) {
	// do nothing if layer does not have a vertical dimension
	return;
    }*/

    var id = Layer.id;

    if (!this.figprops[id]) {
        return;
    }

    anim.prototype.removeLayer.call(this,Layer);

    this.map_selection.removeLayer(this.mini_wms[id]);
    if (this.analysis_wms[id]) {
		this.map.removeLayer(this.analysis_wms[id]);
    }

    delete this.mini_wms[id];
    delete this.analysis_wms[id];
};

VerticalSection.prototype.onvisible = function() {
    var bounds, ms, timeout;

    if (this.map_selection) {
	    console.log("width: " +    $(this.map_selection.div).width());
    }

    bounds = this.get_bounds();
    ms = this.map_selection;

    if (bounds) {
	    // can only zoomToExtent when maps is visible
	    // is there a better way????
	    timeout = setTimeout(function () {
	        //console.log("bounds cb: " + bounds);
	        //console.log("width: " +    $(ms.div).width());

	        //alert("bounds cb: " + bounds);
	        ms.zoomToExtent(bounds,true);
	    },1000);
    }
};


VerticalSection.prototype.generateIsoline = function() {
    // remove previous layers and control
	this.clear_map_selection();

    if (this.click) {
	  this.map_selection.removeControl(this.click);
    }

    var bounds = this.get_bounds();
    var type = $('#vertical_mode_choose_type').val();

    var param = {
	'maxlen': 100,
        'type': type,
        'lev': $('#vertical_mode_form .' + type + ' .level').val(),
        'xr': [bounds.left,bounds.right].join(','),
        'yr':  [bounds.bottom,bounds.top].join(',')};

    $('#vertical_mode_form .spinner').show();

    var that = this;
    $.getJSON(defaults.ContourExtractor_baseurl, param, function(featurecollection) {
		$('#vertical_mode_form .spinner').hide();

        var geojson_format = new OpenLayers.Format.GeoJSON();
        that.vectors.addFeatures(geojson_format.read(featurecollection));
        that.click = new OpenLayers.Control.Click(that.vectors);
        that.map_selection.addControl(that.click);
        that.click.activate();
    });

};

VerticalSection.prototype.draw_axis = function() {
    //alert("vert_map.maxExtent " + this.map.getExtent());
    var b = this.map.getExtent();


    this.xmin = b.left*this.ratio;
    this.xmax = b.right*this.ratio;
    this.ymin = b.bottom;
    this.ymax = b.top;

    this.xaxis.set_range(this.xmin,this.xmax);
    this.yaxis.set_range(this.ymin,this.ymax);

    document.getElementById("xmin").value = "" + this.xmin;
    document.getElementById("xmax").value = "" + this.xmax;
    document.getElementById("ymin").value = "" + this.ymin;
    document.getElementById("ymax").value = "" + this.ymax;
};



VerticalSection.prototype.clear_map_selection = function(event) {
    this.vectors.destroyFeatures();
    this.markerLayer.removeFeatures(this.positionMarker);
    this.positionMarker = null;
	this.sectionCoord = undefined;
};

VerticalSection.prototype.request_stat = function(id,time) {
    var that = this, vtime;

    if (this.Layers[id].WMS_extensions == 'OceanBrowser') {
	vtime = time;

	// default value of time
	if (vtime === undefined) {
	    vtime = this.figprops[id].get_dimension('time');
	}


	var Layer = this.Layers[id];

	var params  = {section: this.section,
		       layer: Layer.name,
		       time: vtime,
		       service: "WMS",
		       version: "1.3.0",
		       request: "GetStats"};

	// closure around id
	var fun = function(id) {
	    return function(xmldoc) {
		that.stat(xmldoc,id);
	    };
	}(id);

	Util.proxy_ajax(this.Layers[id].vert_wms_url,params,null,fun,{proxy: this.Layers[id].use_proxy});
    }
};


VerticalSection.prototype.getSectionCoord = function () {
    var i;

    for (i = 0; i < this.vectors.features.length; i++) {
        if  (this.vectors.features[i].geometry.CLASS_NAME === "OpenLayers.Geometry.LineString") {
            return this.vectors.features[i].geometry.components;
        }
    }
};


VerticalSection.prototype.getSectionCoord2 = function () {
    var i, c;

    for (i = 0; i < this.vectors.features.length; i++) {
        if  (this.vectors.features[i].geometry.CLASS_NAME === "OpenLayers.Geometry.LineString") {
            c = this.vectors.features[i].geometry.components;

            if (c) {
              return c.map(function(p) { return [p.x,p.y]});
            }
        }
    }
};


VerticalSection.prototype.update = function(time,sectionCoord) {
    // make sure that path is visible
    this.vectors.setZIndex(this.map_selection.Z_INDEX_BASE.Feature);

    if (!sectionCoord) {
        sectionCoord = this.getSectionCoord2();
    }
    this.sectionCoord = sectionCoord;

    var section = "";
    for (var i=0; i < sectionCoord.length; i++) {
	section += sectionCoord[i][0] + "," + sectionCoord[i][1] + "|";
    }
    this.section = section.substring(0,section.length-1);

    // test if section was choosen
    if (this.section) {
	//document.getElementById("status").firstChild.data = "Extract section...";
	$('#vertical_mode_form .spinner').show();

	for (var id in this.Layers) {
	    this.request_stat(id,time);
	}
    }
};

VerticalSection.prototype.stat = function(xmldoc,id) {
    this.data_xmin = parseFloat(xmldoc.getElementsByTagName("xmin")[0].firstChild.nodeValue);
    this.data_xmax = parseFloat(xmldoc.getElementsByTagName("xmax")[0].firstChild.nodeValue);
    this.data_ymin = parseFloat(xmldoc.getElementsByTagName("ymin")[0].firstChild.nodeValue);
    this.data_ymax = parseFloat(xmldoc.getElementsByTagName("ymax")[0].firstChild.nodeValue);


    document.getElementById("xmin").value = "" + this.data_xmin;
    document.getElementById("xmax").value = "" + this.data_xmax;
    document.getElementById("ymin").value = "" + this.data_ymin;
    document.getElementById("ymax").value = "" + this.data_ymax;

    this.figprops[id].set_stat(xmldoc);

    //document.getElementById("status").firstChild.data = "";
    $('#vertical_mode_form .spinner').hide();

    this.update_layer(id);
};

VerticalSection.prototype.get_wms_layer = function(id,options) {
    var Layer = this.Layers[id];
    var time = this.figprops[id].get_dimension('time');


    // default parameter values
    var analysis_params = {layers: Layer.name,
			   styles: this.figprops[id].get_style(),
			   format: 'image/png',
			   transparent: "true",
			   ratio: this.ratio,
			   section: this.section,
			   time: time
    };


    // override by parameters in options
    for (var key in options) {
	analysis_params[key] = options[key];
    }


    var layer = new OpenLayers.Layer.WMS(Layer.title,this.Layers[id].vert_wms_url,
					 analysis_params,
                                         {isBaseLayer: false});

    return layer;
};

VerticalSection.prototype.update_all = function() {
    for (var id in this.Layers) {
	  this.update_layer(id);
    }
};

VerticalSection.prototype.update_layer = function(id) {
    // do nothing if section was not created
    if (this.section.length === 0) {
	alert('Please draw a section first.');
	return;
    }

    this.xmin = parseFloat(document.getElementById("xmin").value);
    this.xmax = parseFloat(document.getElementById("xmax").value);
    this.ymin = parseFloat(document.getElementById("ymin").value);
    this.ymax = parseFloat(document.getElementById("ymax").value);


    //var depth_range = parseFloat(document.getElementById("ratio").value);
    var map_height = $('#vert_map').height();
    var map_width = $('#vert_map').width();

    this.ratio = (this.xmax-this.xmin)/(this.ymax-this.ymin) * map_height/map_width;
    this.bounds = new OpenLayers.Bounds(this.xmin/this.ratio,this.ymin,this.xmax/this.ratio,this.ymax);

    //this.map.maxExtent = new OpenLayers.Bounds(this.data_xmin/this.ratio,this.data_ymin,this.data_xmax/this.ratio,this.data_ymax);



    if (!(id in this.analysis_wms)) {
	    this.analysis_wms[id] = this.get_wms_layer(id);

	    this.map.addLayers([this.analysis_wms[id]]);

	    //draw_axis();
    }
    else {
	    var time = this.figprops[id].get_dimension('time');
	    var analysis_params = {layers: this.Layers[id].name,
				   styles: this.figprops[id].get_style(),
				   format: 'image/png',
				   transparent: "true",
				   ratio: this.ratio,
				   section: this.section,
				   time: time};

	    this.analysis_wms[id].mergeNewParams(analysis_params);
	    this.analysis_wms[id].redraw();
    }


    this.map.zoomToExtent(this.bounds,true);
    this.draw_axis();
};

VerticalSection.prototype.change_select_section = function() {
    var mode = $('#vertical_mode_choose_type').val();

    if (mode == "draw") {
	this.control.activate();
	// this.control.handler.layer is a temporary drawing layer
	// make sure it is visible
	this.control.handler.layer.setZIndex(this.map_selection.Z_INDEX_BASE.Feature);
    }
    else {
	this.control.deactivate();
    }

    // make sure that path is visible
    this.vectors.setZIndex(this.map_selection.Z_INDEX_BASE.Feature);

    // remove previous layers and controls
    if (this.click) {
	    this.map_selection.removeControl(this.click);
		this.click = null;
	}

	this.clear_map_selection();

    // hide all paragraphs specific to a mode
    $('#vertical_mode_form p').hide()
    // show the paragraphs corresponding to the selected
    $('#vertical_mode_form').find('.' + mode).show();

};


VerticalSection.prototype.getCurrentExtent = function() {
    var b = this.map.getExtent();
    b.left = b.left*this.ratio;
    b.right = b.right*this.ratio;

    return b;
};


// download current view

VerticalSection.prototype.download = function(id,opt) {
    var layer = this.Layers[id];
    // same extend as current view

    var extent = this.analysis_wms[id].getExtent();
    var b = this.map.getExtent();
    this.xaxis.set_range(b.left*this.ratio,b.right*this.ratio);

    var time = this.figprops[id].get_dimension('time');

    // # axis leaves some room for labels and colorbar
    // self.ax = self.fig.add_axes([0.1, 0.1, 0.7, 0.8])
    // choose ratio fill all space

    var ratio = (opt.xmax-opt.xmin)/(opt.ymax-opt.ymin) * (0.8*opt.height)/(0.7*opt.width);

    var style = this.figprops[id].get_style();
    var bounds = new OpenLayers.Bounds(opt.xmin/ratio,opt.ymin,opt.xmax/ratio,opt.ymax);

    var params =
      {layers: this.Layers[id].name,
       section: this.section,
       service: "WMS",
       request: "GetMap",
       width: opt.width,
       height: opt.height,
       bbox: bounds.toBBOX(),
       transparent: true,
       decorated: true,
       version: '1.3.0',
       styles: style,
       /*cmap: this.figprops[id].get_colormap(),*/
       format: opt.format,
       ratio: ratio,
       time: time
      };

    if (opt.format.slice(0,5) === 'video') {
        // for a video get all time slices
        params.time = this.Layers[id].get_dimension('time').range.join(',');
    }

    params.title = Util.figureTitle(layer,params);


    var url = Util.append_param(this.Layers[id].vert_wms_url,params);

    window.open(url,'Image');
};



function arcdistance(lon0,lat0,lon1,lat1) {
    var d,b,C,d;
    a = lat0 * Math.PI/180;
    b = lat1 * Math.PI/180;
    C = (lon1-lon0) * Math.PI/180;
    d = Math.acos(Math.sin(b) * Math.sin(a) + Math.cos(b) * Math.cos(a) * Math.cos(C));
    return d*180/Math.PI;
}

VerticalSection.prototype.secpos = function (s) {
    var len = 0, seglen, alpha, x,y, c, i;

    //c = this.getSectionCoord2();
    c = this.sectionCoord;

    if (c === undefined) {
        // section has been removed by the user
        return;
    }

    for (var i=0; i < c.length-1; i++) {
	  seglen = arcdistance(c[i+1][0],c[i+1][1],c[i][0],c[i][1]);

	  if (len <= s && s <= len+seglen) {
	    alpha = (s - len)/seglen;
	    x = c[i][0] + alpha * (c[i+1][0]-c[i][0]);
	    y = c[i][1] + alpha * (c[i+1][1]-c[i][1]);
	    //console.log('x,y',x,y);
	    return [x,y];
	  }

	  len += seglen;
    }
};

VerticalSection.prototype.showPos = function(xy) {
    if (xy === undefined) {
        return;
    }

    if (this.positionMarker !== null) {
        this.markerLayer.removeFeatures(this.positionMarker);
    }

    this.positionMarker = new OpenLayers.Feature.Vector(
        new OpenLayers.Geometry.Point(xy[0],xy[1]),
        {type: "circle"});
    this.markerLayer.addFeatures(this.positionMarker);

	//console.log('this.markerLayer.getZIndex() ',this.markerLayer.getZIndex());
	this.markerLayer.setZIndex(this.map_selection.Z_INDEX_BASE.Feature+1);
};
