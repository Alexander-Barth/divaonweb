(function($) {

// list of supported mime types
var mimetypes = [{name: 'PNG',  type: 'image/png'},
		 {name: 'JPEG', type: 'image/jpeg'},
		 {name: 'GIF',  type: 'image/gif'},
		 {name: 'SVG',  type: 'image/svg+xml'}];

var data;
//http://localhost/emodnet/plots.html?image%2Fpng=http%3A%2F%2Fgher-diva.phys.ulg.ac.be%2Femodnet-chemistry%2Fimages%2FMHI%2F20_bottom_Alk.png&image%2Fsvg%2Bxml=http%3A%2F%2Fgher-diva.phys.ulg.ac.be%2Femodnet-chemistry%2Fimages%2FMHI%2F20_bottom_Alk.svg

    function update() {
	$('#plot').attr('src',data[$("#formats").val()]);
    }

    $(document).ready(function() {
	var i,
	query = new Util.query(window.location.search.substring(1));
        
	data = query.param;	    
        
	for (i = 0; i < mimetypes.length; i++) {
	    if (data[mimetypes[i].type]) {
		$('#formats').append($('<option/>').attr('value',mimetypes[i].type).append(mimetypes[i].name));
	    }
	}
        
        if (data.download !== undefined) {
            $('#download').click(function() {
                window.open(data.download,'Download Data'); 
            });
        }
        else {
            $('#download').remove();
        }

        $('#downloadImage').click(function() {
            window.open($('#plot').attr('src'),'Download Image'); 
        });

	update();
	    	    
	$('#formats').change(function() {
	    update();
	});
    
    });    
})(jQuery);
