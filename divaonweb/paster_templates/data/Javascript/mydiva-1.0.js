/******************************************************************************
 *                                                                            *
 *  Copyright (C) 2008-2010 Alexander Barth <barth.alexander@gmail.com>.      *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU Affero General Public License as published  *
 *  by the Free Software Foundation, either version 3 of the License, or      *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                            *
 ******************************************************************************/

// TODO
// no set_styles, set_data_range
// call set_stat

// mydiva(resource,id,options)
//
// resource: filename
// id: div id in html
// options: structure of optinal arguments:
//   max_error: maximum relative error (default 1)
//   analysis_name: name of the analysis visible in the layer-switcher (default "Analysis");
//   errormask_name: name of the error mask visible in the layer-switcher (default "Error mask");
//   xmin, xmax, ymin, ymax: lon/lat range (default: query server; not always possible due to security restriction in the web-browser)
//   vmin, vmax: color-bar range (default: query server; not always possible due to security restriction in the web-browser)
//   baselayer_name: (default "NASA Blue marble")
//   baselayer_url: (default "http://gher-diva.phys.ulg.ac.be/web-vis/Python/web/bluemarble?")
//   baselayer_layers: (default {layers: 'BMNG',format: 'image/png'})
//   baselayer_options: (default {isBaseLayer: true})
//   wmsurl: (default "Python/web/wms")


function mydiva(resource,id,options) {
  this.layersep  = Util.layersep;

  this.resource = resource;
  this.id = id;
  this.map_id = id + "_openlayers";
  this.max_error_id = id + "_max_error";
  //this.colorbar_id = id + "_colorbar";

  // default options
  this.max_error = 1;
  this.wmsurl = "Python/web/diva_on_web";
  //  this.wmsurl = "http://gher-diva.phys.ulg.ac.be/web-vis/Python/web/wms";
  this.analysis_name = "Analysis";
  this.errormask_name = "Error mask";

  /* 
     // Metacarta
  this.baselayer_name = "OpenLayers WMS";
  this.baselayer_url = "http://labs.metacarta.com/wms/vmap0?";
  this.baselayer_layers = {layers: 'basic'};
  this.baselayer_options = {resolutions: [1.40625,0.703125,0.3515625,0.17578125,0.087890625,0.0439453125,0.02197265625,0.010986328125,0.0054931640625,0.00274658203125,0.00137329101]
    };
  */

  /*
  // NASA Blue marble (cached on gher-diva.phys.ulg.ac.be)
  this.baselayer_name = "NASA Blue marble";
  this.baselayer_url = "http://gher-diva.phys.ulg.ac.be/web-vis/Python/web/bluemarble?";
  this.baselayer_layers = {layers: 'BMNG',format: 'image/png'};
  this.baselayer_options = {isBaseLayer: true};
  */

  this.baselayer_name = 'GEBCO';
  this.baselayer_url = 'https://www.gebco.net/data_and_products/gebco_web_services/web_map_service/mapserv';
  this.baselayer_layers = {layers: 'GEBCO_Latest',format: 'image/png'};
  this.baselayer_options = {isBaseLayer: true};



  // overwrite defaults
  if (options) {
      for (k in options) {
	  if (options[k])
	      this[k] = options[k];
      }
  }

  var obj = this;
  var initial = function () { obj.init(); };

// execute initial if html page loaded
  if( window.addEventListener ) {
      window.addEventListener('load',initial,false);
  } else if( document.addEventListener ) {
      document.addEventListener('load',initial,false);
  } else if( window.attachEvent ) { // IE
      window.attachEvent('onload',initial);
  }
}


mydiva.prototype.create_interface = function(root) {

    var oDiv1=document.createElement("div");
    oDiv1.style.width="640px";
    //oDiv1.style.border="1px solid red";
	       
    var oDiv2=document.createElement("div");
    oDiv2.style.cssFloat="left";
    oDiv2.style.styleFloat="left"; /* For IE */
    oDiv2.style.width="512px";
    oDiv2.style.height="350px";
    oDiv2.style.border="1px solid black";
    oDiv2.id=this.map_id;	
    oDiv1.appendChild(oDiv2);	

    this.colorbar_containter = document.createElement("div");
    this.colorbar_containter.style.cssFloat="left";
    this.colorbar_containter.style.styleFloat="left"; /* For IE */
    this.colorbar_containter.style.width="120px";
    this.colorbar_containter.style.height="350px";
    /*this.colorbar_containter.style.border="1px solid black";*/
    oDiv1.appendChild(this.colorbar_containter);	
	
    var oBr=oDiv1.appendChild(document.createElement("br"));
    var oDiv=oDiv1.appendChild(document.createElement("div"));
    oDiv.style.clear="both";
      
    var oTable=document.createElement("table");
    oTable.border="0";
	
    var oTbody=document.createElement("tbody");

    var oTr=oTbody.appendChild(document.createElement("tr"));
    var oTd=oTr.appendChild(document.createElement("td"));
    var oLabel=oTd.appendChild(document.createElement("label"));
	
    var oText = oLabel.appendChild (document.createTextNode("Maximum rel. error (from 0 to 1): "));
    oTd=oTr.appendChild(document.createElement("td"));
    var oInput=oTd.appendChild(document.createElement("input"));
    oInput.type="text";
    oInput.size="5";
    oInput.id=this.max_error_id;
    oInput.value="" + this.max_error;
    
    oTable.appendChild(oTbody);
    oDiv1.appendChild(oTable);	
	
    // in IE7, a button cannot be appended before its type is set
    oInput=document.createElement("input");
    oInput.setAttribute("type","button");
    oInput.style.cssFloat="left";
    oInput.style.styleFloat="left"; /* For IE*/
    oInput.name="button";
    oInput.value="update";

    var obj = this;
    oInput.onclick = function(event) { obj.update(); };

    //addEventListener("click",function(event) {obj.update()},false);		

    oDiv1.appendChild(oInput);
	
    oDiv=oDiv1.appendChild(document.createElement("div"));
    oDiv.style.cssFloat="right";
    oDiv.style.styleFloat="right"; /* For IE*/
    
    var oA = oDiv.appendChild(document.createElement("a"));
    oA.style.fontSize="small";
    oA.style.cssFloat="right";
    oA.style.styleFloat="right"; /* For IE*/
    oA.style.color="gray";
    oA.style.border="thin solid gray";
    oA.style.padding="2px";
    oA.style.backgroundColor="white";
    oA.style.textDecoration="none";
    oA.target="_blank";
    oA.href="http://gher-diva.phys.ulg.ac.be/";
    
    oText = oA.appendChild (document.createTextNode("Diva-on-web"));
    oDiv = oDiv1.appendChild(document.createElement("div"));
    oDiv.style.clear="both";

    root.appendChild(oDiv1);

};


mydiva.prototype.init = function() {
    this.create_interface(document.getElementById(this.id));

  // test if range already defined as optinal parameter

    if (this.xmin && this.xmax && this.ymin && this.ymax &&
	this.vmin && this.vmax) {
	this.create_map();
    }
    else {
	// query range

	var name = this.resource + this.layersep + 'analyzed_field';
	var params = {layer: name, 
		      request: "GetStats", 
		      service: "WMS", 
		      version: "1.3.0"};
	var obj = this;
	Util.ajax(this.wmsurl,Util.encodeparam(params),function (xml) { obj.stat(xml); });
    }
};


// get statistics for field

mydiva.prototype.stat = function (xmldoc) {
      var args = ["xmin","xmax","ymin","ymax","vmin","vmax"];
      for (var i=0; i< args.length; i++) {
	  var kw = args[i];
	 // override only values which are not defined
	 if (!this[kw])
	     this[kw] = parseFloat(xmldoc.getElementsByTagName(kw)[0].firstChild.nodeValue);
      }

    this.create_map();
};

mydiva.prototype.create_map = function () {
    // handle the case when vmin == vmax
    if (this.vmin == this.vmax) {
	this.vmin -= 1;
	this.vmax += 1;
    }

    //this.map = new OpenLayers.Map(this.map_id,{theme: 'css/theme/default/style.css'});
    this.map = new OpenLayers.Map(this.map_id);
    this.map.addControl(new OpenLayers.Control.LayerSwitcher());

    var obj = this;
	var layer = new LayerInfo(null,
                          {wms_url: this.wmsurl,
						   name: this.resource + this.layersep + "analyzed_field",
                           WMS_extensions: "OceanBrowser",
                           bounding_box: [this.xmin,this.ymin,this.xmax,this.ymax],
                          });
	
    this.figprop = new FigureProperties(this.map_id,this.map_id,layer,
					function() { obj.update(); },
                                        {colorbarclass: 'colorbar2',
					 add_style_button: true,
					 colorbar_container: this.colorbar_containter,
					 colorbar_height: 300
					});

    this.figprop.set_data_range(this.vmin,this.vmax);


    this.ol_wms = new OpenLayers.Layer.WMS(this.baselayer_name,this.baselayer_url,this.baselayer_layers,
					    this.baselayer_options);

    this.analysis_wms = new OpenLayers.Layer.WMS(this.analysis_name,
					     this.wmsurl,
					     {layers: this.resource + this.layersep + 'analyzed_field', 
					      styles: this.figprop.get_style(),
					      transparent: "true", format: "image/png", 
					      vmin: this.vmin, vmax: this.vmax,
					      exceptions: "application/vnd.ogc.se_xml"},
					     {minResolution: 0.00001,
					      maxResolution: 0.703125});

    this.error_wms = new OpenLayers.Layer.WMS(this.errormask_name,
					       this.wmsurl,
					  {layers: this.resource + this.layersep + 'error_field' , styles: "max_contourf", max_error: this.max_error,
						transparent: "true", format: "image/png"},
					       {minResolution: 0.00001,
						maxResolution: 0.703125});


    this.map.addLayers([this.ol_wms,this.analysis_wms,this.error_wms]);

    //this.colorbar = new Axis(this.colorbar_id,"y",{tickslabelclass: "mydiva_tickslabel", ticksclass: "mydiva_ticks", len: 350});
    //this.colorbar.set_class("mydiva_colorbar_jet");


    var bounds = new OpenLayers.Bounds(this.xmin,this.ymin,this.xmax,this.ymax);
    this.map.zoomToExtent(bounds);

	


};


mydiva.prototype.update = function () {
    this.max_error = parseFloat(document.getElementById(this.max_error_id).value);

    this.analysis_wms.mergeNewParams({styles: this.figprop.get_style()});
    this.analysis_wms.redraw();

    this.error_wms.mergeNewParams({max_error: this.max_error});
    this.error_wms.redraw();
};
    

