/******************************************************************************
 *                                                                            *
 *  Copyright (C) 2008-2012 Alexander Barth <barth.alexander@gmail.com>.      *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU Affero General Public License as published  *
 *  by the Free Software Foundation, either version 3 of the License, or      *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                            *
 ******************************************************************************/

"use strict";

var OceanBrowser = {};

const COUNTRY_LIST =  [
    {"Code": "AF", "Name": "Afghanistan"},
    {"Code": "AX", "Name": "\u00c5land Islands"},
    {"Code": "AL", "Name": "Albania"},
    {"Code": "DZ", "Name": "Algeria"},
    {"Code": "AS", "Name": "American Samoa"},
    {"Code": "AD", "Name": "Andorra"},
    {"Code": "AO", "Name": "Angola"},
    {"Code": "AI", "Name": "Anguilla"},
    {"Code": "AQ", "Name": "Antarctica"},
    {"Code": "AG", "Name": "Antigua and Barbuda"},
    {"Code": "AR", "Name": "Argentina"},
    {"Code": "AM", "Name": "Armenia"},
    {"Code": "AW", "Name": "Aruba"},
    {"Code": "AU", "Name": "Australia"},
    {"Code": "AT", "Name": "Austria"},
    {"Code": "AZ", "Name": "Azerbaijan"},
    {"Code": "BS", "Name": "Bahamas"},
    {"Code": "BH", "Name": "Bahrain"},
    {"Code": "BD", "Name": "Bangladesh"},
    {"Code": "BB", "Name": "Barbados"},
    {"Code": "BY", "Name": "Belarus"},
    {"Code": "BE", "Name": "Belgium"},
    {"Code": "BZ", "Name": "Belize"},
    {"Code": "BJ", "Name": "Benin"},
    {"Code": "BM", "Name": "Bermuda"},
    {"Code": "BT", "Name": "Bhutan"},
    {"Code": "BO", "Name": "Bolivia, Plurinational State of"},
    {"Code": "BQ", "Name": "Bonaire, Sint Eustatius and Saba"},
    {"Code": "BA", "Name": "Bosnia and Herzegovina"},
    {"Code": "BW", "Name": "Botswana"},
    {"Code": "BV", "Name": "Bouvet Island"},
    {"Code": "BR", "Name": "Brazil"},
    {"Code": "IO", "Name": "British Indian Ocean Territory"},
    {"Code": "BN", "Name": "Brunei Darussalam"},
    {"Code": "BG", "Name": "Bulgaria"},
    {"Code": "BF", "Name": "Burkina Faso"},
    {"Code": "BI", "Name": "Burundi"},
    {"Code": "KH", "Name": "Cambodia"},
    {"Code": "CM", "Name": "Cameroon"},
    {"Code": "CA", "Name": "Canada"},
    {"Code": "CV", "Name": "Cape Verde"},
    {"Code": "KY", "Name": "Cayman Islands"},
    {"Code": "CF", "Name": "Central African Republic"},
    {"Code": "TD", "Name": "Chad"},
    {"Code": "CL", "Name": "Chile"},
    {"Code": "CN", "Name": "China"},
    {"Code": "CX", "Name": "Christmas Island"},
    {"Code": "CC", "Name": "Cocos (Keeling) Islands"},
    {"Code": "CO", "Name": "Colombia"},
    {"Code": "KM", "Name": "Comoros"},
    {"Code": "CG", "Name": "Congo"},
    {"Code": "CD", "Name": "Congo, the Democratic Republic of the"},
    {"Code": "CK", "Name": "Cook Islands"},
    {"Code": "CR", "Name": "Costa Rica"},
    {"Code": "CI", "Name": "C\u00f4te d'Ivoire"},
    {"Code": "HR", "Name": "Croatia"},
    {"Code": "CU", "Name": "Cuba"},
    {"Code": "CW", "Name": "Cura\u00e7ao"},
    {"Code": "CY", "Name": "Cyprus"},
    {"Code": "CZ", "Name": "Czech Republic"},
    {"Code": "DK", "Name": "Denmark"},
    {"Code": "DJ", "Name": "Djibouti"},
    {"Code": "DM", "Name": "Dominica"},
    {"Code": "DO", "Name": "Dominican Republic"},
    {"Code": "EC", "Name": "Ecuador"},
    {"Code": "EG", "Name": "Egypt"},
    {"Code": "SV", "Name": "El Salvador"},
    {"Code": "GQ", "Name": "Equatorial Guinea"},
    {"Code": "ER", "Name": "Eritrea"},
    {"Code": "EE", "Name": "Estonia"},
    {"Code": "ET", "Name": "Ethiopia"},
    {"Code": "FK", "Name": "Falkland Islands (Malvinas)"},
    {"Code": "FO", "Name": "Faroe Islands"},
    {"Code": "FJ", "Name": "Fiji"},
    {"Code": "FI", "Name": "Finland"},
    {"Code": "FR", "Name": "France"},
    {"Code": "GF", "Name": "French Guiana"},
    {"Code": "PF", "Name": "French Polynesia"},
    {"Code": "TF", "Name": "French Southern Territories"},
    {"Code": "GA", "Name": "Gabon"},
    {"Code": "GM", "Name": "Gambia"},
    {"Code": "GE", "Name": "Georgia"},
    {"Code": "DE", "Name": "Germany"},
    {"Code": "GH", "Name": "Ghana"},
    {"Code": "GI", "Name": "Gibraltar"},
    {"Code": "GR", "Name": "Greece"},
    {"Code": "GL", "Name": "Greenland"},
    {"Code": "GD", "Name": "Grenada"},
    {"Code": "GP", "Name": "Guadeloupe"},
    {"Code": "GU", "Name": "Guam"},
    {"Code": "GT", "Name": "Guatemala"},
    {"Code": "GG", "Name": "Guernsey"},
    {"Code": "GN", "Name": "Guinea"},
    {"Code": "GW", "Name": "Guinea-Bissau"},
    {"Code": "GY", "Name": "Guyana"},
    {"Code": "HT", "Name": "Haiti"},
    {"Code": "HM", "Name": "Heard Island and McDonald Islands"},
    {"Code": "VA", "Name": "Holy See (Vatican City State)"},
    {"Code": "HN", "Name": "Honduras"},
    {"Code": "HK", "Name": "Hong Kong"},
    {"Code": "HU", "Name": "Hungary"},
    {"Code": "IS", "Name": "Iceland"},
    {"Code": "IN", "Name": "India"},
    {"Code": "ID", "Name": "Indonesia"},
    {"Code": "IR", "Name": "Iran, Islamic Republic of"},
    {"Code": "IQ", "Name": "Iraq"},
    {"Code": "IE", "Name": "Ireland"},
    {"Code": "IM", "Name": "Isle of Man"},
    {"Code": "IL", "Name": "Israel"},
    {"Code": "IT", "Name": "Italy"},
    {"Code": "JM", "Name": "Jamaica"},
    {"Code": "JP", "Name": "Japan"},
    {"Code": "JE", "Name": "Jersey"},
    {"Code": "JO", "Name": "Jordan"},
    {"Code": "KZ", "Name": "Kazakhstan"},
    {"Code": "KE", "Name": "Kenya"},
    {"Code": "KI", "Name": "Kiribati"},
    {"Code": "KP", "Name": "Korea, Democratic People's Republic of"},
    {"Code": "KR", "Name": "Korea, Republic of"},
    {"Code": "KW", "Name": "Kuwait"},
    {"Code": "KG", "Name": "Kyrgyzstan"},
    {"Code": "LA", "Name": "Lao People's Democratic Republic"},
    {"Code": "LV", "Name": "Latvia"},
    {"Code": "LB", "Name": "Lebanon"},
    {"Code": "LS", "Name": "Lesotho"},
    {"Code": "LR", "Name": "Liberia"},
    {"Code": "LY", "Name": "Libya"},
    {"Code": "LI", "Name": "Liechtenstein"},
    {"Code": "LT", "Name": "Lithuania"},
    {"Code": "LU", "Name": "Luxembourg"},
    {"Code": "MO", "Name": "Macao"},
    {"Code": "MK", "Name": "Macedonia, the Former Yugoslav Republic of"},
    {"Code": "MG", "Name": "Madagascar"},
    {"Code": "MW", "Name": "Malawi"},
    {"Code": "MY", "Name": "Malaysia"},
    {"Code": "MV", "Name": "Maldives"},
    {"Code": "ML", "Name": "Mali"},
    {"Code": "MT", "Name": "Malta"},
    {"Code": "MH", "Name": "Marshall Islands"},
    {"Code": "MQ", "Name": "Martinique"},
    {"Code": "MR", "Name": "Mauritania"},
    {"Code": "MU", "Name": "Mauritius"},
    {"Code": "YT", "Name": "Mayotte"},
    {"Code": "MX", "Name": "Mexico"},
    {"Code": "FM", "Name": "Micronesia, Federated States of"},
    {"Code": "MD", "Name": "Moldova, Republic of"},
    {"Code": "MC", "Name": "Monaco"},
    {"Code": "MN", "Name": "Mongolia"},
    {"Code": "ME", "Name": "Montenegro"},
    {"Code": "MS", "Name": "Montserrat"},
    {"Code": "MA", "Name": "Morocco"},
    {"Code": "MZ", "Name": "Mozambique"},
    {"Code": "MM", "Name": "Myanmar"},
    {"Code": "NA", "Name": "Namibia"},
    {"Code": "NR", "Name": "Nauru"},
    {"Code": "NP", "Name": "Nepal"},
    {"Code": "NL", "Name": "Netherlands"},
    {"Code": "NC", "Name": "New Caledonia"},
    {"Code": "NZ", "Name": "New Zealand"},
    {"Code": "NI", "Name": "Nicaragua"},
    {"Code": "NE", "Name": "Niger"},
    {"Code": "NG", "Name": "Nigeria"},
    {"Code": "NU", "Name": "Niue"},
    {"Code": "NF", "Name": "Norfolk Island"},
    {"Code": "MP", "Name": "Northern Mariana Islands"},
    {"Code": "NO", "Name": "Norway"},
    {"Code": "OM", "Name": "Oman"},
    {"Code": "PK", "Name": "Pakistan"},
    {"Code": "PW", "Name": "Palau"},
    {"Code": "PS", "Name": "Palestine, State of"},
    {"Code": "PA", "Name": "Panama"},
    {"Code": "PG", "Name": "Papua New Guinea"},
    {"Code": "PY", "Name": "Paraguay"},
    {"Code": "PE", "Name": "Peru"},
    {"Code": "PH", "Name": "Philippines"},
    {"Code": "PN", "Name": "Pitcairn"},
    {"Code": "PL", "Name": "Poland"},
    {"Code": "PT", "Name": "Portugal"},
    {"Code": "PR", "Name": "Puerto Rico"},
    {"Code": "QA", "Name": "Qatar"},
    {"Code": "RE", "Name": "R\u00e9union"},
    {"Code": "RO", "Name": "Romania"},
    {"Code": "RU", "Name": "Russian Federation"},
    {"Code": "RW", "Name": "Rwanda"},
    {"Code": "BL", "Name": "Saint Barth\u00e9lemy"},
    {"Code": "SH", "Name": "Saint Helena, Ascension and Tristan da Cunha"},
    {"Code": "KN", "Name": "Saint Kitts and Nevis"},
    {"Code": "LC", "Name": "Saint Lucia"},
    {"Code": "MF", "Name": "Saint Martin (French part)"},
    {"Code": "PM", "Name": "Saint Pierre and Miquelon"},
    {"Code": "VC", "Name": "Saint Vincent and the Grenadines"},
    {"Code": "WS", "Name": "Samoa"},
    {"Code": "SM", "Name": "San Marino"},
    {"Code": "ST", "Name": "Sao Tome and Principe"},
    {"Code": "SA", "Name": "Saudi Arabia"},
    {"Code": "SN", "Name": "Senegal"},
    {"Code": "RS", "Name": "Serbia"},
    {"Code": "SC", "Name": "Seychelles"},
    {"Code": "SL", "Name": "Sierra Leone"},
    {"Code": "SG", "Name": "Singapore"},
    {"Code": "SX", "Name": "Sint Maarten (Dutch part)"},
    {"Code": "SK", "Name": "Slovakia"},
    {"Code": "SI", "Name": "Slovenia"},
    {"Code": "SB", "Name": "Solomon Islands"},
    {"Code": "SO", "Name": "Somalia"},
    {"Code": "ZA", "Name": "South Africa"},
    {"Code": "GS", "Name": "South Georgia and the South Sandwich Islands"},
    {"Code": "SS", "Name": "South Sudan"},
    {"Code": "ES", "Name": "Spain"},
    {"Code": "LK", "Name": "Sri Lanka"},
    {"Code": "SD", "Name": "Sudan"},
    {"Code": "SR", "Name": "Suriname"},
    {"Code": "SJ", "Name": "Svalbard and Jan Mayen"},
    {"Code": "SZ", "Name": "Swaziland"},
    {"Code": "SE", "Name": "Sweden"},
    {"Code": "CH", "Name": "Switzerland"},
    {"Code": "SY", "Name": "Syrian Arab Republic"},
    {"Code": "TW", "Name": "Taiwan, Province of China"},
    {"Code": "TJ", "Name": "Tajikistan"},
    {"Code": "TZ", "Name": "Tanzania, United Republic of"},
    {"Code": "TH", "Name": "Thailand"},
    {"Code": "TL", "Name": "Timor-Leste"},
    {"Code": "TG", "Name": "Togo"},
    {"Code": "TK", "Name": "Tokelau"},
    {"Code": "TO", "Name": "Tonga"},
    {"Code": "TT", "Name": "Trinidad and Tobago"},
    {"Code": "TN", "Name": "Tunisia"},
    {"Code": "TR", "Name": "Turkey"},
    {"Code": "TM", "Name": "Turkmenistan"},
    {"Code": "TC", "Name": "Turks and Caicos Islands"},
    {"Code": "TV", "Name": "Tuvalu"},
    {"Code": "UG", "Name": "Uganda"},
    {"Code": "UA", "Name": "Ukraine"},
    {"Code": "AE", "Name": "United Arab Emirates"},
    {"Code": "GB", "Name": "United Kingdom"},
    {"Code": "US", "Name": "United States"},
    {"Code": "UM", "Name": "United States Minor Outlying Islands"},
    {"Code": "UY", "Name": "Uruguay"},
    {"Code": "UZ", "Name": "Uzbekistan"},
    {"Code": "VU", "Name": "Vanuatu"},
    {"Code": "VE", "Name": "Venezuela, Bolivarian Republic of"},
    {"Code": "VN", "Name": "Viet Nam"},
    {"Code": "VG", "Name": "Virgin Islands, British"},
    {"Code": "VI", "Name": "Virgin Islands, U.S."},
    {"Code": "WF", "Name": "Wallis and Futuna"},
    {"Code": "EH", "Name": "Western Sahara"},
    {"Code": "YE", "Name": "Yemen"},
    {"Code": "ZM", "Name": "Zambia"},
    {"Code": "ZW", "Name": "Zimbabwe"}
];


// Proxy WMS server
OceanBrowser.WMSProxy = null;
OceanBrowser.VERSION = '1.9.0';


OceanBrowser.mimetypes = {
    'application/x-shapefile+zip': 'compressed Shapefile',
    'image/png': 'PNG',
    'text/html': 'HTML',
    'text/xml': 'XML',
    'application/vnd.google-earth.kmz': 'KML',
    'application/pdf': 'PDF',
    'image/eps': 'EPS',
    'image/svg+xml': 'SVG',
    'video/webm': 'WebM (animation)',
    'video/mp4': 'MP4 (animation)'
};

OceanBrowser.UI = {};

OceanBrowser.UI.makedom = function(tag,attr,children) {
    var elem, i;
    attr = attr || {};
    children = children || {};

    elem = $('<' + tag + '></' + tag + '>');
    elem.attr(attr);

    for (i=0; i < children.length; i++) {
        elem.append(children[i]);
    }
    return elem.get()[0];
};

// very simple template function
OceanBrowser.render = function(template,map) {
    var s = template;
    for (var key in map) {
        if (map.hasOwnProperty(key)) {
            // replace all key by map[k]
            // http://stackoverflow.com/questions/1144783/replacing-all-occurrences-of-a-string-in-javascript
            s = s.split('{{' + key + '}}').join(map[key]);
        }
    }
    return s;
};


OceanBrowser.colormaps =
    {'jet':
     [
         [0.000000000000000,0.000000000000000,0.500000000000000],
         [0.000000000000000,0.000000000000000,0.563492063492063],
         [0.000000000000000,0.000000000000000,0.626984126984127],
         [0.000000000000000,0.000000000000000,0.690476190476190],
         [0.000000000000000,0.000000000000000,0.753968253968254],
         [0.000000000000000,0.000000000000000,0.817460317460317],
         [0.000000000000000,0.000000000000000,0.880952380952381],
         [0.000000000000000,0.000000000000000,0.944444444444444],
         [0.000000000000000,0.007936507936508,1.000000000000000],
         [0.000000000000000,0.071428571428571,1.000000000000000],
         [0.000000000000000,0.134920634920635,1.000000000000000],
         [0.000000000000000,0.198412698412698,1.000000000000000],
         [0.000000000000000,0.261904761904762,1.000000000000000],
         [0.000000000000000,0.325396825396825,1.000000000000000],
         [0.000000000000000,0.388888888888889,1.000000000000000],
         [0.000000000000000,0.452380952380952,1.000000000000000],
         [0.000000000000000,0.515873015873016,1.000000000000000],
         [0.000000000000000,0.579365079365079,1.000000000000000],
         [0.000000000000000,0.642857142857143,1.000000000000000],
         [0.000000000000000,0.706349206349206,1.000000000000000],
         [0.000000000000000,0.769841269841270,1.000000000000000],
         [0.000000000000000,0.833333333333333,1.000000000000000],
         [0.000000000000000,0.896825396825397,1.000000000000000],
         [0.000000000000000,0.960317460317460,1.000000000000000],
         [0.023809523809524,1.000000000000000,0.976190476190476],
         [0.087301587301587,1.000000000000000,0.912698412698413],
         [0.150793650793651,1.000000000000000,0.849206349206349],
         [0.214285714285714,1.000000000000000,0.785714285714286],
         [0.277777777777778,1.000000000000000,0.722222222222222],
         [0.341269841269841,1.000000000000000,0.658730158730159],
         [0.404761904761905,1.000000000000000,0.595238095238095],
         [0.468253968253968,1.000000000000000,0.531746031746032],
         [0.531746031746032,1.000000000000000,0.468253968253968],
         [0.595238095238095,1.000000000000000,0.404761904761905],
         [0.658730158730159,1.000000000000000,0.341269841269841],
         [0.722222222222222,1.000000000000000,0.277777777777778],
         [0.785714285714286,1.000000000000000,0.214285714285714],
         [0.849206349206349,1.000000000000000,0.150793650793651],
         [0.912698412698413,1.000000000000000,0.087301587301587],
         [0.976190476190476,1.000000000000000,0.023809523809524],
         [1.000000000000000,0.960317460317460,0.000000000000000],
         [1.000000000000000,0.896825396825397,0.000000000000000],
         [1.000000000000000,0.833333333333333,0.000000000000000],
         [1.000000000000000,0.769841269841270,0.000000000000000],
         [1.000000000000000,0.706349206349207,0.000000000000000],
         [1.000000000000000,0.642857142857143,0.000000000000000],
         [1.000000000000000,0.579365079365080,0.000000000000000],
         [1.000000000000000,0.515873015873016,0.000000000000000],
         [1.000000000000000,0.452380952380953,0.000000000000000],
         [1.000000000000000,0.388888888888889,0.000000000000000],
         [1.000000000000000,0.325396825396826,0.000000000000000],
         [1.000000000000000,0.261904761904762,0.000000000000000],
         [1.000000000000000,0.198412698412699,0.000000000000000],
         [1.000000000000000,0.134920634920635,0.000000000000000],
         [1.000000000000000,0.071428571428572,0.000000000000000],
         [1.000000000000000,0.007936507936508,0.000000000000000],
         [0.944444444444445,0.000000000000000,0.000000000000000],
         [0.880952380952381,0.000000000000000,0.000000000000000],
         [0.817460317460318,0.000000000000000,0.000000000000000],
         [0.753968253968254,0.000000000000000,0.000000000000000],
         [0.690476190476191,0.000000000000000,0.000000000000000],
         [0.626984126984127,0.000000000000000,0.000000000000000],
         [0.563492063492064,0.000000000000000,0.000000000000000],
         [0.500000000000000,0.000000000000000,0.000000000000000]]
    };


OceanBrowser.UI.BaseDialog = EarthGL.Class({
    events: null,
    div: null,
    id: null,

    initialize: function(divid,title) {
        this.events = new EarthGL.Events(this);
        this.id = divid;
        this.div = document.createElement('div');
        this.div.id = divid;
        this.div.title = title;
        this.div.className = "OceanBrowserDialog";
        document.body.appendChild(this.div);
    },

    open: function() {
        $(this.div).dialog('open');
    },

    close: function() {
        $(this.div).dialog('close');
    }
});

OceanBrowser.UI.About = EarthGL.Class(OceanBrowser.UI.BaseDialog,{

    initialize: function(id,servers) {
        var that = this;
        OceanBrowser.UI.BaseDialog.prototype.initialize.apply(this,[id + '_about_dialog','About']);

        $(this.div).append(
            $('<div/>').attr({'style': 'text-align: center'}).append(
                $('<p/>').append(
                    $('<em/>').append('OceanBrowser'),
                    $('<span/>').append(' developped by'),
                    $('<br/>'),
                    $('<span/>').append('Alexander Barth'),
                    $('<br/>'),
                    $('<span/>').append('GHER, University of Liege, Belgium'),
                    $('<br/>'),
                    $('<span/>').append('version ' + OceanBrowser.VERSION)
                ),
                $('<div/>').append(
                    $('<a/>').attr({'href': 'http://labos.ulg.ac.be/gher/'}).append(
                        $('<img/>').attr({'src': 'img/gher-logo.png', 'alt': 'gher-logo'})
                    ),
                    $('<a/>').attr({'href': 'https://github.com/gher-ulg/DIVA'}).append(
                        $('<img/>').attr({'src': 'img/diva-logo.png', 'alt': 'diva-logo'})
                    )
                ),
                $('<p/>').append(
                    'A contribution to ',
                    $('<a/>').attr({'href': 'http://www.seadatanet.org/'}).append('SeaDataNet'),
                    ' and ',
                    $('<br/>'),
                    $('<a/>').attr({'href': 'http://www.emodnet-chemistry.eu/'}).append('EMODNET-Chemistry')
                )
            )
        );

        $(this.div).dialog(
            { autoOpen: false,
              width: 400,
              buttons: {
                  'Close': function() {
                      that.close();
                  }
              }
            });
    }
});



OceanBrowser.UI.Settings = EarthGL.Class(OceanBrowser.UI.BaseDialog,{
    initialize: function(id,servers) {
        var that = this;
        OceanBrowser.UI.BaseDialog.prototype.initialize.apply(this,[id + '_settings_dialog','Settings']);

        $(this.div).append(
            $('<div/>').attr({'id': 'settings_content'}).append(
                       $('<table/>').append(
                           $('<tbody/>').append(
                               $('<tr/>').append(
                                   $('<td/>').append(
                                       $('<label/>').attr({'for': 'max_wfs_features'}).append(
                                           'Maximum individual data records'
                                       )
                                   ),
                                   $('<td/>').append(
                                       $('<input/>').attr({'type': 'text', 'id': 'max_wfs_features', 'value': '10'})
                                   )
                               )
                           )
                       )
                   )
        );

        $(this.div).dialog({
            autoOpen: false,
            width: 400,
            buttons: {
                'Close': function() {
                    that.close();
                }
            }
        });
    }
});

OceanBrowser.UI.chooseServer = EarthGL.Class(OceanBrowser.UI.BaseDialog,{

    initialize: function(id,servers) {
        OceanBrowser.UI.BaseDialog.prototype.initialize.apply(this,[id + '_add_server_dialog','Add server']);
        var that = this;

        // better clone
        this.servers = [{name: 'new',
                         title: 'New Server',
                         url: '',
                         version: '1.3.0',
                         extension: 'none' }].concat(servers);

        // HTML elements for callback
        this.elem = {};

        $(this.div).append(
            $('<p/>').attr({'class': 'instruction'}).append(
                'Choose either a server from the server list or enter the URL of a WMS server directly.'
            ),
            $('<table/>').append(
                $('<tbody/>').append(
                    $('<tr/>').append(
                        $('<td/>').append('Server:'),
                        $('<td/>').append(
                            this.elem.list = $('<select/>').css({"max-width": "380px"})
                        )
                    ),
                    $('<tr/>').append(
                        $('<td/>').append('URL:'),
                        $('<td/>').append(
                            this.elem.url = $('<input/>').attr({'type': 'text', 'size': '40'})
                        )
                    ),
                    $('<tr/>').append(
                        $('<td/>').append('Server:'),
                        $('<td/>').append(
                            this.elem.version = $('<select/>').append(
                                $('<option/>').append('WMS 1.3.0'),
                                $('<option/>').append('WMS 1.1.1'),
                                $('<option/>').append('WFS 1.0.0')
                            )
                        )
                    )
                )
            )
        )

        // contruct list of servers

        for (var i = 0; i < this.servers.length; i++) {

            if (this.servers[i].hide !== true) {
                this.elem.list.append(
                    $('<option/>').attr('value', '' + i).append(this.servers[i].title)
                );
            }
        }


        $(this.div).dialog({ autoOpen: false,
                             width: 500,
                             buttons: {
                                 'Close': function() {
                                     that.close();
                                 },
                                 'Add': function() {
                                     that.add();
                                     that.close();
                                 }
                             }
                           });


        this.elem.list.change(function() {
            var i = parseInt($(this).val(),10);

            that.elem.url.val(that.servers[i].url);
            that.elem.version.val(that.servers[i].service + ' ' + that.servers[i].version);
        });

    },

    add: function() {
        var i = parseInt(this.elem.list.val(),10);
        var version_type = this.elem.version.val().split(' ');

        /*
        var server = {
            url: this.elem.url.val().trim(),
            service: version_type[0],
            version: version_type[1],
            extension: this.servers[i].extension
        };
        */

        // clone entry
        var server = jQuery.extend({},
           this.servers[i],
           {
              url: this.elem.url.val().trim(),
              service: version_type[0],
              version: version_type[1],
           });

        this.events.triggerEvent('serverSelected',server);
    }


});

OceanBrowser.UI.linkify = function(inputText) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;

    //URLs starting with http://, https://, or ftp://
    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

    //Change email addresses to mailto:: links.
    replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

    return replacedText;
};


OceanBrowser.UI.InfoLayer = EarthGL.Class(OceanBrowser.UI.BaseDialog,{

    initialize: function(id,layer) {
        OceanBrowser.UI.BaseDialog.prototype.initialize.apply(this,[id + '_info_layer_dialog','Info Layer']);

        var that = this,
        attr, bbox, $prod, dim, preview;

        function make_table(name,elem) {
            var i, $tbody2;

            if (name) {
                $(that.div).append($('<h4/>').html(name));
            }

            $tbody2 = $('<tbody/>');

            for (i=0; i<elem.length; i++) {
                $tbody2.append($('<tr/>')
                               .append($('<td/>').attr({width: '100'}).append(elem[i][0]))
                               .append($('<td/>').append(elem[i][1]))
                              );
            }

            $(that.div).append($('<table/>').attr({width: '100%'}).addClass('tableInfo').append($tbody2));
        }


        function make_link(url) {
            return $('<a/>').attr({'href': url,'target': '_blank'}).append(url);
        }


        attr = layer.get_attribution() || {};
        $prod = $('<span/>');

        if ('title' in attr) {
            if ('url' in attr) {
                $prod = $('<a/>').attr('href',attr.url).html(attr.title);

            }
            else {
                $prod = $('<span/>').html(attr.title);
            }

            if ('logo' in attr) {
                $prod.append($('<img/>').attr({'src': attr.logo,'class': 'attribution_logo'}));
            }
        }

        bbox = layer.get_bounding_box();

        /*
            var params = {
                    'layers': layer.name,
                    'service': 'WMS',
                    'request': 'GetMap',
		    'service': "WMS",
                    'version': layer.cap.version,
                    'exceptions': 'application/vnd.ogc.se_inimage',
                    'format': 'image/png',
                    'bbox': bbox.join(','),
                    'width': 128,
                    'height': 128
                }

            var styles = layer.get_styles()
            var $legend = $('<span/>');
            if (styles.length > 0) {
                $legend = $('<img/>').attr({'src': styles[0].legend_url,'alt': 'legend'});
            }

            preview = Util.append_param(layer.wms_url,params);
            params.layers = baselayer_name;
            var preview_background = Util.append_param(baselayer_url,params);


            var $preview =  $('<div/>')
               .append($('<span/>').css({'position': 'relative',
                                            'height': params['height'],
                                            'width': params['width']})
                   .append($('<img/>')
                           .css({'position': 'absolute'})
                           .attr({'src': preview_background,'alt': 'preview'})
                           )
                   .append($('<img/>')
                           .css({'position': 'absolute'})
                           .attr({'src': preview,'alt': 'preview'})
                           ));

            if (styles.length > 0) {
                $preview.append($('<img/>').attr({'src': styles[0].legend_url,'alt': 'legend'}));
            }
            */

        var datatable, ids = layer.get_ids(), i;


        datatable = [['Title',layer.title],
                     ['Producer',$prod]];

        if (layer.metadata_url) {
            datatable.push(['Metadata',
                            $('<a/>').attr({'href': layer.metadata_url,'target': '_blank'}).append('Data products catalogue entry')]);
        }

        for (i = 0; i < ids.length; i++) {
            datatable.push(['Identifier (' + ids[i].authority + ')',
                            ids[i].id]);
        }

        if (layer.abstract_info) {
            var abs = OceanBrowser.UI.linkify(layer.abstract_info.replace(/\n/g,'<br/>'));
            datatable.push(['Abstract',abs]);
        }


        /*
        datatable.push([
            ['Preview',$preview],
            ['Legend',$legend]
        ]);
        */

        make_table('Data',datatable);


        // Bounding box

        make_table('Bounding box',[['Northern limit',bbox[3]],
                                   ['Southern limit',bbox[1]],
                                   ['Western limit',bbox[0]],
                                   ['Eastern limit',bbox[2]]]);

        // Dimensions

        dim = layer.get_dimension('time');
        if (dim) {
            var s;
            if (dim.range.min !== undefined) {
                s = 'minimum: ' + dim.range.min + ' maximum:' + dim.range.max;
            }
            if (dim.range.upper !== undefined) {
                s = 'range';
            }
            else {
                s = dim.range.join(', ');
            }

            make_table('Time',[['Time units',dim.units],
                               ['Time range',s]]);
        }

        dim = layer.get_dimension('elevation');
        if (dim) {
            var s;
            if (dim.range.min !== undefined) {
                s = 'minimum: ' + dim.range.min + ' maximum:' + dim.range.max;
            }
            if (dim.range.upper !== undefined) {
                s = 'range';
            }
            else {
                s = dim.range.join(', ');
            }

            make_table('Depth',[['Depth units',dim.units],
                                ['Depth range',s]]);
        }
        // Download
        make_table('Data Access',[['NetCDF file', that.nclink = make_link(layer.http_url)],
                                  ['OPeNDAP', that.opendaplink = make_link(layer.opendap_url)]
                                  ]);


        make_table('Web Map Service',[['Server',layer.wms_url],
                                      ['Name',layer.name],
                                      ['Version',layer.cap.version]]);

        $(that.nclink).click(function() {
            _paq.push(['trackEvent', 'Download', 'Layer', layer.describe()]);
            //_paq.push(['trackEvent', 'Download', 'UUID', Layer.get_uuid()]);
        });

        $(that.opendaplink).click(function() {
            _paq.push(['trackEvent', 'Download', 'Layer', layer.describe()]);
            //_paq.push(['trackEvent', 'Download', 'UUID', Layer.get_uuid()]);
        });

        $(this.div).dialog(
            { autoOpen: false,
              width: 700,
              height: 600,
              buttons: {
                  'Close': function() {
                      that.close();
                  }
              }
            });
    }
});




OceanBrowser.UI.chooseLayer = EarthGL.Class(OceanBrowser.UI.BaseDialog,{

    initialize: function(id,servers,options) {
        OceanBrowser.UI.BaseDialog.prototype.initialize.apply(this,[id + 'add_layer_dialog','Select data products']);
        this.id = id;
        this.caps = [];
        this.showSectionType = true;
        this.showPreview = true;
        this.baselayer = null;

        // overwrite default options
        if (options) {
            for (var p in options) {
                this[p] = options[p];
            }
        }

        this.elem = {};

        var that = this;
        $(this.div).attr({'class': 'OceanBrowserUIchooseLayerWithSearch'});

        $(this.div).append(
            'Search:',
            $('<input/>').attr({
                id: id + '_search',
                type: "text",
                size: "30",
                title: 'Enter search keywords'}),
            $('<button/>').attr({
                id: id + '_add_server_button',
                style: "float: right",
                title: 'Import layers from other servers supporting the Web Map Service protocol'
                }).append('Add external layers'),
            $('<fieldset/>').attr({'class': 'OceanBrowserChooseLayer'}).append(
                $('<legend/>').append('Layer'),
                $('<div/>').attr({id: id + '_search_results','class': "OceanBrowserSearchResult"}),
                $('<div/>').attr({id: id + '_chooser'})));


        if (this.showSectionType) {
            $(this.div).append(
                $('<fieldset/>').attr({'class': 'OceanBrowserUISectionType'}).append(
                    $('<legend/>').append('Section type'),
                    $('<p/>').append(
                        $('<label/>').append(
                            $('<input/>').attr({'checked': 'checked',
                                                'type': 'radio',
                                                'id': id + '_horizontal_section',
                                                'value': 'horizontal',
                                                'name': 'section_type'}),
                            'Horizontal'
                        )
                    ),
                    $('<p/>').append(
                        $('<label/>').append(
                            $('<input/>').attr({'type': 'radio',
                                                'id': id + '_vertical_section',
                                                'value': 'vertical',
                                                'name': 'section_type'}),
                            'Vertical  (draw a vertical section with the mouse in the preview panel)'
                        )
                    )
                )
            );

            this.elem.hs = document.getElementById(id + '_horizontal_section');
            this.elem.vs = document.getElementById(id + '_vertical_section');

        }

        if (this.showPreview) {
            $(this.div).append(
                $('<fieldset/>').attr({'class': 'OceanBrowserPreview'}).append(
                    $('<legend/>').append('Preview'),
                    $('<div/>').attr({'style': 'width: 250px; height: 125px', 'id': id + '_vert_map_selection'})
                ));
        }


        if (!this.showPreview && !this.showSectionType) {
            $(this.div).find('.OceanBrowserChooseLayer').css('height',295);
            $(this.div).find('.OceanBrowserChooseLayer div').css('height',280);
        }


        var diagOptions = { autoOpen: false,
                            width: 700,
                            height: 480,
                            zIndex: 3999,
                            buttons: {
                                "Cancel": function() {
                                    that.close();
                                },
                                "Add layer": function() {
                                    that.close();
                                    that.getSectionObj();
                                }
                            }
                          };
        if ($(window).width() < 700) {
            diagOptions.width = Math.min($(window).width(),700);
            diagOptions.height = Math.min($(window).height(),700);
            diagOptions.draggable = false;
            diagOptions.resizable = false;
            diagOptions.position = { my: "top left", at: "top left"};
        }

        $(this.div).dialog(diagOptions);

        // load servers with 'autoload'

        for (var i in servers) {
            if (servers[i].autoload) {
                //console.log('load',servers[i].name);
                this.loadServer(servers[i]);
            }
        }

        this.chooseServer = new OceanBrowser.UI.chooseServer(id + 'chooseServer',servers);
        this.chooseServer.events.register('serverSelected', this, this.loadServer);

        // HTML elements for callback
        this.elem.server = document.getElementById(id + '_add_server_button');
        this.elem.name = document.getElementById(id + '_wms_server_name');
        this.elem.chooser = document.getElementById(id + '_chooser');
        this.elem.search = document.getElementById(id + '_search');
        this.elem.searchres = document.getElementById(id + '_search_results');

        $(this.elem.searchres).hide();
        $(this.elem.chooser).show();

        //$(this.elem.search).keyup(function(e) {
        $('#add_Layer_search').keyup(function(e) {
            e.preventDefault();
            that.processQuery($(this).val());
        });

        $('#add_Layer_add_server_button').click(function() {
        //$(this.elem.server).click(function() {
            that.chooseServer.open();
        });


        if (this.showPreview) {
            this.map_selection = new OpenLayers.Map(id + "_vert_map_selection",
                                                    {maxResolution: 180/128,
                                                     maxExtent: new OpenLayers.Bounds(-180, -90, 180, 90) ,
                                                     theme: null
                                                    });

            var bloptions = {
                resolutions: [1.40625,
                              0.703125,
                              0.3515625,
                              0.17578125,
                              0.087890625,
                              0.0439453125,
                              0.02197265625,
                              0.010986328125,
                              0.0054931640625,
                              0.00274658203125,
                              0.00137329101]
            };


            if (this.baselayer) {
                this.mini_baselayer_wms = new OpenLayers.Layer.WMS("Continents",this.baselayer.url,
                                                                   {layers: this.baselayer.name,
                                                                    format: this.baselayer.format},
                                                                   bloptions);
                this.map_selection.addLayer(this.mini_baselayer_wms);
            }

            var style = OpenLayers.Util.applyDefaults({strokeWidth: 2, strokeColor: "black"},
                                                      OpenLayers.Feature.Vector.style["default"]);



            this.vectors = new OpenLayers.Layer.Vector("Vector Layer", {style: style});
            this.control = new OpenLayers.Control.DrawFeature(this.vectors,OpenLayers.Handler.Path, {handlerOptions: {style: style}});

            this.map_selection.addLayer(this.vectors);
            this.vectors.setZIndex(this.map_selection.Z_INDEX_BASE.Feature);

            this.map_selection.addControl(this.control);
        }


        if (this.showSectionType) {
            $(this.elem.hs).click(function() {
                that.control.deactivate();
            });

            $(this.elem.vs).click(function() {
                that.control.activate();
                // for some reasons "control.deactivate" demotes the ZIndex for the vector layer
                that.vectors.setZIndex(that.map_selection.Z_INDEX_BASE.Feature);
                that.control.handler.layer.setZIndex(that.map_selection.Z_INDEX_BASE.Feature+1);
            });
        }

    },


    loadServer: function(server) {
        var that = this;
        var options = jQuery.extend(
            {
                namespace: 'layer_list_' + server.url + '_',
                use_proxy: (server.url.indexOf('http://') === 0 ? OceanBrowser.WMSProxy : null),
                WMS_extensions: server.extension,
                choose_multiple: false,
                removeButton: true,
                show_title: true
            },server);

        var cap = new OceanBrowser.UI.LayerTree(this.id + "_chooser",Util.make_https(server.url),options);
        cap.events.register('layerSelected', this, this.fieldSelected);
        cap.events.register('serverRemoved', this, function (c) {
            this.caps.splice(this.caps.indexOf(c),1);
        });

        cap.load();
        this.caps.push(cap);
    },

    fieldSelected: function(field) {
        var display_param = {style: '', param: {}};

        if (this.showPreview) {
            if (this.layerWMS) {
                this.map_selection.removeLayer(this.layerWMS);
            }

            this.layerWMS = new OpenLayers.Layer.WMS(field.title,
                                                     field.wms_url,
                                                     {layers: field.name,
                                                      styles: display_param.style,
                                                      transparent: "true", format: "image/png"});

            this.layerWMS.mergeNewParams(display_param.param);
            this.map_selection.addLayer(this.layerWMS);
            this.layerWMS.setZIndex(this.map_selection.Z_INDEX_BASE.Overlay);
            this.vectors.setZIndex(this.map_selection.Z_INDEX_BASE.Feature);
        }

        //this.selection = field;
        add_layer(field);

    },

    processQuery: function(q) {
        var i, j, k, results, $ul, $li,
        query,
        nres = 0,
        regexp, k,
        are = [],
        $res = $(this.elem.searchres);

        //query = {keywords: q.split(' ')};
        query = OceanBrowser.Query.parse(q);

        // delete all previsous result
        $res.empty();

        if (q ===  ''){
            $(this.elem.searchres).hide();
            $(this.elem.chooser).show();

            //$res.empty();
            return false;
        }

        $(this.elem.searchres).show();
        $(this.elem.chooser).hide();

        console.log('query new ',q);

        // make regular expression for query
        for (k in query.keywords) {
            if (query.keywords[k].length > 0) {
                are.push(Util.escapeRegExp(query.keywords[k]));
            }
        }

        if (are.length > 0) {
            regexp = new RegExp(are.join('|'),'gi');
        }
        else {
            regexp = null;
        }



        // loop over all loaded servers
        for (i = 0; i < this.caps.length; i++) {

            results = this.caps[i].search(query);
            nres += results.length;
            if (results.length > 0) {
                $res.append($('<h3/>').append(this.caps[i].title));
                $ul = $('<ul/>');

                for (j in results) {
                    var item =
                        OceanBrowser.Query.highlightQuery(results[j].FullPath(),regexp)
                        .join(' <span class="separator">&#9656;</span> ');

                    var index = this.caps[i].layer_infos.indexOf(results[j]);
                    var $a = $('<a/>').attr({'href': '#', 'data-layer': index, 'data-server': i}).html(item);
                    $li = $('<li/>').append($a);

                    $a.click(
                        function(cap,Layer) {
                            return function(event) {
                                console.log('clicked ',Layer.title,event);

                                if (event.ctrlKey) {
                                    // add
                                }
                                else {
                                    // remove any selected
                                    $res.find('li').removeClass('OceanBrowserSelected');
                                }

                                $(this).toggleClass('OceanBrowserSelected');

                                cap.events.triggerEvent('layerSelected',Layer);
                            };
                        }(this.caps[i],results[j]));

                    $ul.append($li);
                }

                $res.append($ul);
            }
        }

        if (nres === 0) {
            $res.append('Your search does not match any layers');
        }

        return false;
    },

    getselected: function() {
        var selected = [], i, j, that = this;
        for (i = 0; i < this.caps.length; i++) {
            selected = selected.concat(this.caps[i].getselected());
        }

        $(this.elem.searchres).find('.OceanBrowserSelected').each(function() {
            i = parseInt($(this).attr('data-server'));
            j = parseInt($(this).attr('data-layer'));

            selected.push(that.caps[i].layer_infos[j]);
        });

        return selected;
    },

    getSectionObj: function() {
        var constraints = {}, i;

        var selected = this.getselected();

        //if (!this.selection) {
        if (selected.length === 0) {
            // nothing to do
            return;
        }


        if (this.showSectionType) {
            if (this.elem.hs.checked) {
                constraints.type = 'lon-lat';
            }
            else {
                constraints.type = 'section-depth';
                this.vectors.setZIndex(this.map_selection.Z_INDEX_BASE.Feature);
                var c = this.vectors.features[this.vectors.features.length-1].geometry.components;
                constraints.lon = [];
                constraints.lat = [];

                for (var i=0; i < c.length; i++) {
                    constraints.lon.push(c[i].x);
                    constraints.lat.push(c[i].y);
                }
            }
        }

/*
        this.events.triggerEvent('layerSelected',{field: this.selection,
                                                  constraints: constraints});
*/

        for (i = 0; i < selected.length; i++) {
            this.events.triggerEvent('layerSelected',{field: selected[i],
                                                      constraints: constraints});
        }
    },

    open: function() {
        var i;

        $(this.div).dialog('open');

        if (this.showPreview) {
            this.map_selection.zoomToMaxExtent();
        }

        // deselect all
        // loop over all loaded servers
        for (i = 0; i < this.caps.length; i++) {
            this.caps[i].deselect_all();
        }
    }
});



// form is the DOM element
// in production category should be 'Download_form'
// for testing it should be 'Download_test'
// data_url is the full URL to the NetCDF file

function DownloadForm_submit(form,category,data_url,UUID) {
    var checked = form.querySelector('input[name="reason"]:checked');
    var reason = form.querySelector('input[name="reason"]:checked').value;
    var other_reason = form.querySelector('.download_other_reason textarea').value;

    var formdata = {
        "reason": reason,
        "organisation_type": form.querySelector('input[name="organisation_type"]:checked').value,
        "data_url": data_url,
        "sender_id": "oceanbrowser",
        "UUID": UUID
    };

    if (form.organisation_name.value) {
        formdata["organisation_name"] = form.organisation_name.value;
    }

    if (form.country.value) {
        formdata["country"] = form.country.value;
    }

    if (reason === "Other") {
        formdata["other_reason"] = other_reason;
    }

    console.log(formdata,JSON.stringify(formdata));
    for (var key in formdata) {
        if (formdata.hasOwnProperty(key)) {
            _paq.push(['trackEvent', category, "DownloadForm-" + key, formdata[key]]);
        }
    }
    _paq.push(['trackEvent', category, 'DownloadForm-jsondata', JSON.stringify(formdata)]);

    var reason_code = {
        "Commercial/Industry": 1,
        "Education/Research": 2,
        "Personal use": 3,
        "Policy making": 4,
        "Other": 5
    };

    var org_code = {
        "Academia/Research": 1,
        "Government/Public Administration": 2,
        "Business and Private Company": 3,
        "NGOs/Civil Society": 4,
        "Other": 5
    };

    var formdata_ogs = {
        "api": "2dbSmaEG4Ac27SYT",
        "country": form.country.value,
        "purpose" : reason_code[reason],
        "org_type": org_code[form.querySelector('input[name="organisation_type"]:checked').value],
        "uuid": UUID,
        "service": 1
    };

    if (form.organisation_name.value) {
        formdata_ogs["org_name"] = form.organisation_name.value;
    }

    if (form.email.value) {
        formdata_ogs["email"] = form.email.value;
    }

    if (reason === "Other") {
        formdata_ogs["pur_other"] = other_reason;
    }

    console.log(formdata_ogs,JSON.stringify(formdata_ogs));
    Util.ajax("https://www.emodnet-chemistry.eu/extranet/analytics",formdata_ogs,function() {
        console.log("submitted");
    });

    // prevent the default form behavior
    return true;
}



// form is the DOM element
function DownloadForm_setup(form) {

    form.innerHTML = ' \
      <label>Please share with us the reason for downloading this data.</label><br> \
       \
      <table class="OceanBrowserDownloadForm"> \
        <tr> \
          <td> \
            <label>Purpose*</label> \
          </td> \
          <td> \
            <label><input type="radio" name="reason" value="Commercial/Industry" required>Commercial/industry</label><br> \
            <label><input type="radio" name="reason" value="Education/Research">Education/research</label><br> \
            <label><input type="radio" name="reason" value="Personal use">Personal use</label><br> \
            <label><input type="radio" name="reason" value="Policy making">Policy making</label><br> \
            <label><input type="radio" name="reason" value="Other">Other</label><br> \
            <div id="download_other_reason" class="download_other_reason" style="display:none"> \
              Please specify below:<br> \
              <textarea id="download_other_reason_textarea" name="oreason" maxlength="200"></textarea> \
            </div> \
          </td> \
        </tr> \
         \
        <tr> \
          <td> \
            <label>Organisation name (optional)</label> \
          </td> \
          <td> \
            <input type="text" name="organisation_name" /> \
          </td> \
        </tr> \
        <tr> \
          <td> \
            <label >Organisation type*</label> \
          </td> \
          <td> \
             \
            <label><input type="radio" name="organisation_type" value="Academia/Research" required >Academia/Research</label><br> \
            <label><input type="radio" name="organisation_type" value="Government/Public Administration" required >Government/Public Administration</label><br> \
            <label><input type="radio" name="organisation_type" value="Business and Private Company" required >Business and Private Company</label><br> \
            <label><input type="radio" name="organisation_type" value="NGOs/Civil Society" required >NGOs/Civil Society</label><br> \
            <label><input type="radio" name="organisation_type" value="Other" required >Others</label><br> \
          </td> \
        </tr> \
        <tr> \
          <td> \
            <label>Country</label> \
          </td> \
          <td> \
            <select  name="country" /> \
          </td> \
        </tr> \
        <tr> \
          <td> \
            <label>Email (optional)</label> \
          </td> \
          <td> \
            <input type="text" name="email" /> \
          </td> \
        </tr> \
      </table> \
      <div>*Denotes a required field.</div> \
      <input type="submit" value="Submit and access data"/>';


    var select = form.querySelectorAll("select[name='country']")[0];
    for (var i = 0; i < COUNTRY_LIST.length; i++) {
	select.appendChild(OceanBrowser.UI.makedom("option",{"value": COUNTRY_LIST[i].Code},[COUNTRY_LIST[i].Name]));
    }

    var inputs = form.querySelectorAll("input[name='reason']");

    for (var i = 0; i < inputs.length; i++) {
        var elem = inputs[i];
        elem.addEventListener("change", function(ev) {
            var other = form.querySelector("input[value='Other']");
            var elem = form.querySelector('.download_other_reason');
            var elem2 = form.querySelector('.download_other_reason textarea');

            if (other.checked) {
                elem2.setAttribute("required","required");
                elem.style.display = "block";
            }
            else {
                elem2.removeAttribute("required");
                elem.style.display = "none";
            }
        });
    }
}


OceanBrowser.UI.LayerSimpleDownload = EarthGL.Class(OceanBrowser.UI.BaseDialog,{
    initialize: function(id,layer) {
        var that = this, dom = OceanBrowser.UI.makedom, i;
        this.layer = layer;
        this.download_form = (layer.cap || {}).download_form;

        OceanBrowser.UI.BaseDialog.prototype.initialize.apply(this,[id + 'simple_download_dialog','Download layer']);

        this.category = 'Download_form';

        this.reasonform = document.createElement("form");
        DownloadForm_setup(this.reasonform);

        var that = this;

        this.reasonform.addEventListener("submit", function(event) {
            if (event.preventDefault) {
                event.preventDefault();
            }

            var url = that.format.value;

            var success = DownloadForm_submit(this,that.category,url,that.layer.get_uuid());
            if (success) {
                $(that.reasonform).hide();
                $(that.downloadui).show();
            }

            return false;
        });


        if (this.download_form) {
            this.div.appendChild(this.reasonform);
        }
        this.downloadui = document.createElement("div");
        this.div.appendChild(this.downloadui);

        // DOM tree for Download Dialog
        $(this.downloadui).append(
            dom('div',{'class': 'OceanBrowserDownloadDialog'},[
                dom('h4',{},[
                    'Data products:'
                ]),
                "Format: ",
                this.format = dom('select',{'id': this.id + 'select_format'}),
                this.NetCDFButton = dom('input',{'type': 'button', 'value': 'Download','class': 'OceanBrowserDownloadButton'}),
                dom('br'),
                this.info = dom("span",{}),
                dom('br')
            ])
        );

        // add format list
        for (var i=0; i < layer.data_url.length; i++) {
            var format_name = OceanBrowser.mimetypes[layer.data_url[i].format] || layer.data_url[i].format;

            $(this.format).append(dom('option',{'value': layer.data_url[i].url},format_name));

            if (layer.data_url[i].format == 'application/x-shapefile+zip') {
                $(this.info).append(
                    " More information about the ",
                    dom('a',{
                        'href': 'https://en.wikipedia.org/wiki/Shapefile',
                        'style': 'text-decoration: underline;',
                        'target': '_blank'},'Shapefile'),
                    " format. ");
            }
        }

        if (layer.data_url.length === 0) {
            $(this.info).append(document.createTextNode("The WMS server does not provide any download options for this layer."));
        }

        else {
           $(this.NetCDFButton).click(function() {
              var url = that.format.value;
              _paq.push(['trackEvent', 'Download', 'Layer', layer.describe()]);
              window.open(url,'Data file');
           });
        }

        $(this.div).dialog({ autoOpen: false, width: 500});
    },

    open: function() {
        if (this.download_form) {
          $(this.reasonform).show();
          $(this.downloadui).hide();
          _paq.push(['trackEvent', this.category, 'form', 'shown']);
        }

        $(this.div).dialog('open');
    }

});


OceanBrowser.UI.LayerDownload = EarthGL.Class(OceanBrowser.UI.BaseDialog,{
    /**
 * OceanBrowser.UI.LayerDownload
 * dialog for download a layer (as data or as image)
 * @param {String} id Identifier (must be unique)
 * @param getCurrentExtent is a function which returns a structure (left, right, bottom, top)
 * @constructor
 */


    initialize: function(id,layer,getCurrentExtent,formats) {
        var that = this, dom = OceanBrowser.UI.makedom, i;
        this.layer = layer;
        this.getCurrentExtent = getCurrentExtent;
        this.download_form = (layer.cap || {}).download_form;

        formats = formats || ['image/png', 'application/vnd.google-earth.kmz', 'application/pdf', 'image/eps', 'image/svg+xml'];

        OceanBrowser.UI.BaseDialog.prototype.initialize.apply(this,[id + 'download_dialog','Download layer']);

        this.category = 'Download_form';
        //this.category = 'Download_test';

        this.reasonform = document.createElement("form");
        DownloadForm_setup(this.reasonform);
        this.reasonform.addEventListener("submit", function(event) {
            if (event.preventDefault) {
                event.preventDefault();
            }

            // netcdf name without extension
            var data_url = layer.http_url;

            var success = DownloadForm_submit(this,that.category,data_url,layer.get_uuid());
            if (success) {
                $(that.reasonform).hide();
                $(that.downloadui).show();
            }

            return false;
        });


        if (this.download_form) {
            this.div.appendChild(this.reasonform);
        }
        this.downloadui = document.createElement("div");
        this.div.appendChild(this.downloadui);

        // DOM tree for Download Dialog
        $(this.downloadui).append(
            dom('div',{'class': 'OceanBrowserDownloadDialog'},[
                dom('h4',{},[
                    'Data products:'
                ]),
                this.NetCDFButton = dom('input',{'type': 'button', 'value': 'Download NetCDF','class': 'OceanBrowserDownloadButton'}),
                this.OPENDAPButton = dom('input',{'type': 'button', 'value': 'OPeNDAP Service','class': 'OceanBrowserDownloadButton'}),
                dom('a',{
                    'href': 'http://labos.ulg.ac.be/gher/software/oceanbrowser/',
                    'class': 'OceanBrowserHelpLink',
                    'target': '_blank'},'?'),
/*
                dom('table',{},[
                    dom('tbody',{},[
                        dom('tr',{},[
                            dom('td',{},[
                                dom('a',{'href': layer.http_url, 'target': '_blank'},[
                                    'NetCDF file:'
                                ])
                            ]),
                            dom('td',{},[
                                dom('input',{'readonly': 'readonly', 'type': 'text', 'size': '40', 'value': layer.http_url})
                            ])
                        ]),
                        dom('tr',{},[
                            dom('td',{},[
                                dom('a',{'href': layer.opendap_url, 'target': '_blank'},[
                                    'OPeNDAP:'
                                ])
                            ]),
                            dom('td',{},[
                                dom('input',{'readonly': 'readonly', 'type': 'text', 'size': '40', 'value': layer.opendap_url})
                            ])
                        ])
                    ])
                ]),
*/
                dom('h4',{},[
                    'Image/Animation:'
                ]),
                dom('table',{},[
                    dom('tbody',{},[
                        dom('tr',{},[
                            dom('td',{},[
                                dom('label',{'for': this.id + 'imageWidth'},[
                                    'Width (px):'
                                ])
                            ]),
                            dom('td',{},[
                                this.imageWidth = dom('input',{'type': 'text', 'id': this.id + 'imageWidth', 'value': '800', 'size': '5'})
                            ]),
                            dom('td',{},[
                                dom('label',{'for': this.id + 'imageHidth'},[
                                    'Height (px):'
                                ])
                            ]),
                            dom('td',{},[
                                this.imageHeight = dom('input',{'type': 'text', 'id': this.id + 'imageHeight', 'value': '500', 'size': '5'})
                            ])
                        ]),
                        dom('tr',{},[
                            dom('td',{},[
                                dom('label',{'for': this.id + 'rxmin'},[
                                    'x-range:'
                                ])
                            ]),
                            dom('td',{},[
                                this.rxmin = dom('input',{'type': 'text', 'id': this.id + 'rxmin', 'size': '5'}),
                                this.rxmax = dom('input',{'type': 'text', 'id': this.id + 'rxmax', 'size': '5'})
                            ]),
                            dom('td',{},[
                                dom('label',{'for': this.id + 'rymin'},[
                                    'y-range:'
                                ])
                            ]),
                            dom('td',{},[
                                this.rymin = dom('input',{'type': 'text', 'id': this.id + 'rymin', 'size': '5'}),
                                this.rymax = dom('input',{'type': 'text', 'id': this.id + 'rymax', 'size': '5'})
                            ])
                        ]),
                    ])
                ]),
                dom('label',{'for': this.id + 'select_format'},['Format: ']),
                this.format = dom('select',{'id': this.id + 'select_format'}),
                this.downloadButton = dom('input',{'type': 'button', 'value': 'Download'}),
                dom('p',{},['The creation of animation (WebM or MP4) may take several minutes.'])
            ])
        );

        // add format list
        for (i=0; i<formats.length; i++) {
            $(this.format).append(dom('option',{'value': formats[i]},[OceanBrowser.mimetypes[formats[i]]]));
        }

        $(this.downloadButton).click(function() {
            var param = {
                xmin: parseFloat(that.rxmin.value),
                xmax: parseFloat(that.rxmax.value),
                ymin: parseFloat(that.rymin.value),
                ymax: parseFloat(that.rymax.value),
                height: parseInt(that.imageHeight.value,10),
                width: parseInt(that.imageWidth.value,10),
                format: that.format.value
            };

            _paq.push(['trackEvent', 'Download', 'Layer', layer.describe()]);
            that.events.triggerEvent('download',param);
        });

        $(this.NetCDFButton).click(function() {
            _paq.push(['trackEvent', 'Download', 'Layer', layer.describe()]);
            window.open(Util.make_https(layer.http_url),'NetCDF file');
        });

        $(this.OPENDAPButton).click(function() {
            _paq.push(['trackEvent', 'Download', 'Layer', layer.describe()]);
            window.open(layer.opendap_url,'OPeNDAP server');
        });

        $(this.div).dialog({ autoOpen: false, width: 600});
    },

    open: function() {
        var b = this.getCurrentExtent();

        $(this.rxmin).attr('value',b.left);
        $(this.rxmax).attr('value',b.right);
        $(this.rymin).attr('value',b.bottom);
        $(this.rymax).attr('value',b.top);

        if (this.download_form) {
          $(this.reasonform).show();
          $(this.downloadui).hide();
          _paq.push(['trackEvent', this.category, 'form', 'shown']);
        }

        $(this.div).dialog('open');
    }

});

// Layer with UI elements
OceanBrowser.UI.Layer = EarthGL.Class({
    layer: null,
    info_dialog: null,

    initialize: function(id,layer) {
        this.layer = layer;
        this.info_dialog = OceanBrowser.UI.InfoLayer(layer);
    }
});

OceanBrowser.UI.LayerList = EarthGL.Class(OceanBrowser.UI.BaseDialog,{
    $list: null,
    $controls: null,

    initialize: function(id) {
        OceanBrowser.UI.BaseDialog.prototype.initialize.apply(this,[id + '_layer_list','Layer List']);

        this.$list = $('<ul/>').attr({'class': 'LayerListList'});

        this.$controls = $('<div/>').attr({'class': 'LayerListControls'})
            .append($('<div/>')
                    .append($('<div/>').attr({'class': 'FigurePropertiesConfig'}))
                    .append($('<div/>').attr({'class': 'FigurePropertiesDownload'}))
                    .append($('<div/>').attr({'class': 'FigurePropertiesInfo'}))
                    .append($('<div/>').attr({'class': 'FigurePropertiesRemove'}))
                   );

        $(this.div).append($('<div/>').attr({'class': 'LayerListContainer'})
                           .append(this.$list)
                           .append(this.$controls));

        this.$controls.find('.FigurePropertiesInfo').click(function() {
            alert('toto');
        });


        $(this.div).dialog({ autoOpen: false, width: 300, height: 500});

    },

    add: function(layer) {
        var depth = layer.get_dimension('elevation');
        var time = layer.get_dimension('time');
        var styles = layer.get_styles();
        var j, $legend;
        var $item = ($('<li/>').append($('<input/>').attr({'type': "radio",
                                                           'name': 'LayerListRadio',
                                                           'checked': 'checked'}))
                     .append(layer.title));

        function add_dim(d,name) {
            if (d.range.length > 0) {
                var $sel = $('<select/>').attr('title',name + ' [' + d.units + ']');
                var isselected;

                for (j=0; j < d.range.length; j++) {
                    isselected = d.range[j] == d.default_value;

                    if (jQuery.ui.version === '1.8.16') {
                        $sel.append($('<option/>').attr('selected',isselected).append(d.range[j]));
			        }
			        else {
			            $sel.append($('<option/>').prop('selected',isselected).append(d.range[j]));
			        }


                }
                $item.append($sel);
            }
        }

        add_dim(depth,'depth');
        add_dim(time,'time');

        $item.append($('<div/>').attr({'class': 'LegendButton','title':'Show or hide legend'})
                     .append($('<span/>').attr({'class': "ui-icon ui-icon-triangle-1-s"}))
                     .append($('<div/>')
                             .append('Legend')));

        var legend;
        legend = {url: 'http://localhost/web-vis-test/Python/web/wms?&amp;request=GetLegendGraphic&amp;width=200&amp;height=80&amp;transparent=true&amp;decorated=true&amp;style=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A-1.61687%2Bvmax%3A29.1772%2Bncontours%3A40&amp;color=%23000000&amp;format=image%2Fpng&amp;layer=Atlantic%2FTemperature.19752005.4Danl.nc%23Temperature_L1',
                  width: 200,
                  height: 80};

        var $legendContainer = $('<div/>').attr({'class': 'LayerListLegend'})
            .append($('<div/>').css({'background-image': 'url(' + legend.url + ')',
                                     'width': 200,
                                     'height': 80}));


        $item.append($legendContainer);

        // call-back for showing/hiding legend
        $item.find('.LegendButton').click(function() {
            $legendContainer.toggle("slow");
            $(this).find('.ui-icon').toggleClass("ui-icon-triangle-1-e");
            $(this).find('.ui-icon').toggleClass("ui-icon-triangle-1-s");

        });

        this.$list.append($item);

    },

    open: function() {
        $(this.div).dialog('open');
    }

});


OceanBrowser.UI.reportProblem = EarthGL.Class(OceanBrowser.UI.BaseDialog,{

    initialize: function(id,feedbackurl,layers,figprops) {
        OceanBrowser.UI.BaseDialog.prototype.initialize.apply(this,[id + '_report_problem','Report a problem']);
        this.layers = layers;
        this.figprops = figprops;
        this.feedbackurl = feedbackurl;

        $(this.div).append(
            this.$form = $('<form/>').append(
                $('<table/>').append(
                    $('<tbody/>').append(
                        $('<tr/>').append(
                            $('<td/>').append(
                                $('<label/>').attr({'for': 'report_problem_layer'}).append('Layer')
                            ),
                            $('<td/>').append(
                                this.$layers = $('<select/>').attr({'id': 'report_problem_layer'})
                            )
                        ),
                        $('<tr/>').append(
                            $('<td/>').append(
                                $('<label/>').attr({'for': 'report_problem_depth'}).append('Depth')
                            ),
                            $('<td/>').append(
                                this.$depth= $('<select/>').attr({'id': 'report_problem_depth'})
                            )
                        ),
                        $('<tr/>').append(
                            $('<td/>').append(
                                $('<label/>').attr({'for': 'report_problem_time'}).append('Time')
                            ),
                            $('<td/>').append(
                                this.$time = $('<select/>').attr({'id': 'report_problem_time'})
                            )
                        ),
                        $('<tr/>').append(
                            $('<td/>').append(
                                $('<label/>').attr({'for': 'report_problem_longitude'}).append('Longitude')
                            ),
                            $('<td/>').append(
                                this.$lon = $('<input/>').attr({'type': 'text', 'id': 'report_problem_longitude', 'size': 50})
                            )
                        ),
                        $('<tr/>').append(
                            $('<td/>').append(
                                $('<label/>').attr({'for': 'report_problem_latitude'}).append('Latitude')
                            ),
                            $('<td/>').append(
                                this.$lat = $('<input/>').attr({'type': 'text', 'id': 'report_problem_latitude', 'size': 50})
                            )
                        ),
                        $('<tr/>').append(
                            $('<td/>').append(
                                $('<label/>').attr({'for': 'report_problem_name'}).append(
                                    'Full name',
                                    $('<sup/>').append('*')
                                )
                            ),
                            $('<td/>').append(
                                this.$name = $('<input/>').attr({
                                    'type': 'text', 'id': 'report_problem_name',
                                    'size': 50,
                                    'required': 'required',
                                    'placeholder': 'Your name'})
                            )
                        ),
                        $('<tr/>').append(
                            $('<td/>').append(
                                $('<label/>').attr({'for': 'report_problem_email'}).append(
                                    'Email',
                                    $('<sup/>').append('*')
                                )
                            ),
                            $('<td/>').append(
                                this.$email = $('<input/>').attr({
                                    'type': 'email',
                                    'id': 'report_problem_email',
                                    'size': 50,
                                    'required': 'required',
                                    'placeholder': 'user@domain.com'})
                            )
                        ),
                        $('<tr/>').append(
                            $('<td/>').append(
                                $('<label/>').attr({'for': 'report_problem_comment'}).append(
                                    'Comment',
                                    $('<sup/>').append('*')
                                )
                            ),
                            $('<td/>').append(
                                this.$comment = $('<textarea/>').attr({'rows': 5, 'id': 'report_problem_comment',
                                                                       'cols': 50, 'required': 'required'})
                            )
                        )
                    )
                ),
                $('<p/>').append(
                    $('<sup/>').append(
                        '*'
                    ),
                    'Required fields'
                ),
                $('<div/>').attr({'class': 'OceanBrowserButtons'}).append(
                    this.$cancel = $('<input/>').attr({'type': 'button', 'value': 'Cancel'}),
                    this.$submit = $('<input/>').attr({'type': 'submit', 'value': 'Submit'})
                )
            )
        );

        var that = this;
        this.$layers.change(function() {
            that.layerChanged();
        });

        this.$cancel.click(function() {
            that.close();
        });

        this.$form.submit(function() {
            that.submit();
            return false;
        });

        $(this.div).dialog({ autoOpen: false, width: 700, height: 450});

    },


    open: function() {
        var l;
        this.$submit.removeAttr('disabled');
        this.$submit.val('Submit');
        this.$layers.empty();

        for (l in this.layers) {
            this.$layers.append($('<option/>')
                        .attr({'value': this.layers[l].id})
                        .append(this.layers[l].FullTitle(' > ')));
        }

        // update depth and time
        this.layerChanged();
        $(this.div).dialog('open');
    },

    submit: function() {
        var param = {'layer': this.$layers.val(),
                     'time': this.$time.val(),
                     'elevation': this.$depth.val(),
                     'lon': this.$lon.val(),
                     'lat': this.$lat.val(),
                     'name': this.$name.val(),
                     'email': this.$email.val(),
                     'comment': this.$comment.val()};

        // check if all required fields are present

        var required = [['Full name','name'],['Email','email'],['Comment','comment']];

        var missing = [];
        var that = this;

        for (var i in required) {
            if (param[required[i][1]].length === 0) {
                missing.push(required[i][0]);
            }
        }

        if (missing.length > 0) {
            alert('Please fill out the following fields: ' + missing.join(','));
        }
        else {
            this.$submit.attr('disabled','disabled');
            this.$submit.val('sending...');

            console.log('param ',param);

            Util.ajax(this.feedbackurl,param,function(xmldoc) {
                that.close();
                that.$submit.removeAttr('disabled');
                that.$submit.val('Submit');

                var error = $(xmldoc).find('error');

                if (error.length === 0) {
                    alert('Thank you for your feedback!');
                }
                else {
                    alert(error.text());
                }
                console.log(xmldoc);
            },{'request': 'POST'});

        }
    },

    layerChanged: function() {
        var l, checkedval, deftime, defdepth;

        function set_dim($elem,dim,def) {
            var i;
            $elem.empty();

            if (dim === undefined) {
                return;
            }

            $elem.attr({'title': dim.units});

            for (i in dim.range) {
                $elem.append($('<option/>')
                             .attr({'value': dim.range[i],
                                    'selected': dim.range[i] === def})
                             .append(dim.range[i]));
            }
        }


        for (l in this.layers) {
            if (this.layers[l].id === this.$layers.val()) {
                if (this.figprops) {
                    deftime = this.figprops[l].get_dimension('time');
                    defdepth = this.figprops[l].get_dimension('elevation');
                }

                set_dim(this.$time,this.layers[l].get_dimension('time'),deftime);
                set_dim(this.$depth,this.layers[l].get_dimension('elevation'),defdepth);
                break;
            }
        }
    }

});


OceanBrowser.UI.AddLayer = EarthGL.Class(EarthGL.Control, {
    type: EarthGL.Control.TYPE_BUTTON,
    title: 'add layer',
    layerWMS: null,

    initialize: function(id,viewer,servers,baselayer) {
        this.chooseLayer = new OceanBrowser.UI.chooseLayer(id,servers,{baselayer: baselayer});
        this.chooseLayer.events.register('layerSelected', viewer, viewer.addLayer);
    },

    trigger: function() {
        this.chooseLayer.open();
    },


    CLASS_NAME: "AddLayer"
});


/**
 *      @class OceanBrowser user-interface
 */

OceanBrowser.UI.viewer3D = function(id,options) {
    this.map = null;
    this.mapid = id;

    this.layers = [];
    this.layer_num = 0;
    // ugly !!! needed for AnimLayer
    this.analysis_wms = {};
    this.figprops = {};
    this.rotating = false;
    this.fp = [];
    this.servers = SERVERS;
    this.previewBaselayer = null;

    var topoWidth = 64;
    var topoHeight = 32;
    var baselayer;


    this.events = new EarthGL.Events(this);

    EarthGL.Util.merge(this,options);

    var hmin = -8043.02;
    var hmax = 5472.27;

    var baseurl = '.';

    var topoLayer = new EarthGL.Layer('gebco_30sec_100.nc',
                                      baseurl + '/Python/web/wms',
                                      {layers: 'gebco_30sec_100.nc' + Util.layersep + 'bat',
                                       styles: 'cmap:gray+method:pcolor_flat+vmin:-8043.02+vmax:5472.27'
                                      });


    var topo = new EarthGL.Topo(64,32,hmin,hmax,topoLayer);
    this.map = new EarthGL.Map(id,{topo: topo});
    //this.map = new EarthGL.Map(id,{topo: topo, vscale: 1});

    this.map.addControl(new EarthGL.Control.Config('EarthGL/images/'));
    this.map.addControl(new EarthGL.Control.Rotate());
    this.map.addControl(new EarthGL.Control.Navigation());
    this.map.addControl(new EarthGL.Control.CameraPosition());

    this.addLayerControl = new OceanBrowser.UI.AddLayer(id + '-AddLayerControl',
                                                        this,this.servers,this.previewBaselayer);

    this.map.addControl(this.addLayerControl);

    this.colorbar_container = document.getElementById('colorbar_container');


    /*
    baselayer = new EarthGL.Layer('NASA Blue Marble',
                                      baseurl + '/Python/web/bluemarble',
                                      {layers: 'BMNG'},
                                      {isBaseLayer: true});
*/
    baselayer = new EarthGL.Layer('GEBCO',
                                  baseurl + '/Python/web/bluemarble',
                                  {layers: 'GEBCO_Latest',
                                   _proxy_url: 'https://www.gebco.net/data_and_products/gebco_web_services/web_map_service/mapserv'},
                                  {resolutions:
                                   [360/Math.pow(2,8),360/Math.pow(2,9),360/Math.pow(2,10),
                                    360/Math.pow(2,11),360/Math.pow(2,12),360/Math.pow(2,13),
                                    360/Math.pow(2,14),360/Math.pow(2,15),360/Math.pow(2,16)],
                                   isBaseLayer: true}),

    this.map.addLayer(baselayer);
    this.map.setCenter(10,40);
};


/*
OceanBrowser.UI.viewer3D.prototype.removeLayer = function(field) {
    field.figprops.remove();

    this.map.removeLayer(field.layer);
};
*/

OceanBrowser.UI.viewer3D.prototype.downloadLayer = function(field) {

};

// obsolete
OceanBrowser.UI.viewer3D.prototype.get_wms_layer = function(id,param) {
    return this.figprops[id].getLayer(param);
};

OceanBrowser.UI.viewer3D.prototype.addLayer = function(obj) {
    var field = obj.field;
    var constraints = obj.constraints;


    var param = {};
    var wms_url;
    this.layer_num = this.layer_num+1;

    if (constraints.type === 'lon-lat') {
        wms_url = field.wms_url;
    }
    else {
        param.section = Util.encodesec(constraints.lon,constraints.lat);
        console.log('adding ' + field.title + ' ' + field.name,param.section);
        wms_url = 'Python/web/wms_vert?';
    }

    console.log('adding ' + field.title + ' ' + field.name,constraints.lat);

    var that = this;
    var remove_callback = function() {
        // this is an instance of FigureProperties
        console.log('removing ' + this.Layer.title + ' ' + this.Layer.name);
        that.map.removeLayer(this.layerWMS);
        this.remove();
    };

    var download_callback = function() {
        // that.downloadLayer(field);
    };


    var figprops = new FigureProperties(this.mapid,this.mapid + this.layer_num,
                                        wms_url,
                                        function() {
                                            console.log('update field ' + field.title + ' ' + field.name);
                                            that.map.removeLayer(figprops.layerWMS);
                                            figprops.loadStat(param);
                                        },
                                        {colorbarclass: 'colorbar2',
                                         colorbar_container: this.colorbar_container,
                                         title: field.title,
                                         Layer: field,
                                         abstract_info: field.abstract_info,
                                         metadata_url: field.metadata_url,
                                         add_style_button: true,
                                         download_callback: download_callback,
                                         remove_callback: remove_callback,
                                         constraints: constraints});


    // ugly !!! needed for AnimLayer
    this.figprops[field.id] = figprops;


    if (constraints.type == 'lon-lat') {
        // query depth dimension and add to dialog if appropriate

        var depths = field.get_dimension('elevation');

        if (depths.range.length > 0) {
            figprops.set_dimension('elevation','depth',depths);
        }
    }


    // query time dimension and add to dialog if appropriate

    var time = field.get_dimension('time');

    if (time.range.length > 0) {
        figprops.set_dimension('time','time',time);

        figprops.anim = new AnimLayer(figprops.button_anim,
                                      figprops.dimensions['time'].sel,
                                      this,field.id);

    }

    // update button

    var button_update = document.createElement("input");
    button_update.type = "button";
    button_update.value = "Update";
    figprops.layer_info_div.appendChild(button_update);
    $(button_update).click(function() {
        that.map.removeLayer(figprops.layerWMS);
        figprops.loadStat(param);
    });
    figprops.button_update = button_update;

    figprops.onload = function() {
        figprops.layerWMS = figprops.getLayer();

        // ugly !!! needed for AnimLayer
        that.analysis_wms[field.id] = figprops.layerWMS;
        that.map.addLayer(figprops.layerWMS);
        console.log('loaded ' + field.title + ' ' + field.name);
        that.events.triggerEvent('fieldadded',{field: field});
    };

    figprops.loadStat(param);
    this.fp.push(figprops);
};


// performs the GetCapabilities request and creates a layer tree
// options:

//   version: 1.3.0 (default) or 1.1.1
//   WMS_extensions: null (no WMS extensions), OceanBrowser

OceanBrowser.UI.LayerTree = function(id,url,options) {
    this.id = id;
    this.url = url;
    this.params = '';
    // four digit random number
    this.namespace = 'layer_list_' + Math.floor(Math.random()*1e4);
    this.elem = null;
    this.choose_multiple = true;
    this.show_title = false;

    this.version = '1.3.0';
    this.service = 'WMS';
    this.extra_param = {};
    this.use_proxy = null;
    this.requests = [];
    this.WMS_extensions = 'OceanBrowser';

    // options
    if (options) {
        for (var p in options) {
            this[p] = options[p];
        }
    }

    this.events = new EarthGL.Events(this);

    // linear list of all layers
    this.layer_infos = [];
    this.current_leave = null;
};

// methods
OceanBrowser.UI.LayerTree.prototype.load = function() {
    var cap = document.getElementById(this.id);

    if (!this.elem) {
        this.elem = document.createElement("div");
        cap.appendChild(this.elem);
    }

    Util.removeChildNodes(this.elem);


    var div = document.createElement("div");
    div.className = "layer_loading";
    var img = document.createElement("img");
    img.src = "img/ajax-loader.gif";
    img.alt = "loading";
    div.appendChild(img);
    div.appendChild(document.createTextNode(" Loading..."));
    this.elem.appendChild(div);

    // default parameters for request
    var params = {
        request: "GetCapabilities",
        service: this.service,
        version: this.version
    };

    // over-write with extra parameter for request
    for (var p in this.extra_param) {
        params[p] = this.extra_param[p];
    }

    // use proxy instead of real WMS server
    //console.log('get ',Util.append_param(this.url,params));

    // when the tree is ready view it
    this.events.register('treeReady', this, this.view_tree);

    var that = this;

    // load list of features
    var request = OpenLayers.Request.GET({
        url: this.url,
        params: params,
        success: function(request) {
            // remove 'Loading...' message
            Util.removeChildNodes(that.elem);

            if (request.responseXML === null) {
                var msg = 'Error while connecting to ' +  that.url + ' (GetCapabilities)';
                alert(msg);
                return;
            }

            if (that.service === 'WMS') {
                that.parseWMSCap(request.responseXML);
            }
            else {
                that.parseWFSCap(request.responseXML);
            }



        },
        failure: function(request) {
            //$('#map_loading').hide();

            var msg = 'Error while connecting to ' +  that.url + ' (GetCapabilities)';

            if (request.responseText) {
                msg += '\nError message:\n ' + request.responseText;
            }

            alert(msg);
        }
    });
};

OceanBrowser.UI.LayerTree.prototype.parseWMSCap = function(xmldoc) {
    var elem, url;


    if (xmldoc.firstChild.tagName == 'ServiceExceptionReport') {
        alert('WMS server failed');
    }


   this.xmldoc = xmldoc;

    // service
    var service = xmldoc.getElementsByTagName('Service')[0];
    var item = service.firstChild;

    while (item !== null) {
        if (item.tagName == "Title" && item.firstChild && !(this.title)) {
            this.title = item.firstChild.data;
        }
        else if (item.tagName == "Abstract" && item.firstChild) {
            this.abstract_info = item.firstChild.data;
        }
        item = item.nextSibling;
    }

    // all requests
    var req = xmldoc.getElementsByTagName('Request')[0];
    item = req.firstChild;
    while (item !== null) {
        if (item.tagName) {
            var requestInfo = {formats: []};

            var item2 = item.firstChild;
            while (item2 !== null) {
                if (item2.tagName == "Format" && item2.firstChild) {
                    requestInfo.formats.push(item2.firstChild.data);
                }

                item2 = item2.nextSibling;
            }

            this.requests[item.tagName] = requestInfo;
        }
        item = item.nextSibling;
    }


    // get the url for GetMap requests
    var GetMap = xmldoc.getElementsByTagName('GetMap');

    if (GetMap.length > 0) {
        var get = GetMap[0].getElementsByTagName('Get');
        if (get.length > 0) {
            elem = get[0].getElementsByTagName("OnlineResource")[0];

            if (elem.getAttributeNS) {
                var xlinkns = "http://www.w3.org/1999/xlink";
                url = elem.getAttributeNS(xlinkns,"href");
            }
            else {
                // IE
                url = elem.getAttribute("xlink:href");
            }

            //console.log("url ",url);
            this.url = Util.make_https(url);
        }
    }


    // layer inventory
    var root = xmldoc.getElementsByTagName('Capability')[0];

    // do not show top layer if it is only a folder
    if ($(xmldoc).find('Capability>Layer').length == 1) {
        if ($(xmldoc).find('Capability>Layer>Layer').length > 0) {
            root = $(xmldoc).find('Capability>Layer')[0];
        }
    }

    var that = this;

    this.tree = this.layer_list(root,null);

    this.events.triggerEvent('treeReady',this.tree);

};

OceanBrowser.UI.LayerTree.prototype.WFSLayerTree = function(featureTypes) {
    var tree = [], that = this;

   featureTypes.forEach(function(featureType,index) {
        var title = featureType.title, WFS = that.WFS || {};
        var featureList = WFS.featureList || {};
        //console.log('featureType',featureType.title,featureType.name);

        if (WFS.presentTitle) {
            title = WFS.presentTitle(featureType);
        }


        if (featureList.ignore !== undefined) {
            if (featureList.ignore.indexOf(featureType.name) !== -1) {
                // do nothing
                return;
            }
        }

        if (featureList.keep !== undefined) {
            if (featureList.keep.indexOf(featureType.name) === -1) {
                // do nothing
                return;
            }
        }

        var WFSLayer = new WFSLayerInfo(featureType,that);

        tree.push({text: title, data: WFSLayer});
/*
        if (featureList.selected !== undefined) {
            if (featureList.selected.indexOf(featureType.name) !== -1) {
                that.addLayer({
                    feature: featureType,
                    id: id,
                    server: server
                });
            }
        }*/

    });

    this.tree = tree;
    this.events.triggerEvent('treeReady',this.tree);
};

OceanBrowser.UI.LayerTree.prototype.parseWFSCap = function(responseXML) {
    var parser = new OpenLayers.Format.WFSCapabilities();
    var that = this;
    var featureTypes;

    var name = this.name || this.url;
    var cap = parser.read(responseXML);

    this.title = this.title || cap.service.title;


    // misses long/lat
    //featureTypes = cap.featureTypeList.featureTypes;

    featureTypes = $(responseXML).find('FeatureType').map(function() {
           return {
                   'name': $(this).find('Name').text(),
                   'title': $(this).find('Title').text(),
                   'SRS': $(this).find('SRS').text(),
                   'minx': parseFloat($(this).find('LatLongBoundingBox').attr('minx')),
                   'maxx': parseFloat($(this).find('LatLongBoundingBox').attr('maxx')),
                   'miny': parseFloat($(this).find('LatLongBoundingBox').attr('miny')),
                   'maxy': parseFloat($(this).find('LatLongBoundingBox').attr('maxy'))
           };
        }).get();

    this.cap = cap;

    if ((this.WFS || {}).layerTree) {
        // call WFS.layerTree with the "this" context refering to this objects
        // insteaf of this.WFS
        this.WFS.layerTree.call(this,featureTypes);
    }
    else {
        this.WFSLayerTree(featureTypes);
    }



};

OceanBrowser.UI.LayerTree.prototype.rendertree = function(tree,namespace,ind) {
    var elem;
/*
        <label for="group-1">Atlantic Ocean</label> <!-- Group 1   -->
        <ul>
          <li class="has-children">
            <input type="checkbox" name="group1-sub1" id="group1-sub1"> <!-- group1-sub1   -->
            <label for="group1-sub1">Autumn (October-December - 10 Years running averages</label><!-- group1-sub1  -->
            <ul>
              <li class="has-children">
                <input type="checkbox" name="group1-sub1sub1" id="grou
*/

    if (!tree) {
        return null;
    }

    ind = ind || [1];
    namespace = namespace || '';

    var name;
    var ul = $('<ul/>');

    if (ind.length === 1) {
        ul.attr("class","cd-accordion-menu");
    }

    ind[ind.length] = 0;

    for (var i = 0; i < tree.length; i++) {
        ind[ind.length-1] = i;
        name = namespace + "group-" + String(ind);

         if (tree[i].children) {
          elem = $('<li/>').attr("class","has-children").append(
           $('<input/>').attr({"type": "checkbox","name": name, "id": name, "checked": false}),
           $('<label/>').attr({"for": name}).append(
              tree[i].text
           ),
           this.rendertree(tree[i].children,namespace,ind.slice()));
         }
         else {
             elem = $('<li/>').append(
                $("<a/>").attr({"href":"#0","data-layer": tree[i].data}).append(
                   tree[i].text));
         }

         ul.append(elem);
    }
    //console.log("end");

    return ul;
}

OceanBrowser.UI.LayerTree.prototype.view_tree = function() {
    var elem, url;

    var that = this;

    this.extractLayerInfo(this.tree);
    var domtree = this.rendertree(this.tree,this.namespace);
    //console.log(t.html());
    //$("#add_Layer_chooser2").empty();

    var container = $("<div/>");
    container.append($("<h3/>").append(this.title),domtree);

    $(this.elem).append(container);
    container.click(function(event) {
         //console.log("this",this,event);
         var index = event.target.getAttribute("data-layer");
         if (index) {
             var Layer = that.layer_infos[parseInt(index)];
             console.log(Layer.title);
             that.events.triggerEvent('layerSelected',Layer);
         }
     });

    this.events.triggerEvent('loaded',this);
};


OceanBrowser.UI.LayerTree.prototype.layer_list = function(root,parentLayer) {
    var that = this;
    var item = root.firstChild;
    var list = [];

    var elem, i = 0;
    var keeptest = ((this.WMS || {}).layers || {}).keep;

    var vert_wms_url = null;
    if (this.WMS_extensions == 'OceanBrowser') {
        // replace wms by wms_vert for the WMS server
        // providing vertical sections
        vert_wms_url = this.url.replace(/\/wms\b/,'/wms_vert');
    }


    while (item !== null) {
        if (item.tagName == "Layer") {
            var Layer = new LayerInfo(item,
                                      {wms_url: this.url,
                                       vert_wms_url: vert_wms_url,
                                       use_proxy: this.use_proxy,
                                       WMS_extensions: this.WMS_extensions,
                                       cap: this,
                                       parentLayer: parentLayer
                                      }
                                     );

            if (keeptest === undefined || keeptest(Layer)) {

               var list_item = {
                   'text': Layer.title,
                   'data': Layer
               };

               var sub = this.layer_list(item,Layer);

               if (sub.length !== 0) {
                   list_item.children = sub;
               }

               list.push(list_item);
            }
        }

        item = item.nextSibling;
    }


    return list;
};

// replace the LayerInfo in the data attribute by an index to the
// array layer_infos so that the array can be converted to JSON

OceanBrowser.UI.LayerTree.prototype.extractLayerInfo = function(list) {
    var i, len = list.length, layer_num;
    //console.log('extractLayerInfo',this);
    for (i=0; i < len; i++) {
        layer_num = this.layer_infos.length;
        this.layer_infos.push(list[i].data);
        list[i].data = layer_num;

        if (list[i].children) {
            this.extractLayerInfo(list[i].children);
        }
    }

}


OceanBrowser.UI.LayerTree.prototype.encodeId = function(url,version,index) {
    return url + '---' + version + '---' + index;
};

OceanBrowser.UI.LayerTree.prototype.parseId = function(id) {
    var p = id.split('---');
    return {url: p[0], version: p[1], number: parseInt(p[2])};
};



OceanBrowser.UI.LayerTree.prototype.getNode = function(layer) {
    for (var i=0; i < this.layer_infos.length; i++) {
        if (this.layer_infos[i] == layer) {
            return document.getElementById(this.namespace + i);
        }
    }

    return null;
};

OceanBrowser.UI.LayerTree.prototype.getByName = function(name) {
    for (var i=0; i < this.layer_infos.length; i++) {
        if (this.layer_infos[i].name === name) {
            return this.layer_infos[i];
        }
    }

    return null;
};

/*
  searches among all layers to find layers matching the query
*/
OceanBrowser.UI.LayerTree.prototype.search = function(query) {
    var list = [], tmp = [], s, i;

    // select all layers with positif score

    for (i in this.layer_infos) {
        s = this.layer_infos[i].searchScore(query);
        if (s > 0) {
            tmp.push([s, this.layer_infos[i]]);
        }
    }

    // sort according to score
    tmp = tmp.sort(function(a,b) {
        return b[0]-a[0];
    });

    // keep only LayerInfo (and remove score)
    for (i in tmp) {
        list.push(tmp[i][1]);
    }

    return list;
};



OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {
                defaultHandlerOptions: {
                    'single': true,
                    'double': false,
                    'pixelTolerance': 0,
                    'stopSingle': false,
                    'stopDouble': false
                },

                initialize: function(layer,options) {
                    this.handlerOptions = OpenLayers.Util.extend(
                        {}, this.defaultHandlerOptions
                    );
                    OpenLayers.Control.prototype.initialize.apply(
                        this, arguments
                    );
                    this.handler = new OpenLayers.Handler.Click(
                        this, {
                            'click': this.trigger
                        }, this.handlerOptions
                    );

                    this.layer = layer;
                },

                trigger: function(e) {
                    var styleSelected = {strokeColor: "#333399", 'strokeWidth': 5};
                    var lonlat = this.map.getLonLatFromPixel(e.xy);
                    var p = new OpenLayers.Geometry.Point(lonlat.lon,lonlat.lat);
                    var layer = this.layer;
                    var mindist, i;

                    for (i = 0; i < layer.features.length; i++) {
                        layer.features[i].style = null;
                    }

                    mindist = layer.features.map(function(s) { return s.geometry.distanceTo(p); });
                    i = mindist.indexOf(Math.min.apply(Math, mindist));


                    layer.features[i].style = styleSelected;
                    layer.redraw();

                    // change this
                    var c = layer.features[i].geometry.components[0].components;
                    vsection.update(undefined,c.map(function(p) { return [p.x,p.y]}));

                    /*alert("You clicked near " + lonlat.lat + " N, " +
                                              + lonlat.lon + " E");*/
                }

            });


OceanBrowser.UI.manageSection = EarthGL.Class(OceanBrowser.UI.BaseDialog,{

    initialize: function(DefinedSection,contourExtractorBaseurl) {
        OceanBrowser.UI.BaseDialog.prototype.initialize.apply(this,['section_chooser_dialog','Manage Section']);
        this.contour_extractor_baseurl = contourExtractorBaseurl;


        $(this.div).append(
                   $('<h3/>').append(
                       'Save section'
                   ),
                   $('<form/>').attr({'class': 'save'}).append(
                       'Name:',
                       this.$new_name = $('<input/>').attr({'type': 'text', 'placeholder': 'Name'}),
                       $('<input/>').attr({'type': 'submit', 'value': 'Save'})
                   ),
                   $('<p/>').append(
                       'Coordinates of the section will be saved in the local storage of your browser.'
                   ),
                                   /*
                   $('<h3/>').append(
                       'Contour generator'
                   ),
                   $('<form/>').attr({'class': 'generate'}).append(
                       'Type:',
                       this.$type = $('<select/>').append(
                           $('<option/>').attr({'value': 'dist2coast'}).append('Distance from coast'),
                           $('<option/>').attr({'value': 'bat'}).append('Bathymetry (depth)')
                       ),
                       $('<br/>'),
                       'Distance (km) or depth (m):',
                       this.$lev = $('<input/>').attr({'type': 'text', 'placeholder': '10'}),
                       $('<br/>'),
                       'Near point (lon/lat):',
                       this.$near_lon = $('<input/>').attr({'type': 'text', 'value': '10.331'}),
                       this.$near_lat = $('<input/>').attr({'type': 'text', 'value': '43.340'}),
                       this.$map = $('<div/>').attr({'class': 'map_preview','id': 'mansecmap'}),
                       $('<input/>').attr({'type': 'submit', 'value': 'Generate'})
                   ),*/
                   $('<h3/>').append(
                       'Load section'
                   ),
                   $('<form/>').attr({'class': 'load'}).append(
                       'Choose: ',
                       this.$section_list = $('<select/>'),
                       $('<input/>').attr({'type': 'submit', 'value': 'Load'}),
                       $('<input/>').attr({'type': 'button', 'class': 'delete', 'value': 'Delete'})
                   )
        );

        var that = this;


        // storage
        if (localStorage.section === undefined) {
            console.log('initialize local storag');
            localStorage.section = '[]';
        }

        this.list = JSON.parse(localStorage.section);

        for (var i = 0; i < DefinedSection.length; i++) {
            DefinedSection[i].predefined = true;
            this.list.push(DefinedSection[i]);
        }

        this.addsec = function(sec) {
            that.$section_list.append($('<option/>').attr({'value': sec.name})
                                      .append(sec.name));
        }

        for (var i = 0; i < this.list.length; i++) {
            this.addsec(this.list[i]);
        }

        $(this.div).dialog(
            { autoOpen: false,
              width: 500,
              buttons: {
                  "Close": function() {
                      $(this).dialog("close");
                  }
              }});



        //this.scd =  $('#section_chooser_dialog');

        $(this.div).find('form.save').submit(function() {
            that.save();
            return false;
        });


        $(this.div).find('form.load').submit(function() {
            var name = that.$section_list.val();
            var sec = that.list.filter(function(s) { return s.name === name;})[0];
            that.events.triggerEvent('newSection',sec.val);
            return false;
        });

        $(this.div).find('form.load input.delete').click(function() {
            var name = that.$section_list.val();

            that.$section_list.find('option').filter(function() { return $(this).val() == name; }).remove();
            that.list = that.list.filter(function(s) { return s.name !== name;});
            localStorage.section = JSON.stringify(that.list);
        });


    },

    open: function() {
        $(this.div).dialog('open');
    },


    save: function(currentSection) {
        var sec = {name: this.$new_name.val(), val: this.currentSection};

        if (this.list.filter(function(s) { return s.name === sec.name;}).length > 0) {
            alert('Name already in use');
        }
        else {
            this.addsec(sec);
            this.list.push(sec);
            localStorage.section = JSON.stringify(this.list.filter(
                function(s) {
                    return !s.predefined;
                }));
        }
    },

    getSection: function(currentSection,bb) {
        this.currentSection = currentSection;
        this.currentBoundingBox = bb;

        console.log('scd ',$('#section_chooser_dialog input[value=Save]'),$(this.div).find('input[value=Save]'));

        $(this.div).find('input[value=Save]').prop('disabled',currentSection === null);

        //this.scd.dialog('open');
                this.open();
    }
});

OceanBrowser.Query = {};

// place a strong tag around query items
OceanBrowser.Query.highlightQuery = function(titles,query_re) {
    var k;

    if (query_re !== null) {
        for (k in titles) {
            titles[k] = titles[k].replace(query_re,'<strong>\$&</strong>');
        }
    }

    return titles;
};

OceanBrowser.Query.parse = function(q) {
    var l1, l2, coord, query = {}, kw;

    l1 = q.indexOf('[');
    l2 = q.indexOf(']');

    if (l1 !== -1 && l2 !== -1) {
        // parse [8,44] part of a query
        coord = q.substring(l1+1,l2).split(',').map(parseFloat);
        kw = (q.substring(0,l1) + ' ' + q.substring(l2+1));
        query.point = coord;
    }
    else {
        kw = q;
    }

    var keywords = kw.split(' ');

    // remove empty keywords
    query.keywords = keywords.filter(function(_) { return _.length !== 0; });
    console.log('p query ',query);
    return query;
}
