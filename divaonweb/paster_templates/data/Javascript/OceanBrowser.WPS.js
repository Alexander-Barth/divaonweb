/******************************************************************************
 *                                                                            *
 *  Copyright (C) 2014-2015 Alexander Barth <barth.alexander@gmail.com>.      *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU Affero General Public License as published  *
 *  by the Free Software Foundation, either version 3 of the License, or      *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                            *
 ******************************************************************************/

"use strict";


if (OceanBrowser === undefined) {
    var OceanBrowser = {};
}
OceanBrowser.WPS = {};

OceanBrowser.WPS.server = function(baseurl) {
    this.baseurl = baseurl;
};

OceanBrowser.WPS.server.prototype.execute = function(processid,params,callback,onerror) {
    var datainputs, data, url, that = this, tmp = [];

    for (var key in params) {
      if (params.hasOwnProperty(key)) {
        tmp.push(key + '=' + params[key])
      }
    }

    //datainputs = 'EDMO_code=632;LOCAL_CDI_ID=world_N50W10N40E0_20070101_20080101;parameter=PSSTTS01;clim0=10;clim1=20;colormap=jet';
    datainputs = tmp.join(';');

    data = {
        'service': 'wps',
        'request': 'Execute',
        'Identifier': processid,
        'version': '1.0.0'
    };

    //console.log(params,data);
    url = this.baseurl + '?' + 'DataInputs=[' + datainputs + ']';
    //url = 'http://dtvirt5.deltares.nl/wps?service=wps&request=Execute&Identifier=cdi_plot_timeseries&DataInputs=[EDMO_code=486;LOCAL_CDI_ID=FI35200345015_24FA0_H09;parameter=CPHLZZXX;clim0=0;clim1=2;color=brown;marker=^;markersize=12;alpha=0.2]&version=1.0.0'
    //url = 'http://dtvirt5.deltares.nl/wps?service=wps&request=Execute&Identifier=bbox_plot_map&DataInputs=[bbox=-10,5,45,50;parameter=PSLTZZ01;clim0=34;clim1=36;colormap=jet;]&version=1.0.0';
    //url = 'http://dtvirt5.deltares.nl/wps?service=wps&request=Execute&Identifier=cdi_plot_profile&DataInputs=[EDMO_code=486;LOCAL_CDI_ID=FI35200711007_00010_H09;parameter=NTRAZZXX;z=ADEPZZ01;]&version=1.0.0';
    //url = 'http://dtvirt5.deltares.nl/wps?service=wps&request=Execute&Identifier=bbox_plot_map&DataInputs=[bbox=-10,-5,45,50;parameter=PSLTZZ01;clim0=34;clim1=36;colormap=jet;]&responsedocument=mapname=@mimetype=application/vnd.google-earth.kmz&version=1.0.0';
    //url = 'http://dtvirt5.deltares.nl/wps?service=wps&request=Execute&Identifier=bbox_plot_profile&DataInputs=[bbox=-10,-5,45,50;parameter=PSLTZZ01;z=ADEPZZ01]&version=1.0.0';
    //url = 'http://dtvirt5.deltares.nl/wps?service=wps&request=Execute&Identifier=bbox_plot_timeseries&DataInputs=[bbox=-10,-5,45,50;parameter=PSLTZZ01;]&version=1.0.0';
    //data = {};

    // save URL and data for error message
    this.url = url;
    this.data = data;

    OpenLayers.Request.GET({
        url: url,
        params: data,
        success: function(resp) {
            //console.log('got WPS response',this,resp);
            var output = that.parseExecuteResponse(resp.responseXML);
            callback(output);
        },
        failure: function(ev) {
            alert('Failed to connect to ' + that.baseurl);
            if (onerror) {
                onerror(ev);
            }
        }
    });

}

OceanBrowser.WPS.server.prototype.parseExecuteResponse = function(xml)
{
    var data, image, mimetype, node, exception, message;

    //console.log('loaded');
    //console.log('xml',$(xml).find('ComplexData'));
    var ns = 'http://www.opengis.net/wps/1.0.0';
    //console.log('data',xml.getElementsByTagNameNS(ns, 'ComplexData'));

    if (xml === null) {
        return;
    }


    if (xml.getElementsByTagNameNS) {
        node = xml.getElementsByTagNameNS(ns, 'ComplexData')[0];
    }
    else {
        // for IE9
        node = xml.getElementsByTagName('wps:ComplexData')[0];
    }

    if (node === undefined) {
        if (xml.getElementsByTagNameNS) {
            exception = xml.getElementsByTagNameNS('http://www.opengis.net/ows/1.1','ExceptionText');
        }
        else {
            // for IE9
            exception = xml.getElementsByTagName('ows:ExceptionText');
        }

        message = 'Error while loading data';
        if (exception.length > 0) {
            message = exception[0].firstChild.data;
        }

        var url = Util.append_param(this.url,this.data);
        message += '\nURL: \n' + url;
        alert(message);
        return null;
    }
    //data = node.firstChild.data;
    // FF workaround https://bugzilla.mozilla.org/show_bug.cgi?id=194231
    data = '';
    for (var i=0; i < node.childNodes.length; i++)
    {
        data += node.childNodes[i].data;
    }

    mimetype = node.getAttribute('mimeType');

    //console.log('data.length',data.length);

    // Problem in FF
    //data = $(xml).find('ComplexData').text();
    //mimetype = $(xml).find('ComplexData').attr('mimeType');

    //console.log('data',data);

    //console.log('mimetype',mimetype);

    if (mimetype === 'application/vnd.google-earth.kmz') {
        var blob = b64toBlob(data,mimetype);
        unzipBlob(blob,function(data) {
            //console.log(data);
        });
    }

    return {data: data, mimetype: mimetype}
};


// new functions to replace the previous ones


OceanBrowser.WPS.server.prototype.execute2 = function(processid,params,callback,onerror) {
    var datainputs, data, url, that = this, tmp = [];

    for (var key in params) {
      if (params.hasOwnProperty(key)) {
        tmp.push(key + '=' + params[key])
      }
    }

    //datainputs = 'EDMO_code=632;LOCAL_CDI_ID=world_N50W10N40E0_20070101_20080101;parameter=PSSTTS01;clim0=10;clim1=20;colormap=jet';
    datainputs = tmp.join(';');

    data = {
        'service': 'wps',
        'request': 'Execute',
        'Identifier': processid,
        'version': '1.0.0'
    };

    //console.log(params,data);
    url = this.baseurl + '?' + 'DataInputs=[' + datainputs + ']';
    //url = 'http://dtvirt5.deltares.nl/wps?service=wps&request=Execute&Identifier=cdi_plot_timeseries&DataInputs=[EDMO_code=486;LOCAL_CDI_ID=FI35200345015_24FA0_H09;parameter=CPHLZZXX;clim0=0;clim1=2;color=brown;marker=^;markersize=12;alpha=0.2]&version=1.0.0'
    //url = 'http://dtvirt5.deltares.nl/wps?service=wps&request=Execute&Identifier=bbox_plot_map&DataInputs=[bbox=-10,5,45,50;parameter=PSLTZZ01;clim0=34;clim1=36;colormap=jet;]&version=1.0.0';
    //url = 'http://dtvirt5.deltares.nl/wps?service=wps&request=Execute&Identifier=cdi_plot_profile&DataInputs=[EDMO_code=486;LOCAL_CDI_ID=FI35200711007_00010_H09;parameter=NTRAZZXX;z=ADEPZZ01;]&version=1.0.0';
    //url = 'http://dtvirt5.deltares.nl/wps?service=wps&request=Execute&Identifier=bbox_plot_map&DataInputs=[bbox=-10,-5,45,50;parameter=PSLTZZ01;clim0=34;clim1=36;colormap=jet;]&responsedocument=mapname=@mimetype=application/vnd.google-earth.kmz&version=1.0.0';
    //url = 'http://dtvirt5.deltares.nl/wps?service=wps&request=Execute&Identifier=bbox_plot_profile&DataInputs=[bbox=-10,-5,45,50;parameter=PSLTZZ01;z=ADEPZZ01]&version=1.0.0';
    //url = 'http://dtvirt5.deltares.nl/wps?service=wps&request=Execute&Identifier=bbox_plot_timeseries&DataInputs=[bbox=-10,-5,45,50;parameter=PSLTZZ01;]&version=1.0.0';
    //data = {};

    // save URL and data for error message
    this.url = url;
    this.data = data;

    //var proxy_save = OpenLayers.ProxyHost;
    //OpenLayers.ProxyHost = null;

    OpenLayers.Request.GET({
        url: url,
        params: data,
        success: function(resp) {
            //console.log('got WPS response',this,resp);
            var output = that.parseExecuteResponse2(resp.responseXML);

            if (output !== null) {
                callback(output);
            }
            else {
                onerror('Failed parsing');
            }
        },
        failure: function(ev) {
            alert('Failed to connect to ' + that.baseurl);
            if (onerror) {
                onerror(ev);
            }
        }
    });
    //OpenLayers.ProxyHost = proxy_save;
}

OceanBrowser.WPS.server.prototype.parseExecuteResponse2 = function(xml)
{
    var data, image, mimetype, node, exception, message, result = {};

    //console.log('loaded');
    //console.log('xml',$(xml).find('ComplexData'));

    var namespaces = {
        'wps': 'http://www.opengis.net/wps/1.0.0',
        'ows': 'http://www.opengis.net/ows/1.1'
    }
    //console.log('data',xml.getElementsByTagNameNS(ns, 'ComplexData'));
    var getByTag = function(xml,prefix,tag) {
        if (xml.getElementsByTagNameNS) {
           node = xml.getElementsByTagNameNS(namespaces[prefix], tag);
        }
        else {
            // for IE9
            node = xml.getElementsByTagName(prefix + ':' + tag);
        }
        return node;
    }


    if (xml === null) {
        return null;
    }

    var outputs = getByTag(xml,'wps','Output');


    if (outputs.length === 0) {
        exception = getByTag(xml,'ows','ExceptionText');

        message = 'Error while loading data';
        if (exception.length > 0) {
            message = exception[0].firstChild.data;
        }

        var url = Util.append_param(this.url,this.data);
        message += '\nURL: \n' + url;
        alert(message);
        return null;
    }

    for (var i = 0; i < outputs.length; i++) {
        var id = getByTag(outputs[i],'ows','Identifier')[0].firstChild.data;
        var node = getByTag(outputs[i],'wps','ComplexData')[0];

        if (node) {

           //data = node.firstChild.data;
           // FF workaround https://bugzilla.mozilla.org/show_bug.cgi?id=194231

           data = '';
           for (var j=0; j < node.childNodes.length; j++) {
             data += node.childNodes[j].data;
           }

           mimetype = node.getAttribute('mimeType');

           if (mimetype === 'application/vnd.google-earth.kmz') {
              var blob = b64toBlob(data,mimetype);
              unzipBlob(blob,function(data) {
              //console.log(data);
              });
           }

           if (mimetype === "text/json") {
               data = JSON.parse(data);
           }

           result[id] = {
               data: data,
               mimetype: mimetype
           }
        }
        // check for other data types LiteralData,.... if necessary
    }

    return result;
};


function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
}

//  bbox W,E,S,N
OceanBrowser.WPS.viewer = function(div,server,params,plottype,disclaimer,onready) {
    var that = this, processid;
    this.div = div;
    this.server = server;
    this.params = params;
    this.plottype = plottype;
    this.disclaimer = disclaimer;
    this.onready = onready;

    // clone DOM with form
    this.$form = $('.WPS_form_template').clone();
    this.$new_entry = $('.metadata_list .entry').detach();

    this.$form.appendTo($(div));

    this.$loading = this.$form.find('.spinner');
    this.$output = this.$form.find('.output');
    this.$parameters = this.$form.find('select[name=parameter]');
    this.$type = this.$form.find('select[name=type]');
    this.$disclaimer = this.$form.find('.disclaimer');


    if (plottype !== undefined) {
        // hide this select tag
        this.$type.val('cdi_plot_' + plottype);
        // whole row
        this.$type.parent().parent().hide();
    }

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            this.$form.find('input[name=' + key + ']').val(params[key]);
        }
    }

    this.$form.submit(function(e) {
        e.preventDefault();

        var processid = that.$type.val();

        if (that.params.hasOwnProperty('bbox')) {
            processid = processid.replace('cdi','bbox');
        }

        that.requestPlot(processid);
        return false;
    });

    this.$loading.hide();

/*
    this.$form.find('input[name=y-axis]').change(function() {
        var auto = that.$form.find('input[name=y-axis]:checked').val() === 'auto';
        that.$form.find('input[name=clim0]')[0].disabled = auto;
        that.$form.find('input[name=clim1]')[0].disabled = auto;
    });
    this.$form.find('input[name=y-axis]').change();
*/


    this.$form.find('input[name=timerange]').change(function() {
        var auto = !that.$form.find('input[name=timerange]').prop('checked');
        that.$form.find('input[name=starttime]').prop('disabled',auto);
        that.$form.find('input[name=endtime]').prop('disabled',auto);
    });
    this.$form.find('input[name=timerange]').change();


    this.setParameters();

   this.$form.find('.config').click(function() {
       that.$form.find('.config_param').toggle();
   });

   //this.$form.find('.config_param').hide();
   this.$form.find('.hide_button').click(function(ev) {
       ev.stopPropagation();
       ev.preventDefault();
       that.$form.find('.config_param').hide();
   });


};

OceanBrowser.WPS.viewer.prototype.setParameters = function() {
    var that = this, processid, parameter;

    if (this.params.parameter) {
        parameter = this.params.parameter.split(',');
        parameter.forEach(function(_) {
            that.$parameters.append($('<option>').attr('value',_).append(_));
        });

        if (parameter.length === 1) {
            // hide complete row with this input
            this.$parameters.closest('label').hide();
        }

        this.$form.submit();

        return;
    }

    if (this.params.hasOwnProperty('bbox')) {
        processid = 'bbox_get_parameters';
    }
    else {
        processid = 'cdi_get_parameters';
    }

    this.server.execute2(processid,this.params,function(output) {
        //console.log(output);
        output['all_observations'].data.forEach(function(_) {
            that.$parameters.append($('<option>').attr('value',_.codes).append(_.prefTitle));
        });

        that.$form.submit();

        if (that.onready !== undefined) {
            that.onready();
        };
    });

};

OceanBrowser.WPS.viewer.prototype.requestPlot = function(processid) {
    var params, zname, that = this;

    zname = {'below_sea_surface': 'ADEPZZ01', 'below_sea_floor': ''};

    params = {z: zname['below_sea_surface']};
    /*params = {z: 'ADEPZZ01', starttime: '2006-01-01T00:00:00Z', endtime: '2015-01-01T00:00:00Z',
              'colormap': 'YlGnBu'}*/


    params = jQuery.extend(params,this.params);

    this.$loading.show();
    this.$output.empty();

    this.$form.find('.inputparam').each(function() {
        if (!this.disabled) {
            var key = $(this).attr('name');
            var val =  $(this).val();
            if (val !== "") {
                params[key] = val;
            }
            else {
                // if set exlicetly to empty do not even use default values
                delete params[key]
            }
        }
    });

    if (this.disclaimer === 'True') {
        this.$disclaimer.show();
    }
    else {
        this.$disclaimer.hide();
    }


    // check if time range is not on manual
    if (!this.$form.find('input[name=timerange]').prop('checked')) {
        delete params.starttime;
        delete params.endtime;
    }

    // add hours, minutes and seconds if necessary
    var timeparams = ['starttime','endtime'];
    for (var i=0; i < timeparams.length; i++) {
        var val = params[timeparams[i]];
        if (val !== undefined) {
            if (val.indexOf('T') === -1) {
                params[timeparams[i]]  = params[timeparams[i]] + 'T00:00:00Z';
            }
        }
    }

    this.server.execute2(processid,params,
       function(output) {
           that.showPlot(output['pngname'],output['cdiname'].data);
       },
       function(ev) {
           that.$loading.hide();
       }
    );
};


OceanBrowser.WPS.viewer.prototype.showPlot = function(output,cdilist) {
    var image, that = this;
    this.$loading.hide();

    if (output === null) {
        return;
    }

    this.$output.empty();
    $('.metadata_list ul').empty();

    if (cdilist.length === 0) {
        this.$output.append('<p>No observations at selected location.</p>');
        that.$form.find('.config_param').hide();
        return;
    }

    image = new Image();
    image.src = 'data:' + output.mimetype + ';base64,' + output.data;
    //image.src = URL.createObjectURL(b64toBlob(data,mimetype));
    this.$output.append(image);




    cdilist.forEach(function(entry) {
        var edmo = entry[0];
        var lcdi = entry[1];
        var urlmd = 'https://cdi.seadatanet.org/report/edmo/' + edmo + '/' + lcdi;

        var new_entry = that.$new_entry.clone();
        new_entry.find('a').attr('href',urlmd);
        new_entry.find('.edmo').html(edmo);
        new_entry.find('.lcdi').html(lcdi);
        new_entry.appendTo('.metadata_list ul');
    });
};



function unzipBlob(blob, callback) {
  // use a zip.BlobReader object to read zipped data stored into blob variable
  zip.createReader(new zip.BlobReader(blob), function(zipReader) {
    // get entries from the zip file
    zipReader.getEntries(function(entries) {
      // get data from the first file
      //entries[0].getData(new zip.BlobWriter("text/plain"), function(data) {
      entries[0].getData(new zip.TextWriter(), function(data) {
        // close the reader and calls callback function with uncompressed data as parameter
        zipReader.close();
        callback(data);
      });
    });
  }, onerror);
}
