/******************************************************************************
 *                                                                            *
 *  Copyright (C) 2008-2017 Alexander Barth <barth.alexander@gmail.com>.      *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU Affero General Public License as published  *
 *  by the Free Software Foundation, either version 3 of the License, or      *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                            *
 ******************************************************************************/

// making this a global variable so that it is accessible for
// debugging/inspecting in Firebug

var wms_url = "Python/web/wms";

/*
var baselayer_url = 'http://labs.metacarta.com/wms/vmap0?';
var baselayer_name = 'basic';
var baselayer_format = 'image/jpeg';
*/

var baselayer_url = 'Python/web/continents?';
var baselayer_name = 'cont';
var baselayer_format = 'image/png';


var dinputs = new Util.DocumentInputs();

var vsection = null;
var hsection = null;

var viewer = {};
var defaults = {
    //GlobalCDI_baseurl: "http://seadatanet.maris2.nl/v_cdi_v3/print_ajax.asp?n_code=",
    GlobalCDI_baseurl: "https://cdi.seadatanet.org/report/",
    //LocalCDI_baseurl: 'http://emodnet-chemistry.maris2.nl/v_cdi_v3/print_wfs.asp',
    LocalCDI_baseurl: 'https://emodnet-chemistry.maris.nl/report/edmo/',
    ContourExtractor_baseurl: '/ContourExtractor/cgi-bin/contour_extractor.cgi',
    //ContourExtractor_baseurl: '/cgi-bin/contour_extractor.cgi',
    WFS_baseurl: 'http://geoservice.maris2.nl/wfs/seadatanet/cdi_v2/simorc'
};


function preload_images() {
    var list = [
                "img/mini_colormap_gray.png",
                "img/mini_colormap_hsv.png",
                "img/mini_colormap_jet.png",
                "img/mini_colormap_Paired.png",
                "img/mini_colormap_RdBu.png"
                ];

    for (var i=0; i<list.length; i++) {
        var img = new Image();
        img.src = list[i];
    }
}




function add_layer(Layer) {
    var j;
    var option;

    dinputs.enable();

    if (Layer.id in hsection.analysis_wms) {
        // layer already added
        return;
    }


    hsection.addLayer(Layer);
    vsection.addLayer(Layer);

    // show a horizontal plot for default time and depth

    // layer changed
    hsection.update_all();

    _paq.push(['trackEvent', 'View', 'Layer', Layer.describe()]);
    _paq.push(['trackEvent', 'View', 'UUID', Layer.get_uuid()]);
}


function remove_layer(Layer) {
    hsection.removeLayer(Layer);
    vsection.removeLayer(Layer);
}


function update_view() {
    var index;

    if (jQuery.ui.version === '1.8.16') {
        index = viewer.$tabs.tabs('option','selected');
    }
    else {
        index = viewer.$tabs.tabs('option','active');
    }

    if (index == 0) {
        hsection.update_all();
    }
    else {
        vsection.update_all();
    }
}

OpenLayers.Control.CustomNavToolbar = OpenLayers.Class(OpenLayers.Control.Panel, {
        /**
         * Constructor: OpenLayers.Control.NavToolbar
         * Add our two mousedefaults controls.
         *
         * Parameters:
         * options - {Object} An optional object whose properties will be used
         *     to extend the control.
         */


        initialize: function(section,options) {
            OpenLayers.Control.Panel.prototype.initialize.apply(this, [options]);
            this.addControls([
                              new OpenLayers.Control.Navigation(),
                              new OpenLayers.Control.ZoomBox({alwaysZoom:true, autoActivate: false})
                              ]);
            // To make the custom navtoolbar use the regular navtoolbar style
            this.displayClass = 'olControlNavToolbar';
        },



        /**
         * Method: draw
         * calls the default draw, and then activates mouse defaults.
         */
        draw: function() {
            var div = OpenLayers.Control.Panel.prototype.draw.apply(this, arguments);
            this.controls[0].activate();
            return div;
        }
    });





function init() {
    var i,
        defaultServer,
        defaultLayers = [],
        defaultLayerIDs = [],
        Layer,
        // get query parameters
        query = new Util.query(window.location.search.substring(1));

    // use wms_url as default url or the value of the "server" query parameter

    defaultServer = {
        name: 'default',
        title: null,
        url: query.get("server",wms_url),
        version: query.get('version','1.3.0'),
        service: query.get('service','WMS'),
        autoload: true,
        hide: true,
        extension: 'OceanBrowser' };


    if (window.location.href.indexOf('emodnet/') > 0) {
        var config_server = SERVERS.filter(function(s) { return s.name == 'emodnet-chemical'})[0];
        jQuery.extend(defaultServer, config_server);
	config_server.autoload = false; // prevent from loading it twice
    }

    // will be undefined if server or servername is not prvided
    var query_server = query.get('server');
    var query_servername = query.get('servername');

    // get or guess extension
    if (query.has('extension')) {
        defaultServer.extension = query.get('extension','none');
    }
    else {
	  if (query.has('server')) {
	    // some heuristics
	    re = new RegExp('https?://[a-zA-Z0-9_.]*(gher-diva.phys.ulg.ac.be|oceanbrowser.net)/.*/Python/web/wms?');

	    if (re.test(query_server)) {
		  defaultServer.extension = 'OceanBrowser';
	    }
	    else {
		  defaultServer.extension = 'none';
	    }
	  }
    }

    // default server URL can also be given by its name (as specified in SERVERS)
    if (query.has("servername") || query.has('server')) {
        for (i in SERVERS) {
            SERVERS[i].autoload = false;

            if ((query.has("servername") && SERVERS[i].name === query.get("servername")) ||
                (query.has("server")     && query.get('server','').indexOf(SERVERS[i].url) === 0)) {
                // clone SERVERS[i]
                defaultServer = jQuery.extend({}, SERVERS[i]);
                defaultServer.autoload = true;
            }
        }
    }

    // additional parameter passed to getCapabilities request
    if (query.has("basedir")) {
        defaultServer.url = Util.append_param(defaultServer.url,{basedir: query.get("basedir")});
    }

    if (query.has("layers")) {
        defaultLayers = query.get("layers").split(',');
    }

    // not completely functional yet,
    // need to select all layers with the same ID
    // and present only those to the user
    if (query.has("layerIDs")) {
        defaultLayerIDs = query.get("layerIDs").split(',');
    }

    preload_images();
    hsection = new HorizontalSection();


    // OpenLayer map need to be visible while initializing, otherwise their width/height is zero
    $(".section.vertical").show();
    vsection = new VerticalSection();
    $(".section.vertical").hide();


    // set OpenLayers proxy
    OpenLayers.ProxyHost= "proxy?url=";

    // set OceanBrowser proxy
    OceanBrowser.WMSProxy = 'Python/web/bluemarble';


    $("#metadata_dialog").dialog({ autoOpen: false, width: 500});
    var $tabs = $("#tab").tabs();
    viewer.$tabs = $tabs;

    var tabindex = {'horizontal': 0, 'vertical': 1};
    $('#tab').on('tabsactivate', function(event, ui) {
        ui.index = $(this).tabs('option','active');
        if (ui.index === 0) {
            // horizontal section tab
        }
        else {
            // vertical section tab
            vsection.onvisible();
        }

    });


    $tabs.tabs('option', 'active', tabindex[query.get('view','horizontal')]);

    $('#metadata_dialog input').click(function() {
        $('#metadata_dialog').dialog('close');
    });


    // jQuery UI dialogs

    // dinputs.disable();


    viewer.reportProblem = new OceanBrowser.UI.reportProblem('OceanBrowser',
                                                             'Python/web/send_feedback',
                                                             hsection.Layers,
                                                             hsection.figprops
                                                            );

    viewer.about = new OceanBrowser.UI.About('OceanBrowser');
    viewer.settings = new OceanBrowser.UI.Settings('OceanBrowser');


    $('#add_layer_button').click(function() { viewer.chooseLayer.open(); });
    $('#button_about').click(function() { viewer.about.open(); });
    $('#button_settings').click(function() { viewer.settings.open(); });
    $('#button_report_problem').click(function() { viewer.reportProblem.open(); });

    // three line menu icon
    $('.controls .icon').click(function() { $('.controls .button').toggle(); } );

    // dialog for adding a layer

    var servers = [defaultServer].concat(SERVERS);

    viewer.chooseLayer = new OceanBrowser.UI.chooseLayer('add_Layer',servers,
                                                  {showSectionType: false,
                                                   showPreview: false});

    viewer.chooseLayer.events.register('layerSelected',null, function(s) {
        add_layer(s.field);
    });

    if (defaultLayers.length > 0 || defaultLayerIDs.length > 0) {

        // debug: to be changed to [0] again
        viewer.chooseLayer.caps[0].events.register('loaded', null, function(cap) {
                var Layer, i;
                $('#blanket').hide();

                for (i in defaultLayers) {
                    Layer = cap.getByName(defaultLayers[i]);
                    if (Layer) {
                        add_layer(Layer);
                    }
                    else {
                        console.log('Layer not found ',defaultLayers[i]);
                        alert('Layer not found: ' + defaultLayers[i]);
                    }
                }
            });
    }
    else {
        //viewer.chooseLayer.open();
        $('#blanket').hide();
    }


    $('#slide-sidebar').click(function() {
        hsection.map.updateSize()
    });


    $(".left-nav").click(function(event) {
        console.log(event.target.className);

        var section = event.target.className;

        if (section === "vertical" && Object.keys(hsection.analysis_wms).length === 0) {
            alert("Please select a layer first under 'horizontal section'.");
            return;
        }

        $(".left-nav li").removeClass("active");
        $(".section").hide();


        $(event.target.parentElement).addClass("active");
        $(".section." + section).show();

        if (section === "vertical") {
            vsection.onvisible();
        }
    });

    // debugging


    //viewer.chooseLayer.events.triggerEvent('layerSelected',
    //                                       {field: viewer.chooseLayer.caps[0].layer_infos[1]});

    //chooseServer.events.triggerEvent('serverSelected',SERVERS[6]);
    //chooseServer.events.triggerEvent('serverSelected',SERVERS[10]);
/*
ll = viewer.chooseLayer.caps[1].layer_infos[1]
 viewer.chooseLayer.events.triggerEvent('layerSelected',{field: ll});

*/
}
