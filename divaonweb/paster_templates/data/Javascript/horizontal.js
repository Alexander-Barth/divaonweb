/******************************************************************************
 *                                                                            *
 *  Copyright (C) 2008-2015 Alexander Barth <barth.alexander@gmail.com>.      *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU Affero General Public License as published  *
 *  by the Free Software Foundation, either version 3 of the License, or      *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                            *
 ******************************************************************************/

// HorizontalSection extends anim class

HorizontalSection.prototype = new anim();
HorizontalSection.prototype.constructor = HorizontalSection;

function HorizontalSection() {
    anim.call(this,
              "map",
              document.getElementById('map_colorbars')
              );

    this.downloadFormats.push('application/vnd.google-earth.kmz');

    this.wms_url = "Python/web/wms?";

    // theme is null because it will be include in the html page
    // this allows to override some style elements
    this.map = new OpenLayers.Map('map', {tileSize: new OpenLayers.Size(512,512),
                                          theme: null} );

    // fix zoom level or not
    this.zoom = true;

    // progressbar instance for loading tiles
    this.progressbar = new MapProgressbar("progressbar",this.map);


    /*    var resolutions = [1.40625,0.703125,0.45,0.3515625,0.17578125,0.087890625,0.0439453125,0.02197265625,0.010986328125,0.0054931640625,0.00274658203125,0.00137329101,0.0007346938775510204];*/

/*    var resolutions = [1.40625,0.703125,0.3515625,0.17578125,0.087890625,0.0439453125,0.02197265625,0.010986328125,0.0054931640625,0.00274658203125,0.00137329101,0.0007346938775510204];*/

    var resolutions = [1.40625,0.703125,0.3515625,0.17578125,
                       0.087890625,0.0439453125,0.02197265625,0.010986328125];


    this.map.addLayers(
        [
            new OpenLayers.Layer.WMS("GEBCO",
                                     "https://www.gebco.net/data_and_products/gebco_web_services/web_map_service/mapserv?" ,
                                     {layers: 'GEBCO_Latest'},
                                     {resolutions:
                                      [360/Math.pow(2,8),360/Math.pow(2,9),360/Math.pow(2,10),
                                       360/Math.pow(2,11),360/Math.pow(2,12),360/Math.pow(2,13),
                                       360/Math.pow(2,14),360/Math.pow(2,15),360/Math.pow(2,16),
                                       360/Math.pow(2,17),360/Math.pow(2,18),360/Math.pow(2,19)
                                      ],
                                      isBaseLayer: true}),
            new OpenLayers.Layer.WMS("NASA Blue marble",
                                     "Python/web/bluemarble?",
                                     {layers: 'BMNG', format: 'image/png',
                                      exceptions: 'application/vnd.ogc.se_blank'},
                                     {isBaseLayer: true,
                                      resolutions: resolutions,
                                      buffer: 0 }) //,
/*          new OpenLayers.Layer.WMS("Continents",baselayer_url,
                                     {layers: baselayer_name,
                                      format: baselayer_format,
                                      exceptions: 'application/vnd.ogc.se_xml'},
                                     {resolutions: resolutions,
                                      isBaseLayer: true}),*/
/*
            new OpenLayers.Layer.WMS("Demis Baselayer", "http://www2.demis.nl/wms/wms.ashx?WMS=WorldMap",
                                     {layers:'Countries,Bathymetry,Topography,Hillshading,Coastlines,Builtup+areas,Waterbodies,Rivers',
                                      format: 'image/png'},
                                     {isBaseLayer: true}),
            new OpenLayers.Layer.WMS("Demis Blue Marble Baselayer","http://www2.demis.nl/wms/wms.ashx?WMS=BlueMarble" ,
                                     {layers: 'Earth Image'},
                                     {isBaseLayer: true})*/
        ]);

    //this.map.addControl(new OpenLayers.Control.LayerSwitcher());

    // do not use the default zoom
    //this.map.removeControl(this.map.getControlsByClass("OpenLayers.Control.Zoom"));
    var cc = this.map.getControlsByClass("OpenLayers.Control.Zoom")
    var dom = OceanBrowser.UI.makedom;
    //cc.div.appendChild(dom('a',{'class': 'olButton zoomlayers'},""))

    this.map.addControl(new OpenLayers.Control.CustomNavToolbar(this));
    this.map.addControl(new OpenLayers.Control.MousePosition());

    //this.map.zoomToMaxExtent();
    // zoom over Europe and adjacent seas
    this.map.zoomToExtent(new OpenLayers.Bounds(-70,0,90,85),true);
    // debug
    //this.map.zoomToExtent(new OpenLayers.Bounds(4, 42, 5,  43));

    var obj = this;

    var style = OpenLayers.Util.applyDefaults({strokeWidth: 2, strokeColor: "black"},
                                              OpenLayers.Feature.Vector.style["default"]);

    var control = new OpenLayers.Control();
    OpenLayers.Util.extend(control, {
            draw: function () {
                // this Handler.Box will intercept the shift-mousedown
                // before Control.MouseDefault gets to see it
                this.box = new OpenLayers.Handler.Box( control,
                                                       {"done": this.notice}
                                                       ,{keyMask: OpenLayers.Handler.MOD_SHIFT}
                                                       );
                this.box.activate();
            },

                notice: function (bounds) {
                var ll = this.map.getLonLatFromPixel(new OpenLayers.Pixel(bounds.left, bounds.bottom));
                var ur = this.map.getLonLatFromPixel(new OpenLayers.Pixel(bounds.right, bounds.top));

                /*
                  alert(ll.lon.toFixed(4) + ", " +
                  ll.lat.toFixed(4) + ", " +
                  ur.lon.toFixed(4) + ", " +
                  ur.lat.toFixed(4));
                */

                console.log(ll.lon,ur.lon,ll.lat,ur.lat);
            }
        });



    this.map.addControl(control);

    // botton to change zoom level to include all layers
    var panel = new OpenLayers.Control.Panel({
        displayClass: "ZoomAllLayersVisiblePanel"}
    );
    var button = new OpenLayers.Control.Button({
        displayClass: "ZoomAllLayersVisible",
        title: 'Change zoom to include all layers',
        trigger: function() {
            obj.map.zoomToExtent(obj.get_bounds());
        }
    });
    panel.addControls([button]);
    this.map.addControl(panel);
    //this.map.addControl(button);

    //temporary hack
    this.map.events.register('click',this,this.request_feature_info);


    // hack
    var that = this;
    this.selectFeature = new OpenLayers.Control.SelectFeature([]);



    this.selectFeature.events.register("featurehighlighted", this, function(e) {
            var id, html = '';
            //console.log('e',e);

            //var html = this.presentFeature(e.feature);

            for (id in that.Layers) {
                //console.log(id,that.Layers[id].feature === e.feature);

                if (that.Layers[id].layer === e.feature.layer) {
                    html += that.Layers[id].presentFeature(e.feature);
                }

            }

            var popup = new OpenLayers.Popup.FramedCloud(
                "FeaturePopup",
                e.feature.geometry.getBounds().getCenterLonLat(),
                null,
                html,
                null, true,
                function(e) {
                    that.selectFeature.unhighlight(that.selectedFeature);
                    that.selectFeature.unselect(that.selectedFeature);
                    e.stopPropagation();
                }
            );



            e.feature.popup = popup;
            that.selectedFeature = e.feature;
            that.map.addPopup(popup);
            //$('#toto').append(html);
	    //$('#toto').append("test aa");
        });

        this.selectFeature.events.register("featureunhighlighted", this, function(e) {
            if (e.feature.popup !== null) {
                this.map.removePopup(e.feature.popup);
                e.feature.popup.destroy();
                e.feature.popup = null;
            }
        });

        this.map.addControl(this.selectFeature);
        this.selectFeature.activate();


}

HorizontalSection.prototype.addLayer = function(Layer) {
    anim.prototype.addLayer.call(this,Layer);

    document.querySelector("input#slide-colorbar[type=checkbox]").checked = false;
    document.querySelector("input#slide-colorbar[type=checkbox] ~ label").style.display = "block";

    var id = Layer.id;
    var obj = this;


    // query depth dimension and add to dialog if appropriate

    var depths = Layer.get_dimension('elevation');

    if (depths) {
       //if (depths.range.length > 0) {
              this.figprops[id].set_dimension('elevation','depth',depths);
       //}
    }

    // query time dimension and add to dialog if appropriate

    var time = Layer.get_dimension('time');

    if (time) {
        this.figprops[id].set_dimension('time','time',time);

        // only for single time dimensions for now
        if (time.range.length > 0) {



            this.Layers[id].anim = new AnimLayer(
                                             this.figprops[id].button_anim,
                                             this.figprops[id].dimensions['time'].sel,
                                             this,id);
        }
    }

       // do something more nicer here
       // || ((Layer.cap || {}).WFS || {}).layerTree

    if (Layer.WMS_extensions == 'box-select' || Layer.WMS_extensions == 'box-select-wfs'
    || (Layer.cap || {}).selectBox
      ) {
        console.log('Layer.ext',Layer.WMS_extensions);

        var style = OpenLayers.Util.applyDefaults({strokeWidth: 2, strokeColor: "green", fillOpacity: 0.3},
                                                  OpenLayers.Feature.Vector.style["default"]);

        this.Layers[id].vectors = new OpenLayers.Layer.Vector("Vector Layer", {style: style});
        this.Layers[id].vectors.setZIndex(this.map.Z_INDEX_BASE.Feature);
        this.map.addLayer(this.Layers[id].vectors);

        //control = new OpenLayers.Control.DrawFeature(this.vectors,OpenLayers.Handler.Box, {handlerOptions: {style: style}});

        this.Layers[id].control = new OpenLayers.Control();
        var control = this.Layers[id].control;
        var layer = this.Layers[id];
        layer.download_url = null;
        layer.selected_region = null;

        OpenLayers.Util.extend(this.Layers[id].control, {
            draw: function () {
                // this Handler.Box will intercept the shift-mousedown
                // before Control.MouseDefault gets to see it
                this.box = new OpenLayers.Handler.Box(
                	layer.control,
                    {"done": this.notice}
                    //,{keyMask: OpenLayers.Handler.MOD_SHIFT}
                );
                //this.box.activate();
            },

            notice: function (bounds) {
            	// check if bounds is a rectangle
            	// return directly if this is not the case (a pixel for instance)
            	if (bounds.left === undefined) {
            		return;
            	}
                var ll = this.map.getLonLatFromPixel(new OpenLayers.Pixel(bounds.left, bounds.bottom));
                var ur = this.map.getLonLatFromPixel(new OpenLayers.Pixel(bounds.right, bounds.top));
                layer.vectors.destroyFeatures();

                console.log(ll.lon,ur.lon,ll.lat,ur.lat);

                var bounds2 = new OpenLayers.Bounds(ll.lon,ll.lat,ur.lon,ur.lat);
                var box = new OpenLayers.Feature.Vector(bounds2.toGeometry());
                layer.vectors.addFeatures(box);

                layer.selected_region = {ll: ll, ur: ur};

                if (layer.cap) {
                    if (layer.cap.selectBox) {
                       layer.cap.selectBox(obj.map,layer,[ll.lon,ur.lon,ll.lat,ur.lat]);
                    }
                }

                //window.open(layer.download_url,'Data Download');
            }
        });


        this.map.addControl(this.Layers[id].control);

        // activate or not the select box feature
        var element = this.figprops[id].box_select;
        element.onclick = function(element) {
            return function() {
                if (element.checked) {
                    layer.control.box.activate();
                }
                else {
                	// remove any existing bboxes
                	layer.vectors.destroyFeatures();
                    layer.control.box.deactivate();
                }
            };
        }(element);


        if (Layer.WMS_extensions == 'box-select') {
            this.figprops[id].download_callback = function() {
                if (layer.selected_region) {
                    var ll = layer.selected_region.ll;
                    var ur = layer.selected_region.ur;

                    var step='005' + ll.lon + '|'  + ur.lat + '|'  + ur.lon + '|'  + ll.lat;
                    //layer.download_url = 'http://emodnet-chemistry.maris2.nl/v_cdi_v3/browse_step.asp?step=' + step;
                    //TODO CHECK
                    layer.download_url = 'https://emodnet-chemistry.maris.nl/search?step=' + step;
                    console.log('url',layer.download_url);

                    window.open(layer.download_url,'Download');
                }
                else {
                    alert('Please select a domain first.');
                }
            };
        }

        if (Layer.WMS_extensions == 'box-select-wfs' ) {
            this.figprops[id].button_metadata.onclick = function() {
                if (layer.selected_region) {

                    var ll = layer.selected_region.ll;
                    var ur = layer.selected_region.ur;
                    console.log(ll.lon,ur.lon,ll.lat,ur.lat);

                    var xsl = Util.loadXMLDoc("feature-maris-box.xsl");

		    var params = {
			service: 'WFS',
			version: '1.0.0',
			//version: '1.1.1',
			request: 'getfeature',
			outputformat: 'gml3',
			typename: layer.name,
			bbox: [ll.lon,ll.lat,ur.lon,ur.lat].join(','),
			//maxfeatures: 10
			maxfeatures: $('#max_wfs_features').val()
		    };

                    document.getElementById("metadata_content").innerHTML = 'Loading ...';

                    var metadata_handle = function(resp) {
                        var xml = resp.responseXML;
                        var elem = document.getElementById("metadata_content");


                        // remove previous results
                        if ( elem.hasChildNodes() ) {
                            while ( elem.childNodes.length >= 1 ) {
                                elem.removeChild(elem.firstChild);
                            }
                        }

                        document.getElementById("metadata_content").innerHTML = 'A search returns maximum ' + params.maxfeatures + ' hits. This can be changed in "Settings"';

                        // add new results
                        Util.XSLTransform(xml,xsl,elem);
                        $('#metadata_dialog').dialog('open');
                    };

                    new OpenLayers.Request.GET({url: defaults.WFS_baseurl, params: params, callback: metadata_handle});

                }
                else {
                    alert('Please select a domain first.');
                }
            };
        }

    }

    // zoom if the first layer is added (except base layers)
    this.zoom = this.map.getNumLayers() == 2;
};

HorizontalSection.prototype.removeLayer = function(Layer) {
    var id = Layer.id;

    if (Layer.WMS_extensions == 'box-select' | Layer.WMS_extensions == 'box-select-wfs') {
        this.Layers[id].vectors.destroyFeatures();
        this.map.removeLayer(this.Layers[id].vectors);
        this.map.removeControl(this.Layers[id].control);
    }


    anim.prototype.removeLayer.call(this,Layer);

    this.map.removeLayer(this.analysis_wms[id]);
    delete this.analysis_wms[id];


    // update selectFeature control
    this.updateControlFeature();

    // hide side if no layer are present
    if (Object.keys(this.analysis_wms).length === 0) {
        document.querySelector("input#slide-colorbar[type=checkbox]").checked = true;
        document.querySelector("input#slide-colorbar[type=checkbox] ~ label").style.display = "none";
    }
};


HorizontalSection.prototype.onresize = function() {

};

HorizontalSection.prototype.update_all = function() {
    var depth;
    var time;
    var obj = this;
    var params;

    for (var id in this.Layers) {
        // query first colorbar-range

        depth = this.figprops[id].get_dimension('elevation');
        time = this.figprops[id].get_dimension('time');
        params = {};

        if (depth !== "") {
            params.elevation = depth;
        }

        if (time !== "") {
            params.time = time;
        }

        params = WMSParams(params);
        this.progressbar.start('stat-' + id);

        if (this.Layers[id].WMS_extensions == 'OceanBrowser') {
            params.layer = this.Layers[id].name;
            params.request = "GetStats";
            params.service = "WMS";
            params.version = "1.3.0";

            // closure around id
            var fun = function(id,zoom) {
                return function(xmldoc) {
                    obj.stat(xmldoc,zoom,id);
                };
            }(id,this.zoom);

            Util.proxy_ajax(this.Layers[id].wms_url,params,null,fun,{proxy: this.Layers[id].use_proxy});
        }
        else if (this.Layers[id].WMS_extensions == 'ncWMS') {

            // example
            // http://oceanotrondemo0.ifremer.fr/oceanotron/wms?request=GetMetadata&item=minmax&layers=SEADATANET_TS_ATLANTIC/Temperature&version=1.1.1&bbox=-126,-32.8125,54,107.8125&srs=EPSG:4326&crs=EPSG:4326&colorby/time=2012-03-25T00:00:00.000Z&time=2012-03-24T12:00:00.000Z/2012-03-26T01:00:00.000Z&colorby/depth=125&elevation=0/250&height=100&width=100

            param = jQuery.extend(params,{
                "layers": this.Layers[id].name,
                "request": "GetMetadata",
                "item": "minmax",
                "bbox": this.Layers[id].bounding_box.join(','),
                "version": "1.1.1",
                "srs": "EPSG:4326",
                "crs": "EPSG:4326",
                "colorby/time": "2012-03-25T00:00:00.000Z",
                "colorby/depth": "125",
                "height": 100,
                "width": 100});

            // closure around id
            var fun = function(id,zoom) {
                return function(request) {
                    console.log('request ',request);
                    obj.stat(request.responseText,zoom,id);
                };
            }(id,this.zoom);

            var request = OpenLayers.Request.GET({
               url: this.Layers[id].wms_url,
                params: params,
                callback: fun
            });

        }
        else {
            // add layer directly

            this.stat(null,this.zoom,id);
        }

    }
};

HorizontalSection.prototype.get_layer_param = function(id) {
    //console.log('get_layer_param',id);

    return this.figprops[id].get_layer_param();
};


// should name it to get_layers (as it can be also WFS)

HorizontalSection.prototype.get_wms_layer = function(id,options) {
    //console.log('get_wms_layer',id);

    var analysis_params = this.figprops[id].get_layer_param();

    // override by parameters in options
    for (var key in options) {
        analysis_params.param[key] = options[key];
    }

    return this.Layers[id].get_layer(analysis_params);
};




HorizontalSection.prototype.stat = function(xmldoc,zoom,id) {
    var obj, data, range;

    this.progressbar.end('stat-' + id);
    //alert('stat ' + id);
    //console.log('stat',id);

    if (jQuery.isXMLDoc(xmldoc)) {
        var xmin = xmldoc.getElementsByTagName("xmin")[0].firstChild.nodeValue;
        var xmax = xmldoc.getElementsByTagName("xmax")[0].firstChild.nodeValue;
        var ymin = xmldoc.getElementsByTagName("ymin")[0].firstChild.nodeValue;
        var ymax = xmldoc.getElementsByTagName("ymax")[0].firstChild.nodeValue;

        this.figprops[id].set_stat(xmldoc);
    }
    else if (typeof xmldoc === 'string' || xmldoc instanceof String) {
        obj = JSON.parse(xmldoc);

        if (obj.scaleRange) {
            range = parseFloat(obj.scaleRange);
        }
        else {
            // for item=minmax
            range = [obj.min,obj.max];
        }

        data = [
            {'default': parseFloat(range[0]),
             'id': 'vmin',
             'label': "Minimum color-bar range",
             'type': "float"
            },
            {'default': parseFloat(range[1]),
             'id': "vmax",
             'label': "Maximum color-bar range",
             'type': "float"
            }
        ];

        this.figprops[id].set_stat(data);
    }

    if (zoom) {
        var bounds = this.get_bounds();
        this.map.zoomToExtent(bounds);
    }

    this.update_layer(id);
};


HorizontalSection.prototype.updateControlFeature = function() {
    var layers = [];
    for (var id in this.Layers) {
        if (this.Layers[id].feature) {
            layers.push(this.Layers[id].layer);
        }
    }
    this.selectFeature.setLayer(layers);
};

// update_layer is called when style, elevation or time is changed
// it is called when
// - when a layer is added to a map
// - when user clicks on update
// - when FigureProperties window is closed

HorizontalSection.prototype.update_layer = function(id) {

    if (!(id in this.analysis_wms)) {
        this.analysis_wms[id] = this.get_wms_layer(id);
        this.map.addLayers([this.analysis_wms[id]]);

        // make sure that the vector layer is ontop
        if (this.Layers[id].vectors) {
            // index is 0-based
            this.map.setLayerIndex(this.Layers[id].vectors,this.map.layers.length-1);
        }

        // update selectFeature control
        this.updateControlFeature();
    }
    else {
        // update a WMS layers
        if (!(this.Layers[id] instanceof WFSLayerInfo)) {
           var options = this.get_layer_param(id);

           options.param = WMSParams(options.param);
           this.analysis_wms[id].mergeNewParams(options.param);
           this.analysis_wms[id].mergeNewParams({styles: options.style});
           this.analysis_wms[id].redraw();
        }
        else {
           this.map.removeLayer(this.analysis_wms[id]);
           this.analysis_wms[id] = this.get_wms_layer(id);
           this.map.addLayers([this.analysis_wms[id]]);

           // update selectFeature control
           this.updateControlFeature();
        }
    }

};


// download current view

HorizontalSection.prototype.download = function(id,opt) {
    var layer = this.Layers[id];
    var lp = this.figprops[id].get_layer_param();
    // same extend as current view
    var extent = this.map.getExtent();

    var bounds = new OpenLayers.Bounds(opt.xmin,opt.ymin,opt.xmax,opt.ymax);

    var layers = layer.name;
    var styles = this.figprops[id].get_style();

    var params =
      {layers: layers,
       request: "GetMap",
       service: "WMS",
       width: opt.width,
       height: opt.height,
       bbox: bounds.toBBOX(),
       transparent: true,
       decorated: true,
       crs: 'CRS:84',
       version: '1.3.0',
       styles: styles,
       format: opt.format
      };


    if (lp.param.elevation !== undefined) {
        params.elevation = lp.param.elevation;
    }

    if (lp.param.time !== undefined) {
        if (opt.format.slice(0,5) === 'video') {
            // for a video get all time slices
            params.time = this.Layers[id].get_dimension('time').range.join(',');
        }
        else {
            params.time = lp.param.time;
        }
    }

    params.title = Util.figureTitle(layer,params);

    console.log('params2 ',params);
    var url = Util.append_param(this.Layers[id].wms_url,params);
    window.open(url,'Image');
};


HorizontalSection.prototype.dburl = function (id) {
    return defaults.GlobalCDI_baseurl + id.replace('SDN:CDI:','');
};

HorizontalSection.prototype.dbURLLocalCDI = function (id) {
    var ind, EMDO, LCDI, baseurl;
    if (id.startsWith('Cruise')) {
        return null;
    }

    ind = id.indexOf('-');
    if (ind > 0) {
        EDMO = id.substring(0,ind);
        LCDI = id.substring(ind+1);
        //return Util.append_param(defaults.LocalCDI_baseurl,{popup: 'yes', edmo: EDMO, identifier: LCDI});
        return defaults.LocalCDI_baseurl + EDMO + "/" + LCDI;
    }


//http://emodnet-chemistry.maris2.nl/v_cdi_v2/print_wfs.asp?popup=yes&edmo=486&identifier=FI35197600361_00250_H09
//with edmo = EDMO_code of data provider (486 = IFREMER) and identifier = Local_CDI_ID
}

HorizontalSection.prototype.show_feature_info = function (response,id,latlon,format) {
    var html;
    var nodata;

    function roundr(val,range) {
        digits = Math.ceil(log10(range)+2);
        return Util.round(val,digits);
    }

    nodata = false;
    console.log('got',id);


    // extract information from response.responseXML and format it

    //var xmldoc = response.responseXML;
    // proxy_ajax returns directly an xmldoc
    var xmldoc = response;

    if (format == 'text/xml') {
        var val;
        var digits;

        var lon = parseFloat(xmldoc.getElementsByTagName("longitude")[0].firstChild.nodeValue);
        var lat = parseFloat(xmldoc.getElementsByTagName("latitude")[0].firstChild.nodeValue);

        // round lon and lat based on their range
        var e = this.map.getExtent();
        lon = roundr(lon,e.getWidth());
        lat = roundr(lat,e.getHeight());


        var xmlval = xmldoc.getElementsByTagName("value")[0].firstChild;
        if (xmlval) {
            val = parseFloat(xmlval.nodeValue);

            var style_param = this.figprops[id].get_style_param();

            var range = style_param.vmax - style_param.vmin;
            val = roundr(val,range);
        }
        else {
            nodata = true;
            val = "no data";
        }

        var $domfrag = $('<table/>').append(
                $('<tbody/>').append(
                    $('<tr/>').append(
                        $('<td/>').append('longitude:'),
                        $('<td/>').append(remove_spurious_decimals(lon))
                    ),
                    $('<tr/>').append(
                        $('<td/>').append('latitude:'),
                        $('<td/>').append(remove_spurious_decimals(lat))
                    ),
                    $('<tr/>').append(
                        $('<td/>').append('value:'),
                        $('<td/>').append(remove_spurious_decimals(val))
                    )
                )
            )

        //this.figprops[id].set_info($domfrag);
        html = $domfrag.prop('outerHTML');
    }
    else if (format == 'application/vnd.ogc.gml' || format == 'text/html') {
        if (format == 'application/vnd.ogc.gml') {

            html  = '<div>';
            html += '<table><tbody>';

            var features = xmldoc.getElementsByTagName('REPHY-EDMODNET_feature');
            if (features.length === 0) {
                features = xmldoc.getElementsByTagName('feature');

                if (features.length === 0) {
                    return;
                }
            }

            var item = features[0].firstChild;
            if (!item) {
                return;
            }


            var tagsname = {'longitude': '', 'latitude': '', 'PLATFORM': '', 'IDENT': ''}, title,
            name, url, time, depth, CDI, CDI_elem, ids, data_elem;

            // regular expression matching for example URL_NO2_d
            var re = /URL_(.*)_D/;

            while (item !== null) {
                if (item.tagName) {
                    if (item.tagName in tagsname && item.firstChild) {
                        html += '<tr><td>' + item.tagName + '</td><td>' + item.firstChild.data + '</td></tr>';
                    }
                    else if (item.tagName == 'DataURL' && item.firstChild) {
                        title = item.getElementsByTagName("Title");
                        if (title.length > 0) {
                            html += '<tr><td>parameter</td><td>' + title[0].firstChild.data; + '</td></tr>';
                        }

                        time = item.getElementsByTagName("time");
                        if (time.length > 0) {
                            time = time[0].firstChild.data;
                            time = time.replace(/T00:00:00/g,'');
                            html += '<tr><td>time</td><td>' + time + '</td></tr>';
                        }

                        depth = item.getElementsByTagName("depth");
                        if (depth.length > 0) {
                            html += '<tr><td>depth (m)</td><td>' + depth[0].firstChild.data + '</td></tr>';
                        }

                        // value can be an empty node <value/> if the analysis is masked at the
                        // corresponding location
                        value = item.getElementsByTagName("value");
                        if (value.length > 0) {
                            if (value[0].firstChild) {
                                html += '<tr><td>value</td><td>' + value[0].firstChild.data; + '</td></tr>';
                            }
                            else {
                                nodata = true;
                                html += '<tr><td>value</td><td>no data</td></tr>';
                            }
                        }


                        url = item.getElementsByTagName("OnlineResource");
                        if (url.length > 0) {
                            html += '<tr><td>plot</td><td><a href="' + url[0].getAttribute("href") + '" target="_blank">Image</a></td></tr>';
                        }

                        CDI_elem = item.getElementsByTagName("CDI");

                        if (CDI_elem.length > 0) {
                            CDI = CDI_elem[0].firstChild.data;
                            ids = CDI.split(',');

                            var CDI_list = '';
                            for (var i=0; i < ids.length; i++) {
                                CDI_list += '<a href="' + this.dburl(ids[i]) + '" target="_blank">' + ids[i] + '</a>\n';
                            }

                            html += '<tr><td>CDI metadata</td><td>' + CDI_list + '</td></tr>';
                        }

                        data_elem = item.getElementsByTagName("data");
                        if (data_elem.length > 0) {
                            html += '<tr><td>data access</td><td><a href="' + data_elem[0].firstChild.data + '" target="_blank">download</a></td></tr>';
                        }

                        html += '<tr class="emptyRow"><td></td><td></td></tr>';
                    }
                    else {
                        var m = re.exec(item.tagName);

                        if (m && item.firstChild) {
                            html += '<tr><td>' + m[1] + '</td><td><a href="' + item.firstChild.data + '" target="_blank">Image</a></td></tr>';
                        }

                    }
                }
                item = item.nextSibling;
            }

            html += '</tbody></table></div>';
        }
        else {
            html = response;

            if (html == "") {
                nodata = true;
            }
        }

    }

    if (nodata) {
        // show nothing
       return;
    }

    // for large popup use a dialog
    if (this.Layers[id].cap.popup === 'large') {
        var $diag = $('<div>').attr('title',this.Layers[id].title.replace('Number of ','').replace(' observations',''))
        $(document.body).append($diag);
        $diag.append(html);
        $diag.dialog({width: 840});
    }
    else {
      anim.prototype.show_feature_info.call(this,html,id,latlon);
    }


};
