/******************************************************************************
 *                                                                            *
 *  Copyright (C) 2014,2015 Alexander Barth <barth.alexander@gmail.com>.      *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU Affero General Public License as published  *
 *  by the Free Software Foundation, either version 3 of the License, or      *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                            *
 ******************************************************************************/

"use strict";

OpenLayers.ProxyHost = "proxy?url=";
//zip.workerScriptsPath = "zipjs/WebContent/";

//var baseurl = 'http://dtvirt5.deltares.nl/wps';
var baseurl = 'http://emodnet02.cineca.it/wps';

var WPSServer = new OceanBrowser.WPS.server(baseurl);
var WPSviewer;

$(document).ready(function()
{
    var query = new Util.query(window.location.search.substring(1));
    var params = query.param;
    var plottype, cdis, disclaimer;

    // this is a non WPS parameter
    if (params['plottype'] !== undefined) {
        plottype = params['plottype'];
        delete params['plottype'];
    }

    if (params['disclaimer'] !== undefined) {
        disclaimer = params['disclaimer'];
        delete params['disclaimer'];
    }

    // cdis used to be a parameters for cdi list
    // can be removed in future
    if (params['cdis'] !== undefined) {
        cdis = params['cdis'];
        delete params['cdis'];
    }

    WPSviewer = new OceanBrowser.WPS.viewer(document.getElementById('wps'),WPSServer,params,plottype,disclaimer);  

});


//    http://dtvirt5.deltares.nl/wps?service=wps&request=Execute&Identifier=cdi_get_parameters&DataInputs=[EDMO_code=486;LOCAL_CDI_ID=FI35200404001_00610_H09]&version=1.0.0

