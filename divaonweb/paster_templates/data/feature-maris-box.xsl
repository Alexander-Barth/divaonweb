<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:ms="http://mapserver.gis.umn.edu/mapserver"
	xmlns:gml="http://www.opengis.net/gml"
	xmlns:ogc="http://www.opengis.net/ogc"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
<xsl:param name="pagelen" select="25"/>
<xsl:template match="/">

	<xsl:if test='count(//gml:featureMember/* | //gml:featureMembers/*) > 0'>

		<table width='99%' border='0' cellspacing='0' cellpadding='0'>
		<tr>
		  <xsl:choose>
			<xsl:when test='count(//gml:featureMember/* | //gml:featureMembers/*) = 0'>
			  <td class='buttons' align='left'>
				No results found.
			  </td>
			</xsl:when>
			<xsl:otherwise>
			  <td class='buttons' align='right'>
				<div id='browse_buttons'>
				  Found <xsl:value-of select='count(//gml:featureMember/* | //gml:featureMembers/*)'/>
				</div>
			  </td>
			</xsl:otherwise>
		  </xsl:choose>
		</tr>
		</table>

<!-- Resultaten Mapserver: gml:featureMember, Geoserver: gml:featureMembers :-( -->

		<xsl:for-each select="//gml:featureMember/* | //gml:featureMembers/*">
		<table class='infowindow-browse-table' border='0' cellpadding='0' cellspacing='0'>
		  <tr class='browse-subject'>
			<td colspan='2'>&#32;</td>
		  </tr>

<!-- Laat alles zien behalve de GML (sla elementen over die een element gml:* bevatten) -->

		<xsl:for-each select="*[not(gml:*)]">
			<xsl:element name="tr">

			  <td class='browse_data'>
				<xsl:value-of select="local-name(.)"/>:
			  </td>

			  <td class='browse_data'>


				<xsl:choose>

<!-- maak url clickable -->
				  <xsl:when test='starts-with(., "http://")'>
					<xsl:element name='a'>
					  <xsl:attribute name='href'>
						<xsl:value-of select="."/>
					  </xsl:attribute>
					  <xsl:attribute name="target">_blank</xsl:attribute>
					  Details
					</xsl:element>
				  </xsl:when>
				  <xsl:otherwise>
					<xsl:value-of select="."/>
				  </xsl:otherwise>
				</xsl:choose>
			  </td>
			</xsl:element>
	  </xsl:for-each>

		</table>
      </xsl:for-each>

	</xsl:if>

</xsl:template>

</xsl:stylesheet>

