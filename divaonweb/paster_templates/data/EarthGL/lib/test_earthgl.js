/*if (true) {
    var console =  {};
    console.log = function() {

	var str = '';
	for(var i=0; i<arguments.length; i++)  {
	    str = str + ' ' + arguments[i];
	}

	var s2 = str.replace('&','&amp;');
	document.getElementById('console').innerHTML += ("\n" + s2);
    };
}
*/

function assert(condition,msg) {
	if (condition) {
	    console.info('OK',msg);
	} 
	else {
	    console.error('FAIL',msg);
	}
}

var map;
var baseurl = 'http://localhost/diva-on-web';
var baseurl = 'http://localhost/emodnet';
var baseurl = '/web-vis-test';
var baseurl = 'http://127.0.0.1:8081';
var id = 'example';
var topoWidth = 64;
var topoHeight = 32;
var hmin = -8043.02;
var hmax = 5472.27;
var ti;


function test_map() {   
    var style = 'method:pcolor_flat+ncontours:40+inverted:false+cmap:jet+vmin:-0.474611+vmax:20.0949';
    var zhsec = -200;


    var topoLayer = new EarthGL.Layer('gebco_30sec_100.nc', 
				baseurl + '/Python/web/wms',
				{layers: 'gebco_30sec_100.nc#bat',
				 styles: 'cmap:gray+method:pcolor_flat+vmin:-8043.02+vmax:5472.27',
				});

    var topo = new EarthGL.Topo(64,32,hmin,hmax,topoLayer);
    map = new EarthGL.Map(id,{topo: topo, vscale: 400});
    //map = new EarthGL.Map(id,{topo: topo, vscale: 1});

    this.colorbar_container = document.getElementById('colorbar_container');


    var baselayer = new EarthGL.Layer('NASA Blue Marble', 
				      baseurl + '/Python/web/bluemarble',
				      {layers: 'BMNG'}, 
				      {isBaseLayer: true});

    // http://localhost/emodnet/Python/web/wms?LAYERS=gebco_30sec_100.nc%23bat&STYLES=cmap%3Ajet%2Binverted%3Afalse%2Bmethod%3Apcolor_flat%2Bvmin%3A-8043%2Bvmax%3A5472%2Bncontours%3A40&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&SRS=EPSG%3A4326&BBOX=-180,-90,0,90&WIDTH=512&HEIGHT=512
    var baselayer = new EarthGL.Layer('NASA Blue Marble', 
				      baseurl + '/Python/web/wms',
				      {layers: 'gebco_30sec_100.nc#bat'}, 
				      {isBaseLayer: true});

    map.addLayer(baselayer);
    

    

    var layer = new EarthGL.Layer('Temp', 
				baseurl + '/Python/web/wms',
				{layers: 'Atlantic/Temperature.19752005.4Danl.nc#Temperature',
				 styles: style,
				 elevation: zhsec,
				 time: 180.5});

    
    map.addLayer(layer);
    map.addControl(new EarthGL.Control.Config());
}


function test_events() {   
    var style = 'method:pcolor_flat+ncontours:40+inverted:false+cmap:jet+vmin:-0.474611+vmax:20.0949';
    var zhsec = -2000;


    var topoLayer = new EarthGL.Layer('gebco_30sec_100.nc', 
				baseurl + '/Python/web/wms',
				{layers: 'gebco_30sec_100.nc#bat',
				 styles: 'cmap:gray+method:pcolor_flat+vmin:-8043.02+vmax:5472.27',
				});

    var topo = new EarthGL.Topo(64,32,hmin,hmax,topoLayer);

    map = new EarthGL.Map(id,{topo: topo, vscale: 400});

    this.colorbar_container = document.getElementById('colorbar_container');


    var baselayer = new EarthGL.Layer('NASA Blue Marble', 
				      baseurl + '/Python/web/bluemarble',
				      {layers: 'BMNG'}, 
				      {isBaseLayer: true});

    baselayer.events.register('loadend',null,function() { 
	    console.log('OK base layer loaded ');
	});

    map.addLayer(baselayer);

    
    

    var secz = 5000;
    var name = 'Atlantic/Temperature.19752008.4Danl.nc#Temperature';

    var sec1 = new EarthGL.Section('Section 1',
//				 baseurl + '/Python/web/wms_vert',
				 'http://gher-diva.phys.ulg.ac.be/web-vis/Python/web/wms_vert',
				 [-30,-30],[70,-30],[-secz,0],
				 {layers: name,
				  styles: style});

    //map.addSection(sec1);
    map.addLayer(sec1);

    var sec2 = new EarthGL.Section('Section 2',
				 baseurl + '/Python/web/wms_vert',
				 [-150,0],[50,50],[-secz,0],
				 {layers: name,
				  styles: style});

    //map.addSection(sec2);
    map.addLayer(sec2);
    

    var layer = new EarthGL.Layer('Temp', 
				baseurl + '/Python/web/wms',
				{layers: 'Atlantic/Temperature.19752005.4Danl.nc#Temperature',
				 styles: style,
				 elevation: zhsec,
				 time: 180.5});

    var url = layer.getFullRequestString({elevation: -1});
    assert(layer.param.elevation == zhsec,'check getFullRequestString');
    
    map.addLayer(layer);

    var layer2 = new EarthGL.Layer('Temp', 
				baseurl + '/Python/web/wms',
				{layers: 'diva_results_O2.nc#O2',
				 styles: '',
				 elevation: -500});

    var fun = function() { 
	    console.log('FAIL calling unregistered function');
	};

    var fun_loaded = function() { 
	    console.log('OK layer loaded ');
    };

    var fun_loaded2 = function() { 
	    console.log('OK multiple listeners triggered');
    };

    layer2.events.register('loadend',null,fun);
    layer2.events.register('loadend',null,fun_loaded);
    layer2.events.register('loadend',null,fun_loaded2);

    layer2.events.unregister('loadend',null,fun);
    layer2.events.unregister('loadend',null,fun_loaded);
    layer2.events.unregister('loadend',null,fun_loaded2);


    map.addLayer(layer2);

    assert(map.layers.length == 2,'add 2nd Layer')
    assert(layer2.getZIndex() == 1000,'test zindex')


    map.setCenter(50,20);
    //map.setCenter(-80,20);
    
    map.removeLayer(layer);
    assert(map.layers.length == 1,'remove one layer')


    map.addLayer(layer2);
    layer2.destroy();
    assert(map.layers.length == 1,'destroy layer')   

    //change topography
    //map.setTopo(new EarthGL.Topo(128,64,hmin,hmax));
    

    //map.removeSection(sec1);

    map.addLayer(layer);

}




function test_section() {   
    var style = 'method:pcolor_flat+ncontours:40+inverted:false+cmap:jet+vmin:-0.474611+vmax:20.0949';
    var zhsec = -2000;


    var topoLayer = new EarthGL.Layer('gebco_30sec_100.nc', 
				baseurl + '/Python/web/wms',
				{layers: 'gebco_30sec_100.nc#bat',
				 styles: 'cmap:gray+method:pcolor_flat+vmin:-8043.02+vmax:5472.27',
				});

    var topo = new EarthGL.Topo(64,32,hmin,hmax,topoLayer);

    map = new EarthGL.Map(id,{topo: topo, vscale: 400});

    this.colorbar_container = document.getElementById('colorbar_container');


    var baselayer = new EarthGL.Layer('NASA Blue Marble', 
				      baseurl + '/Python/web/bluemarble',
				      {layers: 'BMNG'}, 
				      {isBaseLayer: true});


    map.addLayer(baselayer);

    
    

    var secz = 5000;
    var name = 'Atlantic/Temperature.19752008.4Danl.nc#Temperature';

    var sec1 = new EarthGL.Section('Section 1',
//				 baseurl + '/Python/web/wms_vert',
				 'http://gher-diva.phys.ulg.ac.be/web-vis/Python/web/wms_vert',
				 [-30,-30],[70,-30],[-secz,0],
				 {layers: name,
				  styles: style});

    //map.addSection(sec1);
    map.addLayer(sec1);
}


function test_2layers() {
    var id = 'example';
    var c = document.getElementById(id);
    w = 768;
    h = 512;

    c.width = w;
    c.height = h;


    var res = document.getElementById('topo_res').value;    
    var tmp = res.split('x');
    var topoWidth = parseInt(tmp[0],10);
    var topoHeight = parseInt(tmp[1],10);
    

    var style = 'method:pcolor_flat+ncontours:40+inverted:false+cmap:jet+vmin:-0.474611+vmax:20.0949';
    var zhsec = -2000;

    var hmin = -8043.02;
    var hmax = 5472.27;

    var topo = new EarthGL.Topo(64,32,hmin,hmax);

    map = new EarthGL.Map(id,{topo: topo, vscale: 400});

    document.getElementById('vscale').value = map.vscale;
    this.colorbar_container = document.getElementById('colorbar_container');


    var baselayer = new EarthGL.Layer('NASA Blue Marble', 
				      baseurl + '/Python/web/bluemarble',
				      {layers: 'BMNG'}, 
				      {isBaseLayer: true});

    map.addLayer(baselayer);
           
   
    

    var layer1 = new EarthGL.Layer('Temp', 
				baseurl + '/Python/web/wms',
				{layers: 'Atlantic/Temperature.19752005.4Danl.nc#Temperature',
				 styles: style,
				 elevation: 0,
				 time: 180.5});
    map.addLayer(layer1);

    var layer2 = new EarthGL.Layer('Temp', 
				baseurl + '/Python/web/wms',
				{layers: 'diva_results_O2.nc#O2',
				 styles: '',
				 elevation: -500});


    map.addLayer(layer2);


};

function test_util() {   
    assert(EarthGL.Util.within(60,120,360,61),'within 1');
    assert(!EarthGL.Util.within(60,120,360,30),'outside 1');
    assert(EarthGL.Util.within(60,120,360,61+4*360),'within 2');
    assert(!EarthGL.Util.within(60,120,360,30-4*360),'outside 2');

    assert(EarthGL.Util.withininter(60,120,360,70,80),'inside interval 1');
    assert(!EarthGL.Util.withininter(60,120,360,30,40),'outside interval 1');

    assert(EarthGL.Util.withininter(60,120,360,70+2*360,80+2*360),'inside interval 2');
    assert(!EarthGL.Util.withininter(60,120,360,30+2*360,40+2*360),'outside interval 2');
}


function test_tile() {
    var layer = new EarthGL.Layer('NASA Blue Marble', 
				      baseurl + '/Python/web/wms',
				      {layers: 'gebco_30sec_100.nc#bat'}, 
				      {isBaseLayer: true});

    //ti = new EarthGL.TileImages(1024,512,layer);
    //ti = new EarthGL.TileImages(64,32,layer);
    ti = new EarthGL.TileImages(333,221,layer);
    ti.events.register('loaded',null,function () { console.log('loaded'); });
    ti.draw([-180, 180],[-90,90]);
    //ti.draw([-180, 180],[-40,40]);

    //ti.draw([-10, 30],[30,50]);
};

//$(document).ready(function() {
$(window).load(function() {
	//alert(document.getElementsByTagName('script').length);
	$('#warning_no_webgl').hide();
	//test_events();
	//test_util();
	//test_map();
	//test_tile();
    test_section();
    });