/*jslint white: false, onevar: false, plusplus: true, browser: true, forin: true */
/******************************************************************************
 *                                                                            *
 *  Copyright (C) 2008-2010 Alexander Barth <barth.alexander@gmail.com>.      *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU Affero General Public License as published  *
 *  by the Free Software Foundation, either version 3 of the License, or      *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                            *
 ******************************************************************************/

(function() {

    var scripts = [
		   "EarthGL/Util.js",
		   "EarthGL/Class.js",
		   "EarthGL/Control.js",
		   "EarthGL/Events.js",
		   "EarthGL/Map.js",
		   "EarthGL/BaseSection.js",
		   "EarthGL/Topo.js"];

    var head = document.getElementsByTagName('head')[0];

    for (var i=0; i<scripts.length; i++) {
	var elem = document.createElement('script');
	elem.src = 'lib/' + scripts[i];
	elem.type = 'text/javascript';
	//alert(elem);
	head.appendChild(elem);
    }
})();