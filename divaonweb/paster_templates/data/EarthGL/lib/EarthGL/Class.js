/*jslint white: false, onevar: false, plusplus: true, browser: true, forin: true */
/******************************************************************************
 *                                                                            *
 *  Copyright (C) 2008-2010 Alexander Barth <barth.alexander@gmail.com>.      *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU Affero General Public License as published  *
 *  by the Free Software Foundation, either version 3 of the License, or      *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                            *
 ******************************************************************************/

"use strict";


EarthGL.Class = function(basetype,properties) {
    var fun = function() {
	    this.initialize.apply(this, arguments);
    };

    var initialize;

    if (typeof basetype == "function") {
	initialize = basetype.prototype.initialize;
	basetype.prototype.initialize = function() {};
	fun.prototype = new basetype();

	if(initialize === undefined) {
	    delete basetype.prototype.initialize;
	} else {
	    basetype.prototype.initialize = initialize;
	}

	EarthGL.Util.extend(fun.prototype,basetype.prototype);
	EarthGL.Util.extend(fun.prototype,properties);
    }
    else {
	fun.prototype = EarthGL.Util.merge(fun.prototype,basetype);
    }
    return fun;
};




// from OpenLayers
/*
EarthGL.Class = function() {
    var Class = function() {
	this.initialize.apply(this, arguments);
    };

    var extended = {};
    var parent, initialize, Type;
    for (var i=0; i<arguments.length; ++i) {
        Type = arguments[i];
        if (typeof Type == "function") {
            // make the class passed as the first argument the superclass
            if(i == 0 && arguments.length > 1) {

                initialize = Type.prototype.initialize;
                // replace the initialize method with an empty function,
                // because we do not want to create a real instance here
                Type.prototype.initialize = function() {};
                // the line below makes sure that the new class has a
                // superclass
                Class.prototype = new Type();
                // restore the original initialize method
                if(initialize === undefined) {
                    delete Type.prototype.initialize;
                } else {
                    Type.prototype.initialize = initialize;
                }
            }

	    EarthGL.Util.extend(Class.prototype, Type.prototype);
        } else {
            // in this case we're extending with the prototype
            parent = Type;
	    EarthGL.Util.extend(Class.prototype, parent);
        }
        
    }

    return Class;
};


*/
