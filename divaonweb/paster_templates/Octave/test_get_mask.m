%addpath('/var/www/web-vis/Python/../Octave')
bathymetry = '/home/abarth/Data/DivaData/Global/gebco_30sec_2.nc';

x0 = 27.000000;
x1 = 41.800000;
dx = 0.120000;
y0 = 40.300000;
y1 = 46.800000;
dy = 0.100000;
level  = 0.000000;
mask = '/tmp/topo.grd';
isglobal = 1.000000;

[xi,yi,mi] = get_mask(bathymetry,isglobal,x0,x1,dx,y0,y1,dy,level);
global UWRITE_DEFAULT_FORMAT
UWRITE_DEFAULT_FORMAT='ieee-le';
gwrite(mask,double(mi));
    
whos mi
y0
y1
dy
fprintf(1,'imax %d ',size(mi,1));
fprintf(1,'jmax %d ',size(mi,2));


[xi,yi,hi] = get_bath(bathymetry,isglobal,x0,x1,dx,y0,y1,dy);

save_bath('/tmp/blacksea_bath.nc',xi,yi,hi)