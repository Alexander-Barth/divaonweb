% flag = gwrite(name,tab)
% 
% save a file in GHER format or a NetCDF variable in NetCDF file
%
% Input:
%   name: name of the GHER file or NetCDF file name and 
%         variable name concanenated by a #
%   tab: the variable to save
%
%
% Output:
%   flag: 1 on success; 0 otherwise
%
% Example:
%
% For GHER format:
% gwrite('data.TEM',temp)
%
% Alexander Barth, 2008-03-19

function flag = gwrite(name,tab)

[name] = gread_tilde_expand(name);

valex = 9999;
tab(find(isnan(tab))) = valex;  

i = find(name == '#');

if isempty(i)
  [imax] = size(tab,1);
  [jmax] = size(tab,2);
  [kmax] = size(tab,3);
  nbmots =  10*1024*1024; % 10 MB record length

%  c4 = reshape(tab,imax*jmax*kmax,1);  
  [flag]=uwrite(name,tab(:),imax,jmax,kmax,valex,nbmots);
else
  fname = name(1:i-1);
  vname = name(i+1:end);
%  nf = netcdf(fname,'write');    
  if (fexist(fname))
    nf = netcdf(fname,'write');      
  else
    nf = netcdf(fname,'clobber');      
  end
% $$$   nf('dim001') = size(tab,1);
% $$$   nf('dim002') = size(tab,2);
% $$$   nf('dim003') = size(tab,3);
  tab = permute(tab,[ndims(tab):-1:1]);
  
  for i=1:myndims(tab);
    dim{i} = gendim(nf,size(tab,i));
  end
  
%  nf{vname} = ncfloat('dim001','dim002','dim003');
  nf{vname} = ncfloat(dim{:});
  
%  
  nf{vname}(:) = tab;
  nf{vname}.missing_value = ncfloat(valex);
  
%  tab = tab(:,:,end:-1:1);
%  tab(find(tab == nv.missing_value(:))) = NaN;
  close(nf);
%  error('not jet implemented');
end

function ncdim = gendim(nf,len)
  d = dim(nf);
  
  for i=1:length(d)
    if length(d{i})==len
      ncdim = name(d{i});
      return
    end
  end
  ncdim = ['dim' num2str(length(nf)+1,'%3.3i')];
  nf(ncdim) = len;
return
