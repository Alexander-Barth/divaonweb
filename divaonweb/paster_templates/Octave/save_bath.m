function save_bath(filename,lon,lat,b)

fv = -999999;

b(isnan(b)) = fv;

nccreate(filename,'lon',...
         'Dimensions',{'lon',size(b,1)},...
         'Format','classic');

nccreate(filename,'lat',...
         'Dimensions',{'lat',size(b,1)});

nccreate(filename,'bat',...
         'Dimensions',{'lon',size(b,1),'lat',size(b,2)});


ncwrite(filename,'lon',lon);
ncwrite(filename,'lat',lat);
ncwrite(filename,'bat',b);

ncwriteatt(filename,'lon','long_name','Longitude');
ncwriteatt(filename,'lon','standard_name','longitude');
ncwriteatt(filename,'lon','modulo',360);
ncwriteatt(filename,'lon','units','degrees_east');

ncwriteatt(filename,'lat','long_name','Latitude');
ncwriteatt(filename,'lat','standard_name','latitude');
ncwriteatt(filename,'lat','units','degrees_north');


ncwriteatt(filename,'bat','long_name','elevation above sea level');
ncwriteatt(filename,'bat','standard_name','height');
ncwriteatt(filename,'bat','units','meters');
ncwriteatt(filename,'bat','_FillValue',fv);
ncwriteatt(filename,'bat','missing_value',fv);



% global attributes

ncwriteatt(filename,'/','title','Bathymetry extracted from OceanBrowser');
ncwriteatt(filename,'/','Conventions','CF-1.0')
