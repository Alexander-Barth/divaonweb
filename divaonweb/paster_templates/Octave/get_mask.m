function [xi,yi,mi] = get_mask(bath_name,isglobal,x0,x1,dx,y0,y1,dy,level)


if 0
% test cases
% blacksea

x0=27.,x1=42.,
y0=41,y1=47.5,
dx = 0.5, dy = 0.5;
level = 0;

% med
%x0=-10,x1=42.797,
%y0=30.750,y1=47.005,
%dx = 0.5, dy = 0.5;
end

%save /home/abarth/www/vis/Data/last_crash.mat x0 x1 dx y0 y1 dy

level = -abs(level);

x = ncread(bath_name,'lon');
y = ncread(bath_name,'lat');

rx = x(2)-x(1);
ry = y(2)-y(1);
X0 = x(1);
Y0 = y(1);

redx = dx/rx;
redy = dy/ry;

xi = x0:dx:x1;
yi = y0:dy:y1;

i0 = round((x0-dx-X0)/rx)+1;
i1 = round((x1+dx-X0)/rx)+1;
i=i0:i1;

j0 = round((y0-dy-Y0)/ry)+1;
j1 = round((y1+dy-Y0)/ry)+1;
j=j0:j1;

%xp = X0 + (i-1)*rx;
%xp2 = mean(reshape(xp,[redx length(xp)/redx]),1);

%i = get_index_range(X0,rx,x0,x1,dx);
%j = get_index_range(Y0,rx,y0,y1,dy);

if isglobal
  i2 = mod(i-1,length(x))+1;
  jumps = [0 find(abs(i2(2:end)-i2(1:end-1)) > 1) length(i2)];

  b = zeros(length(i),length(j));

  for l=1:length(jumps)-1
    i_range = i2(jumps(l)+1):i2(jumps(l+1));

    start = [i_range(1), j(1)];
    count = [length(i_range), length(j)];
    b((jumps(l)+1):jumps(l+1),:) = ncread(bath_name,'bat',start,count);
  end
else
  i = max([min(i) 1]):min([max(i) length(x)]);
  j = max([min(j) 1]):min([max(j) length(y)]);
  start = [i(1), j(1)];
  count = [length(), length(j)];
  b((jumps(l)+1):jumps(l+1),:) = ncread(bath_name,'bat',start,count);
end

mask = b < level;

x = X0 + rx*(i-1);
y = Y0 + ry*(j-1);


% convolution
Fx = round(redx);
Fy = round(redy);

[Fi,Fj] = ndgrid(-Fx:Fx,-Fy:Fy);
F = exp(-2* ((Fi/redx).^2 + (Fj/redy).^2));
F = F/sum(F(:));

m = conv2(double(mask),F,'same');
[Xi,Yi] = ndgrid(xi,yi);

mi = interpn(x,y,m,Xi,Yi);

mi = mi > 1/2;
