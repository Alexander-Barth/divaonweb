fname = 'nobat.nc';

% Variables
nccreate(fname,'lat','Format','classic','Datatype','double','Dimensions',{'lat',2});
ncwriteatt(fname,'lat','long_name','Latitude')
ncwriteatt(fname,'lat','standard_name','latitude')
ncwriteatt(fname,'lat','units','degrees_north')

nccreate(fname,'lon','Datatype','double','Dimensions',{'lon',2});
ncwriteatt(fname,'lon','long_name','Longitude')
ncwriteatt(fname,'lon','standard_name','longitude')
ncwriteatt(fname,'lon','modulo',double(360))
ncwriteatt(fname,'lon','units','degrees_east')

nccreate(fname,'bat','Datatype','double','Dimensions',{'lon','lat'});
ncwriteatt(fname,'bat','long_name','elevation above sea level')
ncwriteatt(fname,'bat','standard_name','height')
ncwriteatt(fname,'bat','units','meters')
ncwriteatt(fname,'bat','_FillValue',double(1))
ncwriteatt(fname,'bat','missing_value',double(1))

% Global attributes
ncwriteatt(fname,'/','title','Dummy flat bathymetry')

ncwrite(fname,'lat',[-90 90]')
ncwrite(fname,'lon',[-180 180]')
ncwrite(fname,'bat',-1e10 * ones(2,2));

