function diva2d2mat(analysis_name_full,mat_fname)

if 0
analysis_name_full='/home/abarth/www/web-vis/Data/60cd0d5664e4cfb80b109106a5fedb1d_analysis_diva_1_1.76184_27_41.75_0.25_40.25_46.75_0.25.nc'
varname = 'analyzed_field'
mat_fname = 'test.mat';
end

nc = netcdf(analysis_name_full,'r');
analyzed_field = nc{'analyzed_field'}(:)';
error_field = nc{'error_field'}(:)';
lon = nc{'x'}(:);
lat = nc{'y'}(:);

fv = -9999;
analyzed_field(analyzed_field==fv) = NaN;
error_field(error_field==fv) = NaN;

readme = 'This file is created by diva on web. It containts 4 variables: analyzed_field, error_field, lon and lat. "analyzed_field" is the gridded variable and error_field represents the relative error.';

save('-v6',mat_fname,'analyzed_field','error_field','lon','lat');
close(nc);