# Copyright (C) 2008-2010 Alexander Barth <barth.alexander@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#                                                                           
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from __future__ import with_statement

import sys
import re
import unittest
from lxml import etree

try:
    import mx.DateTime.ISO
    def mjd(s):
        return mx.DateTime.ISO.ParseDateTimeUTC(s).mjd
except ImportError:
    from dateutil.parser import parse
    from datetime import datetime
    time_origine = datetime(1858,11,17)
    def mjd(s):
        return (parse(s)-time_origine).total_seconds() /(24.*60.*60.)


class ODVError(Exception):
    '''class for exceptions trigged when error while reading and ODV file occurs.'''
    pass


class ODVData(object):
    def __init__(self,f):
        '''Open a ODV file.
        :param f: A file-like object of the ODV data (text file StringIO, not binary like BytesIO).
        '''
        

        def ISOparser(s):
            return mjd(s)

        def JDparser(s):
            return float(s) - 2400000.5


        self.time_parsers = {
            'yyyy-mm-ddThh:mi:ss.sss': ISOparser,
            'yyyy-mm-ddThh:mm:ss.sss': ISOparser,
            'time_ISO8601': ISOparser,
            'Chronological Julian Date': JDparser, 
            'Julian Date (chronological)': JDparser }


        self.f = f
        self.missing_value = None

        self.header = []

        for line in f:
            if line.startswith('//'):
                xmldata = line.strip('//')

                try:
                    xmlroot = etree.fromstring(xmldata)
                    if xmlroot.tag == 'MissingValueIndicators':
                        self.missing_value = xmlroot.text
                except:
                    pass
                

                self.header.append(xmldata)
            else:
                break

        self.variables = []
        self.units = []

        #print "missing val",self.missing_value
        l = re.compile('^(.*)\[(.*)\]$')

        #print('line',line,type(line))
        hlist = line.rstrip('\n').split('\t')
        if not hlist[-1]:
            hlist = hlist[:-1]
    
        for v in hlist:            
            ss = l.search(v)

            if v == "QV:SEADATANET":
                self.variables.append(self.variables[-1] + '/' + v)
                self.units.append("")                               
            elif ss:
                #print ss,v
                self.variables.append(ss.group(1).rstrip())
                self.units.append(ss.group(2).rstrip())
            else:
                self.variables.append(v)
                self.units.append("")               


        #print self.variables

        self._dataline = ['' for i in self.variables]

        self.index = range(len(self.variables))
        self._data = None
        self.requested_variables = self.variables

    def __iter__(self):
        self.fiter = self.f.__iter__()
        return self


    def request(self,requested_variables):
        self.index = []
        self.requested_variables = requested_variables

        for r in requested_variables:

            found_index = None
            for i in range(len(self.variables)):
                #print "vv ",self.variables[i:i+len(r)]
                if self.variables[i] == r:
                    found_index = i
                    break

            if found_index == None:
                raise KeyError("variable %s not found" % str(r))

            self.index.append(found_index)

#        print self.index
        

    def __next__(self):
        return self.next()

    def next(self):
        if sys.version_info[0] == 2:
            line = self.fiter.next().rstrip('\n')
        else:
            line = self.fiter.__next__().rstrip('\n')
            
        #print("line",line,type(line))

        dataline = line.split('\t')

        for i in range(len(dataline)):
            if not dataline[i]:
                dataline[i] = self._dataline[i]

        self._dataline = dataline
        d = []
#        print "index ",self.index,self.variables
        for i in self.index:

            if i >= len(dataline):
                raise KeyError("unable to read column %d from line %s",i,line)

            item = dataline[i]

            if item == self.missing_value:
                item = float('NaN')
            else:
                try:
                    item = float(item)
                except:
                    pass
                
            d.append(item)
                         
#        return [float(dataline[i]) for i in self.index]
#        print d
        return d

    def data(self):
        '''Load all data in dictionnary'''
        if self._data != None:
            return self._data

        d = {}

        keys = []
        for r in self.requested_variables:
            keys.append(r)
            d[r] = []

        for row in self:
#            print row
            for key,val in zip(keys,row):
                d[key].append(val)

        self._data = d
        return d

    def __getitem__(self, key):
        """Retrieve the value associated with 'key' (in any case)."""
        data = self.data()

        if key in data:
            return data[key]
        else:
            for k in data.keys():
                if k.lower() == key.lower():
                    return data[k]

        raise ODVError('Unable to find variable %s in ODV file. Headers found are: %s ' % (key,', '.join(self.variables)))


    def hasvariable(self,variable):

        for t in self.variable:
            if t in self.variables:
                return True

        return False
        

    def hastime(self):
        return self.hasvariable(self.time_parsers.keys())


    def mjd(self):
        data = self.data()

        for header,parser in self.time_parsers.items():
            if header in self.variables:
                return [parser(s) for s in data[header]]

            
            
        raise ODVError('Unable to find time column in ODV file (only %s are supported). Headers found are: %s ' % (', '.join(self.time_parsers.keys()),', '.join(self.variables)))
        
        
    
class TestODVData(unittest.TestCase):

    def setUp(self):
        from io import StringIO

        self.file = StringIO(u"""// user commennts
//
//SDN_parameter_mapping
//<subject>SDN:LOCAL:Julian Date (chronological)</subject><object>SDN:P011::CJDY1101</object><units>SDN:P061::UTAA</units>
//<subject>SDN:LOCAL:Surface elevation (unspecified datum) of the water column</subject><object>SDN:P011::ASLVZZ01</object><units>SDN:P061::ULAA</units>
//
Cruise	Station	Type	yyyy-mm-ddThh:mm:ss.sss	Longitude [degrees_east]	Latitude [degrees_north]	LOCAL_CDI_ID	EDMO_code	Bot. Depth [m]	Julian Date (chronological) [Days]	QV:SEADATANET	Surface elevation (unspecified datum) of the water column [m]	QV:SEADATANET	
WIERMGDN-1999-RIKZMON_WAT-DNM	WIERMGDN	*	1999-01-01T00:00:00.000	5.959	53.517	13728	1526	0	2451179.500000	1	-0.61	1
									2451179.506944	1	-0.7	1
									2451179.513889	1	-0.78	1
									2451179.520833	1	-0.85	1
									2451179.527778	1	-0.93	1
									2451179.534722	1	-1	1
									2451179.541667	1	-1.07	1
									2451179.548611	1	-1.14	1
									2451179.555556	1	-1.19	1
									2451179.562500	1	-1.24	1
									2451179.569444	1	-1.29	1
									2451179.576389	1	-1.33	1
									2451179.583333	1	-1.36	1
""")
        pass

    def test_loading(self):
        
        var = 'Surface elevation (unspecified datum) of the water column'
        requested_variables = ['Longitude','Latitude',var,var + '/QV:SEADATANET']

        
        odv = ODVData(self.file)
        odv.request(requested_variables)

        for (lon,lat,var,qf) in odv:
            #print lon,lat,var,qf
            pass

        self.assertAlmostEqual(lon,5.959)


    def test_load_all(self):
        odv = ODVData(self.file)

        for data in odv:
            pass

        self.assertAlmostEqual(data[4],5.959)


    def test_load_data(self):
        
        var = 'Surface elevation (unspecified datum) of the water column'
        requested_variables = ['Longitude','Latitude',var,var + '/QV:SEADATANET']

        
        odv = ODVData(self.file)
        odv.request(requested_variables)

        data = odv.data()
#        print str(data)
        print(data[var])

    def test_load_data(self):
        
        var = 'Surface elevation (unspecified datum) of the water column'
        requested_variables = ['Longitude','Latitude',var,var + '/QV:SEADATANET']

        
        odv = ODVData(self.file)
        data = odv.data()
#        print str(data)
        print(odv.mjd())

    def test_load_case_insensitif(self):
        
        var = 'Surface elevation (unspecified datum) of the water column'
        requested_variables = ['Longitude','Latitude',var,var + '/QV:SEADATANET']

        
        odv = ODVData(self.file)
        data = odv.data()
#        print str(data)
        print(odv["latitudE"])


if __name__ == "__main__":
    unittest.main()

#    suite = unittest.TestLoader().loadTestsFromTestCase(TestODVData)
#    suite.debug()



    


