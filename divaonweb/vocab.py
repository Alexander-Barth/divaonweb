from __future__ import print_function
from __future__ import absolute_import

try:
    from urllib.parse import urlparse
except ImportError:
    # python 2
    from urlparse import urlparse

import xml.etree.ElementTree as ET
import xml.dom.minidom
import os
from datetime import datetime, timedelta

try:
    from urllib.request import urlopen
    from urllib.error import HTTPError
except ImportError:
    # python 2
    from urllib2 import urlopen
    from urllib2 import HTTPError


namespaces = {'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
              'skos': 'http://www.w3.org/2004/02/skos/core#',
              'dc': 'http://purl.org/dc/terms/',
              'rdfs': 'http://www.w3.org/2000/01/rdf-schema#',
              'grg': 'http://www.isotc211.org/schemas/grg/',
              'owl': 'http://www.w3.org/2002/07/owl#',
              'void': 'http://rdfs.org/ns/void#',
              'gmx': 'http://www.isotc211.org/2005/gmx',
              'gml': 'http://www.opengis.net/gml'
}

collection_URL = 'http://vocab.nerc.ac.uk/collection/'

def parseTimeStr(val):
    # https://docs.python.org/2/library/datetime.html#strftime-strptime-behavior

    # try all these formats
    formats = [
        '%Y-%m-%d %H:%M:%S.%f',
        '%Y-%m-%dT%H:%M:%S.%f',
        '%Y-%m-%dT%H:%M:%S.%fZ',
        '%Y-%m-%d %H:%M:%S',
        '%Y-%m-%dT%H:%M:%S',
        '%Y-%m-%dT%H:%M:%SZ',
        '%Y-%m-%d %H:%M',
        '%Y-%m-%dT%H:%M',
        '%Y-%m-%d %H',
        '%Y-%m-%dT%H',
        '%Y-%m-%d',
        '%Y%m%d']
    
    for f in formats:
        try:
            time = datetime.strptime(val,f)
            return time
        except ValueError as e:
            # try next format
            #print(e)
            pass

    # I give up
    raise Exception('I don''t understand the time "%s". I know only these formats %s' 
                    % (val,', '.join(formats)))


class Vocab(object):
    def __init__(self,tmpdir='/tmp',baseurl = 'http://vocab.nerc.ac.uk/collection/P01/current/'):
        self.baseurl = baseurl
        self.tmpdir = tmpdir



    def url(self,code):
        return self.baseurl + code


    def label(self,code):
        code = code.replace('SDN:P011::','')

        filename = os.path.join(self.tmpdir,code)

        #print 'get ',code

        if os.path.exists(filename):
            f = open(filename,'rb')
            out = f.read()
            f.close()
        else:
            try:
                #print "get ",self.url(code)
                f = urlopen(self.url(code))
                out = f.read()
                f.close()

                f = open(filename,'wb')
                f.write(out)
                f.close()
            except HTTPError:
                raise ValueError(code)

        #print('out',len(out))
        dom = xml.dom.minidom.parseString(out)

        return dom.getElementsByTagName("skos:prefLabel")[0].firstChild.data
        
        altLabel = dom.getElementsByTagName("skos:altLabel")

        if len(altLabel) > 0:
            label = altLabel[0].firstChild.data
        else:
            label = dom.getElementsByTagName("skos:prefLabel")[0].firstChild.data

        return label
        



class ShortVocab(object):
    def __init__(self,tmpdir='/tmp'):

        self.tmpdir = tmpdir
        self.dict = {}

        localfile = os.path.join(self.tmpdir,'_localnames')
        
        if os.path.exists(localfile):
            f = open(localfile,'r')
            for line in f.readlines():
                (k,n) = line.split(':')
                self.dict[k] = n

            f.close()


    def label(self,code):
        code = code.replace('SDN:P011::','')

        return self.dict.get(code,None)
        




def subns(xpath):
    """subsitute namespaces prefixes in xpath as
ElementTree from earlier version (2.6) of python 
does not support namespaces in find and findall.
"""
    for ns in namespaces:
        xpath = xpath.replace(ns + ':','{%s}' % namespaces[ns])

    #print('xpath',xpath)
    return xpath



def splitURL(url):
    o = urlparse(url)
    parts = o.path.split('/')

    p = {"collection": parts[2]}

    if len(parts) >= 4:
        p["tag"] = parts[3]

    if len(parts) >= 5:
        p["key"] = parts[4]

    return p
    

def loader(url):
    #print('loading ',url)
    f = urlopen(url)
    tree = ET.fromstring(f.read())
    f.close()
    return tree
    
class CachedLoader(object):
    def __init__(self,cache):
        self.cache = cache

    def __call__(self,url):    
        if url in self.cache:
            return self.cache[url]

        p = splitURL(url)
        col = p["collection"]

        #print('url',url,p)
        for item in self.cache:
            if url.startswith(item):                
                key = 'SDN:' + col + '::' + p["key"]
                tree = self.cache[item]

                for concept in tree.findall(subns('skos:Collection/skos:member/skos:Concept')):
                    if concept.find(subns('skos:notation')).text == key:
                        return concept


        tree = loader(url)
        self.cache[url] = tree
        return tree
            
        

class Collection(object):
    def __init__(self,url,loader = loader):
        self.url = url
        self.loader = loader
        self.collection = splitURL(url)["collection"]
        self.reload()
        #print('self.collection',self.collection)

    def reload(self):
        self.tree = self.loader(self.url)
        
        #self.date = self.tree.find('skos:Collection/dc:date',namespaces).text
        self.date = parseTimeStr(self.tree.find(subns('skos:Collection/dc:date')).text)

    def labels(self):
        return [(_.find(subns('skos:notation')).text,_.find(subns('skos:prefLabel')).text)
                for _ in self.tree.findall(subns('skos:Collection/skos:member/skos:Concept'))]
        
    def keys(self):
        return [_.find(subns('skos:notation')).text
                for _ in self.tree.findall(subns('skos:Collection/skos:member/skos:Concept'))]
        
    def __getitem__(self,key):
        if key.startswith('SDN:' + self.collection + '::'):
            key = key.split('::')[1]

        #print('self.url + key',self.url + key)
        return Concept(self.url + key,loader = self.loader)



class Concept(object):
    def __init__(self,url=None, tree=None, loader = loader):

        if tree == None:
            self.tree = loader(url)

            # can be in a RDF tag
            if self.tree.tag != '{%s}Concept' % namespaces['skos']:
                self.tree = self.tree.find('skos:Concept',namespaces)
        else:
            self.tree = tree

        self.loader = loader
        self.prefLabel = self.tree.find('skos:prefLabel',namespaces).text
        self.notation = self.tree.find('skos:notation',namespaces).text
        self.altLabel = self.tree.find('skos:altLabel',namespaces).text

    def findall(self,name,collection = None):
        """name can be related, narrower, broader"""

        
        for _ in self.tree.findall('skos:' + name,namespaces):
            url = _.attrib['{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource']

            if collection == None:
                yield Concept(url=url)

            p = splitURL(url)
            if p["collection"] == collection:
                yield Concept(url=url,loader = self.loader)

    def find(self,name,collection = None):
        matchlist = list(self.findall(name,collection))

        if len(matchlist) != 0:
            return matchlist[0]
        else:
            # not found
            return None
