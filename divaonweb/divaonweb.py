# Copyright (C) 2008-2013 Alexander Barth <barth.alexander@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from __future__ import print_function
from __future__ import with_statement
from __future__ import absolute_import

import sys
import os
import traceback
import random
import string
import unittest
import numpy
import matplotlib
matplotlib.use('Agg')  # force the antigrain backend
import subprocess
import time
import netCDF4
import pylab

try:
    import numpy.ma as M
except:
    import matplotlib.numerix.ma as M

import shutil
import re
import signal
import logging

try:
    from mx.DateTime.ISO import ParseDateTimeUTC
except ImportError:
    # python 3
    # works also in python 2, but this routine is very slow and
    # mx.DateTime.ISO is not ported to python 3
    from dateutil.parser import parse as ParseDateTimeUTC


import zipfile
import hashlib

import io
try:
    import StringIO.StringIO as BytesIO
except ImportError:
    # python 3
    from io import BytesIO


#import pickle

from divaonweb import settings
from divaonweb.util import HttpResponse, make_mat, run_octave,select_multires,obsload,join_path,join_new_path
from divaonweb.GHERFormat import GHERFile
from divaonweb import ODVData
from divaonweb import divaplotting
from divaonweb import webmap

time_origine = ParseDateTimeUTC('1858-11-17T00:00:00')
def mjd(s):
    d = ParseDateTimeUTC(s)
    try:
        # for mx.DateTime.DateTime
        return d.mjd
    except AttributeError:
        # for datetime.datetime
        return (d-time_origine).total_seconds() /(24.*60.*60.)

class DivaError(Exception):
    '''class for exceptions trigged when a error in Diva occurs.'''
    pass

def extract_bath(outname,
                 x0, x1, y0, y1,
                 dx, dy,db,outformat="nc"):

        res = min(dx, dy)/3
        bathymetry = os.path.join(
            settings.bathdir,
            select_multires(settings.bath[db],res)['file'])

        cmd = """
bathymetrydb = '%s';
x0 = %f;
x1 = %f;
dx = %f;
y0 = %f;
y1 = %f;
dy = %f;
bathname = '%s';
isglobal = %f;

[xi, yi, hi] = get_bath(bathymetrydb, isglobal, x0, x1, dx, y0, y1, dy);
save_bath(bathname,xi,yi,hi);
""" % (bathymetry, x0, x1, dx, y0, y1, dy, outname, int(db in settings.globalbath))
        run_octave(cmd)



class diva_on_web:


    def __init__(self, root, wms_root,
                 diva_template,
                 datadir,
                 max_obs = 100000,
                 longitude0 = -180,
                 email_fromaddr=None, email_toaddrs=None,
                 email_subject='', email_login=None,
                 email_password=None,
                 do_debug = True, use_cache=True,
                 log=None,
                 **kwargs):

        '''Intialize diva_on_web server instance'''

        self._root = root
        self.wms_root = wms_root

        self.max_obs = max_obs
        self.longitude0 = longitude0
        self.do_debug = do_debug
        self.use_cache = use_cache
        self.diva_template = diva_template
        self.email_fromaddr = email_fromaddr
        self.email_toaddrs = email_toaddrs
        self.email_subject = email_subject
        self.email_login = email_login
        self.email_password = email_password
        # directory with static content, accessible by web server
        self.datadir = datadir
        self.conf = kwargs

        if log:
            self.log = log
        else:
            self.log = logging.Logger(self.__class__.__name__)
            self.log.addHandler(logging.StreamHandler(sys.stdout))

        self.diva_args = self.conf.copy()
        self.diva_args['diva_template'] = diva_template
        self.diva_args['log'] = log


    def sanitize_sessionid(self, s):
        """Sanitize user input. Remove unexpected characters"""

        return ''.join(c for c in s if c in (string.hexdigits + '-'))



    def process_odv4(self, odvfile, fname,
                     variable = 'Surface elevation (unspecified datum) of the water column',
                     time_range = [-1e10,1e10], depth = 0, iszipped=False):

        warning = ""

        self.log.info('Extracted observations: ' + fname)

        # process recursivelly nested zip files
        def process_item(zname,iszipped):
            count = 0
            if iszipped:
                # unzip
                zf = zipfile.ZipFile(zname)
                for filenames in zf.namelist():
                    if filenames.endswith('metadata.csv'):
                        self.log.info('Ignore: ' + filenames)

                    else:
                        self.log.info('Process: ' + filenames)

                        filez = zf.open(filenames,'r')
                        # filez is always in binary mode

                        if filenames.endswith('.zip'):
                            # a zip in a zip
                            count += process_item(filez,True)
                        else:
                            # a regular ODV file
                            # need to use text wrapper for text mode

                            with io.TextIOWrapper(filez,encoding='utf-8') as fileodv:
                                count += process_item(fileodv,False)


                        filez.close()

                zf.close()
            else:
                count += self.process_odv4_file(zname, fname, variable, time_range,
                                                depth)

            return count

        #print("odvfile",type(odvfile),iszipped)

        count = process_item(odvfile,iszipped)
        self.log.info('found %d' % count)

        if count == 0:
            raise DivaError('No data')
        elif count < 10:
            warning += 'Only %d data point(s) found. ' % count

        obs = obsload(fname)

        return (obs.shape[0], obs[:,0].min(), obs[:,0].max(), obs[:,1].min(),
                obs[:,1].max(), obs[:,2].min(), obs[:,2].max(), warning)


    def process_odv4_file(self, odvfile, fname,
                          variable = 'Surface elevation (unspecified datum) of the water column',
                          time_range = [-1e10,1e10], depth = 0):

        f = open(fname, "a")
        count = 0

        zi = float(depth)
        odv = ODVData.ODVData(odvfile)


        if 'Depth' in odv.variables:
            vi = []
            xi = []
            yi = []

            # all unique stations
            for station in set(odv["Station"]):
                #print "station", station
                vs = []
                xs = []
                ys = []
                zs = []
                ts = []

                for (s, v, x, y, z, t) in zip(odv["Station"], odv[variable],
                                         odv["Longitude"], odv["Latitude"],
                                         odv["Depth"], odv.mjd()):
                    if s == station:
                        vs.append(v)
                        xs.append(x)
                        ys.append(y)
                        zs.append(z)
                        ts.append(t)


                try:
                    #zs = numpy.array(zs)
                    #ts = numpy.array(ts)

                    ti = numpy.interp([zi], zs, ts)[0]

                    #print time_range[0] <= ti <= time_range[1]

                    if time_range[0] <= ti <= time_range[1]:
                        xi.append(numpy.interp([zi], zs, xs)[0])
                        yi.append(numpy.interp([zi], zs, ys)[0])
                        vi.append(numpy.interp([zi], zs, vs)[0])
                except BaseException as error:
                    self.log.error(traceback.format_exc())
                    raise DivaError('error  %s, %s' %
                                    (str(error), str(traceback.format_exc())))

                    self.log.info('Interpolation error: %s' % str(error))
                    self.log.info(traceback.format_exc())

            for (lon, lat, var) in zip(xi, yi, vi):
                if not numpy.isnan(var):
                    f.write(" ".join([str(x) for x in [lon, lat, var]]) + "\n")
                    count += 1

        else:
            for (v, x, y, t) in zip(odv[variable], odv["Longitude"],
                                 odv["Latitude"], odv.mjd()):
                if time_range[0] <= t <= time_range[1] and not numpy.isnan(v):
                    f.write(" ".join([str(_) for _ in [x, y, v]]) + "\n")
                    count += 1


        f.close()
        return count

    def process_input(self, odvfile, fname, columnsep = None, decimalsep = '.'):
        if columnsep == "whitespace":
            columnsep = None

        if columnsep == decimalsep:
            raise DivaError("column and decimal separators cannot be the same")

        f = open(fname, "w")

        # save observations
        count = 0
        columns = None
        warning = ""

        for line_byte in odvfile:
            #print("type",type(line_byte))
            line = line_byte.decode("utf-8")

            if count >= self.max_obs:
                warning = "Number of observation exceeds limit of %d. " + \
                    "Only %d first retained." % (self.max_obs,
                                                 self.max_obs)
                break
            count += 1

            # convert line into numbers
            try:
                # split lines in maximum 5 rows
                parts = line.split(columnsep,4)
                if len(parts) == 3 or len(parts) == 4:
                    values = [float(s.replace(decimalsep, '.')) for s in parts ]
                    extra = ''
                elif len(parts) == 5:
                    values = [float(s.replace(decimalsep, '.'))
                              for s in parts[0:3] ]
                    extra = parts[4]
                else:
                    f.close()
                    raise DivaError("The data file should have at least 3 " +
                                    " columns. Line %d contains only %d."
                                    % (count, len(parts)))
            except BaseException as error:
                f.close()
                raise DivaError("Unable to read line number %d" % (count))


            if columns == None:
                columns = len(values)
            else:
                if columns != len(values):
                    f.close()
                    raise DivaError("All lines should have the same number " +
                                    "of columns (line %d)." % (count))

            # convert longitude to range
            # [self.longitude0, self.logitude0+360]
            values[0] = (values[0]-self.longitude0) % 360 + \
                self.longitude0

            # check range of latitude
            if (values[1] < -90 or values[1] > 90):
                raise DivaError("Latitude is not between -90 and 90 " +
                                "(line %d)." % (count))


            f.write(" ".join([str(x) for x in values]) + "\n")

        f.close()

        obs = obsload(fname)

        return (count, obs[:,0].min(), obs[:,0].max(), obs[:,1].min(),
                obs[:,1].max(), obs[:,2].min(), obs[:,2].max(), warning)


    def upload(self, sessionid, data, **kwargs):
        content_type = 'text/xml'
        datatype = kwargs.get('type', 'text')
        if 'type' in kwargs:
            del kwargs['type']

        xml  = '<?xml version="1.0"?>\n'
        xml += "<stats>"
        warning = ""

        try:
            # 10 digit random hexadecimal number
            rstring = "%x" % random.randint(0, 16**10)
            sessionid = sessionid + '-' + rstring

            fname = os.path.join(self.wms_root, sessionid + '.dat')

            if datatype == 'text':
                (count, x0, x1, y0, y1, v0, v1, warning) = self.process_input(data.file, fname, **kwargs)
            elif datatype == 'odv4':
                if 'depth' in kwargs:
                    kwargs['depth'] = float(kwargs['depth'])

                if 't0' in kwargs:
                    t0 = kwargs['t0']
                    del kwargs['t0']

                    if 't1' in kwargs:
                        t1 = kwargs['t1']
                        del kwargs['t1']
                    else:
                        t1 = t0

                    kwargs['time_range'] = [mjd(t0),mjd(t1)]

                (count, x0, x1, y0, y1, v0, v1, warning) = self.process_odv4(data.file, fname,
                                                                        iszipped=data.filename.endswith('.zip'), **kwargs)
            else:
                raise DivaError('unknown data type (%s)' % datatype)


            diva = Diva(sessionid,**self.diva_args)
            diva.copy_obs(fname)

            #s["diva_file"] = fname + '.pickle'
            #fp = open(s["diva_file"], 'w')
            #pickle.dump(diva, fp)
            #fp.close()
            diva.save()

            out = """
      <stat id='obs_count'>%d</stat>
      <stat id='obs_x0'>%g</stat>
      <stat id='obs_x1'>%g</stat>
      <stat id='obs_y0'>%g</stat>
      <stat id='obs_y1'>%g</stat>
      <stat id='obs_v0'>%g</stat>
      <stat id='obs_v1'>%g</stat>
      <stat id='sessionid'>%s</stat>
    """ % (count, x0, x1, y0, y1, v0, v1, sessionid)
        except BaseException as error:
            out = "<error><![CDATA["
            out += str(error)
            if self.do_debug:
                out += "\n" + traceback.format_exc()
            out +="]]></error>"

        xml += out

        if warning:
            xml += "<warning><![CDATA[%s]]></warning>" % warning

        xml += "</stats>"

        if self.do_debug:
            self.log.info('Upload response:')
            self.log.info(xml)

        return HttpResponse(xml.encode('utf-8'),content_type)


    def uploadvel(self, sessionid, data,
                  uname = 'u', vname = 'v', **kwargs):
        content_type = 'text/xml'

        sessionid = self.sanitize_sessionid(sessionid)
        wdir = self.diva_template + "_" + sessionid
        velname = sessionid + '-vel.nc'
        ncname = os.path.join(self.wms_root, velname)

        f = open(ncname,'wb')

        count = 1024
        hash = hashlib.md5()

        while True:
            buf = data.file.read(count)
            if buf:
                hash.update(buf)
                f.write(buf)
            else:
                break

        f.close()

        xml  = '<?xml version="1.0"?>\n'
        xml += "<stats>"
        xml += "<result src='%s' />" % velname
        xml += "<stat id='vel_md5'>" + hash.hexdigest() + "</stat>"
        out = ''

        try:
            nc = netCDF4.Dataset(ncname)
            latname,lonname = nc.variables[uname].dimensions
            velx = nc.variables[lonname][:]
            vely = nc.variables[latname][:]
            u = nc.variables[uname][:]
            v = nc.variables[vname][:]
            nc.close()

            veldx = velx[1]-velx[0]
            veldy = vely[1]-vely[0]
            shape = u.shape
            tol = 1e-6

            if numpy.all(abs((velx[2:] - velx[1:-1]) / veldx - 1) > tol):
                raise DivaError("increments in longitude are not constant")

            if numpy.all(abs((vely[2:] - vely[1:-1]) / veldy - 1) > tol):
                raise DivaError("increments in latitude are not constant")

            x0 = kwargs.get("x0")
            x1 = kwargs.get("x1")
            y0 = kwargs.get("y0")
            y1 = kwargs.get("y1")

            # check if domain is well covered

            #print 'x',x,v.shape
            endianess = 'little'
            GHERFile(os.path.join(wdir,'input','Uvel.dat'),'w',endianess=endianess).save(u)
            GHERFile(os.path.join(wdir,'input','Vvel.dat'),'w',endianess=endianess).save(v)

            f = open(os.path.join(wdir,'input','UVinfo.dat'), "w")
            f.write("%g\n%g\n%g\n%g\n%g\n%g\n" % (velx[0], vely[0], veldx, veldy, shape[0], shape[1]))
            f.close()

        except BaseException as error:
            out += "<error><![CDATA["
            out += str(error)
            if self.do_debug:
                out += "\n" + traceback.format_exc()
            out +="]]></error>"

        xml += out
        xml += "</stats>"

        return HttpResponse(xml.encode('utf-8'),content_type)

    def make_analysis(self, sessionid='x', variable="temp", len=1, stn=1,
                      dx = 0.5, dy = 0.5, level = 0,method="diva", **kwargs):

        content_type = "text/xml"
        sessionid = self.sanitize_sessionid(sessionid)

        xml  = '<?xml version="1.0"?>\n'
        xml += "<layers>\n"

        try:
            datafile = sessionid

            len = float(len)
            stn = float(stn)
            level = float(level)
            adv = float(kwargs.get('adv',0))
            diff = float(kwargs.get('diff',0))

            vel_md5 = kwargs.get('vel_md5','')
            vel_md5 = ''.join(c for c in vel_md5 if c in string.hexdigits)

            x0 = kwargs.get("x0")
            x1 = kwargs.get("x1")
            y0 = kwargs.get("y0")
            y1 = kwargs.get("y1")

            database = kwargs.get("db", "gebco")

            x0 = float(x0)
            x1 = float(x1)
            y0 = float(y0)
            y1 = float(y1)

            dx = float(dx)
            dy = float(dy)

            filename = "%s_analysis_%s_%s_%g_%g_%g_%g_%g_%g_%g_%g_%g_%g_%g_vel%s.nc" % (datafile, method, database, len, stn, x0, x1, dx, y0, y1, dy, level,adv,diff,vel_md5)
            analysis_name_full = os.path.join(self.wms_root, filename)


            out = "<result src='%s' />" % filename
            out += "<stats>"

            if (not os.path.exists(analysis_name_full) or not self.use_cache):
                if (method=="diva"):
                    diva = Diva(sessionid,**self.diva_args)
                    diva.load()

                    diva.analysis(analysis_name_full, database, len, stn, x0, x1, y0, y1, dx, dy, level, adv, diff)
                    diva.save()
                else:
                    fname = os.path.join(self.wms_root, sessionid + '.dat')
                    debug = analysis(fname, analysis_name_full, datafile, len, stn, x0, x1, y0, y1, dx, dy, level)
                    self.log.debug(debug)
            else:
                self.log.debug('Analysis cached')
                diva = Diva(sessionid,**self.diva_args)
                diva.load()


            nc = netCDF4.Dataset(analysis_name_full, 'r')
            v = nc.variables['analyzed_field'][:]
            try:
                fv = getattr(nc.variables['analyzed_field'],  '_FillValue')
            except:
                fv = -9999

            v = numpy.array(v)
            v[v == fv] = numpy.nan
            v = M.masked_where(numpy.isnan(v), v)

            out += "<stat id='vmin'>%g</stat>\n" % v.min()
            out += "<stat id='vmax'>%g</stat>\n" % v.max()
            out += "<stat id='stat_obs_count_used'>%g</stat>\n" % diva.ndata
            out += "<stat id='stat_posteriori_stn'>%g</stat>\n" % diva.posteriori_stn
            out += "</stats>"
            nc.close()

            self.log.info('Analysis parameters datafile, method, database, len, stn, x0, x1, dx, y0, y1, dy, level: %s',
                          str((datafile, method, database, len, stn, x0, x1, dx, y0, y1, dy, level)))

        except BaseException as error:
            out  = "<error><![CDATA["
            out += str(error)
            if self.do_debug:
                out += "\n" + traceback.format_exc()
            out +="]]></error>"

        xml += out
#        xml += "<debug> </debug>"
        xml += "</layers>"

        #self.log.info('Analysis response:')
        #self.log.info(xml)

        return HttpResponse(xml.encode('utf-8'), content_type)

    def cancel(self, sessionid = '0'):
        """Cancel a request to Diva by creating the file "cancel" in the working directory"""

        content_type = "text/xml"
        sessionid = self.sanitize_sessionid(sessionid)

        wdir = self.diva_template + "_" + sessionid
        f = open(os.path.join(wdir, 'cancel'), 'w')
        f.close()

        xml  = '<?xml version="1.0"?>\n'
        xml += "<layers>\n"
        xml += "</layers>"
        return HttpResponse(xml.encode('utf-8'), content_type)

    def save(self, sessionid, fieldname, embed_name, embed_email):
        """Save a field"""

        content_type = "text/xml"
        sessionid = self.sanitize_sessionid(sessionid)

        filename = join_path(self.wms_root, fieldname)

        saved_file = join_new_path(os.path.join(self.wms_root, 'Saved'), fieldname)
        shutil.copy2(filename, saved_file)

        f = open(saved_file + '.txt', 'w')
        print("sessionid", sessionid, "Name: ", embed_name, "Email: ", embed_email, "File: ", fieldname,file=f)
        f.close()

        xml  = '<?xml version="1.0"?>\n'
        xml += "<layers>\n"
        xml += "</layers>"
        return HttpResponse(xml.encode('utf-8'), content_type)


    def send_bug(self, sessionid, fieldname, bug_name, bug_email, bug_description):
        """send email on bug report"""

        content_type = "text/xml"
        sessionid = self.sanitize_sessionid(sessionid)

        saved_file = os.path.join(self.wms_root, 'Saved', 'bug_repports.txt')

        f = open(saved_file + '.bug', 'a')
        print("sessionid", sessionid, "Name: ", bug_name, "Email: ", bug_email, "File: ", fieldname, "Description", bug_description,file=f)
        f.close()

        return self.send_feedback(description = bug_description,
                                  sessionid = sessionid,
                                  name = bug_name,
                                  email = bug_email,
                                  observations = os.path.join(self.wms_root, sessionid + '.dat'),
                                  analysis = fieldname,
                                  )


    def send_feedback(self, **kwargs):
        """Save a field"""

        content_type = "text/xml"
        body = '\r\n'.join([item + ': ' + kwargs[item] for item in kwargs])

        try:
            # send email on bug report
            import smtplib

            message = ("From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n"
                       % (self.email_fromaddr, ", ".join(self.email_toaddrs), self.email_subject) )

            message += body

            xml  = '<?xml version="1.0"?>\n'
            xml += '<layers>\n'

            server = smtplib.SMTP('smtp.gmail.com', 587)
            #server = smtplib.SMTP('smtp.gmail.com_nowhere', 587)
            #server.set_debuglevel(1)
            server.ehlo()
            server.starttls()
            server.login(self.email_login, self.email_password)
            server.sendmail(self.email_fromaddr, self.email_toaddrs, message)
            server.quit()
            xml += 'OK'
        except BaseException as error:
            xml += '<error><![CDATA['
            xml += 'An unexpected internal error occured and your message could not be send. Please send an email to '
            xml += ', '.join(self.email_toaddrs) + ' with the following information:\n\n'
            xml += body
            xml += '\n\nThank you for your feedback!'
            #xml += str(error)
            #if self.do_debug:
            #    xml += "\n" + traceback.format_exc()
            xml +=']]></error>'

        xml += '</layers>'
        return HttpResponse(xml.encode('utf-8'), content_type)



    def fit(self, sessionid):
        content_type = "text/xml"
        xml  = '<?xml version="1.0"?>\n'
        xml  = '<diva>\n'
        sessionid = self.sanitize_sessionid(sessionid)

        out = ""

        try:
            diva = Diva(sessionid,**self.diva_args)
            diva.load()

            try:
                (L, stn, varbak, quality) = diva.fit(N = 1000)
            except DivaError:
                (L, stn, varbak, quality) = diva.fit()

            figname = os.path.join(self.datadir, "%s_divafit.png" % (sessionid))
            figname_full = os.path.join(self._root, figname)
            diva.plot_fit(figname_full)

            out += "<stats>\n"
            out += "<stat id='len'>%s</stat>\n" % L
            out += "<stat id='stn'>%g</stat>\n" % stn
            out += "<stat id='varbak'>%g</stat>\n" % varbak
            out += "<stat id='quality'>%g</stat>\n" % quality
            out += "<plot>%s</plot>\n" % figname
            out += "</stats>"
        except BaseException as error:
            out += "<error><![CDATA["
            out += str(error)
            if self.do_debug:
                out += "\n" + traceback.format_exc()
            out +="]]></error>"

        xml += out
#        xml += "<debug> </debug>"
        xml += '</diva>\n'

        return HttpResponse(xml.encode('utf-8'), content_type)



    def download(self,db="gebco",
                 x0=None, x1=None, y0=None, y1=None,
                 dx=None, dy=None, **kwargs):
        '''Handle download requests. If necessary data is converted in the
        appropriate format'''

        fieldname = kwargs.get("fieldname","default.nc")
        type = kwargs.get("type","nc")
        # all keys with non empty value
        keys = [k for k in kwargs.keys() if kwargs[k] != '']
        varname = kwargs.get('varname','analyzed_field')

        if 'errname' in kwargs:
            errname = kwargs['errname']
        elif 'varname' in kwargs:
            errname = varname + '_error_field'
        else:
            errname = 'error_field'

        if (type == "nc"):

            filename = join_path(self.wms_root, fieldname)

            if ('varname' in keys or
                any([k.startswith('ga_') for k in keys]) or
                any([k.startswith('va_') for k in keys])):

                # load input
                nc = netCDF4.Dataset(filename, 'r')
                analyzed_field = nc.variables['analyzed_field'][:]
                error_field = nc.variables['error_field'][:]
                lon = nc.variables['x'][:]
                lat = nc.variables['y'][:]
                nc.close()

                # save output
                outname = join_new_path(self.wms_root, fieldname + ".nc")
                nc = netCDF4.Dataset(outname,'w',format='NETCDF3_64BIT')
                nc.createDimension('lon',len(lon))
                nc.createDimension('lat',len(lat))

                vlon = nc.createVariable('lon','f',('lon',))
                vlat = nc.createVariable('lat','f',('lat',))
                va = nc.createVariable(varname,'f',('lat','lon'),fill_value=-9999)
                ve = nc.createVariable(errname,'f',('lat','lon'),fill_value=-9999)

                vlon.units = 'degree_east'
                vlat.units = 'degree_north'
                #va._FillValue = -9999
                #ve._FillValue = -9999

                # set global and variable attributes
                for key in kwargs:
                    if key.startswith('ga_'):
                        attr = key[3:]
                        #nc.setncattr(attr,kwargs[key])
                        nc.__setattr__(attr,kwargs[key])
                    elif key.startswith('va_'):
                        attr = key[3:]
                        #v.setncattr(attr,kwargs[key])
                        va.__setattr__(attr,kwargs[key])

                # write data
                vlon[:] = lon
                vlat[:] = lat
                va[:] = analyzed_field
                ve[:] = error_field

#                 nc.renameVariable('x', 'longitude')
#                 nc.renameVariable('y', 'latitude')
#                 nc.renameDimension('x', 'longitude')
#                 nc.renameDimension('y', 'latitude')

                nc.close()
            else:
                outname = filename



            content_type = "application/x-netcdf"
            savename = "diva_results.nc"
        elif (type == "mat"):
            filename = join_path(self.wms_root, fieldname)
            outname = join_new_path(self.wms_root, fieldname + ".mat")
            make_mat(filename, "analyzed_field", outname)

            content_type = "application/octet-stream"
            savename = "diva_results.mat"
        elif (type == "bat-nc"):
            # determine which bathymetry to take
            content_type = "application/x-netcdf"
            savename = "diva_bath.nc"
            x0 = float(x0)
            x1 = float(x1)
            y0 = float(y0)
            y1 = float(y1)
            dx = float(dx)
            dy = float(dy)

            outname = join_new_path(self.wms_root,
                                    filename = "bath_%s_%g_%g_%g_%g_%g_%g.nc" % (db, x0, x1, dx, y0, y1, dy))
            extract_bath(outname,x0,x1,y0,y1,
                         dx,dy,db)

        # fixme: file not not closed
        return HttpResponse(open(outname,'rb'),content_type,
                            headers = {'Content-Disposition':
                                       'attachment;filename="%s"' % savename})



# return all child processes
# is there a more portable way?

def child_processes(pid):
    output = subprocess.Popen(["/bin/ps", "-o", "pid=", "--ppid", str(pid)], stdout=subprocess.PIPE).communicate()[0]
    return [int(s) for s in output.splitlines() ]

def analysis(fname, analysis_name, temp_name, len, stn, x0, x1, y0, y1, dx, dy, level):
    cmd = """
ndvar_web('%s', '%s',%f,%f,%f,%f,%f,%f,%f,%f,%f);
""" % (fname, analysis_name, len, stn, x0, x1, y0, y1, dx, dy, level)

    return run_octave(cmd)

class Diva:
    """diva session"""

    def __init__(self, temp_name, diva_template,
                 log = None, max_cputime = 10*60,**kwargs):

        self.diva_template = diva_template
        self.wdir = diva_template + "_" + temp_name
        self.out = ""
        self.env={"PATH": ".:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games"}
        self.len = -1
        self.x0 = -1
        self.y0 = -1
        self.x1 = -1
        self.y1 = -1
        self.dx = -1
        self.dy = -1
        self.imax = 0
        self.jmax = 0
        self.ndata = 0
        self.posteriori_stn = 0
        self.level = -9999999999
        self.database = ''
        self.log = log
        self.max_cputime = max_cputime
        self.bath = settings.bath
        self.bathdir = settings.bathdir

        if log:
            self.log = log
        else:
            self.log = logging.Logger(self.__class__.__name__)
            self.log.addHandler(logging.StreamHandler(sys.stdout))

        if not os.path.exists(self.wdir):
            shutil.copytree(self.diva_template, self.wdir, symlinks=True)

    def copy_obs(self, fname):
        shutil.copy2(fname, os.path.join(self.wdir,'input','data.dat'))

    def save(self):
        f = open(os.path.join(self.wdir, "diva_obj.txt"), "w")
        f.write("%f\n" % self.x0)
        f.write("%f\n" % self.y0)
        f.write("%f\n" % self.x1)
        f.write("%f\n" % self.y1)
        f.write("%f\n" % self.dx)
        f.write("%f\n" % self.dy)
        f.write("%f\n" % self.len)
        f.write("%f\n" % self.imax)
        f.write("%f\n" % self.jmax)
        f.write("%f\n" % self.ndata)
        f.write("%f\n" % self.posteriori_stn)
        f.write("%f\n" % self.level)
        f.write("%s\n" % self.database)
        f.close()

    def load(self):
        f = open(os.path.join(self.wdir, "diva_obj.txt"), "r")
        self.x0 = float(f.readline())
        self.y0 = float(f.readline())
        self.x1 = float(f.readline())
        self.y1 = float(f.readline())
        self.dx = float(f.readline())
        self.dy = float(f.readline())
        self.len = float(f.readline())
        self.imax = int(float(f.readline()))
        self.jmax = int(float(f.readline()))
        self.ndata = int(float(f.readline()))
        self.posteriori_stn = float(f.readline())
        self.level = float(f.readline())
        self.database = f.readline().rstrip('\n')
        f.close()


    def plot_fit(self, figname, figsize = (6,5)):
        covar_name = os.path.join(self.wdir, 'output', 'covariance.dat')
        covarfit_name = os.path.join(self.wdir, 'output', 'covariancefit.dat')

        f = pylab.figure(figsize=figsize)
        divaplotting.plot_covafit(f.gca(), covar_name, covarfit_name)
        f.savefig(figname)

    def fit(self, N = None):

        cmd = [os.path.join(self.wdir, 'divafit')]
        if N != None:
            cmd.append('%g' % N)

        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, env=self.env, cwd=self.wdir)
        p.wait()
        out = p.stdout.read().decode('utf-8')
        self.log.debug('divafit %s ' % out)

        if out.find("too few data") != -1:
            raise DivaError("too few data to use divafit")

        lines = open(os.path.join(self.wdir, 'output', 'paramfit.dat'), 'r').readlines()

        L = float(lines[1])
        snr = float(lines[3])
        varbak = float(lines[5])
        quality = float(lines[7])
        return (L, snr, varbak, quality)

    def kill_command(self, pid):
        tokill = child_processes(pid)
        tokill.append(pid)
        self.log.debug("killing process %s\n" % str(tokill))

        for pid in tokill:
            os.kill(pid, signal.SIGKILL)

    def command(self, cmd):
        finish=time.time() + self.max_cputime
        p = subprocess.Popen([self.wdir + '/' + cmd], stdout=subprocess.PIPE, stderr=subprocess.PIPE, \
                                   env=self.env, cwd=self.wdir)

        cancel_file = os.path.join(self.wdir, 'cancel')

        while (p.poll() == None) and (time.time() < finish):

            time.sleep(1) # wait 1 s
            if os.path.exists(cancel_file):
                os.remove(cancel_file)
                self.kill_command(p.pid)
                out = p.stdout.read()
                self.log.debug('cancel... %s' % out)
                raise DivaError("%s is canceld." % (cmd))

        result=p.poll()

        if (result==None):
            self.kill_command(p.pid)
            self.log.debug(p.stdout.read())

            raise DivaError("%s exceeded maximum allowed CPU time. Consider to install DIVA on your computer or contact the GHER to ask for a temporary higher CPU time limit." % (cmd))

        err = p.stderr.read()
        out = p.stdout.read()
        self.log.debug(out.decode('utf-8'))

        if (p.returncode != 0):
            raise DivaError("%s returned %d" % (cmd, p.returncode))
        elif (err != b"" and err != b"STOP FINISHED\n"):
            raise DivaError("%s returned %s" % (cmd, err))

        return (out, err)

    def write_paramfile(self, len, ispec, x0, y0, dx, dy, stn):
        f = open(self.wdir + "/input/param.par", "w")
        f.write("""# Lc: correlation length (in units coherent with your data)
%g
# icoordchange (=0 if no change of coordinates is to be performed; =1 if positions are in degrees and if you want to use real distances)
0
# ispec: output files required
%g
# ireg: mode selected for background field: 0=null guess; 1=mean of data; 2=regression plan if at least 3 non-aligned data provided
1
# xori: x-coordinate of the first grid point of the output
%g
# xori: y-coordinate of the first grid point of the output
%g
# dx: step of output grid
%g
# dy: step of output grid
%g
# nx: number of grid points in the x-direction
%g
# ny: number of grid points in the y-direction
%g
# valex: exclusion value
-9999.0
# snr: signal to noise ratio of the whole dataset
%g
# varbak: variance of the background field. If zero, no error fields are produced. If one, relative errors are obtained
1
""" % (len, ispec, x0, y0, dx, dy, self.imax, self.jmax, stn))
        f.close()


    def load_mesh(self, FAC = 1):
        """load diva mesh"""

        statfile = self.wdir + "/meshgenwork/fort.23"
        meshfile = self.wdir + "/meshgenwork/fort.22"

        with open(statfile,'r') as f:
            # number of vertices/nodes ???
            nd1 = int(f.readline())
            nd2 = int(f.readline())
            # number of triangles ???
            nd3 = int(f.readline())

        inode = []
        x = []
        y = []

        meshx = []
        meshy = []

        with open(meshfile,'r') as f:
            for i in xrange(nd1):
                a = f.readline().split()
                inode.append(int(a[0]))
                x.append(float(a[1]))
                y.append(float(a[2]))


            for i in xrange(nd2):
                f.readline()


            for i in xrange(nd3):
                # -1: convert to 0-based indexes
                i = [int(p)-1 for p in f.readline().split()]
                # only every second element is used
                i = i[0:-1:2]


                xc=(x[i[0]]+x[i[1]]+x[i[2]])/3
                yc=(y[i[0]]+y[i[1]]+y[i[2]])/3

                xt = [xc+(x[i[j]]-xc)*FAC for j in range(3)]
                yt = [yc+(y[i[j]]-yc)*FAC for j in range(3)]

                meshx.extend(xt)
                meshx.extend([xt[0], numpy.NaN])

                meshy.extend(yt)
                meshy.extend([yt[0], numpy.NaN])

        return (meshx, meshy)



    def analysis(self, analysis_name, database, len, stn, x0, x1, y0, y1, dx, dy, level,adv,diff):
        mask = os.path.join(self.wdir,"input","topo.grd")
        ispec = 11

        # skip grid generation if possible

        if (self.len == -1 or abs(len-self.len) > 0.1*len
            or level != self.level
            or x0 != self.x0 or x1 != self.x1 or dx != self.dx
            or y0 != self.y0 or y1 != self.y1 or dy != self.dy
            or database != self.database
        ):
#          if (True):

            # determine which bathymetry to take
            res = min(dx, dy)/3
            bathymetry = select_multires(self.bath[database],res)['file']

            bathymetry = os.path.join(self.bathdir, bathymetry)


            cmd = """
bathymetry = '%s';
x0 = %f;
x1 = %f;
dx = %f;
y0 = %f;
y1 = %f;
dy = %f;
level  = %f;
mask = '%s';
isglobal = %f;

    [xi, yi, mi] = get_mask(bathymetry, isglobal, x0, x1, dx, y0, y1, dy, level);
    global UWRITE_DEFAULT_FORMAT
    UWRITE_DEFAULT_FORMAT='ieee-le';
    gwrite(mask, double(mi));

whos mi
y0
y1
dy
fprintf(1, 'imax %%d ', size(mi,1));
fprintf(1, 'jmax %%d ', size(mi,2));
    disp('end octave');
    """ % (bathymetry, x0, x1, dx, y0, y1, dy, level, mask, int(database == "gebco"))

            self.log.debug('octave: %s ' % cmd)

            out = run_octave(cmd)
            out = out.decode('utf-8')
            self.log.debug('octave output: %s ' % out)

            # (x1-x0)/dx should be integer
            p = re.compile(r'imax\s(?P<imax>\d+)\s+jmax\s(?P<jmax>\d+)').search(out)
            self.imax = float(p.group('imax'))
            self.jmax = float(p.group('jmax'))

            #imax = numpy.floor((x1-x0)/dx + .5)+1
            #jmax = numpy.floor((y1-y0)/dy + .5)+1

            f = open(self.wdir + "/input/TopoInfo.dat", "w")
            f.write("%g\n%g\n%g\n%g\n%g\n%g\n" % (x0, y0, dx, dy, self.imax, self.jmax))
            f.close()


            f = open(self.wdir + "/input/contour.depth", "w")
            f.write("%g" % (.5))
            f.close()

            self.write_paramfile(len, ispec, x0, y0, dx, dy, stn)

            self.command("divaclean")
            self.command("divacont")

            shutil.copy2(self.wdir + '/output/coast.cont', self.wdir + '/input/coast.cont')

            self.command("divamesh")

            self.len = len
            self.x0 = x0
            self.y0 = y0
            self.x1 = x1
            self.y1 = y1
            self.dx = dx
            self.dy = dy
            self.level = level
            self.database = database
        else:
            self.write_paramfile(len, ispec, x0, y0, dx, dy, stn)


        # remove constraint.dat if exists
        cname = os.path.join(self.wdir,'input','constraint.dat')

        #print 'cname ',cname
        if os.path.exists(cname):
            os.remove(cname)

        if adv != 0. or diff != 0.:
            if not os.path.exists(os.path.join(self.wdir,'input','UVinfo.dat')):
                raise DivaError("If advection contraint is activated, then a velocity field must be uploaded")

            f = open(cname, "w")
            f.write("%g\n%g\n" % (adv, diff))
            f.close()



        (out, err) = self.command("divacalc")
        out = out.decode('utf-8')
        #p = re.compile(r'Total number of data constraint: ndata =\s*(?P<ndata>\d+)').search(out)
        p = re.compile(r'There are\s*(?P<ndata>\d+)\s*data localized in the mesh').search(out)

        self.ndata = float(p.group('ndata'))


        f = open(self.wdir + '/divawork/fort.27', 'r')
        stat = f.readlines()
        self.posteriori_stn = float(stat[1])
        f.close()

        shutil.copy2(self.wdir + '/output/ghertonetcdf/results.nc', analysis_name)



class mesh_webmapserver(webmap.caching_webmapserver):
    def __init__(self, diva_template, use_pic_cache = True):
        """Initialize the WMS instance"""
        super(mesh_webmapserver, self).__init__(use_pic_cache)
        self.diva_template = diva_template

    def make_plot(self, **kwargs):
        # layers will be the sessionid
        layer = kwargs.get('layers')[0]
        (x0, y0, x1, y1) = kwargs.get('bbox',(-180,-90,180,90))

        width = kwargs.get('width',256)
        height = kwargs.get('height',256)
        format = kwargs.get('format', 'image/png')


        self.log.debug('path %s' % str(sys.path))
        self.log.debug('numpy version %s' % numpy.__version__)
        self.log.debug('matplotlib version %s' % matplotlib.__version__)

        #y1 = min(y1,90)

        diva = Diva(layer,self.diva_template)
        diva.load()
        (meshx, meshy) = diva.load_mesh()

        wmsc = webmap.wms_canvas(layer, x0, y0, x1, y1, width, height)
        wmsc.m.plot(meshx, meshy, '#cccccc')

        return HttpResponse(wmsc.render(format),format)
