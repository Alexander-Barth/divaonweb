# Copyright (C) 2008-2014 Alexander Barth <barth.alexander@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import print_function

import sys
import time
import os

from divaonweb import settings
from divaonweb.util import HttpResponse
from divaonweb.webmap import caching_webmapserver

try:
    from urllib.request import pathname2url
except ImportError:
    # python 2
    from urllib import pathname2url

try:
    from urllib.request import urlopen
except ImportError:
    # python 2
    from urllib2 import urlopen


class proxy_webmapserver(caching_webmapserver):
    def __init__(self, url, **kwargs):
        """Initialize the WMS instance"""
        super(proxy_webmapserver,self).__init__(**kwargs)
        self.url = url

    def get_capabilities(self, **kwargs):
        return self.forward(**kwargs)

    def get_stats(self, **kwargs):
        return self.forward(**kwargs)

    def make_plot(self,**kwargs):
        return self.forward(**kwargs)

    def forward(self,**kwargs):
        error = True
        N = 0

        kw=kwargs['original_arguments'].copy()

        url = self.url

        if '_proxy_url' in kw:
            url = kw['_proxy_url']
            del kw['_proxy_url']
            self.log.info('proxy: access %s' % (url))


        # append ? or & if necessary
        if url[-1] not in ['?','&']:
            if '?' not in url:
                url += '?'
            elif url[-1] != '&':
                url += '&'

        for k in kw:
            if not k.startswith('_'):
                url += k + "=" + pathname2url(kw[k]) + "&"

        self.log.debug('getting %s' % (url))

        while error and N < 1:
            f = urlopen(url)

            error = False
            content_type = None

            for h in f.headers.items():
                if h[0].lower() == 'content-type':
                    content_type = h[1]


            error = content_type == "application/vnd.ogc.se_xml"

            if error:
                time.sleep(1)

            N = N+1

        if error:
            self.log.error('failed after %d tries to get %s ' % (N,url))
            self.log.error(error)
            raise BaseException("no image from server")

        content = f.read()

        if content_type in ['application/vnd.ogc.wms_xml','application/vnd.ogc.gml']:
            content_type = 'text/xml'

        #print('content_type',content_type, type(content), file=sys.stderr)
        # content should now be bytes (or str) and never unicode

        # old code:
        #if content_type == 'text/xml':
        #    # will be re-encoded in HttpResponse
        #    content = content.decode('utf-8')

        return HttpResponse(content,content_type)
