#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import


from divaonweb.webmap import base_webmapserver, HttpResponse, webmapserver, folder_description, layer_description, wms_canvas, catalogue_url, WMSStyleEncode
from divaonweb import vocab
from divaonweb.vocab import parseTimeStr
from divaonweb.util import distance
from divaonweb import settings
import xml.etree.ElementTree as ET
import json
import requests
import sys
import base64

try:
    # For Python 3.0 and later
    from urllib.request import urlopen
    from urllib.parse import urlencode
except ImportError:
    # Fall back to Python 2's urllib2
    from urllib2 import urlopen
    from urllib import  urlencode

import requests
import os

namespaces = {
    'wps': 'http://www.opengis.net/wps/1.0.0',
    'ows': 'http://www.opengis.net/ows/1.1'
}

def short(k):
    return k.split('::')[-1]


def list_used_params(url,loader = vocab.loader):

    r = requests.get(url)

    d = r.json()

    variables = [];

    for feature in d['features']:
        prop = feature['properties'];

        #print(prop['p35_id'],file=sys.stderr)

        c = vocab.Concept(vocab.collection_URL + 'P35/current/' + prop['p35_id'],loader = loader)
        p36 = c.find('broader','P36')
        units = c.find('related','P06')
        #s26 = c.find('narrower','P01').find("broader","S26")

        if p36 != None and units != None:
            variables.append({
                "prefTitle":c.prefLabel,
                "codes":short(c.notation),
                "altTitle":c.altLabel,
                "p36_code":short(p36.notation),
                "p36_title":p36.prefLabel,
                #"s26_code":short(s26.notation),
                #"s26_title":s26.prefLabel,
                "starttime":"1900-00-00 00:00:00",
                "endtime":"2100-00-00 00:00:00",
                "id": short(units.notation),
                "units": units.altLabel,
                "units_title": units.prefLabel,
                "zidentifier": prop['zidentifier']
            })
    return variables



def WPSProcessURL(baseurl,datainputs,identifier):
   url = (baseurl + '?DataInputs=[' + ';'.join([k + '=' + str(v) for k,v in datainputs.items()])
          + ']&service=wps&request=Execute&Identifier=' + identifier + '&version=1.0.0')

   print("WPSProcessURL ",url,file=sys.stderr)
   r = requests.get(url)
   data = r.text
   tree = ET.fromstring(data)
   return tree,url


class WPS_webmapserver(webmapserver):
    def __init__(self,baseurl,usedparams_url,wms_url,vocabpath,service = {}, **kwargs):
        super(WPS_webmapserver, self).__init__('','',service,**kwargs)

        self.baseurl = baseurl
        self.usedparams_url = usedparams_url
        self.wms_url = wms_url
        self.variables = []
        self.feature_info_formats = ['text/html']

        self.vocabloader = vocab.CachedLoader({
            'http://vocab.nerc.ac.uk/collection/P35/current/': ET.parse(os.path.join(vocabpath,'P35.xml')),
            'http://vocab.nerc.ac.uk/collection/P36/current/': ET.parse(os.path.join(vocabpath,'P36.xml')),
            'http://vocab.nerc.ac.uk/collection/P06/current/': ET.parse(os.path.join(vocabpath,'P06.xml'))
            #'http://vocab.nerc.ac.uk/collection/S26/current/': ET.parse(os.path.join(vocabpath,'S26.xml'))
        })
        #print('self.wms_url',self.wms_url,file=sys.stderr)

    def get_capabilities_time_modified(self):
        return None


    def get_folder_description(self,basedir = ''):

        fd = folder_description(name="root", title="root")

        self.variables = list_used_params(self.usedparams_url,loader = self.vocabloader)


        for groups in sorted(set([(_['p36_code'],_['p36_title']) for _ in self.variables])):
            gd = folder_description(name=groups[0], title=groups[1])

            for var in self.variables:
                if var['p36_code'] == groups[0]:
                    layer_name = var['codes']
                    x0 = -180
                    x1 = 180
                    y0 = -90
                    y1 = 90
                    time = [('1900-01-01T00:00:00.000Z','2100-12-31T00:00:00.000Z')]
                    time_units = 'ISO8601'

                    if var['zidentifier'] == 'COREDIST':
                        elevation_units = 'm (below sea-floor)'
                        elevation = [(0,1)]
                    else:
                        elevation_units = 'm'
                        elevation = [(0,1000)]

                    legend = {'width': 150, 'height': 50,
                              #'format': 'image/png',
                              'format': 'image/svg+xml',
                              'layer': layer_name, 'style': '' }
                    # list of name and title
                    styles = [
                        (WMSStyleEncode({'method': 'pcolor','vmin': 1,'vmax': 1e4,
                                         'norm': 'log'}),
                         'default')]

                    attribution = {}
                    title = 'Number of observations of %s' % var['prefTitle']
                    item = layer_description(
                        layer_name, title, x0, x1, y0, y1,
                        time, time_units,
                        elevation, elevation_units, attribution, legend,
                        self.wms_url, styles = styles)

                    gd.layers.append(item)

            fd.layers.append(gd)

        return fd


    def get_map(self, **kwargs):
        imgformat = kwargs.get('format', 'image/png')
        layers = kwargs['layers']
        (x0, y0, x1, y1) = kwargs.get('bbox', (-180, -90, 180, 90))

        # must be ranges
        elevation = kwargs.get('elevation')
        time = kwargs.get('time', 'default').split('/')

        styles = kwargs['styles']
        width = kwargs.get('width', 256)
        height = kwargs.get('height', 256)
        decorated = kwargs.get('decorated', False)
        CRS = kwargs.get('crs', 'EPSG:4326')


        #print("styles",styles,file=sys.stderr)
        if y1-y0 > 22.5:
            markersize = 0.5
        elif y1-y0 >= 11.5:
            markersize = 2
        elif y1-y0 >= 5.625:
            markersize = 3
        else:
            markersize = 5


        datainputs = {
            'bbox': '%g,%g,%g,%g'  % (x0,y0,x1,y1),
            'mindatetime': time[0],
            'maxdatetime': time[1],
            'minz': '%g' % elevation[0],
            'maxz': '%g' % elevation[1],
            'p35': layers[0],
            'markersize': markersize,
            'width': width,
            'vmin': styles[0]["vmin"],
            'vmax': styles[0]["vmax"]
        }

        # 'bbox=BOX(-324.51171875 -57.34375,344.51171875 142.34375);mindatetime=1900-01-01T00:00:00Z;maxdatetime=2100-01-01T00:00:00Z;minz=0;maxz=10;p35=EPC00004;width=1000

        #http://localhost/cgi-bin/pywps.cgi?DataInputs=[bbox=BOX(-324.51171875 -57.34375,344.51171875 142.34375);mindatetime=1900-01-01T00:00:00Z;maxdatetime=2100-01-01T00:00:00Z;minz=0;maxz=10;p35=EPC00004;width=1000]&service=wps&request=Execute&Identifier=observed_cindex&version=1.0.0

        tree,url = WPSProcessURL(self.baseurl,datainputs,"observed_cindex")

        # url = self.baseurl + '?DataInputs=[' + ';'.join([k + '=' + str(v) for k,v in datainputs.items()]) + ']&service=wps&request=Execute&Identifier=observed_cindex&version=1.0.0'

        # #print('datainputs',datainputs,file=sys.stderr)

        # #print('markersize',y1-y0,markersize,file=sys.stderr)

        coded_image = None
        for output in tree.findall('.//wps:Output',namespaces):
            if output.find('ows:Identifier',namespaces).text == 'image':
                coded_image = output.find('.//wps:LiteralData',namespaces).text

        if coded_image == None:
            raise Exception("no ows:Identifier 'image' found for URL %s" % (url,))

        img = base64.b64decode(coded_image)

        #f = open('/tmp/image.png','wb')
        #f.write(img)
        #f.close()

        return HttpResponse(img,'image/png')

    def get_stats(self,**kwargs):
        (x0, y0, x1, y1) = (-180, -90, 180, 90)

        layer = kwargs["layer"]
        elevation = kwargs.get('elevation')
        time = kwargs.get('time', "default").split('/')
        env = kwargs.get('_env')

        datainputs = {
            'bbox': '%g,%g,%g,%g'  % (x0,y0,x1,y1),
            'mindatetime': time[0],
            'maxdatetime': time[1],
            'minz': '%g' % elevation[0],
            'maxz': '%g' % elevation[1],
            'p35': layer,
        }

        #url = "http://emodnet02.cineca.it/wps?DataInputs=[mindatetime=2000-01-01T00:00:00.000Z;maxz=10;maxdatetime=2001-01-01T00:00:00.000Z;bbox=0,0,45,45;width=512;minz=0;p35=EPC00007]&service=wps&request=Execute&Identifier=legenda_bnd&version=1.0.0"
        tree,url = WPSProcessURL(self.baseurl,datainputs,"legenda_bnd")

        overlaylim = None
        for output in tree.findall('.//wps:Output',namespaces):
            if output.find('ows:Identifier',namespaces).text == 'overlaylim':
                text = output.find('.//wps:ComplexData',namespaces).text
                overlaylim = json.loads("[" + text + "]")

        #print("overlaylim",overlaylim,file=sys.stderr)
        out = """<stats>
<param id="vmin" type="float" default="%g" label="Minimum color-bar range"></param>
<param id="vmax" type="float" default="%g" label="Maximum color-bar range"></param>
<xmin>%g</xmin><xmax>%g</xmax><ymin>%g</ymin><ymax>%g</ymax><vmin>%g</vmin><vmax>%g</vmax>
</stats>""" % (overlaylim[0],overlaylim[1],-1,1,-1,1,overlaylim[0],overlaylim[1])

        return HttpResponse(out,'text/xml')

    def get_feature_info(self, **kwargs):
        '''gets the value of the field at selected location'''

        # get parameters
        version = kwargs.get('version','1.1.1')
        layers = kwargs['query_layers'].split(',')
        (x0, y0, x1, y1) = kwargs.get('bbox', (-180, -90, 180, 90))

        if version == '1.1.1':
            i = float(kwargs['x'])
            j = float(kwargs['y'])
        else:
            i = float(kwargs['i'])
            j = float(kwargs['j'])

        elevation = kwargs.get('elevation')
        time = kwargs.get('time', 'default').split('/')

        width = kwargs.get('width', 256)
        height = kwargs.get('height', 256)

        # TODO take projection into account

        # calculate longitude and latitude of selected point
        x = x0 + i * (x1-x0)/width
        y = y1 + j * (y0-y1)/height

        pixels = 2.5
        pixels = 10
        dx = pixels/float(width) * (x1-x0)
        dy = pixels/float(height) * (y1-y0)

        # bounding box should never be larger than 0.4 x 0.4 degree
        dx = min(dx,0.4/2)
        dy = min(dy,0.4/2)

        #print('bbox',pixels,dx,dy,file=sys.stderr)

        p36 = vocab.Concept(vocab.collection_URL + 'P35/current/' + layers[0],loader = self.vocabloader).find('broader','P36').prefLabel;

        disclaimer = p36.lower() in ['antifoulants','hydrocarbons','heavy metals','polychlorinated biphenyls','pesticides and biocides','radionuclides']

        #print('vocab',p36,disclaimer)

        bbox = [str(_) for _ in [x-dx,x+dx,y-dy,y+dy]]

        # load variable if necessary
        if len(self.variables) == 0:
            print('load variables',file=sys.stderr)
            self.variables = list_used_params(self.usedparams_url,loader = self.vocabloader)
        else:
            print('variables already loaded',file=sys.stderr)

        zid = [v['zidentifier'] for v in self.variables if v['codes'] == layers[0]][0]

        url = 'view.html?' + urlencode({
            'bbox': ','.join(bbox),
            'parameter': layers[0],
            'disclaimer': disclaimer,
            'starttime': parseTimeStr(time[0]).strftime('%Y-%m-%dT%H:%M:%SZ'),
            'endtime': parseTimeStr(time[1]).strftime('%Y-%m-%dT%H:%M:%SZ'),
            'zlim0': '%g' % elevation[0],
            'zlim1': '%g' % elevation[1],
            'z': zid,
        });

        html = '<div><iframe width="815" height="590" src="%s" style="border-width: 0px"></iframe></div>' % (url,)

        return HttpResponse(html,'text/html')
