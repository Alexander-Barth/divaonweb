# Copyright (C) 2008-2015 Alexander Barth <barth.alexander@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from __future__ import absolute_import

import sys
import os
from divaonweb import settings
from divaonweb import webmap
from divaonweb import webmap_proxy
from divaonweb import obsplots
from divaonweb import WPSwebmapserver
import numpy
import matplotlib
import mimetypes
import logging
import string

from paste.request import parse_formvars
from paste.deploy.converters import asbool, aslist

from divaonweb.divaonweb import diva_on_web, mesh_webmapserver
from divaonweb.util import HttpResponse, proxy


class DivaServer(object):
    def __init__(self, root, name="diva-on-web server", template=None, verbose=False,
                 theme = 'default', diva_template = '', **kwargs):

        # log file
        settings.log.addHandler(logging.FileHandler(kwargs['log']))
        settings.log.info('init DivaServer')

        # Ensure that root ends with /.
        if not root.endswith('/'): root = '%s/' % root

        self.root = root
        self.template = template
        self.name = name
        self.theme = theme
        self.header = kwargs.get('header','')
        self.title = kwargs.get('title',name)
        self.template_namespace =  {
            'title': self.title,
            'header': self.header,
            'theme': self.theme,
            'name': self.name,
            'URL': kwargs.get('URL',''),
            'contact_URL': kwargs.get('contact_URL',''),
            'subtitle': kwargs.get('subtitle',''),
            'version':  '.'.join(str(d) for d in webmap.__version__)
        }

        # wms_url can be a relative URL or a full URL
        wms_url = kwargs.get('wms_url','Python/web/wms?')

        # make a fully qualified domain name
        #if not wms_url.startswith('http://'):
        #    import socket
        #    wms_url = "http://" + socket.getfqdn() + wms_url


        #print('wms_url',wms_url,file=sys.stderr)


        settings.diva_template = diva_template
        settings.tmpdir = kwargs['tmpdir']
        settings.octave = {'bin': 'octave',
#                           'env': {},
                           'env': os.environ,
                           'path': os.path.join(root,'..','Octave')}


        settings.bathdir = kwargs['bathdir']

        settings.use_pic_cache = asbool(kwargs.get('enable_cache_wms','1'))
        settings.use_cache     = asbool(kwargs.get('enable_cache_diva','1'))
        settings.opendap_baseurl = kwargs.get('opendap_baseurl',None);
        settings.http_baseurl = kwargs.get('http_baseurl',None);
        settings.LocalCDI_baseurl = kwargs.get('LocalCDI_baseurl',None)
        settings.GlobalCDI_baseurl = kwargs.get('GlobalCDI_baseurl',None)
        settings.catalogue_url = kwargs.get('catalogue_url',None)
        settings.catalogue_url_xml = kwargs.get('catalogue_url_xml',None)

        self.bath = settings.bath

        service = {
            'accessconstraints': 'None',
            'fees': 'no conditions apply',
            'contact_position': ''
        }

        for k in ['title','abstract','contact_person','contact_organization','email',
                  'inspire_metadata_url']:
            if 'wms_' + k in kwargs:
                service[k] = kwargs['wms_' + k]

        service['keywords'] = aslist(kwargs.get('wms_keywords',''))

        fontfile = kwargs.get('fontfile','/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans.ttf')

        os.environ['HOME'] = settings.tmpdir

        self.wms_root = kwargs.get('static_wms_root', '/var/www/data')
        self.dyn_wms_root = os.path.join(root, settings.datadir)


        email_conf = {}

        for k in ['fromaddr','login','password','subject']:
            email_conf[k] = kwargs.get('email_' + k,'')

        email_conf['toaddrs'] = aslist(kwargs.get('email_toaddrs',''))

        settings.wms_root = self.wms_root
        # Wep Map Server instances

        self.WMS = webmap.webmapserver(root=self.wms_root,
                                       directory='',
                                       wms_url = wms_url,
                                       log = settings.log,
                                       fontfile = fontfile,
                                       use_pic_cache = settings.use_pic_cache,
                                       tmpdir = settings.tmpdir,
                                       service = service)

        self.WMS_diva_on_web = webmap.webmapserver(root=self.dyn_wms_root,
                                                   directory='',
                                                   wms_url = wms_url,
                                                   log = settings.log,
                                                   fontfile = fontfile,
                                                   use_pic_cache = settings.use_pic_cache,
                                                   tmpdir = settings.tmpdir,
                                                   service = service)

        self.VerticalSectionWMS = webmap.vertsection_webmapserver(root=self.wms_root,tmpdir = kwargs['tmpdir'],
                                                                  log = settings.log,
                                                                  fontfile = fontfile,
                                                                  use_pic_cache = settings.use_pic_cache)

        self.ContWMS = webmap.continents_webmapserver(bathdir = kwargs['bathdir'],
                                                      log = settings.log,
                                                      fontfile = fontfile,
                                                      use_pic_cache = settings.use_pic_cache,
                                                      tmpdir = settings.tmpdir)

        self.ProxyWMS = webmap_proxy.proxy_webmapserver("http://wms.jpl.nasa.gov/wms.cgi?",
                                                        log = settings.log,
                                                        fontfile = fontfile,
                                                        use_pic_cache = settings.use_pic_cache,
                                                        tmpdir = settings.tmpdir)

        if 'obs_index' in kwargs:
            obsservice = service.copy()
            obsservice['title'] = kwargs.get('obs_wms_title',obsservice['title'] + " - Plots")
            dbfile = kwargs['obs_dbfile']
            groupsname = kwargs['obs_groupsname']

            self.PlotsWMS = obsplots.obs_webmapserver(
                kwargs['obs_index'],
                os.path.join(kwargs['tmpdir'],'P011'),
                wms_url.replace('wms','plots'),
                obsservice,
                dbfile,
                groupsname,
                title = self.title + " - Plots",
                log = settings.log)

        self.divaonweb = diva_on_web(root,
                                               self.dyn_wms_root,
                                               diva_template,
                                               settings.datadir,
                                               log = settings.log,
                                               email_fromaddr=email_conf['fromaddr'],
                                               email_toaddrs=email_conf['toaddrs'],
                                               email_subject=email_conf['subject'],
                                               email_login=email_conf['login'],
                                               email_password=email_conf['password'],
                                               )

        self.MeshWMS = mesh_webmapserver(diva_template)


        vocabpath = os.path.join(kwargs['tmpdir'],'vocab')
        if os.path.exists(vocabpath):
            # Deltares WPS
            baseurl = 'http://emodnet02.cineca.it/wps'
            #usedparams_url = 'http://emodnet02.cineca.it/geoserver/emodnet/ows?service=WFS&version=1.0.0&outputFormat=json&request=GetFeature&typeName=emodnet:p35_used'
            usedparams_url = 'http://emodnet02.cineca.it/geoserver/emodnet2019/ows?service=WFS&version=1.0.0&outputFormat=json&request=GetFeature&typeName=emodnet2019:p35_used'
            service = {'title': 'EMODNET Chemistry - Dynamic Plots'}

            self.WPSWMS = WPSwebmapserver.WPS_webmapserver(
                baseurl,
                usedparams_url,
                wms_url.replace('wms','wpswms'),
                vocabpath,
                service)

    def __call__(self, environ, start_response):
        path_info = environ.get('PATH_INFO', '').lstrip('/')
        response = HttpResponse()

        if path_info == 'clim.html' or path_info == '':

            headers = [('Content-Type', 'text/html'),
                       ('Content-Encoding', 'utf-8'), ]

            index = os.path.join(self.template, 'index.tmpl')

            f = open(index,'r')
            t = string.Template(f.read()).substitute(self.template_namespace)
            f.close()

            start_response('200 OK', headers)

            if sys.version_info.major == 3:
                return [t.encode('utf-8')]
            else:
                return [unicode(t).encode('utf-8')]


        elif path_info.startswith('Python'):
            # dynamic
            part = path_info.split('/', 1)[1]
            kwargs = parse_formvars(environ).mixed()

            #print "filename", path_info, part

            if part.startswith('web'):
                method = part.split('/', 1)[1]
                #print "filename", path_info, part, kwargs
                response = getattr(self,method)(environ,**kwargs)

                start_response(response.http_code, response.getheaders())
                return response.content
        elif path_info.startswith('proxy'):
            return proxy(environ,start_response)

        settings.log.warn('path %s not found' % path_info)
        start_response("404 Not Found", [('Content-type', 'text/plain')])
        return ['Page not found: ' + path_info]


    def wms(self,environ,**kwargs):
        #print('environ',environ,file=sys.stderr)
        #print('wms request',file=sys.stderr)
        kwargs['_env'] = environ
        return self.WMS.request(**kwargs)

    def diva_on_web(self,environ,**kwargs):
        kwargs['_env'] = environ
        return self.WMS_diva_on_web.request(**kwargs)

    def continents(self,environ,**kwargs):
        kwargs['_env'] = environ
        return  self.ContWMS.request(**kwargs)

    def bluemarble(self,environ,**kwargs):
        kwargs['_env'] = environ
        return self.ProxyWMS.request(**kwargs)

    def plots(self,environ,**kwargs):
        kwargs['_env'] = environ
        return self.PlotsWMS.request(**kwargs)

    def wpswms(self,environ,**kwargs):
        kwargs['_env'] = environ
        return self.WPSWMS.request(**kwargs)

    def download(self,environ,**kwargs):
        return self.divaonweb.download(**kwargs)

    def upload(self,environ, data, **kwargs):
        sessionid = '1234'
        return self.divaonweb.upload(sessionid,data,**kwargs)

    def uploadvel(self,environ, sessionid, data_vel, **kwargs):
        return self.divaonweb.uploadvel(sessionid,data_vel,**kwargs)


    def make_analysis(self,environ, sessionid, variable='temp',len=1,stn=1,
             dx = 0.5, dy = 0.5, level = 0,method='diva',**kwargs):

        return self.divaonweb.make_analysis(sessionid, variable,len,stn,
                                                                                  dx, dy, level,method,**kwargs)

    def cancel(self,environ, sessionid):
        '''Cancel a request to Diva by creating the file 'cancel' in the working directory'''

        return self.divaonweb.cancel(sessionid)


    def save(self,environ, sessionid, fieldname, embed_name, embed_email):
        '''Save a field'''

        return self.divaonweb.save(sessionid, fieldname, embed_name, embed_email)

    def send_bug(self,environ, sessionid, fieldname='', bug_name='', bug_email='', bug_description=''):
        '''Send a bug report'''
        return self.divaonweb.send_bug(sessionid, fieldname, bug_name, bug_email, bug_description)

    def send_feedback(self,environ, **kwargs):
        '''Send a feedback'''
        return self.divaonweb.send_feedback(**kwargs)


    def fit(self,environ,sessionid):
        '''apply divafit on the data and provide an estimate for teh signal-to-noise ratio and correlation length'''
        return self.divaonweb.fit(sessionid)


    def mesh(self,environ,**kwargs):
        return self.MeshWMS.request(**kwargs)

    def info(self,environ):
        out = 'numpy ' + numpy.__version__
        out += '\nmatplotlib ' + matplotlib.__version__
        for p in sys.path:
              out += '\npath %s' % p

        return HttpResponse(out,content_type = 'text/plain')

    def wms_vert(self,environ,**kwargs):
        '''plotting a vertical section'''

        return self.VerticalSectionWMS.request(**kwargs)
