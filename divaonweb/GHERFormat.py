# Copyright (C) 2008-2014 Alexander Barth <barth.alexander@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#                                                                           
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Python module to read and write GHER binary files. The origin of this 
file format are the Fortran function ureadc and uwritec.

= High-level API =

# save data
        GHERFile('testpy.dat','w').save(data)
# load data
        data2 = GHERFile('testpy.dat').load()

= Lower-level API =

This is only useful is very specific cases when the data is a flat array but 
it should be saved with a given shape.

# write data
        GHERFile('testpy.dat', 'w').write(data, shape)
# read data
        shape2, valex2, data2 = GHERFile('testpy.dat', 'r').read()

The shape parameter of previous version of GHERFormat module was the shape in Fortran
convention (column-major). Now it is the shape in Python convention (row-major)
Previous version of GHER

Note: If for a Fortran or Matlab/Octave (column-major) program a array has the
shape 10x20x30, then in Python it has the shape 30x20x10 (row-major).
http://en.wikipedia.org/wiki/Row-major_order

This is done for performance reasons and it the same behavior as for instance the 
NetCDF4 module.
'''


import numpy
import unittest


class GHERFile(object):
    '''Class representing a GHER file'''

    def __init__(self, fileparam, mode = 'r', endianess='big'):
        '''endianess is 'big' or 'little'.'''

        if 'b' not in mode:
            # make sure that mode is binary (python 3)
            mode = mode + 'b'


        if hasattr(fileparam, 'read'):
            self.file = fileparam
        else:
            self.file = open(fileparam, mode)
        
        self.mode = mode
        self.fileparam = fileparam
        self.endianess = endianess

    def read(self):
        '''Read data contained in GHER file (low-level API)'''

        # ignore 10 lines
        self.header = self.file.read(20 * 4)

        reclen = self.file.read(1 * 4)

        if numpy.fromstring(reclen, dtype=numpy.dtype('>i')) == 24:
            int32 = numpy.dtype('>i')
            float32 = numpy.dtype('>f')
            float64 = numpy.dtype('>d')
        elif numpy.fromstring(reclen, dtype=numpy.dtype('<i')) == 24:
            int32 = numpy.dtype('<i')
            float32 = numpy.dtype('<f')
            float64 = numpy.dtype('<d')
        else:
            raise Exception('not a GHER file')

        fortran_shape = numpy.fromfile(self.file, dtype=int32, count=3)
        iprec = numpy.fromfile(self.file, dtype=int32, count=1)[0]
        nbmots = numpy.fromfile(self.file, dtype=int32, count=1)[0]
        valex = numpy.fromfile(self.file, dtype=float32, count=1)[0]

        reclen = numpy.fromfile(self.file, dtype=int32, count=1)

        if iprec == 4:
            vtype = float32
        else:
            vtype = float64

        # number of complete records
        nrec = int(numpy.prod(fortran_shape)/nbmots)

        # number of values on last incomplete record
        irec = numpy.prod(fortran_shape) - nbmots * nrec

        if any(fortran_shape < 0):
            nrec = 0
            irec = 4

        # load all full records including leading and tailing 4 byte integer
        # for efficiency these integers are read as two floats or one double

        reclen = numpy.fromfile(self.file, dtype=int32, count=1)

        if iprec == 4: 
            data = numpy.fromfile(self.file, dtype=vtype, count=nrec*(nbmots+2))
            data = data.reshape(nrec, nbmots+2)
        else:
            data = numpy.fromfile(self.file, dtype=vtype, count=nrec*(nbmots+1))
            data = data.reshape(nrec, nbmots+1)

        
        data2 = numpy.fromfile(self.file, dtype=vtype, count=irec)
        
        #print "data2 ",data2.min(),data[:,0:nbmots].min(),iprec

        data = numpy.concatenate(
            # remove leading and tailing 4 byte integer
            (data[:,0:nbmots].flatten('C'),
             data2))

            
        # data = numpy.zeros((nrec*nbmots + irec,))

        # # load all records
        # for n in range(nrec):
        #     reclen = numpy.fromfile(self.file, dtype=int32, count=1)
        #     assert(reclen == iprec*nbmots)
        #     d = numpy.fromfile(self.file, dtype=vtype, count=nbmots)
        #     data[n*nbmots:(n+1)*nbmots] = d
        #     reclen = numpy.fromfile(self.file, dtype=int32, count=1)
        #     assert(reclen == iprec*nbmots)
                    
        # # load remaining records
        # reclen = numpy.fromfile(self.file, dtype=int32, count=1)
        # assert(reclen == iprec*irec)
        # d = numpy.fromfile(self.file, dtype=vtype, count=irec)
        # data[nrec*nbmots:nrec*nbmots+irec] = d
        # reclen = numpy.fromfile(self.file, dtype=int32, count=1)
        # assert(reclen == iprec*irec)

        # reverse shape (Fortran to C or Python)
        shape = fortran_shape[::-1]
        return (shape, valex, data)
        
    def load(self):
        '''Loads a GHER file. The data is returned as a masked numpy array.'''
        (shape, valex, data) = self.read()
        #print (shape, valex, data)
        data = data.reshape(shape)
        data = numpy.ma.masked_values(data, value = valex)
        return data

    
    def write(self, data, shape, valex = 9999, iprec = 4):
        '''Write data to a file in the GHER format (low-level API)'''

        # reverse shape (C or Python to Fortran)
        fortran_shape = shape[::-1]
        
        if len(fortran_shape) == 1:
            fortran_shape = [fortran_shape[0], 1, 1]
        elif len(fortran_shape) == 2:
            fortran_shape = [fortran_shape[0], fortran_shape[1], 1]
        
        nbmots = 1024
        fortran_shape = numpy.array(fortran_shape)
        numel = numpy.prod(fortran_shape)

        nrec = int(numel/nbmots)
        irec = numel-nbmots*nrec
        ide = 0

        if any(fortran_shape < 0):
            nrec = 0
            irec = 4
        else:
            if len(data) != numel:
                raise Exception('number of elements in array is ' + 
                                'inconsistent with its shape')        
        
        if self.endianess == 'big':
            int32 = numpy.dtype('>i')
            float32 = numpy.dtype('>f')
            float64 = numpy.dtype('>d')
        else:
            int32 = numpy.dtype('<i')
            float32 = numpy.dtype('<f')
            float64 = numpy.dtype('<d')

        def _write(value, dtype):
            '''writes a value of type dtype to file object'''
            self.file.write(numpy.array(value, dtype).tostring())
            
        # header

        for i in range(10):
            _write([0, 0], int32)



        _write(24, int32)
        _write(fortran_shape, int32)
        _write(iprec, int32)
        _write(nbmots, int32)
        _write(valex, float32)
        _write(24, int32)


        if iprec == 4:
            vtype = float32
        else:
            vtype = float64
        
            
        #print 'ide ',ide,type(ide),type(irec)
        for i in range(nrec):
            _write(4*nbmots, int32)
            _write(data[ide:ide+nbmots], vtype)
            _write(4*nbmots, int32)
            ide = ide + nbmots


        _write(4*irec, int32)
        _write(data[ide:ide+irec], vtype)
        _write(4*irec, int32)

        self.file.close()

    def save(self, data, valex = 9999., iprec = 4):
        '''Save a GHER file. The data is provided as a masked numpy array.'''
        
        shape = numpy.array(data).shape
        if hasattr(data,'fill_value'):
            data.fill_value = valex
            data = data.filled()

        self.write(data.flatten(), shape, valex, iprec)
        
    def __del__(self):
        if not hasattr(self.fileparam, 'read'):
            self.file.close()

    
class TestGHERFile(unittest.TestCase):
    '''Unittest class for GHERFile class'''

#    def setUp(self):
#        pass

#     def test_loading(self):
#         mask = '/var/www/web-vis/test.dat'
#         data = GHERFile(mask).load()
#         print data[2,1,1]


    def test_save2D(self):
        '''Test case for saving and loading a vector of data in a GHER file'''
        data = numpy.array(range(2*3)).reshape(2,3)


        GHERFile('testpy.dat','w').save(data)
        data2 = GHERFile('testpy.dat').load()
        data2 = data2.squeeze()

        self.assertTrue(numpy.array_equal(data,data2))

    def test_save2Dmedium(self):
        '''Test case for saving and loading a vector of data in a GHER file'''

        data = numpy.ones((100,101))

        GHERFile('testpy.dat','w').save(data)
        data2 = GHERFile('testpy.dat').load()
        data2 = data2.squeeze()

        self.assertTrue(numpy.array_equal(data,data2))

    def test_save3D(self):
        '''Test case for saving and loading a vector of data in a GHER file'''
        data = numpy.array(range(2*3*4)).reshape(2,3,4)


        GHERFile('testpy.dat','w').save(data)
        data2 = GHERFile('testpy.dat').load()

#        print numpy.abs(data-data2).max()

        self.assertTrue(numpy.array_equal(data,data2))

    def test_save3Dlittle(self):
        '''Test case for saving and loading a vector of data in a GHER file'''
        data = numpy.array(range(2*3*4)).reshape(2,3,4)


        GHERFile('testpy.dat','w',endianess='little').save(data)
        data2 = GHERFile('testpy.dat').load()

#        print numpy.abs(data-data2).max()

        self.assertTrue(numpy.array_equal(data,data2))


    def test_write(self):
        '''Test case for saving and loading a vector of data in a GHER file 
        using low-level API.'''

        data = [100.0, 33.]
        shape = numpy.array([2, 1, 1])

        GHERFile('testpy.dat', 'w').write(data, shape)
        shape2, valex2, data2 = GHERFile('testpy.dat', 'r').read()

        self.assertTrue(numpy.array_equal(data,data2))
        self.assertTrue(numpy.array_equal(shape,shape2))

    
    
if __name__ == "__main__":
#    suite = unittest.TestLoader().loadTestsFromTestCase(TestGHERFile)
#    suite.debug()
    unittest.main()

#  LocalWords:  GHER Fortran ureadc uwritec API GHERFile testpy dat
#  LocalWords:  valex GHERFormat Matlab NetCDF numpy
