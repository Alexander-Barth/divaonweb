# Copyright (C) 2008-2014 Alexander Barth <barth.alexander@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#                                                                           
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from pylab import array, arange, size, ceil, concatenate, linspace, zeros, pi, sqrt, cos, shape, mod

try:
    import numpy.ma as M
except:
    import matplotlib.numerix.ma as M

try:
    from matplotlib.toolkits.basemap import interp, shiftgrid, addcyclic
except ImportError:
    from mpl_toolkits.basemap import interp, shiftgrid, addcyclic

def vertical_section(x,y,v,sx,sy):
#    lon0 = mod(min(sx)+x[0],360)-x[0]
#    (h2,x2) = addcyclic(h, x)
#    (h,x) = shiftgrid(lon0, h2, x2)

    dx = x[1] - x[0]
    dy = y[1] - y[0]

    if v.ndim != 3:
        raise Exception('Parameter v has only %d dimension(s) and a shape of %s' % (v.ndim,str(v.shape)))

    #print 'sx,sy ',len(sx),len(sy),sx,sy
    
    cyclic = abs(mod(x[-1]+dx-x[0],360)) < 1e-4

    if cyclic:
        x = concatenate(( [2*x[0]-x[1]],x,[2*x[-1]-x[-2]]))
    
        sz = shape(v);
        tmp = v;
        v = M.zeros( (sz[0], sz[1], sz[2]+2) )

#        print shape(v[:,:,1:sz[2]+1])
#        print shape(tmp)

        v[:,:,0] = tmp[:,:,1]
        v[:,:,1:sz[2]+1] = tmp
        v[:,:,sz[2]+1] = tmp[:,:,-1]
        del tmp

    #v = concatenate( (v[:,:,1],v,v[:,:,-1]), 2)

#    print shape(v)
    x0 = x[0]
    y0 = y[0]
    
    
#    print cyclic
    ivals = (sx - x0)/dx
    jvals = (sy - y0)/dy


    # refine section

    si = array([])
    sj = array([])


    for i in arange(size(ivals)-1):
        nb = ceil(max(abs(ivals[i+1]-ivals[i]),abs(jvals[i+1]-jvals[i])))
        si = concatenate( (si,linspace(ivals[i],ivals[i+1],nb)) )
        sj = concatenate( (sj,linspace(jvals[i],jvals[i+1],nb)) )


    distance = zeros(len(si))

    for i in arange(len(si)-1):
        lat = (y0 + dy * (sj[i+1]+sj[i])/2.) * pi/180;
        ds = sqrt((dx *(si[i+1]-si[i]) * cos(lat))**2 + (dy *(sj[i+1]-sj[i]))**2)
        distance[i+1] = distance[i] + ds

    #print 'distance ',distance[-1],lat,sj.min(),sj.max(), (y0 + dy * (sj[1]+sj[0])/2.) * pi/180

    coords = array([si, sj])

    sec = M.zeros((shape(v)[0],len(si)))

    si = mod(si,360/dx)

    #print "si",si
    #print "sj",sj
    
    # interpolation
    for k in arange(shape(v)[0]):    
        #sec[k,:] = ndimage.map_coordinates(v[k,:,:], coords)
        #print('k',k,v.shape)
        sec[k,:] = interp(v[k,:,:],(x-x0)/dx,(y-y0)/dy,si,sj,masked=True)
    return (sec,distance)




    
