# Copyright (C) 2007 Alexander Barth <barth.alexander@gmail.com>
#
# This file is part of pyocean-web
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

"""interpolation routines"""


from numpy import *

import scipy.interpolate
#from scipy.sandbox import delaunay
#import numpy.core.ma as ma

try:
    import numpy.ma as ma
except ImportError:
    import numpy.core.ma as ma



import numpy

def vinterp_python(v3,z,zi):
    """vertical interpolated section

    zi
    """
    #import time
    #t0 = time.time()
    
    if v3.size == 0:
        return ma.MaskedArray([])

    v3 = v3.reshape(z.shape)

    sz = (size(v3,1),size(v3,2))
    #sz = v3.shape[1:-1]
    v = numpy.ndarray(sz)
    mask = numpy.ndarray(sz,dtype='bool')
    mask[:] = False;
    
       
    for i in range(sz[1]):
        for j in range(sz[0]):
            if not v3.mask[-1,j,i]:

                try:
                  v[j,i] = scipy.interpolate.interp1d(z[:,j,i],v3[:,j,i].__array__())(zi)
                except ValueError:
                  mask[j,i] = True;
                      
            else:
                mask[j,i] = True;

#            try:
#                v[j,i] = scipy.interpolate.interp1d(z[:,j,i],v3[:,j,i])(zi)
#            except:
#                v.mask[j,i] = True;

    #print "time in interpolation", time.time()-t0

    return ma.MaskedArray(v,mask=mask)



def vinterp_fortran(v,z,zi):
    tr = numpy.transpose
    v1f = v.filled()

    #t0 = time.time()
    vi = modelinterp.modelinterp(tr(z),0,tr(v1f),v.fill_value,zi,0)
    #print "time in fortran interpolation", time.time()-t0

    vi = tr(vi.squeeze())
    vi = ma.masked_where(vi == v.fill_value,vi)
    return vi


def griddata(x,y,z,xi,yi):
    """interpolate arbitrartly located data
    
    """
    # triangulate data
    tri = delaunay.Triangulation(x.flatten(),y.flatten())

    # interpolate data
    interp = tri.nn_interpolator(z.flatten())

    zi = interp(xi,yi)
    return zi


try:
    import modelinterp
    vinterp = vinterp_fortran
except ImportError:
    vinterp = vinterp_python

#vinterp = vinterp_python
